setlocal
SET BSE_PATH=C:\SOURCE\BSE_PROJECT
xcopy "%~dp0\bin\*.dll" "%BSE_PATH%\bin\*.dll" /Y
xcopy "%~dp0"bin\*.exe "%BSE_PATH%\bin\*.exe" /Y
xcopy "%~dp0\bin\*.map" "%BSE_PATH%\bin\*.map" /Y
xcopy "%~dp0"hl2.exe "%BSE_PATH%\hl2.exe" /Y
xcopy "%~dp0"hl2.map "%BSE_PATH%\hl2.map" /Y
xcopy "%~dp0"hl2\bin\*.dll "%BSE_PATH%\hl2\bin\*.dll" /Y
xcopy "%~dp0"hl2\bin\*.map "%BSE_PATH%\hl2\bin\*.map" /Y
xcopy "%~dp0"platform\Browser\*.dll "%BSE_PATH%\platform\Browser\*.dll" /Y
endlocal
pause
