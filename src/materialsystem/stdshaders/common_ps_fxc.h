// This is where all common code for pixel shaders go.
#include "common_fxc.h"


// Put global skip commands here. . make sure and check that the appropriate vars are defined
// so these aren't used on the wrong shaders!

// --------------------------------------------------------------------------------
// HDR should never be enabled if we don't aren't running in float or integer HDR mode.
// SKIP: defined $HDRTYPE && defined $HDRENABLED && !$HDRTYPE && $HDRENABLED
// --------------------------------------------------------------------------------
// We don't ever write water fog to dest alpha if we aren't doing water fog.
// SKIP: defined $FOGTYPE && defined $WRITEWATERFOGTODESTALPHA && ( $FOGTYPE != 2 ) && $WRITEWATERFOGTODESTALPHA
// --------------------------------------------------------------------------------
// We don't need fog in the pixel shader if we aren't in float fog mode
// NOSKIP: defined $HDRTYPE && defined $HDRENABLED && defined $FOGTYPE && $HDRTYPE != HDR_TYPE_FLOAT && $FOGTYPE != 0
// --------------------------------------------------------------------------------
// We don't do HDR and LIGHTING_PREVIEW at the same time since it's runnin LDR in hammer.
//  SKIP: defined $LIGHTING_PREVIEW && defined $HDRTYPE && $LIGHTING_PREVIEW && $HDRTYPE != 0
// --------------------------------------------------------------------------------
// Ditch all fastpath attemps if we are doing LIGHTING_PREVIEW.
//	SKIP: defined $LIGHTING_PREVIEW && defined $FASTPATHENVMAPTINT && $LIGHTING_PREVIEW && $FASTPATHENVMAPTINT
//	SKIP: defined $LIGHTING_PREVIEW && defined $FASTPATHENVMAPCONTRAST && $LIGHTING_PREVIEW && $FASTPATHENVMAPCONTRAST
//	SKIP: defined $LIGHTING_PREVIEW && defined $FASTPATH && $LIGHTING_PREVIEW && $FASTPATH
// --------------------------------------------------------------------------------
// Ditch flashlight depth when flashlight is diabled
//  SKIP: ($FLASHLIGHT || $FLASHLIGHTDEPTH) && $LIGHTING_PREVIEW
// --------------------------------------------------------------------------------

// System defined pixel shader constants

// w/a unused.
const float4 g_GammaFogColor : register( c29 );
// NOTE: w == 1.0f
const float4 cGammaLightScale : register( c30 );
// NOTE: w == 1.0f
const float4 cLinearLightScale : register( c31 );
#define LIGHT_MAP_SCALE (cLinearLightScale.y)
#define ENV_MAP_SCALE (cLinearLightScale.z)

#if defined( SHADER_MODEL_PS_2_0 ) || defined( SHADER_MODEL_PS_2_B )
const float4 cFlashlightColor : register( c28 );
#endif

struct HDR_PS_OUTPUT
{
    float4 color : COLOR0;
};


HDR_PS_OUTPUT LinearColorToHDROutput( float4 linearColor, float fogFactor )
{
	// assume that sRGBWrite is enabled
	linearColor.xyz *= cLinearLightScale.x;
	HDR_PS_OUTPUT output;
	output.color = linearColor;
	return output;
}

HDR_PS_OUTPUT LinearColorToHDROutput_NoScale( float4 linearColor, float fogFactor )
{
	// assume that sRGBWrite is enabled
//	linearColor.xyz *= cLinearLightScale.xyz;
	HDR_PS_OUTPUT output;
	output.color = linearColor;
	return output;
}

HDR_PS_OUTPUT GammaColorToHDROutput( float4 gammaColor )
{
	// assume that sRGBWrite is disabled.
	HDR_PS_OUTPUT output;
	gammaColor.xyz *= cGammaLightScale.x;
	output.color.xyz = gammaColor;
	output.color.a = gammaColor.a;
	return output;
}

// unused
HALF Luminance( HALF3 color )
{
	return dot( color, HALF3( HALF_CONSTANT(0.30f), HALF_CONSTANT(0.59f), HALF_CONSTANT(0.11f) ) );
}

HALF LuminanceScaled( HALF3 color )
{
	return dot( color, HALF3( HALF_CONSTANT(0.30f) / MAX_HDR_OVERBRIGHT, HALF_CONSTANT(0.59f) / MAX_HDR_OVERBRIGHT, HALF_CONSTANT(0.11f) / MAX_HDR_OVERBRIGHT ) );
}


HALF AvgColor( HALF3 color )
{
	return dot( color, HALF3( HALF_CONSTANT(0.33333f), HALF_CONSTANT(0.33333f), HALF_CONSTANT(0.33333f) ) );
}

HALF4 DiffuseBump( sampler lightmapSampler,
                   float2  lightmapTexCoord1,
                   float2  lightmapTexCoord2,
                   float2  lightmapTexCoord3,
                   HALF3   normal )
{
	HALF3 lightmapColor1 = tex2D( lightmapSampler, lightmapTexCoord1 );
	HALF3 lightmapColor2 = tex2D( lightmapSampler, lightmapTexCoord2 );
	HALF3 lightmapColor3 = tex2D( lightmapSampler, lightmapTexCoord3 );

	HALF3 diffuseLighting;
	diffuseLighting = saturate( dot( normal, bumpBasis[0] ) ) * lightmapColor1 +
					  saturate( dot( normal, bumpBasis[1] ) ) * lightmapColor2 +
					  saturate( dot( normal, bumpBasis[2] ) ) * lightmapColor3;

	return HALF4( diffuseLighting, LuminanceScaled( diffuseLighting ) );
}


HALF Fresnel( HALF3 normal,
              HALF3 eye,
              HALF2 scaleBias )
{
	HALF fresnel = HALF_CONSTANT(1.0f) - dot( normal, eye );
	fresnel = pow( fresnel, HALF_CONSTANT(5.0f) );

	return fresnel * scaleBias.x + scaleBias.y;
}

HALF4 GetNormal( sampler normalSampler,
                 float2 normalTexCoord )
{
	HALF4 normal = tex2D( normalSampler, normalTexCoord );
	normal.rgb = HALF_CONSTANT(2.0f) * normal.rgb - HALF_CONSTANT(1.0f);

	return normal;
}

#ifdef NV3X

HALF4 EnvReflect( sampler envmapSampler,
				 sampler normalizeSampler,
				 HALF3 normal,
				 float3 eye,
				 HALF2 fresnelScaleBias )
{
	HALF3 normEye = texCUBE( normalizeSampler, eye );
	HALF fresnel = Fresnel( normal, normEye, fresnelScaleBias );
	HALF3 reflect = CalcReflectionVectorUnnormalized( normal, eye );
	return texCUBE( envmapSampler, reflect );
}

#else // NV3X

HALF4 EnvReflect( sampler envmapSampler,
				 HALF3 normal,
				 HALF3 eye,
				 HALF2 fresnelScaleBias )
{
	eye = normalize( eye );
	
	HALF fresnel = Fresnel( normal, eye, fresnelScaleBias );
	HALF3 reflect = CalcReflectionVectorUnnormalized( normal, eye );
	
	return texCUBE( envmapSampler, reflect ) * fresnel;
}

#endif
