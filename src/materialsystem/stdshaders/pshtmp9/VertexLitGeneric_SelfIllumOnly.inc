//
// Generated by Microsoft (R) D3DX9 Shader Assembler
//
//  Source: VertexLitGeneric_SelfIllumOnly.psh
//  Flags: /Zi 
//


static unsigned int pixelShader_VertexLitGeneric_SelfIllumOnly_0[] =
{
    0xffff0101, 0x0029fffe, 0x47554244, 0x00000028, 0x00000088, 0x00000000, 
    0x00000001, 0x00000074, 0x00000002, 0x00000078, 0x00000000, 0x00000000, 
    0x00000000, 0x625c3a64, 0x5c617465, 0x5c637273, 0x6574616d, 0x6c616972, 
    0x74737973, 0x735c6d65, 0x68736474, 0x72656461, 0x65565c73, 0x78657472, 
    0x4774694c, 0x72656e65, 0x535f6369, 0x49666c65, 0x6d756c6c, 0x796c6e4f, 
    0x6873702e, 0xababab00, 0x00000028, 0x00000003, 0x000000ac, 0x00000005, 
    0x000000b4, 0x58443344, 0x68532039, 0x72656461, 0x73734120, 0x6c626d65, 
    0xab007265, 0x00000042, 0xb00f0000, 0x00000005, 0x800f0000, 0xa0e40000, 
    0xb0e40000, 0x0000ffff
};
static PrecompiledShaderByteCode_t VertexLitGeneric_SelfIllumOnly_pixel_shaders[1] = 
{
	{ pixelShader_VertexLitGeneric_SelfIllumOnly_0, sizeof( pixelShader_VertexLitGeneric_SelfIllumOnly_0 ) },
};
struct VertexLitGeneric_SelfIllumOnlyPixelShader_t : public PrecompiledShader_t
{
	VertexLitGeneric_SelfIllumOnlyPixelShader_t()
	{
		m_nFlags = SHADER_CUSTOM_ENUMERATION;
		m_pByteCode = VertexLitGeneric_SelfIllumOnly_pixel_shaders;
		m_nShaderCount = 1;
		m_pName = "VertexLitGeneric_SelfIllumOnly";
		GetShaderDLL()->InsertPrecompiledShader( PRECOMPILED_PIXEL_SHADER, this );
	}
};
static VertexLitGeneric_SelfIllumOnlyPixelShader_t VertexLitGeneric_SelfIllumOnly_PixelShaderInstance;
