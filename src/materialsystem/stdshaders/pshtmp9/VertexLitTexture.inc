//
// Generated by Microsoft (R) D3DX9 Shader Assembler
//
//  Source: VertexLitTexture.psh
//  Flags: /Zi 
//


static unsigned int pixelShader_VertexLitTexture_0[] =
{
    0xffff0101, 0x0025fffe, 0x47554244, 0x00000028, 0x00000078, 0x00000000, 
    0x00000001, 0x00000064, 0x00000002, 0x00000068, 0x00000000, 0x00000000, 
    0x00000000, 0x625c3a64, 0x5c617465, 0x5c637273, 0x6574616d, 0x6c616972, 
    0x74737973, 0x735c6d65, 0x68736474, 0x72656461, 0x65565c73, 0x78657472, 
    0x5474694c, 0x75747865, 0x702e6572, 0xab006873, 0x00000028, 0x0000000c, 
    0x0000009c, 0x0000000e, 0x000000a4, 0x58443344, 0x68532039, 0x72656461, 
    0x73734120, 0x6c626d65, 0xab007265, 0x00000042, 0xb00f0000, 0x00000005, 
    0x800f0000, 0xb0e40000, 0x90e40000, 0x0000ffff
};
static PrecompiledShaderByteCode_t VertexLitTexture_pixel_shaders[1] = 
{
	{ pixelShader_VertexLitTexture_0, sizeof( pixelShader_VertexLitTexture_0 ) },
};
struct VertexLitTexturePixelShader_t : public PrecompiledShader_t
{
	VertexLitTexturePixelShader_t()
	{
		m_nFlags = SHADER_CUSTOM_ENUMERATION;
		m_pByteCode = VertexLitTexture_pixel_shaders;
		m_nShaderCount = 1;
		m_pName = "VertexLitTexture";
		GetShaderDLL()->InsertPrecompiledShader( PRECOMPILED_PIXEL_SHADER, this );
	}
};
static VertexLitTexturePixelShader_t VertexLitTexture_PixelShaderInstance;
