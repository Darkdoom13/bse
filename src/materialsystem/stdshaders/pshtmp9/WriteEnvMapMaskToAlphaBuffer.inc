//
// Generated by Microsoft (R) D3DX9 Shader Assembler
//
//  Source: WriteEnvMapMaskToAlphaBuffer.psh
//  Flags: /Zi 
//


static unsigned int pixelShader_WriteEnvMapMaskToAlphaBuffer_0[] =
{
    0xffff0101, 0x0028fffe, 0x47554244, 0x00000028, 0x00000084, 0x00000000, 
    0x00000001, 0x00000070, 0x00000002, 0x00000074, 0x00000000, 0x00000000, 
    0x00000000, 0x625c3a64, 0x5c617465, 0x5c637273, 0x6574616d, 0x6c616972, 
    0x74737973, 0x735c6d65, 0x68736474, 0x72656461, 0x72575c73, 0x45657469, 
    0x614d766e, 0x73614d70, 0x416f546b, 0x6168706c, 0x66667542, 0x702e7265, 
    0xab006873, 0x00000028, 0x0000000c, 0x000000a8, 0x0000000d, 0x000000b0, 
    0x58443344, 0x68532039, 0x72656461, 0x73734120, 0x6c626d65, 0xab007265, 
    0x00000042, 0xb00f0000, 0x00000001, 0x800f0000, 0xb0e40000, 0x0000ffff
};
static PrecompiledShaderByteCode_t WriteEnvMapMaskToAlphaBuffer_pixel_shaders[1] = 
{
	{ pixelShader_WriteEnvMapMaskToAlphaBuffer_0, sizeof( pixelShader_WriteEnvMapMaskToAlphaBuffer_0 ) },
};
struct WriteEnvMapMaskToAlphaBufferPixelShader_t : public PrecompiledShader_t
{
	WriteEnvMapMaskToAlphaBufferPixelShader_t()
	{
		m_nFlags = SHADER_CUSTOM_ENUMERATION;
		m_pByteCode = WriteEnvMapMaskToAlphaBuffer_pixel_shaders;
		m_nShaderCount = 1;
		m_pName = "WriteEnvMapMaskToAlphaBuffer";
		GetShaderDLL()->InsertPrecompiledShader( PRECOMPILED_PIXEL_SHADER, this );
	}
};
static WriteEnvMapMaskToAlphaBufferPixelShader_t WriteEnvMapMaskToAlphaBuffer_PixelShaderInstance;
