//
// Generated by Microsoft (R) D3DX9 Shader Assembler
//
//  Source: predator.psh
//  Flags: /Zi 
//


static unsigned int pixelShader_predator_0[] =
{
    0xffff0101, 0x0023fffe, 0x47554244, 0x00000028, 0x00000070, 0x00000000, 
    0x00000001, 0x0000005c, 0x00000002, 0x00000060, 0x00000000, 0x00000000, 
    0x00000000, 0x625c3a64, 0x5c617465, 0x5c637273, 0x6574616d, 0x6c616972, 
    0x74737973, 0x735c6d65, 0x68736474, 0x72656461, 0x72705c73, 0x74616465, 
    0x702e726f, 0xab006873, 0x00000028, 0x00000004, 0x00000094, 0x00000007, 
    0x0000009c, 0x58443344, 0x68532039, 0x72656461, 0x73734120, 0x6c626d65, 
    0xab007265, 0x00000042, 0xb00f0000, 0x00000005, 0x800f0000, 0xb0e40000, 
    0xa0e40002, 0x0000ffff
};
static PrecompiledShaderByteCode_t predator_pixel_shaders[1] = 
{
	{ pixelShader_predator_0, sizeof( pixelShader_predator_0 ) },
};
struct predatorPixelShader_t : public PrecompiledShader_t
{
	predatorPixelShader_t()
	{
		m_nFlags = SHADER_CUSTOM_ENUMERATION;
		m_pByteCode = predator_pixel_shaders;
		m_nShaderCount = 1;
		m_pName = "predator";
		GetShaderDLL()->InsertPrecompiledShader( PRECOMPILED_PIXEL_SHADER, this );
	}
};
static predatorPixelShader_t predator_PixelShaderInstance;
