//
// Generated by Microsoft (R) D3DX9 Shader Assembler
//
//  Source: SpriteRenderTransAdd.psh
//  Flags: /Zi 
//


static unsigned int pixelShader_SpriteRenderTransAdd_0[] =
{
    0xffff0101, 0x0026fffe, 0x47554244, 0x00000028, 0x0000007c, 0x00000000, 
    0x00000001, 0x00000068, 0x00000002, 0x0000006c, 0x00000000, 0x00000000, 
    0x00000000, 0x625c3a64, 0x5c617465, 0x5c637273, 0x6574616d, 0x6c616972, 
    0x74737973, 0x735c6d65, 0x68736474, 0x72656461, 0x70535c73, 0x65746972, 
    0x646e6552, 0x72547265, 0x41736e61, 0x702e6464, 0xab006873, 0x00000028, 
    0x00000003, 0x000000a0, 0x00000006, 0x000000a8, 0x58443344, 0x68532039, 
    0x72656461, 0x73734120, 0x6c626d65, 0xab007265, 0x00000042, 0xb00f0000, 
    0x00000005, 0x800f0000, 0xb0e40000, 0xa0e40000, 0x0000ffff
};
static PrecompiledShaderByteCode_t SpriteRenderTransAdd_pixel_shaders[1] = 
{
	{ pixelShader_SpriteRenderTransAdd_0, sizeof( pixelShader_SpriteRenderTransAdd_0 ) },
};
struct SpriteRenderTransAddPixelShader_t : public PrecompiledShader_t
{
	SpriteRenderTransAddPixelShader_t()
	{
		m_nFlags = SHADER_CUSTOM_ENUMERATION;
		m_pByteCode = SpriteRenderTransAdd_pixel_shaders;
		m_nShaderCount = 1;
		m_pName = "SpriteRenderTransAdd";
		GetShaderDLL()->InsertPrecompiledShader( PRECOMPILED_PIXEL_SHADER, this );
	}
};
static SpriteRenderTransAddPixelShader_t SpriteRenderTransAdd_PixelShaderInstance;
