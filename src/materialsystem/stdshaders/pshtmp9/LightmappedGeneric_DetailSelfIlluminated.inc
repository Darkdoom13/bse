//
// Generated by Microsoft (R) D3DX9 Shader Assembler
//
//  Source: LightmappedGeneric_DetailSelfIlluminated.psh
//  Flags: /Zi 
//


static unsigned int pixelShader_LightmappedGeneric_DetailSelfIlluminated_0[] =
{
    0xffff0101, 0x003bfffe, 0x47554244, 0x00000028, 0x000000d0, 0x00000000, 
    0x00000001, 0x0000007c, 0x0000000a, 0x00000080, 0x00000000, 0x00000000, 
    0x00000000, 0x625c3a64, 0x5c617465, 0x5c637273, 0x6574616d, 0x6c616972, 
    0x74737973, 0x735c6d65, 0x68736474, 0x72656461, 0x694c5c73, 0x6d746867, 
    0x65707061, 0x6e654764, 0x63697265, 0x7465445f, 0x536c6961, 0x49666c65, 
    0x6d756c6c, 0x74616e69, 0x702e6465, 0xab006873, 0x00000028, 0x0000000b, 
    0x000000f4, 0x0000000c, 0x000000fc, 0x0000000d, 0x00000104, 0x0000000e, 
    0x0000010c, 0x0000000f, 0x0000011c, 0x00000011, 0x00000128, 0x00000012, 
    0x00000138, 0x00000013, 0x00000148, 0x00000015, 0x00000158, 0x00000016, 
    0x00000168, 0x58443344, 0x68532039, 0x72656461, 0x73734120, 0x6c626d65, 
    0xab007265, 0x00000042, 0xb00f0000, 0x00000042, 0xb00f0001, 0x00000042, 
    0xb00f0002, 0x00000005, 0x80070000, 0xb0e40000, 0x90e40000, 0x40000001, 
    0x80080000, 0x90ff0000, 0x00000005, 0x80070000, 0xb0e40001, 0x80e40000, 
    0x00000005, 0x81070000, 0x80e40000, 0xb0e40002, 0x00000005, 0x81070000, 
    0xa0e40000, 0x80e40000, 0x00000005, 0x800f0001, 0xa0e40001, 0xb0e40000, 
    0x00000012, 0x80070000, 0xb0ff0000, 0x80e40001, 0x80e40000, 0x0000ffff
};
static PrecompiledShaderByteCode_t LightmappedGeneric_DetailSelfIlluminated_pixel_shaders[1] = 
{
	{ pixelShader_LightmappedGeneric_DetailSelfIlluminated_0, sizeof( pixelShader_LightmappedGeneric_DetailSelfIlluminated_0 ) },
};
struct LightmappedGeneric_DetailSelfIlluminatedPixelShader_t : public PrecompiledShader_t
{
	LightmappedGeneric_DetailSelfIlluminatedPixelShader_t()
	{
		m_nFlags = SHADER_CUSTOM_ENUMERATION;
		m_pByteCode = LightmappedGeneric_DetailSelfIlluminated_pixel_shaders;
		m_nShaderCount = 1;
		m_pName = "LightmappedGeneric_DetailSelfIlluminated";
		GetShaderDLL()->InsertPrecompiledShader( PRECOMPILED_PIXEL_SHADER, this );
	}
};
static LightmappedGeneric_DetailSelfIlluminatedPixelShader_t LightmappedGeneric_DetailSelfIlluminated_PixelShaderInstance;
