//-----------------------------------------------------------------------------
// This is just a little redirection tool so I can get all the dlls in bin
//-----------------------------------------------------------------------------

#include <windows.h>
#include <stdio.h>
#include <assert.h>
#include "interface.h"
#include <vgui\IScheme.h>
#include <vgui_controls\Controls.h>
#include <vgui\MouseCode.h>
#include <vgui\KeyCode.h>
#include <vgui\IVGui.h>
#include <vgui\ISurface.h>
#include <vgui\ILocalize.h>
#include <vgui_controls\Panel.h>
#include "filesystem.h"
#include "MainDialog.h"

#include "IVGuiModule.h"
#include "IVGuiModuleLoader.h"

#include "ServerBrowser/IServerBrowser.h"

#include "vgui/Cursor.h"

#include "clientstats.h"


#include "vguimatsurface/IMatSystemSurface.h"

#include <conio.h>


// top level VGUI2 panel
CStaticPanel *staticPanel = NULL;


int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{

	// Load vgui
	CSysModule *vguiModule = Sys_LoadModule("bin/vgui2.dll");
	if (!vguiModule)
	{
		vguiModule = Sys_LoadModule("vgui2.dll");
	}

	CreateInterfaceFn vguiFactory = Sys_GetFactory(vguiModule);
	if (!vguiFactory)
	{
		MessageBox(0,"Fatal error: Could not load vgui2.dll\n","Error",MB_OK);
		return 2;
	}	  

	CSysModule *filesystemModule = Sys_LoadModule("bin/filesystem_stdio.dll");
	if (!filesystemModule)
	{
		filesystemModule = Sys_LoadModule("filesystem_stdio.dll");
	}

	CreateInterfaceFn filesystemFactory = Sys_GetFactory(filesystemModule);
	if (!filesystemFactory)
	{
		MessageBox(0,"Fatal error: Could not load bin/filesystem_stdio.dll\n","Error",MB_OK);
		return 2;
	}
	
	CSysModule *Vguimat = Sys_LoadModule("vguimatsurface.dll");
	if (!Vguimat)
	{

		MessageBox(0,"Not Load vguimatsurface.dll","Error",MB_OK);

	}


	CreateInterfaceFn g_AppSystemFactory = Sys_GetFactory(Vguimat);
	if (!g_AppSystemFactory)
	{

		MessageBox(0,"Fatal error: Could not load vguimatsurface.dll","Error",MB_OK);

		return 2;
	}

	// Initialize interfaces
	CreateInterfaceFn factories[2];
	factories[0] = vguiFactory;
	factories[1] = filesystemFactory;
	factories[3] = g_AppSystemFactory;

	if (!vgui::VGui_InitInterfacesList( "VGUI", factories, 3))
	{
		MessageBox(0,"Fatal error: Could not initalize vgui2.dll\n","Error",MB_OK);
		return 3;
	}


	vgui::ISurface *pVGuiSurface = (vgui::ISurface*)g_AppSystemFactory( VGUI_SURFACE_INTERFACE_VERSION, NULL );
	if (!pVGuiSurface)
	{
		MessageBox(0,"Could not get vgui::ISurface interface, VGUI_Surface027 from vguimatsurface.dll","Error",MB_OK);
		return 2;
	}

	pVGuiSurface->Connect( g_AppSystemFactory );

	IMatSystemSurface *g_pMatSystemSurface = (IMatSystemSurface *)pVGuiSurface->QueryInterface(MAT_SYSTEM_SURFACE_INTERFACE_VERSION);
	if ( !g_pMatSystemSurface )
	{
		MessageBox(0,"Could not get IMatSystemSurface interface VGUI_Surface027 from vguimatsurface.dll","Error",MB_OK);
		return 2;
	
	}

// Fire64:  not use,not dpaw and crashed not implementaed material system and shader system

//	if ( pVGuiSurface->Init() != INIT_OK )
//	{
//		MessageBox(0,"IMatSystemSurface Init != INIT_OK\n","Error",MB_OK);
//		return 2;
//	}

	IClientStats* pVGuiMatSurfaceStats = ( IClientStats * )g_AppSystemFactory( INTERFACEVERSION_CLIENTSTATS, NULL );
	if (!pVGuiMatSurfaceStats)
	{

		MessageBox(0,"Unable to init vgui materialsystem surface  stats version ClientStats004\n","Error",MB_OK);		
	}
	else
	{
//		g_EngineStats.InstallClientStats( pVGuiMatSurfaceStats );
	}
		
	
	// Load ServerBrowser
	CSysModule *Browser = Sys_LoadModule("ServerBrowser.dll");
	if (!Browser)
	{

		MessageBox(0,"Not Load ServerBrowser.dll","Error",MB_OK);

	}


	CreateInterfaceFn serverbrowser = Sys_GetFactory(Browser);
	if (!serverbrowser)
	{

		MessageBox(0,"Fatal error: Could not load ServerBrowser.dll","Error",MB_OK);

		return 2;
	}


	IVGuiModule *vguiserver = (IVGuiModule *)(serverbrowser)("VGuiModuleServerBrowser001", NULL);	
	

	IServerBrowser *server = (IServerBrowser *)(serverbrowser)(SERVERBROWSER_INTERFACE_VERSION, NULL);	
	
	
	
	if (!vguiserver)
	{

		MessageBox(0,"Unable to get VGuiModuleServerBrowser001 from factory","Error",MB_OK);

		return 2;
	}	
	
	if (!server)
	{

		MessageBox(0,"Unable to get ServerBrowser001 from factory","Error",MB_OK);

		return 2;
	}

		// load scheme
	if (!vgui::scheme()->LoadSchemeFromFile("Resource/TrackerScheme.res", "Tracker"))
	{
//		MessageBox(0,"Error loading Resource/TrackerScheme.res\n", "Error",MB_OK);
	}

	// Start the App running
	vgui::ivgui()->Start();
	vgui::ivgui()->SetSleep(false);

	staticPanel = new CStaticPanel( NULL, "staticPanel" );	
	
	staticPanel->SetBounds( 0,0,800,600);
	staticPanel->SetPaintBorderEnabled(false);
	staticPanel->SetPaintBackgroundEnabled(false);
	staticPanel->SetPaintEnabled(false);
	staticPanel->SetVisible(true);
	staticPanel->SetScheme("Resource/TrackerScheme.res");
	staticPanel->SetCursor( vgui::dc_none );
	staticPanel->SetZPos(0);
	staticPanel->SetVisible(true);
	staticPanel->SetParent( vgui::surface()->GetEmbeddedPanel() );

	// Fire64:  not use,not dpaw and crashed not implementaed material system and shader system

	//	materialSystemInterface->CacheUsedMaterials();	
	
	// load the base localization file
	vgui::localize()->AddFile( vgui::filesystem(), "Resource/platform_english.txt" );

	// Fire64:  not use,not dpaw and crashed not implementaed material system and shader system

//	vgui::Panel *main = new CMainDialog(staticPanel);

//	vgui::Frame *pFrame = new vgui::Frame( staticPanel, "MyFrame" );
//	pFrame->SetScheme("ClientScheme.res");
//	pFrame->SetSize( 100, 100 );
//	pFrame->SetTitle("My First Frame", true );
//	pFrame->Activate();


#if 0  // I can't get Server Browser Dialog

	vguiserver->Activate();
	server->Activate();

#endif




return 1;

}
