//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef MAINDIALOG_H
#define MAINDIALOG_H
#ifdef _WIN32
#pragma once
#endif

#include <vgui_controls\Frame.h>

namespace vgui
{
class PropertySheet;
};

//-----------------------------------------------------------------------------
// Purpose: Main dialog for server monitor
//-----------------------------------------------------------------------------
class CMainDialog : public vgui::Frame
{
public:
	CMainDialog(vgui::Panel *parent);
	~CMainDialog();

	// vgui overrides
//	virtual void PerformLayout();
//	virtual void OnClose();

private:
//	virtual void OnRefreshServers();

//	DECLARE_PANELMAP();

	vgui::PropertySheet *m_pSheet;

	typedef vgui::Frame BaseClass;
};


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class CStaticPanel : public vgui::Panel
{
	typedef vgui::Panel BaseClass;

public:
	CStaticPanel( vgui::Panel *pParent, const char *pName ) : vgui::Panel( pParent, pName )
	{
//		SetCursor( vgui::dc_none );
		SetKeyBoardInputEnabled( false );
		SetMouseInputEnabled( false );
	}

};









#endif // MAINDIALOG_H
