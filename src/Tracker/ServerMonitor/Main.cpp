//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include <vgui_controls/BuildGroup.h>
#include <vgui_controls/Controls.h>
#include <vgui/ISystem.h>
#include <vgui/IPanel.h>
#include <vgui/IScheme.h>
#include <vgui/ISurface.h>
#include <vgui/IVGUI.h>
#include <KeyValues.h>
#include <vgui\ILocalize.h>
#include <vgui_controls/Panel.h>
#include "interface.h"
#include "utlbuffer.h"
#include "FileSystem.h"
#include "MainDialog.h"
#include <stdio.h>
#include <windows.h>
#include "winlite.h"
#include "../TrackerNET/TrackerNET_Interface.h"
#include "../TrackerNET/Threads.h"

#include "ServerList.h"

using namespace vgui;

// networking
ITrackerNET *g_pTrackerNET = NULL;

// server list
CServerList *g_pServerList = NULL;

//-----------------------------------------------------------------------------
// Purpose: Entry point
//-----------------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	// Load vgui
	CSysModule *vguiModule = Sys_LoadModule("bin/vgui2.dll");
	if (!vguiModule)
	{
		vguiModule = Sys_LoadModule("vgui2.dll");
	}

	CreateInterfaceFn vguiFactory = Sys_GetFactory(vguiModule);
	if (!vguiFactory)
	{
		MessageBox(0,"Fatal error: Could not load vgui2.dll\n","Error",MB_OK);
		return 2;
	}	  

	CSysModule *filesystemModule = Sys_LoadModule("bin/FileSystem_Stdio.dll");
	if (!filesystemModule)
	{
		filesystemModule = Sys_LoadModule("FileSystem_Stdio.dll");
	}

	CreateInterfaceFn filesystemFactory = Sys_GetFactory(filesystemModule);
	if (!filesystemFactory)
	{
		MessageBox(0,"Fatal error: Could not load FileSystem_Stdio.dll\n","Error",MB_OK);
		return 2;
	}

	CSysModule *netModule = Sys_LoadModule("bin/TrackerNET.dll");
	if (!netModule)
	{
		netModule = Sys_LoadModule("TrackerNET.dll");
	}

	CreateInterfaceFn netFactory = Sys_GetFactory(netModule);
	if (!netFactory)
	{
		MessageBox(0,"Fatal error: Could not load TrackerNET.dll\n","Error",MB_OK);
		return 2;
	}

	// Initialize interfaces
	CreateInterfaceFn factories[3];
	factories[0] = Sys_GetFactoryThis();
	factories[1] = vguiFactory;
	factories[2] = filesystemFactory;

	if (!vgui::VGui_InitInterfacesList( "ServerMonitor", factories, 3))
	{
		MessageBox(0,"Fatal error: Could not int ServerMonitor\n","Error",MB_OK);
		return 3;
	}

	// initialize interfaces
	g_pTrackerNET = (ITrackerNET *)netFactory(TRACKERNET_INTERFACE_VERSION, NULL);
	g_pTrackerNET->Initialize(1300, 1400);

	filesystem()->AddSearchPath("../", "");

	// Init the surface
	vgui::surface()->Init();

	// Load the scheme
	if (!vgui::scheme()->LoadSchemeFromFile("Resource/TrackerScheme.res", "Main"))
		return 1;

	// localization
	vgui::localize()->AddFile(vgui::filesystem(), "Resource/platform_english.txt");
    vgui::localize()->AddFile(vgui::filesystem(), "Resource/vgui_english.txt");

	// Make a embedded panel
	vgui::Panel *panel = new vgui::Panel(NULL, "TopPanel");
	vgui::surface()->SetEmbeddedPanel( panel->GetVPanel() );

	// Start vgui
	vgui::ivgui()->Start();

	// add our main window
	CMainDialog *main = new CMainDialog(panel);

	// server list
	g_pServerList = new CServerList(main);

	main->Activate();

	// Run app frame loop
	while (vgui::ivgui()->IsRunning())
	{
		vgui::ivgui()->RunFrame();

		// networking
		g_pServerList->RunFrame();
	}

	delete g_pServerList;

	// Shutdown
	vgui::surface()->Shutdown();
	Sys_UnloadModule(vguiModule);
	Sys_UnloadModule(netModule);
	return 1;
}

