//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include "LoadTestApp.h"
#include <winsock2.h>

//-----------------------------------------------------------------------------
// Purpose: Executable entry-point
//-----------------------------------------------------------------------------
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

	// winsock aware
	WSAData wsaData;
	int nReturnCode = ::WSAStartup(MAKEWORD(2,0), &wsaData);

	gApp.Run();

	::WSACleanup();


	return 0;
}
