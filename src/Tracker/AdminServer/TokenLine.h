//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef TOKENLINE_H
#define TOKENLINE_H
#ifdef _WIN32
#pragma once
#endif


//-----------------------------------------------------------------------------
// Purpose: Token Line Header
//-----------------------------------------------------------------------------
class TokenLine
{
public:

	virtual void SetLine(const char *word);

	virtual int CountToken(void);

	virtual const char *GetToken(int number);

	virtual char *GetRestOfLine(int number);

};


#endif // TOKENLINE_H
