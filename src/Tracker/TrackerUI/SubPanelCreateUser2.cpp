//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include "SubPanelCreateUser2.h"
#include "SubPanelError.h"
#include "Tracker.h"

#include <KeyValues.h>
#include <vgui_controls/TextEntry.h>
#include <vgui_controls/Label.h>
#include <vgui_controls/WizardPanel.h>

using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : 
// Output : 
//-----------------------------------------------------------------------------
CSubPanelCreateUser2::CSubPanelCreateUser2(vgui::Panel *parent, const char *panelName) : WizardSubPanel(parent, panelName)
{
	m_pUserNameEdit = new TextEntry( this, "LoginIDEdit" );
	m_pFirstNameEdit = new TextEntry( this, "FirstNameEdit" );
	m_pLastNameEdit = new TextEntry( this, "LastNameEdit" );
	m_pPasswordEdit = new TextEntry( this, "PasswordEdit" );
	m_pPasswordRepeatEdit = new TextEntry( this, "PasswordRepeatEdit" );

	Label *UserNameLabel = new Label( this, "UserName", ""  );
	Label *FirstNameLabel = new Label( this, "FirstNameLabel", ""  );
	Label *LastNameLabel = new Label( this, "LastNameLabel", ""  );
	Label *PasswordLabel = new Label( this, "PasswordLabel", ""  );
	Label *RepeatPasswordLabel = new Label( this, "RepeatPassword", ""  );

	m_pUserNameEdit->AddActionSignalTarget(this);
	m_pFirstNameEdit->AddActionSignalTarget(this);
	m_pLastNameEdit->AddActionSignalTarget(this);
	m_pPasswordEdit->AddActionSignalTarget(this);
	m_pPasswordRepeatEdit->AddActionSignalTarget(this);

	LoadControlSettings("Friends/SubPanelCreateUser2.res");

	UserNameLabel->SetText("#TrackerUI_UserName");
	FirstNameLabel->SetText("#TrackerUI_FirstName");
	LastNameLabel->SetText("#TrackerUI_LastName");
	PasswordLabel->SetText("#TrackerUI_Password");
	RepeatPasswordLabel->SetText("#TrackerUI_RepeatPassword");
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : 
//-----------------------------------------------------------------------------
void CSubPanelCreateUser2::PerformLayout()
{
	GetWizardPanel()->SetFinishButtonEnabled(false);
	GetWizardPanel()->SetTitle("#TrackerUI_Friends_CreateNewUser_2_of_3_Title", false);
	GetWizardPanel()->SetNextButtonEnabled(true);
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *previousSubPanel - 
//			*wizardPanel - 
//-----------------------------------------------------------------------------
void CSubPanelCreateUser2::OnDisplay()
{
	GetWizardPanel()->SetNextButtonEnabled(true);
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CSubPanelCreateUser2::OnNextButton()
{
	// write the data out into the wizard
	KeyValues *dat = GetWizardData();

	char buf[256];

	m_pUserNameEdit->GetText(buf, 255);
	dat->SetString("username", buf);

	m_pFirstNameEdit->GetText(buf, 255);
	dat->SetString("firstname", buf);

	m_pLastNameEdit->GetText(buf, 255);
	dat->SetString("lastname", buf);

	m_pPasswordEdit->GetText(buf, 255);
	dat->SetString("password", buf);

	return true;
}


//-----------------------------------------------------------------------------
// Purpose: Checks to see if the user has entered enough info to be allowed to continue
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CSubPanelCreateUser2::VerifyEntriesAreValid()
{
	char buf[256], buf2[256];

	// check user name
	m_pUserNameEdit->GetText(buf, 255);
	if (strlen(buf) < 4)
		return false;

	// check passwords are the same
	m_pPasswordEdit->GetText(buf, 255);
	m_pPasswordRepeatEdit->GetText(buf2, 255);
	if (strlen(buf) < 3 || strcmp(buf, buf2))
		return false;

	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Called when the text in one of the text entries changes
//-----------------------------------------------------------------------------
void CSubPanelCreateUser2::OnTextChanged()
{
	if (IsVisible())
	{
		GetWizardPanel()->SetNextButtonEnabled(true);
	}
}

//-----------------------------------------------------------------------------
// Purpose: Checks for errors before moving to next panel
// Output : WizardSubPanel
//-----------------------------------------------------------------------------
WizardSubPanel *CSubPanelCreateUser2::GetNextSubPanel()
{
	CSubPanelError *errorPanel = dynamic_cast<CSubPanelError *>(GetWizardPanel()->FindChildByName("SubPanelError"));
	if (errorPanel)
	{
		char buf[256], buf2[256];

		// check user name
		m_pUserNameEdit->GetText(buf, 255);
		if (strlen(buf) < 3)
		{
			GetWizardPanel()->SetTitle("#TrackerUI_Friends_InvalidUserNameTitle", true);
			errorPanel->SetErrorText("#TrackerUI_InvalidUserNameLessThan3");
			return errorPanel;
		}
		if (strlen(buf) > 24)
		{
			GetWizardPanel()->SetTitle("#TrackerUI_Friends_InvalidUserNameTitle", true);
			errorPanel->SetErrorText("#TrackerUI_InvalidUserNameGreaterThan24");
			return errorPanel;
		}

		// check passwords are the same
		m_pPasswordEdit->GetText(buf, 255);
		m_pPasswordRepeatEdit->GetText(buf2, 255);
		if (strlen(buf) < 6)
		{
			GetWizardPanel()->SetTitle("#TrackerUI_Friends_InvalidPasswordTitle", true);
			errorPanel->SetErrorText("#TrackerUI_InvalidPasswordMustBeAtLeast6");
			return errorPanel;
		}
		if (strcmp(buf, buf2))
		{
			GetWizardPanel()->SetTitle("#TrackerUI_Friends_InvalidPasswordTitle", true);
			errorPanel->SetErrorText("#TrackerUI_SecondPasswordDidntMatchFirst\n");
			return errorPanel;
		}
	}

	return dynamic_cast<WizardSubPanel *>(GetWizardPanel()->FindChildByName("SubPanelCreateUser3"));
}



//-----------------------------------------------------------------------------
// Purpose: Message map
//-----------------------------------------------------------------------------
MessageMapItem_t CSubPanelCreateUser2::m_MessageMap[] =
{
	MAP_MESSAGE( CSubPanelCreateUser2, "TextChanged", OnTextChanged ),	// custom message
};
IMPLEMENT_PANELMAP(CSubPanelCreateUser2, Panel);

