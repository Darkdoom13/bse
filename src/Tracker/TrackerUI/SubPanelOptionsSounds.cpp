//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include "SubPanelOptionsSounds.h"
#include "Tracker.h"
#include "TrackerDoc.h"
#include <vgui_controls/CheckButton.h>

using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CSubPanelOptionsSounds::CSubPanelOptionsSounds() : PropertyPage(NULL, "SubPanelOptionsSounds")
{

	CheckButton* m_pIngameSoundCheck = new CheckButton(this, "IngameSoundCheck", "");
	CheckButton* m_pOnlineSoundCheck = new CheckButton(this, "OnlineSoundCheck", "");
	CheckButton* m_pMessageSoundCheck = new CheckButton(this, "MessageSoundCheck", "");

	LoadControlSettings("Friends/SubPanelOptionsSounds.res");

	m_pIngameSoundCheck->SetText("#TrackerUI_PlaySoundWhenFriendJoins");
	m_pOnlineSoundCheck->SetText("#TrackerUI_PlaySoundWhenFriendComesOnline");
	m_pMessageSoundCheck->SetText("#TrackerUI_PlaySoundWhenReceiveAMessage");

}

//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
CSubPanelOptionsSounds::~CSubPanelOptionsSounds()
{
}

//-----------------------------------------------------------------------------
// Purpose: Loads data from doc
//-----------------------------------------------------------------------------
void CSubPanelOptionsSounds::OnResetData()
{
	KeyValues *docData = GetDoc()->Data()->FindKey("User/Sounds", true);

	SetControlInt("IngameSoundCheck", docData->GetInt("Ingame", 1));
	SetControlInt("OnlineSoundCheck", docData->GetInt("Online", 0));
	SetControlInt("MessageSoundCheck", docData->GetInt("Message", 1));
}


//-----------------------------------------------------------------------------
// Purpose: Writes data to doc
//-----------------------------------------------------------------------------
void CSubPanelOptionsSounds::OnApplyChanges()
{
	KeyValues *docData = GetDoc()->Data()->FindKey("User/Sounds", true);

	docData->SetInt("Ingame", GetControlInt("IngameSoundCheck", docData->GetInt("Ingame", 1)));
	docData->SetInt("Online", GetControlInt("OnlineSoundCheck", docData->GetInt("Online", 0)));
	docData->SetInt("Message", GetControlInt("MessageSoundCheck", docData->GetInt("Message", 1)));
}

