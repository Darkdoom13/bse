//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include "ServerSession.h"
#include "SubPanelOptionsPersonal.h"
#include "Tracker.h"
#include "TrackerDialog.h"
#include "TrackerDoc.h"
#include <vgui_controls/TextEntry.h>
#include <vgui_controls/Label.h>

#include <KeyValues.h>

using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CSubPanelOptionsPersonal::CSubPanelOptionsPersonal() : PropertyPage(NULL, "SubPanelOptionsPersonal")
{
	Label *UserNameLabel = new Label( this, "UserName", ""  );
	Label *FirstNameLabel = new Label( this, "FirstNameLabel", ""  );
	Label *LastNameLabel = new Label( this, "LastNameLabel", ""  );

	TextEntry *m_pUserNameEdit = new TextEntry(this, "UserNameEdit");
	TextEntry *m_pFirstNameEdit = new TextEntry(this, "FirstNameEdit");
	TextEntry *m_pLastNameEdit = new TextEntry(this, "LastNameEdit");

	LoadControlSettings("Friends/SubPanelOptionsPersonal.res");

	UserNameLabel->SetText("#TrackerUI_UserName");
	FirstNameLabel->SetText("#TrackerUI_FirstName");
	LastNameLabel->SetText("#TrackerUI_LastName");
}

//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
CSubPanelOptionsPersonal::~CSubPanelOptionsPersonal()
{

}

//-----------------------------------------------------------------------------
// Purpose: Loads data from doc
//-----------------------------------------------------------------------------
void CSubPanelOptionsPersonal::OnResetData()
{
	KeyValues *docData = GetDoc()->Data()->FindKey("User", true);

	SetControlString("UserNameEdit", docData->GetString("UserName", ""));
	SetControlString("FirstNameEdit", docData->GetString("FirstName", ""));
	SetControlString("LastNameEdit", docData->GetString("LastName", ""));
	SetControlString("EmailEdit", docData->GetString("Email", ""));
}

//-----------------------------------------------------------------------------
// Purpose: Writes data to doc
//-----------------------------------------------------------------------------
void CSubPanelOptionsPersonal::OnApplyChanges()
{
	KeyValues *docData = GetDoc()->Data()->FindKey("User", true);

	// if anything has changed, reset data on server
	if (stricmp(GetControlString("UserNameEdit", ""), docData->GetString("UserName")) 
		|| stricmp(GetControlString("FirstNameEdit", ""), docData->GetString("FirstName")) 
		|| stricmp(GetControlString("LastNameEdit", ""), docData->GetString("LastName")))
	{
		// update document
		docData->SetString("UserName", GetControlString("UserNameEdit", ""));
		docData->SetString("FirstName", GetControlString("FirstNameEdit", ""));
		docData->SetString("LastName", GetControlString("LastNameEdit", ""));

		// upload
		ServerSession().UpdateUserInfo(GetDoc()->GetUserID(), docData->GetString("UserName"), docData->GetString("FirstName"), docData->GetString("LastName"));

		// redraw local
		CTrackerDialog::GetInstance()->OnFriendsStatusChanged(1);
	}
}


