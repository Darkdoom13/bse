//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include "SubPanelCreateUser.h"
#include "SubPanelError.h"
#include "Tracker.h"

#include <vgui_controls/Controls.h>
#include <vgui/ISurface.h>
#include <KeyValues.h>
#include <vgui_controls/TextEntry.h>
#include <vgui_controls/Label.h>
#include <vgui_controls/WizardPanel.h>

#include <string.h>

using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : 
// Output : 
//-----------------------------------------------------------------------------
CSubPanelCreateUser::CSubPanelCreateUser(Panel *parent, const char *panelName) : WizardSubPanel(parent, panelName)
{
	// get the email edit control
	emailEdit = new TextEntry( this, "EmailEdit" );

	Label *emailLabel = new Label( this, "EmailLabel", ""  );  

	if (emailEdit)
	{
		emailEdit->AddActionSignalTarget(this);
	}

	LoadControlSettings("Friends/SubPanelCreateUser.res");

	emailLabel->SetText("#TrackerUI_EMailAddress");

}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CSubPanelCreateUser::OnNextButton()
{
	if (!emailEdit)
		return false;

	char buf[256];
	emailEdit->GetText(buf, 255);

	// write the data out into the wizard
	KeyValues *dat = GetWizardData();
	dat->SetString("email", buf);

	return true;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : 
//-----------------------------------------------------------------------------
void CSubPanelCreateUser::PerformLayout()
{
	GetWizardPanel()->SetTitle("#TrackerUI_Friends_CreateNewUser_1_of_3_Title", false);
	GetWizardPanel()->SetFinishButtonEnabled(false);

	// make sure we have a valid email address before letting the user move on
	bool nextValid = false;
	if (emailEdit)
	{
		char buf[256];
		emailEdit->GetText(buf, 255);
		if (strlen(buf) > 0)
		{
			nextValid = true;
		}
	}

	GetWizardPanel()->SetNextButtonEnabled(nextValid);
}

//-----------------------------------------------------------------------------
// Purpose: Returns the next sub panel is email address is valid, error box otherwise.
// Input  : 
// Output : WizardSubPanel
//-----------------------------------------------------------------------------
WizardSubPanel *CSubPanelCreateUser::GetNextSubPanel()
{
	if (emailEdit)
	{
		char buf[256];
		emailEdit->GetText(buf, 255);
		if (!IsEmailAddressValid(buf))
		{
			// return error dialog
			CSubPanelError *errorPanel = dynamic_cast<CSubPanelError *>(GetWizardPanel()->FindChildByName("SubPanelError"));
			if (errorPanel)
			{
				GetWizardPanel()->SetTitle("#TrackerUI_Friends_InvalidEmailTitle", true);
				errorPanel->SetErrorText("#TrackerUI_EMailIsInvalidTryAgain");
				return errorPanel;
			}
		}
	}

	return dynamic_cast<WizardSubPanel *>(GetWizardPanel()->FindChildByName("SubPanelCreateUser2"));
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *text - 
//-----------------------------------------------------------------------------
void CSubPanelCreateUser::OnTextChanged()
{
	if (IsVisible())
	{
		if (emailEdit)
		{
			char text[256];
			emailEdit->GetText(text, 254);
			GetWizardPanel()->SetNextButtonEnabled(strlen(text) > 0);
		}
	}
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CSubPanelCreateUser::IsEmailAddressValid(const char *email)
{
	// check for initial valid characters
	const char *ch = email;

	// look for the '@'
	while (1)
	{
		if (*ch == 0)
		{
			return false;
		}

		if (*ch == '@')
		{
			if ((ch - email) < 1)
				return false;

			// first test passed
			break;
		}

		ch++;
	}

	const char *andpoint = ch;

	// look for the period
	while (1)
	{
		if (*ch == 0)
		{
			return false;
		}

		if (*ch == '.')
		{
			if ((ch - andpoint) < 1)
				return false;

			// second test passed
			break;
		}

		ch++;
	}

	// make sure there is at least one more character following the period
	if (ch[1] == 0)
		return false;

	// success
	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Message map
//-----------------------------------------------------------------------------
MessageMapItem_t CSubPanelCreateUser::m_MessageMap[] =
{
	MAP_MESSAGE( CSubPanelCreateUser, "TextChanged", OnTextChanged ),	// custom message
};
IMPLEMENT_PANELMAP(CSubPanelCreateUser, Panel);


