//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include <vgui_controls/WizardPanel.h>
#include <vgui_controls/Button.h>
#include <KeyValues.h>

#include "SubPanelConnectionTest.h"
#include "ServerSession.h"
#include "TrackerProtocol.h"

using namespace vgui;

#define PING_TIMEOUT_TIME_SECONDS 8.1f

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CSubPanelConnectionTest::CSubPanelConnectionTest(vgui::Panel *parent, const char *panelName) : WizardSubPanel(parent, panelName)
{
	RetryButton = new vgui::Button( this, "RetryButton", "" );

	RetryButton->SetEnabled(true);

	RetryButton->SetCommand( "RetryConnect" );

	m_pInfoText = new Label(this, "InfoText", "");

	m_pDescriptionText = new Label(this, "DescriptionText", "");

	m_iCurrentPingID = 0;
	m_bConnectFailed = false;
	m_bServerFound = false;

	ServerSession().AddNetworkMessageWatch(this, TSVC_PINGACK);

	LoadControlSettings("Friends/SubPanelConnectionTest.res");

	RetryButton->SetText("#TrackerUI_Retry");


}

//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
CSubPanelConnectionTest::~CSubPanelConnectionTest()
{
}

//-----------------------------------------------------------------------------
// Purpose: returns the next panel to let the user Start logging in
//-----------------------------------------------------------------------------
WizardSubPanel *CSubPanelConnectionTest::GetNextSubPanel()
{
	return dynamic_cast<WizardSubPanel *>(GetWizardPanel()->FindChildByName("SubPanelSelectLoginOption"));
}

//-----------------------------------------------------------------------------
// Purpose: Sets up the to Start
//-----------------------------------------------------------------------------
void CSubPanelConnectionTest::OnDisplayAsNext()
{
	// Start the refresh
	StartServerSearch();
}

//-----------------------------------------------------------------------------
// Purpose: sets up layout
//-----------------------------------------------------------------------------
void CSubPanelConnectionTest::PerformLayout()
{
	BaseClass::PerformLayout();

	GetWizardPanel()->SetTitle("#TrackerUI_TestingNetworkConnectionTitle", true);

	GetWizardPanel()->SetFinishButtonEnabled(false);
	GetWizardPanel()->SetCancelButtonEnabled(true);

	// set the text that we've succeeded	
	GetWizardPanel()->SetNextButtonEnabled(m_bServerFound);
		
	RetryButton->SetEnabled(true);
	if (m_bServerFound)
	{
		m_pInfoText->SetText("#TrackerUI_AttemptingToConnect_Success");
		m_pDescriptionText->SetText("#TrackerUI_SuccessfullyConnectedClickNext");

	}
	else if (m_bConnectFailed)
	{
		m_pInfoText->SetText("#TrackerUI_AttemptingToConnect_Failure");
		m_pDescriptionText->SetText("#TrackerUI_FailedToConnect_CheckFirewall");
	}
	else
	{
		m_pInfoText->SetText("#TrackerUI_AttemptingToConnect");
		m_pDescriptionText->SetText("");

		RetryButton->SetEnabled(false);
	}
}

//-----------------------------------------------------------------------------
// Purpose: Starts the searching for a server, and updates the UI appropriately
//-----------------------------------------------------------------------------
void CSubPanelConnectionTest::StartServerSearch()
{
	// Start looking for a server to connect to while the user enters their info
	ServerSession().UnconnectedSearchForServer();

	m_iCurrentPingID++;
	m_bConnectFailed = false;
	m_bServerFound = false;

	// post a timeout message to ourselves
	PostMessage(this, new KeyValues("PingTimeout", "pingID", m_iCurrentPingID), PING_TIMEOUT_TIME_SECONDS);

	InvalidateLayout();
}

//-----------------------------------------------------------------------------
// Purpose: Handles ping acknowledged message
//-----------------------------------------------------------------------------
void CSubPanelConnectionTest::OnPingAck()
{
	// increment the ping id
	m_iCurrentPingID++;
	m_bServerFound = true;

	InvalidateLayout();
}

//-----------------------------------------------------------------------------
// Purpose: Handles a command from a button
//-----------------------------------------------------------------------------
void CSubPanelConnectionTest::OnCommand(const char *command)
{
	if (!stricmp(command, "RetryConnect"))
	{
		StartServerSearch();
	}
	else
	{
		BaseClass::OnCommand(command);
	}
}


//-----------------------------------------------------------------------------
// Purpose: Checks to see if the attempt at connection timed out
// Input  : pingID - identifier of the ping
//-----------------------------------------------------------------------------
void CSubPanelConnectionTest::OnPingTimeout(int pingID)
{
	if (pingID == m_iCurrentPingID)
	{
		m_bConnectFailed = true;
	}

	InvalidateLayout();
}

//-----------------------------------------------------------------------------
// Purpose: Message map
//-----------------------------------------------------------------------------
MessageMapItem_t CSubPanelConnectionTest::m_MessageMap[] =
{
	MAP_MSGID( CSubPanelConnectionTest, TSVC_PINGACK, OnPingAck ),		// network message
	MAP_MESSAGE_INT( CSubPanelConnectionTest, "PingTimeout", OnPingTimeout, "pingID" ),		// network message
};
IMPLEMENT_PANELMAP(CSubPanelConnectionTest, BaseClass);
