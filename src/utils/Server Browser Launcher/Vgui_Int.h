//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef SERVERBROWSER_H
#define SERVERBROWSER_H
#ifdef _WIN32
#pragma once
#endif

//#include "ServerBrowser/IServerBrowser.h"
#include "IVGuiModule.h"

#include <vgui_controls/PHandle.h>

//class CServerBrowserDialog;

//-----------------------------------------------------------------------------
// Purpose: Handles the UI and pinging of a half-life game server list
//-----------------------------------------------------------------------------
class CTrackerUIVGuiModule : public IVGuiModule
{
public:
	CTrackerUIVGuiModule();
	~CTrackerUIVGuiModule();

	// IVGui module implementation
	virtual bool Initialize(CreateInterfaceFn *factorylist, int numFactories);
	virtual bool PostInitialize(CreateInterfaceFn *modules, int factoryCount);
	virtual vgui::VPANEL GetPanel();
	virtual bool Activate();
	virtual bool IsValid();
	virtual void Shutdown();
	virtual void Deactivate();
	virtual void Reactivate();
	virtual void SetParent(vgui::VPANEL parent);

private:
	vgui::DHANDLE<CTrackerUIVGuiModule> m_hfriendlylg;
};


#endif // SERVERBROWSER_H
