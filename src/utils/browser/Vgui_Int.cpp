//=========== (C) Copyright 2009 BSE, L.L.C. All rights reserved. ===========
//
// The copyright to the contents herein is the property of BSE.
// The contents may be used and/or copied only with the written permission of
// BSE., or in accordance with the terms and conditions stipulated in
// the agreement/contract under which the contents have been supplied.
//
// Purpose: 
//=============================================================================

#include <vgui\IScheme.h>
#include "interface.h"
#include "..\..\tracker\common\winlite.h"
#include <vgui_controls\Controls.h>
#include <vgui\MouseCode.h>
#include <vgui\KeyCode.h>
#include <vgui\IVGui.h>
#include <vgui\ISurface.h>
#include <vgui\ILocalize.h>
#include <vgui_controls\Panel.h>
#include "utlbuffer.h"
#include "filesystem.h"

#include "CTrackerVgui.h"

#include <stdio.h>

//-----------------------------------------------------------------------------
// Purpose: Entry point
//			loads interfaces and initializes dialog
//-----------------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	// Load vgui
	CSysModule *vguiModule = Sys_LoadModule("bin/vgui2.dll");
	if (!vguiModule)
	{
		vguiModule = Sys_LoadModule("vgui2.dll");
	}

	CreateInterfaceFn vguiFactory = Sys_GetFactory(vguiModule);
	if (!vguiFactory)
	{
		printf("Fatal error: Could not load vgui2.dll\n");
		return 2;
	}	  

	CSysModule *filesystemModule = Sys_LoadModule("bin/filesystem_stdio.dll");
	if (!filesystemModule)
	{
		filesystemModule = Sys_LoadModule("filesystem_stdio.dll");
	}

	CreateInterfaceFn filesystemFactory = Sys_GetFactory(filesystemModule);
	if (!filesystemFactory)
	{
		printf("Fatal error: Could not load bin/filesystem_stdio.dll\n");
		return 2;
	}	  

	// Initialize interfaces
	CreateInterfaceFn factories[3];
	factories[0] = Sys_GetFactoryThis();
	factories[1] = vguiFactory;
	factories[2] = filesystemFactory;

	if (!vgui::VGui_InitInterfacesList( "Browser", factories, 3))
	{
		printf("Fatal error: Could not initalize vgui2.dll\n");
		return 3;
	}

	// In order to load resource files the file must be in your vgui filesystem path.
	filesystem()->AddSearchPath("../", "");

	// Init the surface
	vgui::surface()->Init();

	// Load the scheme
	if (!vgui::scheme()->LoadSchemeFromFile("Resource/TrackerScheme.res", "Tracker"))
		return 1;

	// localization
	vgui::localize()->AddFile(vgui::filesystem(), "Resource/platform_english.txt");
    vgui::localize()->AddFile(vgui::filesystem(), "Resource/vgui_english.txt");

	// Make a embedded panel
	vgui::Panel *panel = new vgui::Panel(NULL, "TopPanel");
	vgui::surface()->SetEmbeddedPanel( panel->GetVPanel() );

	// Start vgui
	vgui::ivgui()->Start();

	// Add our main window
	VInternetDlg *panelTracker = new VInternetDlg();
	panelTracker->Activate();

	// Run app frame loop
	while (vgui::ivgui()->IsRunning())
	{
		vgui::ivgui()->RunFrame();
	}

	// Shutdown
	vgui::surface()->Shutdown();

//	delete panelTracker;

	Sys_UnloadModule(vguiModule);
	return 1;
}






