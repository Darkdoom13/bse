//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================
#include "DemoPage.h"

#include <vgui\IVGui.h>
#include <KeyValues.h>
#include <vgui_controls\Controls.h>

#include <vgui_controls/URLLabel.h>


using namespace vgui;

class URLLabelDemo: public DemoPage
{
	public:
		URLLabelDemo(Panel *parent, const char *name);
		~URLLabelDemo();

	private:
		URLLabel *m_pUrlLabel;

};

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
URLLabelDemo::URLLabelDemo(Panel *parent, const char *name) : DemoPage(parent, name)
{
	m_pUrlLabel = new URLLabel(this, "AURLLabel", "http://csmania.ru", "http://csmania.ru");

	m_pUrlLabel->SetSize(100, 100);

	m_pUrlLabel->SetPos(100, 100);

//	int wide, tall;
//	m_pToggleButton->GetContentSize(wide, tall);
//	m_pToggleButton->SetSize(wide + Label::Content, tall + Label::Content);

}

//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
URLLabelDemo::~URLLabelDemo()
{
}

Panel* URLLabelDemo_Create(Panel *parent)
{
	return new URLLabelDemo(parent, "URLLabelDemo");
}


