//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vtf.h"


void Usage( void )
{
	fprintf( stderr, "Usage: printbones blah.mdl\n" );
	exit( -1 );
}

int main( int argc, char **argv )
{
	if( argc != 2 )
	{
		Usage();
	}

	FILE *fp;
	fp = fopen( argv[1], "rb" );
	if( !fp )
	{
		fprintf( stderr, "Can't open: %s\n", argv[1] );
		Usage();
	}
	fseek( fp, 0, SEEK_END );
	int len = ftell( fp );
	rewind( fp );

	VTFFileHeader_t *pVhdr = ( VTFFileHeader_t * )malloc( len );
	fread( pVhdr, 1, len, fp );


	printf( "File Type: %s\n", pVhdr->fileTypeString );

	printf( "File Version: %d\n", pVhdr->version[0] );

	printf( "File Subversion: %d\n", pVhdr->version[1] );

	printf( "Header Size: %d\n", pVhdr->headerSize );

	fclose( fp );


	return 0;
}