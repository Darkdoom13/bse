struct VTFFileHeader_t
{
	char fileTypeString[4]; // "VTF" Valve texture file
	int version[2]; // version[0].version[1]
	int headerSize;
	unsigned short width;
	unsigned short height;
	unsigned int flags;
	unsigned short numFrames;
	unsigned short startFrame;
//	VectorAligned reflectivity; // This is a linear value, right?  Average of all frames?
//	float bumpScale;
//	ImageFormat imageFormat;
//	unsigned char numMipLevels;
//
//	ImageFormat lowResImageFormat;
//	unsigned char lowResImageWidth;
//	unsigned char lowResImageHeight;	
};