//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef HLDS_GUI_H
#define HLDS_GUI_H
#ifdef _WIN32
#pragma once
#endif

#include "IVGuiModule.h"

#include <vgui_controls/PHandle.h>

//class VInternetDlg;

//-----------------------------------------------------------------------------
// Purpose: Handles the UI and pinging of a half-life game server list
//-----------------------------------------------------------------------------
class CHLDS : public IVGuiModule
{
public:
	CHLDS();
	~CHLDS();

	// IVGui module implementation
	virtual bool Initialize(CreateInterfaceFn *factorylist, int numFactories);
	virtual bool PostInitialize(CreateInterfaceFn *modules, int factoryCount);
	virtual vgui::VPANEL GetPanel();
	virtual void SetParent(vgui::VPANEL parent);
	virtual bool Activate();
	virtual bool IsValid();
	virtual void Deactivate();
	virtual void Reactivate();
	virtual void Shutdown();

//	virtual void CreateDialog();
	virtual void Test();
	virtual void Open();

//private:
//	vgui::DHANDLE<VInternetDlg> m_hInternetDlg;
};


#endif // HLDS_GUI_H
