//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include <stdio.h>
#include <string.h>
#include "studio.h"
#include <iostream>

using namespace std;


int g_NumBonesInLOD[MAX_NUM_LODS];

void Usage( void )
{
	fprintf( stderr, "Usage: animatinfo blah.mdl\n" );
	exit( -1 );
}

int main( int argc, char **argv )
{
	if( argc != 2 )
	{
		Usage();
	}

	FILE *fp;
	fp = fopen( argv[1], "rb" );
	if( !fp )
	{
		fprintf( stderr, "Can't open: %s\n", argv[1] );
		Usage();
	}
	fseek( fp, 0, SEEK_END );
	int len = ftell( fp );
	rewind( fp );

	studiohdr_t *pStudioHdr = ( studiohdr_t * )malloc( len );
	fread( pStudioHdr, 1, len, fp );

	int i;
	for (i = 0; i < pStudioHdr->GetNumSeq(); i++)
	{
		printf( "Activity : %s\n", pStudioHdr->pSeqdesc( i ).pszActivityName() );

	}
	
	fclose( fp );

	cin.get();


	return 0;
}

studiohdr_t *FindOrLoadGroupFile( char const *modelname )
{
	FILE *fp;
	fp = fopen( modelname, "rb" );
	if( !fp )
	{
		return NULL;
	}
	fseek( fp, 0, SEEK_END );
	int len = ftell( fp );
	rewind( fp );

	studiohdr_t *hdr = ( studiohdr_t * )malloc( len );
	fread( hdr, 1, len, fp );

	return hdr;
}