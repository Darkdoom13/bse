//=========== (C) Copyright 1999 Valve, L.L.C. All rights reserved. ===========
//
// The copyright to the contents herein is the property of Valve, L.L.C.
// The contents may be used and/or copied only with the written permission of
// Valve, L.L.C., or in accordance with the terms and conditions stipulated in
// the agreement/contract under which the contents have been supplied.
//
// Purpose: 
//=============================================================================


#include "CTrackerVgui.h"
#include "stdio.h"

#include <vgui\ISurface.h>
#include <vgui_controls\Controls.h>
#include <KeyValues.h>

#include <vgui_controls\Label.h>
#include <vgui_controls\ComboBox.h>

#include <vgui\IVGui.h> // for dprinf statements

#include "filesystem.h"


using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
CTrackerVgui::CTrackerVgui(): Frame(NULL, "Tracker Vgui")
{
	SetTitle("VGUI Tracker", true);
	// calculate defaults
	int x, y, wide, tall;
	vgui::surface()->GetScreenSize(wide, tall);
	int dwide, dtall;
	dwide = 535;
	dtall = 405;
	x = (int)((wide - dwide) * 0.5);
	y = (int)((tall - dtall) * 0.5);
	SetBounds (x, y, dwide, dtall);

}


//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
CTrackerVgui::~CTrackerVgui()
{ 
}
 
//-----------------------------------------------------------------------------
// Purpose: Handles closing of the dialog - shuts down the whole app
//-----------------------------------------------------------------------------
void CTrackerVgui::OnClose()
{
	Frame::OnClose();

	// stop vgui running
	vgui::ivgui()->Stop();
}




