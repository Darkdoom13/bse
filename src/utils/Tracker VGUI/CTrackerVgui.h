//=========== (C) Copyright 1999 Valve, L.L.C. All rights reserved. ===========
//
// The copyright to the contents herein is the property of Valve, L.L.C.
// The contents may be used and/or copied only with the written permission of
// Valve, L.L.C., or in accordance with the terms and conditions stipulated in
// the agreement/contract under which the contents have been supplied.
//
// Purpose: 
//=============================================================================


#ifndef CTrackerVgui_H
#define CTrackerVgui_H
#ifdef _WIN32
#pragma once
#endif

#include <vgui_controls\Frame.h>
#include <UtlVector.h>

namespace vgui
{
	class ComboBox;
	class Label;
};

using namespace vgui;
class CTrackerVgui: public Frame
{
public:
	CTrackerVgui();
	~CTrackerVgui();

	void OnClose();	

private:

};

#endif // CTrackerVgui_H


