//====== Copyright � 1996-2003, Valve Corporation, All rights reserved. =======
//
// Purpose: static_prop - don't move, don't animate, don't do anything.
//			physics_prop - move, take damage, but don't animate
//
//=============================================================================

#include "cbase.h"
#include "BasePropDoor.h"
#include "ai_basenpc.h"
#include "npcevent.h"
#include "animation.h"
#include "engine/IEngineSound.h"
#include "locksounds.h"
#include "filters.h"
#include "physics.h"
#include "vphysics_interface.h"
#include "entityoutput.h"
#include "vcollide_parse.h"
#include "bone_setup.h"
#include "studio.h"
#include "explode.h"
#include "entityblocker.h"
#include "igamesystem.h"
#include "tier1/utlrbtree.h"
#include "tier1/strtools.h"
#include "physics_impact_damage.h"
#include "tier1/KeyValues.h"
#include "filesystem.h"
#include "igamesystem.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#define DOOR_HARDWARE_GROUP 1

// Any barrel farther away than this is ignited rather than exploded.
#define PROP_EXPLOSION_IGNITE_RADIUS	32.0f

ConVar g_debug_doors( "g_debug_doors", "0" );

const char *GetMassEquivalent(float flMass);


//=============================================================================================================
// PROP DATA
//=============================================================================================================
//-----------------------------------------------------------------------------
// Purpose: Gamesystem that parses the prop data file
//-----------------------------------------------------------------------------
class CPropData : public CAutoGameSystem
{
public:
	CPropData( void );

	// Inherited from IAutoServerSystem
	virtual void LevelInitPreEntity( void );
	virtual void ShutdownAllSystems( void );

	// Read in the data from the prop data file
	void ParsePropDataFile( void );

	// Parse a keyvalues section into the prop
	int ParsePropFromKV( CBaseProp *pProp, KeyValues *pSection );

	// Fill out a prop's with base data parsed from the propdata file
	int ParsePropFromBase( CBaseProp *pProp, const char *pszPropData );

private:
	KeyValues	*m_pKVPropData;
	bool		m_bPropDataLoaded;
};

static CPropData g_PropDataSystem;

//-----------------------------------------------------------------------------
// Constructor, destructor
//-----------------------------------------------------------------------------
CPropData::CPropData( void )
{
	m_bPropDataLoaded = false;
	m_pKVPropData = NULL;
}

//-----------------------------------------------------------------------------
// Inherited from IAutoServerSystem
//-----------------------------------------------------------------------------
void CPropData::LevelInitPreEntity( void )
{
	ParsePropDataFile();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CPropData::ShutdownAllSystems( void )
{
	if ( m_pKVPropData )
	{
		m_pKVPropData->deleteThis();
	}
}

//-----------------------------------------------------------------------------
// Clear out the stats + their history
//-----------------------------------------------------------------------------
void CPropData::ParsePropDataFile( void )
{
	m_pKVPropData = new KeyValues( "PropDatafile" );
	if ( !m_pKVPropData->LoadFromFile( filesystem, "scripts/propdata.txt" ) )
	{
		m_pKVPropData->deleteThis();
		return;
	}

	m_bPropDataLoaded = true;
}

//-----------------------------------------------------------------------------
// Purpose: Parse a keyvalues section into the prop
//-----------------------------------------------------------------------------
int CPropData::ParsePropFromKV( CBaseProp *pProp, KeyValues *pSection )
{
	// Do we have a base?
	char const *pszBase = pSection->GetString( "base" );
	if ( pszBase && pszBase[0] )
	{
		int iResult = ParsePropFromBase( pProp, pszBase );
		if ( iResult != PARSE_SUCCEEDED )
			return iResult;
	}

	// Only breakable props need to get health data
	CBreakableProp *pBreakable = dynamic_cast<CBreakableProp *>(pProp);
	if ( pBreakable )
	{
		// Get damage modifiers, but only if they're specified, because our base may have already overridden them.
		pBreakable->SetDmgModBullet( pSection->GetFloat( "dmg.bullets", pBreakable->GetDmgModBullet() ) );
		pBreakable->SetDmgModClub( pSection->GetFloat( "dmg.club", pBreakable->GetDmgModClub() ) );
		pBreakable->SetDmgModExplosive( pSection->GetFloat( "dmg.explosive", pBreakable->GetDmgModExplosive() ) );

		// Get the health (unless this is an override prop)
		if ( !FClassnameIs( pProp, "prop_physics_override" ) && !FClassnameIs( pProp, "prop_dynamic_override" ) )
		{
			pBreakable->SetHealth( pSection->GetInt( "health", pBreakable->GetHealth() ) );
		}

		// Otherwise, see if our propdata says we are allowed to be static
		if ( pSection->GetInt( "allowstatic", 0 ) )
			return PARSE_SUCCEEDED_ALLOWED_STATIC;
	}

	return PARSE_SUCCEEDED;
}

//-----------------------------------------------------------------------------
// Purpose: Fill out a prop's with base data parsed from the propdata file
//-----------------------------------------------------------------------------
int CPropData::ParsePropFromBase( CBaseProp *pProp, const char *pszPropData )
{
	if ( !m_bPropDataLoaded )
		return PARSE_FAILED_NO_DATA;

	// Find the specified propdata
	KeyValues *pSection = m_pKVPropData->FindKey( pszPropData );
	if ( !pSection )
	{
		Warning("%s '%s' has a base specified as '%s', but there is no matching entry in propdata.txt.\n", pProp->GetClassname(), pProp->GetModelName(), pszPropData );
		return PARSE_FAILED_BAD_DATA;
	}

	// Store off the first base data for debugging
	if ( pProp->m_iszBasePropData == NULL_STRING )
	{
		pProp->m_iszBasePropData = AllocPooledString( pszPropData );
	}

	return ParsePropFromKV( pProp, pSection );
}

// Damage type modifiers for breakable objects.
ConVar func_breakdmg_bullet( "func_breakdmg_bullet", "0.5" );
ConVar func_breakdmg_club( "func_breakdmg_club", "1.5" );
ConVar func_breakdmg_explosive( "func_breakdmg_explosive", "1.25" );

//-----------------------------------------------------------------------------
// Purpose: Breakable objects take different levels of damage based upon the damage type.
//			This isn't contained by CBaseProp, because func_breakables use it as well.
//-----------------------------------------------------------------------------
float GetBreakableDamage( const CTakeDamageInfo &inputInfo, CBreakableProp *pProp )
{
	float flDamage = inputInfo.GetDamage();
	int iDmgType = inputInfo.GetDamageType();

	// Bullet damage?
	if ( iDmgType & DMG_BULLET )
	{
		if ( pProp )
		{
			flDamage *= pProp->GetDmgModBullet();
		}
		else
		{
			// Bullets do little damage to breakables
			flDamage *= func_breakdmg_bullet.GetFloat();
		}
	}

	// Club damage?
	if ( iDmgType & DMG_CLUB )
	{
		if ( pProp )
		{
			flDamage *= pProp->GetDmgModClub();
		}
		else
		{
			// Club does extra damage
			flDamage *= func_breakdmg_club.GetFloat();
		}
	}

	// Explosive damage?
	if ( iDmgType & DMG_BLAST )
	{
		if ( pProp )
		{
			flDamage *= pProp->GetDmgModExplosive();
		}
		else
		{
			// Explosions do extra damage
			flDamage *= func_breakdmg_explosive.GetFloat();
		}
	}

	// Poison & other timebased damage types do no damage
	if ( iDmgType & DMG_TIMEBASED )
	{
		flDamage = 0;
	}

	return flDamage;
}

//=============================================================================================================
// BASE PROP
//=============================================================================================================
//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBaseProp::Spawn( void )
{
	char *szModel = (char *)STRING( GetModelName() );
	if (!szModel || !*szModel)
	{
		Warning( "prop at %.0f %.0f %0.f missing modelname\n", GetAbsOrigin().x, GetAbsOrigin().y, GetAbsOrigin().z );
		UTIL_Remove( this );
		return;
	}

	Precache();
	SetModel( szModel );

	// Load this prop's data from the propdata file
	int iResult = ParsePropData();
	if ( !OverridePropdata() )
	{
		if ( iResult == PARSE_FAILED_BAD_DATA )
		{
			Warning( "%s at %.0f %.0f %0.f uses model %s, which has an invalid prop_data type. DELETED.\n", GetClassname(), GetAbsOrigin().x, GetAbsOrigin().y, GetAbsOrigin().z, szModel );
			UTIL_Remove( this );
			return;
		}
		else if ( iResult == PARSE_FAILED_NO_DATA )
		{
			// If we don't have data, but we're a prop_physics, fail
			if ( FClassnameIs( this, "prop_physics" ) )
			{
				Warning( "%s at %.0f %.0f %0.f uses model %s, which requires that it be used on a prop_static. DELETED.\n", GetClassname(), GetAbsOrigin().x, GetAbsOrigin().y, GetAbsOrigin().z, szModel );
				UTIL_Remove( this );
				return;
			}
			if ( FClassnameIs( this, "prop_physics_respawnable" ) )
			{
				Warning( "%s at %.0f %.0f %0.f uses model %s, which requires that it be used on a prop_static. DELETED.\n", GetClassname(), GetAbsOrigin().x, GetAbsOrigin().y, GetAbsOrigin().z, szModel );
				UTIL_Remove( this );
				return;
			}

			if ( FClassnameIs( this, "prop_physics_multiplayer" ) )
			{
				Warning( "%s at %.0f %.0f %0.f uses model %s, which requires that it be used on a prop_static. DELETED.\n", GetClassname(), GetAbsOrigin().x, GetAbsOrigin().y, GetAbsOrigin().z, szModel );
				UTIL_Remove( this );
				return;
			}

		}
		else if ( iResult == PARSE_SUCCEEDED )
		{
			// If we have data, and we're a static prop, fail
			//if ( !FClassnameIs( this, "prop_physics" ) && !FClassnameIs( this, "prop_physics_override" ) && !FClassnameIs( this, "prop_dynamic_override" ) )
			if ( !dynamic_cast<CPhysicsProp*>(this) )
			{
				Warning( "%s at %.0f %.0f %0.f uses model %s, which requires that it be used on a prop_physics. DELETED.\n", GetClassname(), GetAbsOrigin().x, GetAbsOrigin().y, GetAbsOrigin().z, szModel );
				UTIL_Remove( this );
				return;
			}
		}
	}

	SetMoveType( MOVETYPE_NONE );
	m_takedamage = DAMAGE_NO;
	SetNextThink( TICK_NEVER_THINK );

	m_flAnimTime = gpGlobals->curtime;
	m_flPlaybackRate = 0.0;
	m_flCycle = 0;
	Relink();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBaseProp::Precache( void )
{
	engine->PrecacheModel( STRING( GetModelName() ) );
	BaseClass::Precache();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBaseProp::Activate( void )
{
	BaseClass::Activate();
	
	// Make sure mapmakers haven't used the wrong prop type.
	if ( m_takedamage == DAMAGE_NO && m_iHealth != 0 )
	{
		Warning("%s has a health specified in model '%s'. Use prop_physics or prop_dynamic instead.\n", GetClassname(), GetModelName() );
	}
}

//-----------------------------------------------------------------------------
// Purpose: Handles keyvalues from the BSP. Called before spawning.
//-----------------------------------------------------------------------------
bool CBaseProp::KeyValue( const char *szKeyName, const char *szValue )
{
	if ( FStrEq(szKeyName, "health") )
	{
		// Only override props are allowed to override health.
		if ( FClassnameIs( this, "prop_physics_override" ) || FClassnameIs( this, "prop_dynamic_override" ) )
			return BaseClass::KeyValue( szKeyName, szValue );

		return true;
	}
	else
	{ 
		return BaseClass::KeyValue( szKeyName, szValue );
	}

	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Parse this prop's data from the model, if it has a keyvalues section.
//			Returns true only if this prop is using a model that has a prop_data section that's invalid.
//-----------------------------------------------------------------------------
int CBaseProp::ParsePropData( void )
{
	KeyValues *modelKeyValues = new KeyValues("");
	if ( !modelKeyValues->LoadFromBuffer( modelinfo->GetModelName( GetModel() ), modelinfo->GetModelKeyValueText( GetModel() ) ) )
	{
		modelKeyValues->deleteThis();
		return PARSE_FAILED_NO_DATA;
	}

	// Do we have a props section?
	KeyValues *pkvPropData = modelKeyValues->FindKey("prop_data");
	if ( !pkvPropData )
	{
		modelKeyValues->deleteThis();
		return PARSE_FAILED_NO_DATA;
	}

	int iResult = g_PropDataSystem.ParsePropFromKV( this, pkvPropData );
	modelKeyValues->deleteThis();
	return iResult;
}

// HACK: Re-use NPC bit for prop debug
#define OVERLAY_PROP_DEBUG		OVERLAY_NPC_STEERING_REGULATIONS

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBaseProp::DrawDebugGeometryOverlays( void )
{
	BaseClass::DrawDebugGeometryOverlays();

	if ( m_debugOverlays & OVERLAY_PROP_DEBUG )  
	{
		if ( m_takedamage == DAMAGE_NO )
		{
			NDebugOverlay::EntityBounds(this, 255, 0, 0, 0, 0 );
		}
		else if ( m_takedamage == DAMAGE_EVENTS_ONLY )
		{
			NDebugOverlay::EntityBounds(this, 255, 255, 255, 0, 0 );
		}
		else
		{
			// Remap health to green brightness
			float flG = RemapVal( m_iHealth, 0, 100, 64, 255 );
			flG = clamp( flG, 0, 255 );
			NDebugOverlay::EntityBounds(this, 0, flG, 0, 0, 0 );
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: Turn on prop debugging mode
//-----------------------------------------------------------------------------
void CC_Prop_Debug( void )
{
	// Toggle the prop debug bit on all props
	for ( CBaseEntity *pEntity = gEntList.FirstEnt(); pEntity != NULL; pEntity = gEntList.NextEnt(pEntity) )
	{
		CBaseProp *pProp = dynamic_cast<CBaseProp*>(pEntity);
		if ( pProp )
		{
			if ( pProp->m_debugOverlays )
			{
				pProp->m_debugOverlays &= ~OVERLAY_PROP_DEBUG;
			}
			else
			{
				pProp->m_debugOverlays |= OVERLAY_PROP_DEBUG;
			}
		}
	}
}
static ConCommand prop_debug("prop_debug", CC_Prop_Debug, "Toggle prop debug mode. If on, props will show colorcoded bounding boxes. Red means ignore all damage. White means respond physically to damage but never break. Green maps health in the range of 100 down to 1.", FCVAR_CHEAT);

//=============================================================================================================
// BREAKABLE PROPS
//=============================================================================================================
IMPLEMENT_SERVERCLASS_ST(CBreakableProp, DT_BreakableProp)
END_SEND_TABLE()

BEGIN_DATADESC( CBreakableProp )

	DEFINE_KEYFIELD( m_explodeDamage, FIELD_FLOAT, "ExplodeDamage"),	
	DEFINE_KEYFIELD( m_explodeRadius, FIELD_FLOAT, "ExplodeRadius"),	
	DEFINE_KEYFIELD( m_iMinHealthDmg, FIELD_INTEGER, "minhealthdmg" ),
	DEFINE_FIELD( m_createTick, FIELD_INTEGER ),
	DEFINE_FIELD( m_hBreaker, FIELD_EHANDLE ),

	DEFINE_FIELD( m_flDmgModBullet, FIELD_FLOAT ),
	DEFINE_FIELD( m_flDmgModClub, FIELD_FLOAT ),
	DEFINE_FIELD( m_flDmgModExplosive, FIELD_FLOAT ),

	DEFINE_KEYFIELD( m_flPressureDelay, FIELD_FLOAT, "PressureDelay" ),

	// Inputs
	DEFINE_INPUTFUNC( FIELD_VOID, "Break", InputBreak ),
	DEFINE_INPUTFUNC( FIELD_INTEGER, "SetHealth", InputSetHealth ),
	DEFINE_INPUTFUNC( FIELD_INTEGER, "AddHealth", InputAddHealth ),
	DEFINE_INPUTFUNC( FIELD_INTEGER, "RemoveHealth", InputRemoveHealth ),
	DEFINE_INPUT( m_impactEnergyScale, FIELD_FLOAT, "physdamagescale" ),


	// Outputs
	DEFINE_OUTPUT( m_OnBreak, "OnBreak" ),
	DEFINE_OUTPUT( m_OnHealthChanged, "OnHealthChanged" ),

	// Function Pointers
	DEFINE_FUNCTION( FadeOut ),
	DEFINE_FUNCTION( BreakThink ),
	DEFINE_FUNCTION( BreakablePropTouch ),

END_DATADESC()

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBreakableProp::Spawn()
{
	// Initialize damage modifiers. Must be done before baseclass spawn.
	m_flDmgModBullet = 1.0;
	m_flDmgModClub = 1.0;
	m_flDmgModExplosive = 1.0;

	BaseClass::Spawn();

	// Setup takedamage based upon the health we parsed earlier
	if ( m_iHealth == 0 )
	{
		m_takedamage = DAMAGE_EVENTS_ONLY;
	}
	else
	{
		m_takedamage = DAMAGE_YES;
	}

	m_createTick = gpGlobals->tickcount;
	if ( m_impactEnergyScale == 0 )
	{
		m_impactEnergyScale = 0.1f;
	}

	m_hBreaker = NULL;
	SetTouch( &CBreakableProp::BreakablePropTouch );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pOther - 
//-----------------------------------------------------------------------------
void CBreakableProp::BreakablePropTouch( CBaseEntity *pOther )
{
	if ( HasSpawnFlags( SF_PHYSPROP_TOUCH ) )
	{
		// can be broken when run into 
		float flDamage = pOther->GetAbsVelocity().Length() * 0.01;

		if ( flDamage >= m_iHealth )
		{
			// Make sure we can take damage
			m_takedamage = DAMAGE_YES;
			OnTakeDamage( CTakeDamageInfo( pOther, pOther, flDamage, DMG_CRUSH ) );

			// do a little damage to player if we broke glass or computer
			CTakeDamageInfo info( pOther, pOther, flDamage/4, DMG_SLASH );
			CalculateMeleeDamageForce( &info, (pOther->GetAbsOrigin() - GetAbsOrigin()), GetAbsOrigin() );
			pOther->TakeDamage( info );
		}
	}

	if ( HasSpawnFlags( SF_PHYSPROP_PRESSURE ) && pOther->GetGroundEntity() == this )
	{
		// can be broken when stood upon
		// play creaking sound here.
		// DamageSound();

		m_hBreaker = pOther;

		if ( m_pfnThink != (void (CBaseEntity::*)())&CBreakableProp::BreakThink )
		{
			SetThink( &CBreakableProp::BreakThink );
			//SetTouch( NULL );
		
			// Add optional delay 
			SetNextThink( gpGlobals->curtime + m_flPressureDelay );
		}

	}
}

//-----------------------------------------------------------------------------
// UNDONE: Time stamp the object's creation so that an explosion or something doesn't break the parent object
// and then break the children who spawn afterward ?
// Explosions should use entities in box before they start to do damage.  Make sure nothing traverses the list
// in a way that would hose this.
int CBreakableProp::OnTakeDamage( const CTakeDamageInfo &inputInfo )
{
	CTakeDamageInfo info = inputInfo;

	// If attacker can't do at least the min required damage to us, don't take any damage from them
 	if ( info.GetDamage() < m_iMinHealthDmg )
		return 0;

	if (!PassesDamageFilter( info.GetAttacker() ))
	{
		return 1;
	}

	float flPropDamage = GetBreakableDamage( info, this );
	info.SetDamage( flPropDamage );

	// UNDONE: Do this?
#if 0
	// Make a shard noise each time func breakable is hit.
	// Don't play shard noise if being burned.
	// Don't play shard noise if cbreakable actually died.
	if ( ( bitsDamageType & DMG_BURN ) == false )
	{
		DamageSound();
	}
#endif

	// don't take damage on the same frame you were created 
	// (avoids a set of explosions progressively vaporizing a compound breakable)
	if ( m_createTick == (unsigned int)gpGlobals->tickcount )
	{
		int saveFlags = m_takedamage;
		m_takedamage = DAMAGE_EVENTS_ONLY;
		int ret = BaseClass::OnTakeDamage( info );
		m_takedamage = saveFlags;

		return ret;
	}

	int ret = BaseClass::OnTakeDamage( info );
	m_OnHealthChanged.Set( m_iHealth, info.GetAttacker(), this );

	return ret;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBreakableProp::Event_Killed( const CTakeDamageInfo &info )
{
	IPhysicsObject *pPhysics = VPhysicsGetObject();
	if ( pPhysics && !pPhysics->IsMoveable() )
	{
		pPhysics->EnableMotion( true );
		VPhysicsTakeDamage( info );
	}
	Break( info.GetInflictor(), &info );
	BaseClass::Event_Killed( info );
}

//-----------------------------------------------------------------------------
// Purpose: Input handler for breaking the breakable immediately.
//-----------------------------------------------------------------------------
void CBreakableProp::InputBreak( inputdata_t &inputdata )
{
	Break( inputdata.pActivator, NULL );
}


//-----------------------------------------------------------------------------
// Purpose: Input handler for adding to the breakable's health.
// Input  : Integer health points to add.
//-----------------------------------------------------------------------------
void CBreakableProp::InputAddHealth( inputdata_t &inputdata )
{
	m_iHealth += inputdata.value.Int();
	m_OnHealthChanged.Set( m_iHealth, inputdata.pActivator, this );

	if ( m_iHealth <= 0 )
	{
		Break( inputdata.pActivator, NULL );
	}
}

//-----------------------------------------------------------------------------
// Purpose: Input handler for removing health from the breakable.
// Input  : Integer health points to remove.
//-----------------------------------------------------------------------------
void CBreakableProp::InputRemoveHealth( inputdata_t &inputdata )
{
	m_iHealth -= inputdata.value.Int();
	m_OnHealthChanged.Set( m_iHealth, inputdata.pActivator, this );

	if ( m_iHealth <= 0 )
	{
		Break( inputdata.pActivator, NULL );
	}
}


//-----------------------------------------------------------------------------
// Purpose: Input handler for setting the breakable's health.
//-----------------------------------------------------------------------------
void CBreakableProp::InputSetHealth( inputdata_t &inputdata )
{
	m_iHealth = inputdata.value.Int();
	m_OnHealthChanged.Set( m_iHealth, inputdata.pActivator, this );

	if ( m_iHealth <= 0 )
	{
		Break( inputdata.pActivator, NULL );
	}
}

void CBreakableProp::StartFadeOut( float delay )
{
	SetThink( &CBreakableProp::FadeOut );
	SetNextThink( gpGlobals->curtime + delay );
	SetRenderColorA( 255 );
	m_nRenderMode = kRenderNormal;
}

void CBreakableProp::FadeOut( void )
{
	float dt = gpGlobals->frametime;
	if ( dt > 0.1f )
	{
		dt = 0.1f;
	}
	m_nRenderMode = kRenderTransTexture;
	int speed = max(1,256*dt); // fade out over 1 second
	SetRenderColorA( UTIL_Approach( 0, m_clrRender->a, speed ) );
	NetworkStateChanged();

	if ( m_clrRender->a == 0 )
	{
		UTIL_Remove(this);
	}
	else
	{
		SetNextThink( gpGlobals->curtime );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBreakableProp::BreakThink( void )
{
	Break( m_hBreaker, NULL );
}

void CBreakableProp::Precache()
{
	PropBreakablePrecacheAll( GetModelName() );
	BaseClass::Precache();
}

void CBreakableProp::Break( CBaseEntity *pBreaker, const CTakeDamageInfo *pDamageInfo )
{
	m_takedamage = DAMAGE_NO;
	m_OnBreak.FireOutput( pBreaker, this );

	Vector velocity;
	AngularImpulse angVelocity;
	IPhysicsObject *pPhysics = VPhysicsGetObject();
	Vector origin;
	QAngle angles;
	AddSolidFlags( FSOLID_NOT_SOLID );
	if ( pPhysics )
	{
		pPhysics->GetVelocity( &velocity, &angVelocity );
		pPhysics->GetPosition( &origin, &angles );
		pPhysics->RecheckCollisionFilter();
	}
	else
	{
		velocity = GetAbsVelocity();
		QAngleToAngularImpulse( GetLocalAngularVelocity(), angVelocity );
		origin = GetAbsOrigin();
		angles = GetAbsAngles();
	}
	UTIL_Relink(this);

	if ( m_explodeDamage > 0 || m_explodeRadius > 0 )
	{
		ExplosionCreate( GetAbsOrigin(), GetAbsAngles(), NULL, m_explodeDamage, m_explodeRadius, true );
	}

	breakablepropparams_t params( GetAbsOrigin(), GetAbsAngles(), velocity, angVelocity );
	params.impactEnergyScale = m_impactEnergyScale;
	params.defCollisionGroup = GetCollisionGroup();
	if ( params.defCollisionGroup == COLLISION_GROUP_NONE )
	{
		// don't automatically make anything COLLISION_GROUP_NONE or it will
		// collide with debris being ejected by breaking
		params.defCollisionGroup = COLLISION_GROUP_INTERACTIVE;
	}

	// no damage/damage force? set a burst of 100 for some movement
	params.defBurstScale = pDamageInfo ? 0 : 100;
	PropBreakableCreateAll( GetModelIndex(), pPhysics, params );

	UTIL_Remove(this);
}

//=============================================================================================================
// DYNAMIC PROPS
//=============================================================================================================
LINK_ENTITY_TO_CLASS( dynamic_prop, CDynamicProp );
LINK_ENTITY_TO_CLASS( prop_dynamic, CDynamicProp );	
LINK_ENTITY_TO_CLASS( prop_dynamic_override, CDynamicProp );	

BEGIN_DATADESC( CDynamicProp )

	// Fields
	DEFINE_KEYFIELD( m_bRandomAnimator, FIELD_BOOLEAN, "RandomAnimation"),	
	DEFINE_FIELD(	 m_flNextRandAnim, FIELD_TIME ),
	DEFINE_KEYFIELD( m_flMinRandAnimTime, FIELD_FLOAT, "MinAnimTime"),
	DEFINE_KEYFIELD( m_flMaxRandAnimTime, FIELD_FLOAT, "MaxAnimTime"),
		
	// Inputs
	DEFINE_INPUTFUNC( FIELD_STRING,	"SetAnimation",	InputSetAnimation ),
	DEFINE_INPUTFUNC( FIELD_VOID,		"TurnOn",		InputTurnOn ),
	DEFINE_INPUTFUNC( FIELD_VOID,		"TurnOff",		InputTurnOff ),

	// Outputs
	DEFINE_OUTPUT( m_pOutputAnimBegun, "OnAnimationBegun" ),
	DEFINE_OUTPUT( m_pOutputAnimOver, "OnAnimationDone" ),

	// Function Pointers
	DEFINE_FUNCTION( AnimThink ),

END_DATADESC()

IMPLEMENT_SERVERCLASS_ST(CDynamicProp, DT_DynamicProp)
END_SEND_TABLE()


CDynamicProp::CDynamicProp()
{
	UseClientSideAnimation();
}


//------------------------------------------------------------------------------
// Purpose:
//------------------------------------------------------------------------------
void CDynamicProp::Spawn( )
{
	// Condense classname's to one, except for "prop_dynamic_override"
	if ( FClassnameIs( this, "dynamic_prop" ) )
	{
		SetClassname( "prop_dynamic" );
	}

	BaseClass::Spawn();

	if ( IsMarkedForDeletion() )
		return;

	// Now condense all classnames to one
	SetClassname("prop_dynamic");

	AddFlag( FL_STATICPROP );
	Relink();
	NetworkStateManualMode( true );

	if ( m_bRandomAnimator )
	{
		RemoveFlag( FL_STATICPROP );
		SetThink( &CDynamicProp::AnimThink );
		m_flNextRandAnim = gpGlobals->curtime + random->RandomFloat( m_flMinRandAnimTime, m_flMaxRandAnimTime );
		SetNextThink( gpGlobals->curtime + m_flNextRandAnim + 0.1 );
	}

	CreateVPhysics();
}


//------------------------------------------------------------------------------
// Purpose:
//------------------------------------------------------------------------------
bool CDynamicProp::CreateVPhysics( void )
{
	if ( GetSolid() != SOLID_NONE )
	{
		VPhysicsInitStatic();
	}
	return true;
}


//------------------------------------------------------------------------------
// Purpose:
//------------------------------------------------------------------------------
void CDynamicProp::AnimThink( void )
{
	if ( m_bRandomAnimator && m_flNextRandAnim < gpGlobals->curtime )
	{
		ResetSequence( SelectWeightedSequence( ACT_IDLE ) );
		ResetClientsideFrame();

		// Fire output
		m_pOutputAnimBegun.FireOutput( NULL,this );

		m_flNextRandAnim = gpGlobals->curtime + random->RandomFloat( m_flMinRandAnimTime, m_flMaxRandAnimTime );
	}

	StudioFrameAdvance();
	DispatchAnimEvents(this);

	if ( IsSequenceFinished() && !SequenceLoops() )
	{
		// Fire output
		m_pOutputAnimOver.FireOutput(NULL,this);

		// If I'm a random animator, think again when it's time to change sequence
		if ( m_bRandomAnimator )
		{
			SetNextThink( gpGlobals->curtime + m_flNextRandAnim + 0.1 );
		}
	}
	else
	{
		SetNextThink( gpGlobals->curtime + 0.1f );
	}
}


//------------------------------------------------------------------------------
// Purpose:
//------------------------------------------------------------------------------
void CDynamicProp::InputSetAnimation( inputdata_t &inputdata )
{
	int nSequence = LookupSequence ( inputdata.value.String() );

	// Set to the desired anim, or default anim if the desired is not present
	if ( nSequence > ACTIVITY_NOT_AVAILABLE )
	{
		PropSetSequence( nSequence );

		// Fire output
		m_pOutputAnimBegun.FireOutput( NULL,this );
	}
	else
	{
		// Not available try to get default anim
		Msg( "Dynamic prop no sequence named:%s\n", inputdata.value.String() );
		SetSequence( 0 );
	}
}


//-----------------------------------------------------------------------------
// Purpose: Sets the sequence and starts thinking.
// Input  : nSequence - 
//-----------------------------------------------------------------------------
void CDynamicProp::PropSetSequence( int nSequence )
{
	m_flCycle = 0;
	ResetSequence( nSequence );
	ResetClientsideFrame();

	RemoveFlag( FL_STATICPROP );
	SetThink( &CDynamicProp::AnimThink );
	SetNextThink( gpGlobals->curtime + 0.1f );
}


// NOTE: To avoid risk, currently these do nothing about collisions, only visually on/off
void CDynamicProp::InputTurnOn( inputdata_t &inputdata )
{
	m_fEffects &= ~EF_NODRAW;
}

void CDynamicProp::InputTurnOff( inputdata_t &inputdata )
{
	m_fEffects |= EF_NODRAW;
}

//-----------------------------------------------------------------------------
// Purpose: Ornamental prop that follows a studio
//-----------------------------------------------------------------------------
class COrnamentProp : public CDynamicProp
{
	DECLARE_CLASS( COrnamentProp, CDynamicProp );
public:
	DECLARE_DATADESC();

	void Spawn();
	void Activate();
	void AttachTo( const char *pAttachEntity, CBaseEntity *pActivator );
	void DetachFromOwner();

	// Input handlers
	void InputSetAttached( inputdata_t &inputdata );
	void InputDetach( inputdata_t &inputdata );

private:
	string_t	m_initialOwner;
};

LINK_ENTITY_TO_CLASS( prop_dynamic_ornament, COrnamentProp );	

BEGIN_DATADESC( COrnamentProp )

	DEFINE_KEYFIELD( m_initialOwner, FIELD_STRING, "InitialOwner" ),
	// Inputs
	DEFINE_INPUTFUNC( FIELD_STRING,	"SetAttached",	InputSetAttached ),
	DEFINE_INPUTFUNC( FIELD_VOID,		"Detach",	InputDetach ),

END_DATADESC()

void COrnamentProp::Spawn()
{
	BaseClass::Spawn();
	DetachFromOwner();
}

void COrnamentProp::DetachFromOwner()
{
	SetOwnerEntity( NULL );
	AddSolidFlags( FSOLID_NOT_SOLID );
	SetMoveType( MOVETYPE_NONE );
	m_fEffects |= EF_NODRAW;
	UTIL_Relink(this);
}

void COrnamentProp::Activate()
{
	BaseClass::Activate();
	
	if ( m_initialOwner != NULL_STRING )
	{
		AttachTo( STRING(m_initialOwner), this );
	}
}

void COrnamentProp::InputSetAttached( inputdata_t &inputdata )
{
	AttachTo( inputdata.value.String(), inputdata.pActivator );
}

void COrnamentProp::AttachTo( const char *pAttachName, CBaseEntity *pActivator )
{
	// find and notify the new parent
	CBaseEntity *pAttach = gEntList.FindEntityByName( NULL, pAttachName, pActivator );
	if ( pAttach )
	{
		m_fEffects &= ~EF_NODRAW;
		FollowEntity( pAttach );
	}
}

void COrnamentProp::InputDetach( inputdata_t &inputdata )
{
	DetachFromOwner();
}


//=============================================================================================================
// PHYSICS PROPS
//=============================================================================================================
LINK_ENTITY_TO_CLASS( physics_prop, CPhysicsProp );
LINK_ENTITY_TO_CLASS( prop_physics_multiplayer, CPhysicsProp );
LINK_ENTITY_TO_CLASS( prop_physics_respawnable, CPhysicsProp );	
LINK_ENTITY_TO_CLASS( prop_physics, CPhysicsProp );	
LINK_ENTITY_TO_CLASS( prop_physics_override, CPhysicsProp );	

BEGIN_DATADESC( CPhysicsProp )

	DEFINE_INPUTFUNC( FIELD_VOID, "EnableMotion", InputEnableMotion ),
	DEFINE_INPUTFUNC( FIELD_VOID, "DisableMotion", InputDisableMotion ),
	DEFINE_INPUTFUNC( FIELD_VOID, "Wake", InputWake ),
	DEFINE_INPUTFUNC( FIELD_VOID, "Sleep", InputSleep ),
	DEFINE_INPUT( m_fadeMinDist, FIELD_FLOAT, "fademindist" ),
	DEFINE_INPUT( m_fadeMaxDist, FIELD_FLOAT, "fademaxdist" ),

	DEFINE_KEYFIELD( m_massScale, FIELD_FLOAT, "massscale" ),
	DEFINE_KEYFIELD( m_inertiaScale, FIELD_FLOAT, "inertiascale" ),
	DEFINE_KEYFIELD( m_damageType, FIELD_INTEGER, "Damagetype" ),
	DEFINE_KEYFIELD( m_iszOverrideScript, FIELD_STRING, "overridescript" ),

	DEFINE_KEYFIELD( m_damageToEnableMotion, FIELD_INTEGER, "damagetoenablemotion" ), 
	DEFINE_KEYFIELD( m_flForceToEnableMotion, FIELD_FLOAT, "forcetoenablemotion" ), 
	DEFINE_OUTPUT( m_MotionEnabled, "OnMotionEnabled" ),

END_DATADESC()

IMPLEMENT_SERVERCLASS_ST( CPhysicsProp, DT_PhysicsProp )

	SendPropFloat( SENDINFO( m_fadeMinDist ), 0, SPROP_NOSCALE ),
	SendPropFloat( SENDINFO( m_fadeMaxDist ), 0, SPROP_NOSCALE ),

END_SEND_TABLE()


//-----------------------------------------------------------------------------
// Purpose: Create a physics object for this prop
//-----------------------------------------------------------------------------
void CPhysicsProp::Spawn( )
{
	// Condense classname's to one, except for "prop_physics_override"
	if ( FClassnameIs( this, "physics_prop" ) )
	{
		SetClassname( "prop_physics" );
	}

	BaseClass::Spawn();

	if ( IsMarkedForDeletion() )
		return;

	// Now condense all classnames to one
	SetClassname( "prop_physics" );

	if ( HasSpawnFlags( SF_PHYSPROP_DEBRIS ) )
	{
		SetCollisionGroup( COLLISION_GROUP_DEBRIS );
	}

	//m_fadeMinDist = 0; m_fadeMaxDist = 1300;

	CreateVPhysics();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
bool CPhysicsProp::CreateVPhysics()
{
	// Create the object in the physics system
	bool asleep = HasSpawnFlags( SF_PHYSPROP_START_ASLEEP ) ? true : false;

	solid_t tmpSolid;
	PhysModelParseSolid( tmpSolid, this, GetModelIndex() );
	
	if ( m_massScale > 0 )
	{
		float mass = tmpSolid.params.mass * m_massScale;
		mass = clamp( mass, 0.5, 1e6 );
		tmpSolid.params.mass = mass;
	}

	if ( m_inertiaScale > 0 )
	{
		tmpSolid.params.inertia *= m_inertiaScale;
		if ( tmpSolid.params.inertia < 0.5 )
			tmpSolid.params.inertia = 0.5;
	}

	PhysSolidOverride( tmpSolid, m_iszOverrideScript );

	IPhysicsObject *pPhysicsObject = VPhysicsInitNormal( SOLID_VPHYSICS, 0, asleep, &tmpSolid );

	if ( !pPhysicsObject )
	{
		SetSolid( SOLID_NONE );
		SetMoveType( MOVETYPE_NONE );
		Warning("ERROR!: Can't create physics object for %s\n", STRING( GetModelName() ) );
		// update engine data
		Relink();
	}
	else
	{
		if ( m_damageType == 1 )
		{
			PhysSetGameFlags( pPhysicsObject, FVPHYSICS_DMG_SLICE );
		}
		if ( HasSpawnFlags( SF_PHYSPROP_MOTIONDISABLED ) || m_damageToEnableMotion > 0 || m_flForceToEnableMotion > 0 )
		{
			pPhysicsObject->EnableMotion( false );
		}
	}
	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Input handler to start the physics prop simulating.
//-----------------------------------------------------------------------------
void CPhysicsProp::InputWake( inputdata_t &inputdata )
{
	IPhysicsObject *pPhysicsObject = VPhysicsGetObject();
	if ( pPhysicsObject != NULL )
	{
		pPhysicsObject->Wake();
	}
}

//-----------------------------------------------------------------------------
// Purpose: Input handler to stop the physics prop simulating.
//-----------------------------------------------------------------------------
void CPhysicsProp::InputSleep( inputdata_t &inputdata )
{
	IPhysicsObject *pPhysicsObject = VPhysicsGetObject();
	if ( pPhysicsObject != NULL )
	{
		pPhysicsObject->Sleep();
	}
}

//-----------------------------------------------------------------------------
// Purpose: Enable physics motion and collision response (on by default)
//-----------------------------------------------------------------------------
void CPhysicsProp::InputEnableMotion( inputdata_t &inputdata )
{
	EnableMotion();
}

//-----------------------------------------------------------------------------
// Purpose: Disable any physics motion or collision response
//-----------------------------------------------------------------------------
void CPhysicsProp::InputDisableMotion( inputdata_t &inputdata )
{
	IPhysicsObject *pPhysicsObject = VPhysicsGetObject();
	if ( pPhysicsObject != NULL )
	{
		pPhysicsObject->EnableMotion( false );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CPhysicsProp::EnableMotion( void )
{
	IPhysicsObject *pPhysicsObject = VPhysicsGetObject();
	if ( pPhysicsObject )
	{
		pPhysicsObject->Wake();
		pPhysicsObject->EnableMotion( true );

		m_MotionEnabled.FireOutput( this, this, 0 );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CPhysicsProp::OnPhysGunPickup( CBasePlayer *pPhysGunUser )
{
	IPhysicsObject *pPhysicsObject = VPhysicsGetObject();
	if ( pPhysicsObject && !pPhysicsObject->IsMoveable() )
	{
		EnableMotion();
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CPhysicsProp::VPhysicsCollision( int index, gamevcollisionevent_t *pEvent )
{
	BaseClass::VPhysicsCollision( index, pEvent );

	// If we have a force to enable motion, and we're still disabled, check to see if this should enable us
	if ( m_flForceToEnableMotion )
	{
		// Large enough to enable motion?
		float flForce = pEvent->collisionSpeed * pEvent->pObjects[!index]->GetMass();
		if ( flForce >= m_flForceToEnableMotion )
		{
			EnableMotion();
			m_flForceToEnableMotion = 0;
		}
	}

	if ( !HasSpawnFlags( SF_PHYSPROP_DONT_TAKE_PHYSICS_DAMAGE ) )
	{
		int damageType = 0;
		float damage = CalculateDefaultPhysicsDamage( index, pEvent, m_impactEnergyScale, true, damageType );
		if ( damage > 0 )
		{
			CBaseEntity *pHitEntity = pEvent->pEntities[!index];
			if ( !pHitEntity )
			{
				// hit world
				pHitEntity = GetContainingEntity( INDEXENT(0) );
			}
			Vector damagePos;
			pEvent->pInternalData->GetContactPoint( damagePos );
			Vector damageForce = pEvent->postVelocity[index] * pEvent->pObjects[index]->GetMass();
			if ( damageForce == vec3_origin )
			{
				// This can happen if this entity is motion disabled, and can't move.
				// Use the velocity of the entity that hit us instead.
				damageForce = pEvent->postVelocity[!index] * pEvent->pObjects[!index]->GetMass();
			}

			PhysCallbackDamage( this, CTakeDamageInfo( pHitEntity, pHitEntity, damageForce, damagePos, damage, damageType ) );
		}
	}
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int CPhysicsProp::OnTakeDamage( const CTakeDamageInfo &info )
{
	// note: if motion is disabled, OnTakeDamage can't apply physics force
	int ret = BaseClass::OnTakeDamage( info );
	
	// If we have a force to enable motion, and we're still disabled, check to see if this should enable us
	if ( m_flForceToEnableMotion )
	{
		// Large enough to enable motion?
		float flForce = info.GetDamageForce().Length();
		if ( flForce >= m_flForceToEnableMotion )
		{
			IPhysicsObject *pPhysicsObject = VPhysicsGetObject();
			if ( pPhysicsObject )
			{
				pPhysicsObject->Wake();
				pPhysicsObject->EnableMotion( true );
			}
			m_flForceToEnableMotion = 0;
		}
	}

	// Check our health against the threshold:
	if( m_damageToEnableMotion > 0 && GetHealth() < m_damageToEnableMotion )
	{
		// only do this once
		m_damageToEnableMotion = 0;

		EnableMotion();
		VPhysicsTakeDamage( info );
	}
	
	return ret;
}

//-----------------------------------------------------------------------------
// Purpose: Draw any debug text overlays
// Output : Current text offset from the top
//-----------------------------------------------------------------------------
int CPhysicsProp::DrawDebugTextOverlays(void) 
{
	int text_offset = BaseClass::DrawDebugTextOverlays();

	if (m_debugOverlays & OVERLAY_TEXT_BIT) 
	{
		if (VPhysicsGetObject())
		{
			char tempstr[512];
			Q_snprintf(tempstr, sizeof(tempstr),"Mass: %.2f kg / %.2f lb (%s)", VPhysicsGetObject()->GetMass(), kg2lbs(VPhysicsGetObject()->GetMass()), GetMassEquivalent(VPhysicsGetObject()->GetMass()));
			NDebugOverlay::EntityText(entindex(), text_offset, tempstr, 0);
			text_offset++;

			if ( !VPhysicsGetObject()->IsMoveable() )
			{
				Q_snprintf(tempstr, sizeof(tempstr),"Motion Disabled" );
				NDebugOverlay::EntityText(entindex(), text_offset, tempstr, 0);
				text_offset++;
			}

			if ( m_iszBasePropData != NULL_STRING )
			{
				Q_snprintf(tempstr, sizeof(tempstr),"Base PropData: %s", STRING(m_iszBasePropData) );
				NDebugOverlay::EntityText(entindex(), text_offset, tempstr, 0);
				text_offset++;
			}
		}
	}

	return text_offset;
}


//-----------------------------------------------------------------------------
// breakable prop functions
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
// list of models to break into
struct breakmodel_t
{
	Vector		offset;
	char		modelName[1024];
	float		fadeTime;
	float		health;
	float		burstScale;
	int			collisionGroup;
};

class CBreakParser : public IVPhysicsKeyHandler
{
public:
	CBreakParser( float defaultBurstScale, int defaultCollisionGroup ) 
		: m_defaultBurstScale(defaultBurstScale), m_defaultCollisionGroup(defaultCollisionGroup) {}

	virtual void ParseKeyValue( void *pData, const char *pKey, const char *pValue )
	{
		breakmodel_t *pModel = (breakmodel_t *)pData;
		if ( !strcmp( pKey, "model" ) )
		{
			char tmp[1024];
			Q_strncpy( tmp, pValue, sizeof(pModel->modelName) );
			if ( strnicmp( tmp, "models/", 7 ) )
			{
				Q_strncpy( pModel->modelName, "models/" ,sizeof(pModel->modelName));
				Q_strncat( pModel->modelName, tmp, sizeof(pModel->modelName), COPY_ALL_CHARACTERS );
			}
			else
			{
				Q_strncpy( pModel->modelName, tmp ,sizeof(pModel->modelName));
			}
			int len = strlen(pModel->modelName);
			if ( len < 4 || strcmpi( pModel->modelName + (len-4), ".mdl" ) )
			{
				Q_strncat( pModel->modelName, ".mdl", sizeof(pModel->modelName), COPY_ALL_CHARACTERS );
			}
		}
		else if ( !strcmpi( pKey, "offset" ) )
		{
			UTIL_StringToVector( pModel->offset.Base(), pValue );
		}
		else if ( !strcmpi( pKey, "health" ) )
		{
			pModel->health = atof(pValue);
		}
		else if ( !strcmpi( pKey, "fadetime" ) )
		{
			pModel->fadeTime = atof(pValue);
			if ( !m_wroteCollisionGroup )
			{
				pModel->collisionGroup = COLLISION_GROUP_DEBRIS;
			}
		}
		else if ( !strcmpi( pKey, "debris" ) )
		{
			pModel->collisionGroup = atoi(pValue) > 0 ? COLLISION_GROUP_DEBRIS : COLLISION_GROUP_INTERACTIVE;
			m_wroteCollisionGroup = true;
		}
		else if ( !strcmpi( pKey, "burst" ) )
		{
			pModel->burstScale = atof( pValue );
		}
	}
	virtual void SetDefaults( void *pData ) 
	{
		breakmodel_t *pModel = (breakmodel_t *)pData;
		pModel->modelName[0] = 0;
		pModel->offset = vec3_origin;
		pModel->health = 1;
		pModel->fadeTime = 0;
		pModel->burstScale = m_defaultBurstScale;
		pModel->collisionGroup = m_defaultCollisionGroup;
		m_wroteCollisionGroup = false;
	}

private:
	int		m_defaultCollisionGroup;
	float	m_defaultBurstScale;
	bool	m_wroteCollisionGroup;
};

static void BreakModelList( CUtlVector<breakmodel_t> &list, int modelindex, float defBurstScale, int defCollisionGroup )
{
	vcollide_t *pCollide = modelinfo->GetVCollide( modelindex );
	if ( !pCollide )
		return;

	IVPhysicsKeyParser *pParse = physcollision->VPhysicsKeyParserCreate( pCollide->pKeyValues );
	while ( !pParse->Finished() )
	{
		CBreakParser breakParser( defBurstScale, defCollisionGroup );
		
		const char *pBlock = pParse->GetCurrentBlockName();
		if ( !strcmpi( pBlock, "break" ) )
		{
			int index = list.AddToTail();
			breakmodel_t &breakModel = list[index];
			pParse->ParseCustom( &breakModel, &breakParser );
		}
		else
		{
			pParse->SkipBlock();
		}
	}
	physcollision->VPhysicsKeyParserDestroy( pParse );
}

static void BreakModelCreateSingle( CBaseEntity *pOwner, breakmodel_t *pModel, const Vector &position, 
	const QAngle &angles, const Vector &velocity, const AngularImpulse &angVelocity, int nSkin, const breakablepropparams_t &params )
{
	CBreakableProp *pEntity = (CBreakableProp *)CBaseEntity::CreateNoSpawn( "prop_physics", position, angles, pOwner );
	if ( pEntity )
	{
		pEntity->m_nSkin = nSkin;
		pEntity->SetModelName( AllocPooledString( pModel->modelName ) );
		pEntity->SetModel( STRING(pEntity->GetModelName()) );
		pEntity->m_iHealth = pModel->health;
		// UNDONE: Allow .qc to override spawnflags for child pieces
		if ( pOwner )
		{
			pEntity->AddSpawnFlags( pOwner->GetSpawnFlags() );

			// We never want to be motion disabled
			pEntity->RemoveSpawnFlags( SF_PHYSPROP_MOTIONDISABLED );
		}
		pEntity->m_impactEnergyScale = params.impactEnergyScale;	// assume the same material
		pEntity->SetCollisionGroup( pModel->collisionGroup );
		
		// Inherit the base object's damage modifiers
		CBreakableProp *pBreakableOwner = dynamic_cast<CBreakableProp *>(pOwner);
		if ( pBreakableOwner )
		{
			pEntity->SetDmgModBullet( pBreakableOwner->GetDmgModBullet() );
			pEntity->SetDmgModClub( pBreakableOwner->GetDmgModClub() );
			pEntity->SetDmgModExplosive( pBreakableOwner->GetDmgModExplosive() );
		}
		pEntity->Spawn();
		if ( pModel->fadeTime )
		{
			pEntity->StartFadeOut( pModel->fadeTime );
		}

		IPhysicsObject *pPhysics = pEntity->VPhysicsGetObject();
		if ( pPhysics )
		{
			pPhysics->SetVelocity( &velocity, &angVelocity );
		}
		else
		{
			// failed to create a physics object
			UTIL_Remove( pEntity );
		}
	}
}

class CBreakModelsPrecached : public CAutoGameSystem
{
public:
	CBreakModelsPrecached()
	{
		m_modelList.SetLessFunc( BreakLessFunc );
	}

	static bool BreakLessFunc( const string_t &lhs, const string_t &rhs )
	{
		return ( lhs.ToCStr() < rhs.ToCStr() );
	}

	bool IsInList( string_t modelName )
	{
		if ( m_modelList.Find(modelName) != m_modelList.InvalidIndex() )
			return true;

		return false;
	}

	void AddToList( string_t modelName )
	{
		m_modelList.Insert( modelName );
	}

	void LevelShutdownPostEntity()
	{
		m_modelList.RemoveAll();
	}

private:
	CUtlRBTree<string_t>	m_modelList;
};

static CBreakModelsPrecached g_BreakModelsPrecached;

void PropBreakablePrecacheAll( string_t modelName )
{
	int iBreakables = 0;
	if ( g_BreakModelsPrecached.IsInList( modelName ) )
		return;

	if ( modelName == NULL_STRING )
	{
		Msg("Trying to precache breakable prop, but has no model name\n");
		return;
	}

	int modelIndex = engine->PrecacheModel( STRING(modelName) );

	CUtlVector<breakmodel_t> list;

	BreakModelList( list, modelIndex, COLLISION_GROUP_NONE, 0 );
	iBreakables = list.Count();

	g_BreakModelsPrecached.AddToList( modelName );

	for ( int i = 0; i < iBreakables; i++ )
	{
		string_t breakModelName = AllocPooledString(list[i].modelName);
		PropBreakablePrecacheAll( breakModelName );
	}
}

void PropBreakableCreateAll( int modelindex, IPhysicsObject *pPhysics, const breakablepropparams_t &params )
{
	vcollide_t *pCollide = modelinfo->GetVCollide( modelindex );
	if ( !pCollide )
		return;

	int nSkin = 0;
	CBaseEntity *pOwnerEntity = NULL; 
	if ( pPhysics )
	{
		pOwnerEntity = static_cast<CBaseEntity *>(pPhysics->GetGameData());
		CBaseAnimating *pAnim = dynamic_cast<CBaseAnimating*>(pOwnerEntity);
		if ( pAnim )
		{
			nSkin = pAnim->m_nSkin;
		}

	}
	matrix3x4_t localToWorld;

	studiohdr_t *pStudioHdr = NULL;
	const model_t *model = modelinfo->GetModel( modelindex );
	if ( model )
	{
		pStudioHdr = static_cast< studiohdr_t * >( modelinfo->GetModelExtraData( model ) );
	}

	Vector parentOrigin = vec3_origin;
	int parentAttachment = 	Studio_FindAttachment( pStudioHdr, "placementOrigin" ) + 1;
	if ( parentAttachment > 0 )
	{
		GetAttachmentLocalSpace( pStudioHdr, parentAttachment, localToWorld );
		MatrixGetColumn( localToWorld, 3, parentOrigin );
	}
	else
	{
		AngleMatrix( vec3_angle, localToWorld );
	}
	
	matrix3x4_t matrix;
	AngleMatrix( params.angles, params.origin, matrix );
	CUtlVector<breakmodel_t> list;

	BreakModelList( list, modelindex, params.defBurstScale, params.defCollisionGroup );

	for ( int i = 0; i < list.Count(); i++ )
	{
		int modelIndex = modelinfo->GetModelIndex( list[i].modelName );
		if ( modelIndex <= 0 )
			continue;
		
		studiohdr_t *pStudioHdr = NULL;
		const model_t *model = modelinfo->GetModel( modelIndex );
		if ( model )
		{
			pStudioHdr = static_cast< studiohdr_t * >( modelinfo->GetModelExtraData( model ) );
		}

		int placementIndex = Studio_FindAttachment( pStudioHdr, "placementOrigin" ) + 1;
		Vector placementOrigin = parentOrigin;
		if ( placementIndex > 0 )
		{
			GetAttachmentLocalSpace( pStudioHdr, placementIndex, localToWorld );
			MatrixGetColumn( localToWorld, 3, placementOrigin );
			placementOrigin -= parentOrigin;
		}

		Vector position;
		VectorTransform( list[i].offset - placementOrigin, matrix, position );
		Vector objectVelocity = params.velocity;

		if (pPhysics)
		{
			pPhysics->GetVelocityAtPoint( position, objectVelocity );
		}
		if( list[i].burstScale != 0.0 )
		{
			// If burst scale is set, this piece should 'burst' away from
			// the origin in addition to travelling in the wished velocity.
			Vector vecBurstDir;

			vecBurstDir = position - params.origin;

			VectorNormalize( vecBurstDir );

			objectVelocity += vecBurstDir * list[i].burstScale;
		}

		int nActualSkin = nSkin;
		if ( nActualSkin > pStudioHdr->numskinfamilies )
			nActualSkin = 0;

		BreakModelCreateSingle( pOwnerEntity, &list[i], position, params.angles, objectVelocity, params.angularVelocity, nActualSkin, params );
	}
}

void PropBreakableCreateAll( int modelindex, IPhysicsObject *pPhysics, const Vector &origin, const QAngle &angles, const Vector &velocity, const AngularImpulse &angularVelocity, float impactEnergyScale, float defBurstScale, int defCollisionGroup )
{
	breakablepropparams_t params( origin, angles, velocity, angularVelocity );
	params.impactEnergyScale = impactEnergyScale;
	params.defBurstScale = defBurstScale;
	params.defCollisionGroup = defCollisionGroup;
	PropBreakableCreateAll( modelindex, pPhysics, params );
}

//-----------------------------------------------------------------------------
// Purpose: Returns a string describing a real-world equivalent mass.
// Input  : flMass - mass in kg
//-----------------------------------------------------------------------------
const char *GetMassEquivalent(float flMass)
{
	static struct
	{
		float flMass;
		char *sz;
	} masstext[] =
	{
		{ 5e-6,		"snowflake" },
		{ 2.5e-3,	"ping-pong ball" },
		{ 5e-3,		"penny" },
		{ 0.2,		"mouse" },
		{ 0.05,		"golf ball" },
		{ 0.17,		"billard ball" },
		{ 2,		"bag of sugar" },
		{ 7,		"male cat" },
		{ 10,		"bowling ball" },
		{ 30,		"dog" },
		{ 60,		"cheetah" },
		{ 90,		"adult male human" },
		{ 250,		"refrigerator" },
		{ 600,		"race horse" },
		{ 1000,		"small car" },
		{ 1650,		"medium car" },
		{ 2500,		"large car" },
		{ 6000,		"t-rex" },
		{ 7200,		"elephant" },
		{ 8e4,		"space shuttle" },
		{ 2e5,		"loaded boxcar" },
		{ 7e5,		"locomotive" },
		{ 1036,		"Eiffel tower" },
		{ 6e24,		"the Earth" },
		{ 7e24,		"really freaking heavy" },
	};

	for (int i = 0; i < sizeof(masstext) / sizeof(masstext[0]) - 1; i++)
	{
		if (flMass < masstext[i].flMass)
		{
			return masstext[i].sz;
		}
	}

	return masstext[ sizeof(masstext) / sizeof(masstext[0]) - 1 ].sz;
}

//=============================================================================================================
// BASE PROP DOOR
//=============================================================================================================
#define	SF_DOOR_START_OPEN		1		// Door is initially open (by default, doors start closed).
#define SF_DOOR_LOCKED			2048	// Door is initially locked.
#define SF_DOOR_SILENT			4096	// Door makes no sounds, despite the settings for sound files. Also does not alert NPCs
#define	SF_DOOR_USE_CLOSES		8192	// Door can be +used to close before its autoreturn delay has expired.
#define SF_DOOR_SILENT_TO_NPCS	16384	// Does not alert NPC's when opened.
#define SF_DOOR_IGNORE_USE		32768	// Completely ignores player +use commands.


//
// Private activities.
//
static int ACT_DOOR_OPEN = 0;
static int ACT_DOOR_LOCKED = 0;

//
// Anim events.
//
enum
{
	AE_DOOR_OPEN = 1,	// The door should start opening.
};


void PlayLockSounds(CBaseEntity *pEdict, locksound_t *pls, int flocked, int fbutton);

BEGIN_DATADESC_NO_BASE(locksound_t)

	DEFINE_FIELD( sLockedSound,	FIELD_STRING),
	DEFINE_FIELD( sLockedSentence,	FIELD_STRING ),
	DEFINE_FIELD( sUnlockedSound,	FIELD_STRING ),
	DEFINE_FIELD( sUnlockedSentence, FIELD_STRING ),
	DEFINE_FIELD( iLockedSentence, FIELD_INTEGER ),
	DEFINE_FIELD( iUnlockedSentence, FIELD_INTEGER ),
	DEFINE_FIELD( flwaitSound,		FIELD_FLOAT ),
	DEFINE_FIELD( flwaitSentence,	FIELD_FLOAT ),
	DEFINE_FIELD( bEOFLocked,		FIELD_CHARACTER ),
	DEFINE_FIELD( bEOFUnlocked,	FIELD_CHARACTER ),

END_DATADESC()

BEGIN_DATADESC(CBasePropDoor)
	//DEFINE_FIELD(m_bLockedSentence, FIELD_CHARACTER),
	//DEFINE_FIELD(m_bUnlockedSentence, FIELD_CHARACTER),	
	DEFINE_KEYFIELD(m_nHardwareType, FIELD_INTEGER, "hardware"),
	DEFINE_KEYFIELD(m_flAutoReturnDelay, FIELD_FLOAT, "returndelay"),
	DEFINE_FIELD( m_hActivator, FIELD_EHANDLE ),
	DEFINE_KEYFIELD(m_SoundMoving, FIELD_SOUNDNAME, "soundmoveoverride"),
	DEFINE_KEYFIELD(m_SoundOpen, FIELD_SOUNDNAME, "soundopenoverride"),
	DEFINE_KEYFIELD(m_SoundClose, FIELD_SOUNDNAME, "soundcloseoverride"),
	DEFINE_KEYFIELD(m_ls.sLockedSound, FIELD_SOUNDNAME, "soundlockedoverride"),
	DEFINE_KEYFIELD(m_ls.sUnlockedSound, FIELD_SOUNDNAME, "soundunlockedoverride"),
	DEFINE_FIELD(m_bLocked, FIELD_BOOLEAN),
	//DEFINE_KEYFIELD(m_flBlockDamage, FIELD_FLOAT, "dmg"),
	DEFINE_KEYFIELD( m_bForceClosed, FIELD_BOOLEAN, "forceclosed" ),
	DEFINE_FIELD(m_eDoorState, FIELD_INTEGER),
	DEFINE_FIELD( m_hMaster, FIELD_EHANDLE ),
	DEFINE_FIELD( m_hBlocker, FIELD_EHANDLE ),
	DEFINE_FIELD( m_bFirstBlocked, FIELD_BOOLEAN ),
	//DEFINE_FIELD(m_hDoorList, FIELD_CLASSPTR),	// Reconstructed
	
	DEFINE_INPUTFUNC(FIELD_VOID, "Open", InputOpen),
	DEFINE_INPUTFUNC(FIELD_STRING, "OpenAwayFrom", InputOpenAwayFrom),
	DEFINE_INPUTFUNC(FIELD_VOID, "Close", InputClose),
	DEFINE_INPUTFUNC(FIELD_VOID, "Toggle", InputToggle),
	DEFINE_INPUTFUNC(FIELD_VOID, "Lock", InputLock),
	DEFINE_INPUTFUNC(FIELD_VOID, "Unlock", InputUnlock),

	DEFINE_OUTPUT(m_OnBlockedOpening, "OnBlockedOpening"),
	DEFINE_OUTPUT(m_OnBlockedClosing, "OnBlockedClosing"),
	DEFINE_OUTPUT(m_OnUnblockedOpening, "OnUnblockedOpening"),
	DEFINE_OUTPUT(m_OnUnblockedClosing, "OnUnblockedClosing"),
	DEFINE_OUTPUT(m_OnFullyClosed, "OnFullyClosed"),
	DEFINE_OUTPUT(m_OnFullyOpen, "OnFullyOpen"),
	DEFINE_OUTPUT(m_OnClose, "OnClose"),
	DEFINE_OUTPUT(m_OnOpen, "OnOpen"),

	DEFINE_EMBEDDED( m_ls ),

	// Function Pointers
	DEFINE_THINKFUNC(DoorOpenMoveDone),
	DEFINE_THINKFUNC(DoorCloseMoveDone),
	DEFINE_THINKFUNC(DoorAutoCloseThink),
END_DATADESC()

CBasePropDoor::CBasePropDoor( void )
{
	m_hMaster = NULL;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBasePropDoor::Spawn()
{
	BaseClass::Spawn();

//	DisableAutoFade();
	Precache();

	DoorTeleportToSpawnPosition();

	if (HasSpawnFlags(SF_DOOR_LOCKED))
	{
		m_bLocked = true;
	}

	SetMoveType(MOVETYPE_PUSH);
	
	if (m_flSpeed == 0)
	{
		m_flSpeed = 100;
	}
	
	RemoveFlag(FL_STATICPROP);

	SetSolid(SOLID_VPHYSICS);
	VPhysicsInitShadow(false, false);
	AddSolidFlags( FSOLID_CUSTOMRAYTEST | FSOLID_CUSTOMBOXTEST );

	SetBodygroup( DOOR_HARDWARE_GROUP, m_nHardwareType );
	if ((m_nHardwareType == 0) && (!HasSpawnFlags(SF_DOOR_LOCKED)))
	{
		// Doors with no hardware must always be locked.
		DevWarning(1, "Unlocked prop_door '%s' at (%.0f %.0f %.0f) has no hardware. All openable doors must have hardware!\n", GetDebugName(), GetAbsOrigin().x, GetAbsOrigin().y, GetAbsOrigin().z);
	}

//	if ( !PropDataOverrodeBlockLOS() )
//	{
//		CalculateBlockLOS();
//	}

	SetDoorBlocker( NULL );

	// Fills out the m_Soundxxx members.
	CalcDoorSounds();
}


//-----------------------------------------------------------------------------
// Purpose: Returns our capabilities mask.
//-----------------------------------------------------------------------------
int	CBasePropDoor::ObjectCaps()
{
	return BaseClass::ObjectCaps() | ( HasSpawnFlags( SF_DOOR_IGNORE_USE ) ? 0 : FCAP_IMPULSE_USE );
};


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBasePropDoor::Precache(void)
{
	BaseClass::Precache();

	RegisterPrivateActivities();
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBasePropDoor::RegisterPrivateActivities(void)
{
	static bool bRegistered = false;

	if (bRegistered)
		return;

	REGISTER_PRIVATE_ACTIVITY( ACT_DOOR_OPEN );
	REGISTER_PRIVATE_ACTIVITY( ACT_DOOR_LOCKED );
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBasePropDoor::Activate( void )
{
	BaseClass::Activate();
	
	UpdateAreaPortals( !IsDoorClosed() );

	// If we have a name, we may be linked
	if ( GetEntityName() != NULL_STRING )
	{
		CBaseEntity	*pTarget = NULL;

		// Find all entities with the same name
		while ( ( pTarget = gEntList.FindEntityByName( pTarget, GetEntityName(), NULL ) ) != NULL )
		{
			if ( pTarget != this )
			{
				CBasePropDoor *pDoor = dynamic_cast<CBasePropDoor *>(pTarget);

				if ( pDoor != NULL && pDoor->HasSlaves() == false )
				{
					m_hDoorList.AddToTail( pDoor );
					pDoor->SetMaster( this );
					pDoor->SetOwnerEntity( this );
				}
			}
		}
	}
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBasePropDoor::HandleAnimEvent(animevent_t *pEvent)
{
	// Opening is called here via an animation event if the open sequence has one,
	// otherwise it is called immediately when the open sequence is set.
	if (pEvent->event == AE_DOOR_OPEN)
	{
		DoorActivate();
	}
}


// Only overwrite str1 if it's NULL_STRING.
#define ASSIGN_STRING_IF_NULL( str1, str2 ) \
	if ( ( str1 ) == NULL_STRING ) { ( str1 ) = ( str2 ); }

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBasePropDoor::CalcDoorSounds()
{
	ErrorIfNot( GetModel() != NULL, ( "prop_door with no model at %.2f %.2f %.2f\n", GetAbsOrigin().x, GetAbsOrigin().y, GetAbsOrigin().z ) );

	string_t strSoundOpen = NULL_STRING;
	string_t strSoundClose = NULL_STRING;
	string_t strSoundMoving = NULL_STRING;
	string_t strSoundLocked = NULL_STRING;
	string_t strSoundUnlocked = NULL_STRING;

	// Otherwise, use the sounds specified by the model keyvalues. These are looked up
	// based on skin and hardware.
	KeyValues *modelKeyValues = new KeyValues("");
	if ( modelKeyValues->LoadFromBuffer( modelinfo->GetModelName( GetModel() ), modelinfo->GetModelKeyValueText( GetModel() ) ) )
	{
		KeyValues *pkvDoorSounds = modelKeyValues->FindKey("door_options");
		if ( pkvDoorSounds )
		{
			// Open / close / move sounds are looked up by skin index.
			char szSkin[80];
			int skin = m_nSkin;
			Q_snprintf( szSkin, sizeof( szSkin ), "skin%d", skin );
			KeyValues *pkvSkinData = pkvDoorSounds->FindKey( szSkin );
			if ( pkvSkinData )
			{
				strSoundOpen = AllocPooledString( pkvSkinData->GetString( "open" ) );
				strSoundClose = AllocPooledString( pkvSkinData->GetString( "close" ) );
				strSoundMoving = AllocPooledString( pkvSkinData->GetString( "move" ) );
				const char *pSurfaceprop = pkvSkinData->GetString( "surfaceprop" );
				if ( pSurfaceprop && VPhysicsGetObject() )
				{
					VPhysicsGetObject()->SetMaterialIndex( physprops->GetSurfaceIndex( pSurfaceprop ) );
				}
			}

			// Locked / unlocked sounds are looked up by hardware index.
			char szHardware[80];
			Q_snprintf( szHardware, sizeof( szHardware ), "hardware%d", m_nHardwareType );
			KeyValues *pkvHardwareData = pkvDoorSounds->FindKey( szHardware );
			if ( pkvHardwareData )
			{
				strSoundLocked = AllocPooledString( pkvHardwareData->GetString( "locked" ) );
				strSoundUnlocked = AllocPooledString( pkvHardwareData->GetString( "unlocked" ) );
			}

			// If any sounds were missing, try the "defaults" block.
			if ( ( strSoundOpen == NULL_STRING ) || ( strSoundClose == NULL_STRING ) || ( strSoundMoving == NULL_STRING ) ||
				 ( strSoundLocked == NULL_STRING ) || ( strSoundUnlocked == NULL_STRING ) )
			{
				KeyValues *pkvDefaults = pkvDoorSounds->FindKey( "defaults" );
				if ( pkvDefaults )
				{
					ASSIGN_STRING_IF_NULL( strSoundOpen, AllocPooledString( pkvDefaults->GetString( "open" ) ) );
					ASSIGN_STRING_IF_NULL( strSoundClose, AllocPooledString( pkvDefaults->GetString( "close" ) ) );
					ASSIGN_STRING_IF_NULL( strSoundMoving, AllocPooledString( pkvDefaults->GetString( "move" ) ) );
					ASSIGN_STRING_IF_NULL( strSoundLocked, AllocPooledString( pkvDefaults->GetString( "locked" ) ) );
					ASSIGN_STRING_IF_NULL( strSoundUnlocked, AllocPooledString( pkvDefaults->GetString( "unlocked" ) ) );
					// NOTE: No default needed for surfaceprop, it's set by the model
				}
			}
		}
	}

	// Any sound data members that are already filled out were specified as level designer overrides,
	// so they should not be overwritten.
	ASSIGN_STRING_IF_NULL( m_SoundOpen, strSoundOpen );
	ASSIGN_STRING_IF_NULL( m_SoundClose, strSoundClose );
	ASSIGN_STRING_IF_NULL( m_SoundMoving, strSoundMoving );
	ASSIGN_STRING_IF_NULL( m_ls.sLockedSound, strSoundLocked );
	ASSIGN_STRING_IF_NULL( m_ls.sUnlockedSound, strSoundUnlocked );

	// Make sure we have real, precachable sound names in all cases.
	UTIL_ValidateSoundName( m_SoundMoving, "DoorSound.Null" );
	UTIL_ValidateSoundName( m_SoundOpen, "DoorSound.Null" );
	UTIL_ValidateSoundName( m_SoundClose, "DoorSound.Null" );
	UTIL_ValidateSoundName( m_ls.sLockedSound, "DoorSound.Null" );
	UTIL_ValidateSoundName( m_ls.sUnlockedSound, "DoorSound.Null" );

	PrecacheScriptSound( STRING( m_SoundMoving ) );
	PrecacheScriptSound( STRING( m_SoundOpen ) );
	PrecacheScriptSound( STRING( m_SoundClose ) );
	PrecacheScriptSound( STRING( m_ls.sLockedSound ) );
	PrecacheScriptSound( STRING( m_ls.sUnlockedSound ) );
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : isOpen - 
//-----------------------------------------------------------------------------
void CBasePropDoor::UpdateAreaPortals(bool isOpen)
{
	string_t name = GetEntityName();
	if (!name)
		return;
	
	CBaseEntity *pPortal = NULL;
	while ((pPortal = gEntList.FindEntityByClassname(pPortal, "func_areaportal")) != NULL)
	{
		if (pPortal->HasTarget(name))
		{
			// USE_ON means open the portal, off means close it
			pPortal->Use(this, this, isOpen?USE_ON:USE_OFF, 0);
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : state - 
//-----------------------------------------------------------------------------
void CBasePropDoor::SetDoorBlocker( CBaseEntity *pBlocker )
{ 
	m_hBlocker = pBlocker; 

	if ( m_hBlocker == NULL )
	{
		m_bFirstBlocked = false;
	}
}
//-----------------------------------------------------------------------------
// Purpose: Called when the player uses the door.
// Input  : pActivator - 
//			pCaller - 
//			useType - 
//			value - 
//-----------------------------------------------------------------------------
void CBasePropDoor::Use( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value)
{
	if ( GetMaster() != NULL )
	{
		// Tell our owner we've been used
		GetMaster()->Use( pActivator, pCaller, useType, value );
	}
	else
	{
		// Just let it through
		OnUse( pActivator, pCaller, useType, value );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pActivator - 
//			*pCaller - 
//			useType - 
//			value - 
//-----------------------------------------------------------------------------
void CBasePropDoor::OnUse( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value )
{
	m_hActivator = pActivator;

	// If we're blocked while closing, open away from our blocker. This will
	// liberate whatever bit of detritus is stuck in us.
	if ( IsDoorBlocked() && IsDoorClosing() )
	{
		DoorOpen( m_hBlocker );
		return;
	}

	if (IsDoorClosed() || (IsDoorOpen() && HasSpawnFlags(SF_DOOR_USE_CLOSES)))
	{
		// Ready to be opened or closed.
		if (m_bLocked)
		{
			PropSetSequence(SelectWeightedSequence((Activity)ACT_DOOR_LOCKED));
			PlayLockSounds(this, &m_ls, TRUE, FALSE);
		}
		else
		{
			PlayLockSounds(this, &m_ls, FALSE, FALSE);
			int nSequence = SelectWeightedSequence((Activity)ACT_DOOR_OPEN);
			PropSetSequence(nSequence);

			if ((nSequence == -1) || !HasAnimEvent(nSequence, AE_DOOR_OPEN))
			{
				// No open anim event, we need to open the door here.
				DoorActivate();
			}
		}
	}
	else if ( IsDoorOpening() )
	{
		// We've been used while opening, close.
		DoorClose();
	}
	else if ( IsDoorClosing() || IsDoorAjar() )
	{
		DoorOpen( m_hActivator );
	}
}

//-----------------------------------------------------------------------------
// Purpose: Closes the door if it is not already closed.
//-----------------------------------------------------------------------------
void CBasePropDoor::InputClose(inputdata_t &inputdata)
{
	if (!IsDoorClosed())
	{	
		m_OnClose.FireOutput(inputdata.pActivator, this);
		DoorClose();
	}
}


//-----------------------------------------------------------------------------
// Purpose: Input handler that locks the door.
//-----------------------------------------------------------------------------
void CBasePropDoor::InputLock(inputdata_t &inputdata)
{
	Lock();
}


//-----------------------------------------------------------------------------
// Purpose: Opens the door if it is not already open.
//-----------------------------------------------------------------------------
void CBasePropDoor::InputOpen(inputdata_t &inputdata)
{
	OpenIfUnlocked(inputdata.pActivator, NULL);
}


//-----------------------------------------------------------------------------
// Purpose: Opens the door away from a specified entity if it is not already open.
//-----------------------------------------------------------------------------
void CBasePropDoor::InputOpenAwayFrom(inputdata_t &inputdata)
{
	CBaseEntity *pOpenAwayFrom = gEntList.FindEntityByName(NULL, inputdata.value.String(), inputdata.pActivator);
	OpenIfUnlocked(inputdata.pActivator, pOpenAwayFrom);
}


//-----------------------------------------------------------------------------
// Purpose: 
// 
// FIXME: This function should be combined with DoorOpen, but doing that
//		  could break existing content. Fix after shipping!	
//
// Input  : *pOpenAwayFrom - 
//-----------------------------------------------------------------------------
void CBasePropDoor::OpenIfUnlocked(CBaseEntity *pActivator, CBaseEntity *pOpenAwayFrom)
{
	// I'm locked, can't open
	if (m_bLocked)
		return; 

	if (!IsDoorOpen() && !IsDoorOpening())
	{	
		// Play door unlock sounds.
		PlayLockSounds(this, &m_ls, false, false);
		m_OnOpen.FireOutput(pActivator, this);
		DoorOpen(pOpenAwayFrom);
	}
}


//-----------------------------------------------------------------------------
// Purpose: Opens the door if it is not already open.
//-----------------------------------------------------------------------------
void CBasePropDoor::InputToggle(inputdata_t &inputdata)
{
	if (IsDoorClosed())
	{	
		// I'm locked, can't open
		if (m_bLocked)
			return; 

		DoorOpen(NULL);
	}
	else if (IsDoorOpen())
	{
		DoorClose();
	}
}


//-----------------------------------------------------------------------------
// Purpose: Input handler that unlocks the door.
//-----------------------------------------------------------------------------
void CBasePropDoor::InputUnlock(inputdata_t &inputdata)
{
	Unlock();
}


//-----------------------------------------------------------------------------
// Purpose: Locks the door so that it cannot be opened.
//-----------------------------------------------------------------------------
void CBasePropDoor::Lock(void)
{
	m_bLocked = true;
}


//-----------------------------------------------------------------------------
// Purpose: Unlocks the door so that it can be opened.
//-----------------------------------------------------------------------------
void CBasePropDoor::Unlock(void)
{
	if (!m_nHardwareType)
	{
		// Doors with no hardware must always be locked.
		DevWarning(1, "Unlocking prop_door '%s' at (%.0f %.0f %.0f) with no hardware. All openable doors must have hardware!\n", GetDebugName(), GetAbsOrigin().x, GetAbsOrigin().y, GetAbsOrigin().z);
	}

	m_bLocked = false;
}

//-----------------------------------------------------------------------------
// Purpose: Causes the door to "do its thing", i.e. start moving, and cascade activation.
//-----------------------------------------------------------------------------
bool CBasePropDoor::DoorActivate( void )
{
	if ( IsDoorOpen() && DoorCanClose( false ) )
	{
		DoorClose();
	}
	else
	{
		DoorOpen( m_hActivator );
	}

	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Starts the door opening.
//-----------------------------------------------------------------------------
void CBasePropDoor::DoorOpen(CBaseEntity *pOpenAwayFrom)
{
	// Don't bother if we're already doing this
	if ( IsDoorOpen() || IsDoorOpening() )
		return;

	UpdateAreaPortals(true);

	// It could be going-down, if blocked.
	ASSERT( IsDoorClosed() || IsDoorClosing() || IsDoorAjar() );

	// Emit door moving and stop sounds on CHAN_STATIC so that the multicast doesn't
	// filter them out and leave a client stuck with looping door sounds!
	if (!HasSpawnFlags(SF_DOOR_SILENT))
	{
		EmitSound( STRING( m_SoundMoving ) );

		if ( m_hActivator && m_hActivator->IsPlayer() && !HasSpawnFlags( SF_DOOR_SILENT_TO_NPCS ) )
		{

		}
	}

	m_eDoorState = DOOR_STATE_OPENING;
	
	SetMoveDone(&CBasePropDoor::DoorOpenMoveDone);

	// Virtual function that starts the door moving for whatever type of door this is.
	BeginOpening(pOpenAwayFrom);

	m_OnOpen.FireOutput(this, this);

	// Tell all the slaves
	if ( HasSlaves() )
	{
		int	numDoors = m_hDoorList.Count();

		CBasePropDoor *pLinkedDoor = NULL;

		// Open all linked doors
		for ( int i = 0; i < numDoors; i++ )
		{
			pLinkedDoor = m_hDoorList[i];

			if ( pLinkedDoor != NULL )
			{
				// If the door isn't already moving, get it moving
				pLinkedDoor->m_hActivator = m_hActivator;
				pLinkedDoor->DoorOpen( pOpenAwayFrom );
			}
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: The door has reached the open position. Either close automatically
//			or wait for another activation.
//-----------------------------------------------------------------------------
void CBasePropDoor::DoorOpenMoveDone(void)
{
	SetDoorBlocker( NULL );

	if (!HasSpawnFlags(SF_DOOR_SILENT))
	{
		EmitSound( STRING( m_SoundOpen ) );
	}

	ASSERT(IsDoorOpening());
	m_eDoorState = DOOR_STATE_OPEN;
	
	if (WillAutoReturn())
	{
		// In flWait seconds, DoorClose will fire, unless wait is -1, then door stays open
		SetMoveDoneTime(m_flAutoReturnDelay + 0.1);
		SetMoveDone(&CBasePropDoor::DoorAutoCloseThink);

		if (m_flAutoReturnDelay == -1)
		{
			SetNextThink( TICK_NEVER_THINK );
		}
	}

	CAI_BaseNPC *pNPC = dynamic_cast<CAI_BaseNPC *>(m_hActivator.Get());
	if (pNPC)
	{
		// Notify the NPC that opened us.
		pNPC->OnDoorFullyOpen(this);
	}

	m_OnFullyOpen.FireOutput(this, this);

	// Let the leaf class do its thing.
	OnDoorOpened();

	m_hActivator = NULL;
}


//-----------------------------------------------------------------------------
// Purpose: Think function that tries to close the door. Used for autoreturn.
//-----------------------------------------------------------------------------
void CBasePropDoor::DoorAutoCloseThink(void)
{
	// When autoclosing, we check both sides so that we don't close in the player's
	// face, or in an NPC's face for that matter, because they might be shooting
	// through the doorway.
	if ( !DoorCanClose( true ) )
	{
		if (m_flAutoReturnDelay == -1)
		{
			SetNextThink( TICK_NEVER_THINK );
		}
		else
		{
			// In flWait seconds, DoorClose will fire, unless wait is -1, then door stays open
			SetMoveDoneTime(m_flAutoReturnDelay + 0.1);
			SetMoveDone(&CBasePropDoor::DoorAutoCloseThink);
		}

		return;
	}

	DoorClose();
}


//-----------------------------------------------------------------------------
// Purpose: Starts the door closing.
//-----------------------------------------------------------------------------
void CBasePropDoor::DoorClose(void)
{
	// Don't bother if we're already doing this
	if ( IsDoorClosed() || IsDoorClosing() )
		return;

	if (!HasSpawnFlags(SF_DOOR_SILENT))
	{
		EmitSound( STRING( m_SoundMoving ) );

		if ( m_hActivator && m_hActivator->IsPlayer() )
		{

		}
	}
	
	ASSERT(IsDoorOpen() || IsDoorOpening());
	m_eDoorState = DOOR_STATE_CLOSING;

	SetMoveDone(&CBasePropDoor::DoorCloseMoveDone);

	// This will set the movedone time.
	BeginClosing();

	m_OnClose.FireOutput(this, this);

	// Tell all the slaves
	if ( HasSlaves() )
	{
		int	numDoors = m_hDoorList.Count();

		CBasePropDoor *pLinkedDoor = NULL;

		// Open all linked doors
		for ( int i = 0; i < numDoors; i++ )
		{
			pLinkedDoor = m_hDoorList[i];

			if ( pLinkedDoor != NULL )
			{
				// If the door isn't already moving, get it moving
				pLinkedDoor->DoorClose();
			}
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: The door has reached the closed position. Return to quiescence.
//-----------------------------------------------------------------------------
void CBasePropDoor::DoorCloseMoveDone(void)
{
	SetDoorBlocker( NULL );

	if (!HasSpawnFlags(SF_DOOR_SILENT))
	{
		StopSound( STRING( m_SoundMoving ) );
		EmitSound( STRING( m_SoundClose ) );
	}

	ASSERT(IsDoorClosing());
	m_eDoorState = DOOR_STATE_CLOSED;

	m_OnFullyClosed.FireOutput(m_hActivator, this);
	UpdateAreaPortals(false);

	// Let the leaf class do its thing.
	OnDoorClosed();

	m_hActivator = NULL;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pOther - 
//-----------------------------------------------------------------------------
void CBasePropDoor::MasterStartBlocked( CBaseEntity *pOther )
{
	if ( HasSlaves() )
	{
		int	numDoors = m_hDoorList.Count();

		CBasePropDoor *pLinkedDoor = NULL;

		// Open all linked doors
		for ( int i = 0; i < numDoors; i++ )
		{
			pLinkedDoor = m_hDoorList[i];

			if ( pLinkedDoor != NULL )
			{
				// If the door isn't already moving, get it moving
				pLinkedDoor->OnStartBlocked( pOther );
			}
		}
	}

	// Start ourselves blocked
	OnStartBlocked( pOther );
}

//-----------------------------------------------------------------------------
// Purpose: Called the first frame that the door is blocked while opening or closing.
// Input  : pOther - The blocking entity.
//-----------------------------------------------------------------------------
void CBasePropDoor::StartBlocked( CBaseEntity *pOther )
{
	m_bFirstBlocked = true;

	if ( GetMaster() != NULL )
	{
		GetMaster()->MasterStartBlocked( pOther );
		return;
	}

	// Start ourselves blocked
	OnStartBlocked( pOther );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pOther - 
//-----------------------------------------------------------------------------
void CBasePropDoor::OnStartBlocked( CBaseEntity *pOther )
{
	if ( m_bFirstBlocked == false )
	{
		DoorStop();
	}

	SetDoorBlocker( pOther );

	if (!HasSpawnFlags(SF_DOOR_SILENT))
	{
		StopSound( STRING( m_SoundMoving ) );
	}

	//
	// Fire whatever events we need to due to our blocked state.
	//
	if (IsDoorClosing())
	{
		// Closed into an NPC, open.
		if ( pOther->MyNPCPointer() )
		{
			DoorOpen( pOther );
		}
		m_OnBlockedClosing.FireOutput(pOther, this);
	}
	else
	{
		// Opened into an NPC, close.
		if ( pOther->MyNPCPointer() )
		{
			DoorClose();
		}

		CAI_BaseNPC *pNPC = dynamic_cast<CAI_BaseNPC *>(m_hActivator.Get());
		
		if ( pNPC != NULL )
		{
			// Notify the NPC that tried to open us.
			pNPC->OnDoorBlocked( this );
		}

		m_OnBlockedOpening.FireOutput( pOther, this );
	}
}

//-----------------------------------------------------------------------------
// Purpose: Called every frame when the door is blocked while opening or closing.
// Input  : pOther - The blocking entity.
//-----------------------------------------------------------------------------
void CBasePropDoor::Blocked(CBaseEntity *pOther)
{
	// dvs: TODO: will prop_door apply any blocking damage?
	// Hurt the blocker a little.
	//if (m_flBlockDamage)
	//{
	//	pOther->TakeDamage(CTakeDamageInfo(this, this, m_flBlockDamage, DMG_CRUSH));
	//}

	if ( m_bForceClosed && ( pOther->GetMoveType() == MOVETYPE_VPHYSICS ) &&
		 ( pOther->m_takedamage == DAMAGE_NO || pOther->m_takedamage == DAMAGE_EVENTS_ONLY ) )
	{
//		EntityPhysics_CreateSolver( this, pOther, true, 4.0f );
	}
	else if ( m_bForceClosed && ( pOther->GetMoveType() == MOVETYPE_VPHYSICS ) && ( pOther->m_takedamage == DAMAGE_YES ) )
	{
		pOther->TakeDamage( CTakeDamageInfo( this, this, pOther->GetHealth(), DMG_CRUSH ) );
	}

	// If we're set to force ourselves closed, keep going
	if ( m_bForceClosed )
		return;

	// If a door has a negative wait, it would never come back if blocked,
	// so let it just squash the object to death real fast.
//	if (m_flAutoReturnDelay >= 0)
//	{
//		if (IsDoorClosing())
//		{
//			DoorOpen();
//		}
//		else
//		{
//			DoorClose();
//		}
//	}

	// Block all door pieces with the same targetname here.
//	if (GetEntityName() != NULL_STRING)
//	{
//		CBaseEntity pTarget = NULL;
//		for (;;)
//		{
//			pTarget = gEntList.FindEntityByName(pTarget, GetEntityName(), NULL);
//
//			if (pTarget != this)
//			{
//				if (!pTarget)
//					break;
//
//				if (FClassnameIs(pTarget, "prop_door_rotating"))
//				{
//					CPropDoorRotating *pDoor = (CPropDoorRotating *)pTarget;
//
//					if (pDoor->m_fAutoReturnDelay >= 0)
//					{
//						if (pDoor->GetAbsVelocity() == GetAbsVelocity() && pDoor->GetLocalAngularVelocity() == GetLocalAngularVelocity())
//						{
//							// this is the most hacked, evil, bastardized thing I've ever seen. kjb
//							if (FClassnameIs(pTarget, "prop_door_rotating"))
//							{
//								// set angles to realign rotating doors
//								pDoor->SetLocalAngles(GetLocalAngles());
//								pDoor->SetLocalAngularVelocity(vec3_angle);
//							}
//							else
//							//{
//							//	// set origin to realign normal doors
//							//	pDoor->SetLocalOrigin(GetLocalOrigin());
//							//	pDoor->SetAbsVelocity(vec3_origin);// stop!
//							//}
//						}
//
//						if (IsDoorClosing())
//						{
//							pDoor->DoorOpen();
//						}
//						else
//						{
//							pDoor->DoorClose();
//						}
//					}
//				}
//			}
//		}
//	}
}


//-----------------------------------------------------------------------------
// Purpose: Called the first frame that the door is unblocked while opening or closing.
//-----------------------------------------------------------------------------
void CBasePropDoor::EndBlocked( void )
{
	if ( GetMaster() != NULL )
	{
		GetMaster()->EndBlocked();
		return;
	}

	if ( HasSlaves() )
	{
		int	numDoors = m_hDoorList.Count();

		CBasePropDoor *pLinkedDoor = NULL;

		// Check all links as well
		for ( int i = 0; i < numDoors; i++ )
		{
			pLinkedDoor = m_hDoorList[i];

			if ( pLinkedDoor != NULL )
			{
				// Make sure they can close as well
				pLinkedDoor->OnEndBlocked();
			}
		}
	}

	// Emit door moving and stop sounds on CHAN_STATIC so that the multicast doesn't
	// filter them out and leave a client stuck with looping door sounds!
	if (!HasSpawnFlags(SF_DOOR_SILENT))
	{
		EmitSound( STRING( m_SoundMoving ) );
	}

	//
	// Fire whatever events we need to due to our unblocked state.
	//
	if (IsDoorClosing())
	{
		m_OnUnblockedClosing.FireOutput(this, this);
	}
	else
	{
		m_OnUnblockedOpening.FireOutput(this, this);
	}

	OnEndBlocked();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CBasePropDoor::OnEndBlocked( void )
{
	if ( m_bFirstBlocked )
		return;

	// Restart us going
	DoorResume();
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pNPC - 
//-----------------------------------------------------------------------------
bool CBasePropDoor::NPCOpenDoor( CAI_BaseNPC *pNPC )
{
	// dvs: TODO: use activator filter here
	// dvs: TODO: outboard entity containing rules for whether door is operable?
	
	if ( IsDoorClosed() )
	{
		// Use the door
		Use( pNPC, pNPC, USE_ON, 0 );
	}

	return true;
}

bool CBasePropDoor::TestCollision( const Ray_t &ray, unsigned int mask, trace_t& trace )
{
	if ( !VPhysicsGetObject() )
		return false;

	studiohdr_t *pStudioHdr = GetModelPtr( );
	if (!pStudioHdr)
		return false;

	physcollision->TraceBox( ray, VPhysicsGetObject()->GetCollide(), GetAbsOrigin(), GetAbsAngles(), &trace );

	if ( trace.DidHit() )
	{
		trace.surface.surfaceProps = VPhysicsGetObject()->GetMaterialIndex();
		return true;
	}

	return false;
}


//-----------------------------------------------------------------------------
// Custom trace filter for doors
// Will only test against entities and rejects physics objects below a mass threshold
//-----------------------------------------------------------------------------

class CTraceFilterDoor : public CTraceFilterEntitiesOnly
{
public:
	// It does have a base, but we'll never network anything below here..
	DECLARE_CLASS_NOBASE( CTraceFilterDoor );
	
	CTraceFilterDoor( const IHandleEntity *pDoor, const IHandleEntity *passentity, int collisionGroup )
		: m_pDoor(pDoor), m_pPassEnt(passentity), m_collisionGroup(collisionGroup)
	{
	}
	
	virtual bool ShouldHitEntity( IHandleEntity *pHandleEntity, int contentsMask )
	{
		if ( !StandardFilterRules( pHandleEntity, contentsMask ) )
			return false;

		if ( !PassServerEntityFilter( pHandleEntity, m_pDoor ) )
			return false;

		if ( !PassServerEntityFilter( pHandleEntity, m_pPassEnt ) )
			return false;

		// Don't test if the game code tells us we should ignore this collision...
		CBaseEntity *pEntity = EntityFromEntityHandle( pHandleEntity );
		
		if ( pEntity )
		{
			if ( !pEntity->ShouldCollide( m_collisionGroup, contentsMask ) )
				return false;
			
			if ( !g_pGameRules->ShouldCollide( m_collisionGroup, pEntity->GetCollisionGroup() ) )
				return false;

			// If objects are small enough and can move, close on them
			if ( pEntity->GetMoveType() == MOVETYPE_VPHYSICS )
			{
				IPhysicsObject *pPhysics = pEntity->VPhysicsGetObject();
				Assert(pPhysics);
				
				// Must either be squashable or very light
				if ( pPhysics->IsMoveable() && pPhysics->GetMass() < 32 )
					return false;
			}
		}

		return true;
	}

private:

	const IHandleEntity *m_pDoor;
	const IHandleEntity *m_pPassEnt;
	int m_collisionGroup;
};

inline void TraceHull_Door( const CBasePropDoor *pDoor, const Vector &vecAbsStart, const Vector &vecAbsEnd, const Vector &hullMin, 
					 const Vector &hullMax,	unsigned int mask, const CBaseEntity *ignore, 
					 int collisionGroup, trace_t *ptr )
{
	Ray_t ray;
	ray.Init( vecAbsStart, vecAbsEnd, hullMin, hullMax );
	CTraceFilterDoor traceFilter( pDoor, ignore, collisionGroup );
	enginetrace->TraceRay( ray, mask, &traceFilter, ptr );
}

// Check directions for door movement
enum doorCheck_e
{
	DOOR_CHECK_FORWARD,		// Door's forward opening direction
	DOOR_CHECK_BACKWARD,	// Door's backward opening direction
	DOOR_CHECK_FULL,		// Door's complete movement volume
};


enum PropDoorRotatingSpawnPos_t
{
	DOOR_SPAWN_CLOSED = 0,
	DOOR_SPAWN_OPEN_FORWARD,
	DOOR_SPAWN_OPEN_BACK,
	DOOR_SPAWN_AJAR,
};

//===============================================
// Rotating prop door
//===============================================

class CPropDoorRotating : public CBasePropDoor
{
	DECLARE_CLASS( CPropDoorRotating, CBasePropDoor );

public:

	~CPropDoorRotating();

	int		DrawDebugTextOverlays(void);

	void	Spawn( void );
	void	MoveDone( void );
	void	BeginOpening(CBaseEntity *pOpenAwayFrom);
	void	BeginClosing( void );
	void	OnRestore( void );

	void	DoorTeleportToSpawnPosition();

	void	GetNPCOpenData(CAI_BaseNPC *pNPC, opendata_t &opendata);

	void	DoorClose( void );
	bool	DoorCanClose( bool bAutoClose );
	void	DoorOpen( CBaseEntity *pOpenAwayFrom );

	void	OnDoorOpened();
	void	OnDoorClosed();

	void	DoorResume( void );
	void	DoorStop( void );

	float	GetOpenInterval();

	bool	OverridePropdata() { return true; }

	DECLARE_DATADESC();

private:

	void	AngularMove(const QAngle &vecDestAngle, float flSpeed);
	void	CalculateDoorVolume( QAngle closedAngles, QAngle openAngles, Vector *destMins, Vector *destMaxs );

	bool	CheckDoorClear( doorCheck_e state );
	
	doorCheck_e	GetOpenState( void );

	Vector	m_vecAxis;					// The axis of rotation.
	float	m_flDistance;				// How many degrees we rotate between open and closed.

	PropDoorRotatingSpawnPos_t m_eSpawnPosition;

	QAngle	m_angRotationAjar;			// Angles to spawn at if we are set to spawn ajar.
	QAngle	m_angRotationClosed;		// Our angles when we are fully closed.
	QAngle	m_angRotationOpenForward;	// Our angles when we are fully open towards our forward vector.
	QAngle	m_angRotationOpenBack;		// Our angles when we are fully open away from our forward vector.

	QAngle	m_angGoal;

	Vector	m_vecForwardBoundsMin;
	Vector	m_vecForwardBoundsMax;
	Vector	m_vecBackBoundsMin;
	Vector	m_vecBackBoundsMax;

	CHandle<CEntityBlocker>	m_hDoorBlocker;
};


BEGIN_DATADESC(CPropDoorRotating)
	DEFINE_KEYFIELD(m_eSpawnPosition, FIELD_INTEGER, "spawnpos"),
	DEFINE_KEYFIELD(m_vecAxis, FIELD_VECTOR, "axis"),
	DEFINE_KEYFIELD(m_flDistance, FIELD_FLOAT, "distance"),
	DEFINE_KEYFIELD( m_angRotationAjar, FIELD_VECTOR, "ajarangles" ),
	DEFINE_FIELD( m_angRotationClosed, FIELD_VECTOR ),
	DEFINE_FIELD( m_angRotationOpenForward, FIELD_VECTOR ),
	DEFINE_FIELD( m_angRotationOpenBack, FIELD_VECTOR ),
	DEFINE_FIELD( m_angGoal, FIELD_VECTOR ),
	DEFINE_FIELD( m_hDoorBlocker, FIELD_EHANDLE ),
	//m_vecForwardBoundsMin
	//m_vecForwardBoundsMax
	//m_vecBackBoundsMin
	//m_vecBackBoundsMax
END_DATADESC()

LINK_ENTITY_TO_CLASS(prop_door_rotating, CPropDoorRotating);

//-----------------------------------------------------------------------------
// Destructor
//-----------------------------------------------------------------------------
CPropDoorRotating::~CPropDoorRotating( void )
{
	// Remove our door blocker entity
	if ( m_hDoorBlocker != NULL )
	{
		UTIL_Remove( m_hDoorBlocker );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &mins1 - 
//			&maxs1 - 
//			&mins2 - 
//			&maxs2 - 
//			*destMins - 
//			*destMaxs - 
//-----------------------------------------------------------------------------
void UTIL_ComputeAABBForBounds( const Vector &mins1, const Vector &maxs1, const Vector &mins2, const Vector &maxs2, Vector *destMins, Vector *destMaxs )
{
	// Find the minimum extents
	(*destMins)[0] = min( mins1[0], mins2[0] );
	(*destMins)[1] = min( mins1[1], mins2[1] );
	(*destMins)[2] = min( mins1[2], mins2[2] );

	// Find the maximum extents
	(*destMaxs)[0] = max( maxs1[0], maxs2[0] );
	(*destMaxs)[1] = max( maxs1[1], maxs2[1] );
	(*destMaxs)[2] = max( maxs1[2], maxs2[2] );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CPropDoorRotating::Spawn()
{
	// Doors are built closed, so save the current angles as the closed angles.
	m_angRotationClosed = GetLocalAngles();

	// The axis of rotation must be axial for now.
	// dvs: TODO: finalize data definition of hinge axis
	// HACK: convert the axis of rotation to dPitch dYaw dRoll
	m_vecAxis = Vector(0, 0, 1);
	//VectorNormalize(m_vecAxis);
	//ASSERT((m_vecAxis.x == 0 && m_vecAxis.y == 0) ||
	//		(m_vecAxis.y == 0 && m_vecAxis.z == 0) ||
	//		(m_vecAxis.z == 0 && m_vecAxis.x == 0));
	Vector vecMoveDir(m_vecAxis.y, m_vecAxis.z, m_vecAxis.x);

	if (m_flDistance == 0)
	{
		m_flDistance = 90;
	}
	m_flDistance = fabs(m_flDistance);

	// Calculate our orientation when we are fully open.
	m_angRotationOpenForward.x = m_angRotationClosed.x - (vecMoveDir.x * m_flDistance);
	m_angRotationOpenForward.y = m_angRotationClosed.y - (vecMoveDir.y * m_flDistance);
	m_angRotationOpenForward.z = m_angRotationClosed.z - (vecMoveDir.z * m_flDistance);

	m_angRotationOpenBack.x = m_angRotationClosed.x + (vecMoveDir.x * m_flDistance);
	m_angRotationOpenBack.y = m_angRotationClosed.y + (vecMoveDir.y * m_flDistance);
	m_angRotationOpenBack.z = m_angRotationClosed.z + (vecMoveDir.z * m_flDistance);

	// Call this last! It relies on stuff we calculated above.
	BaseClass::Spawn();

	// Figure out our volumes of movement as this door opens
	CalculateDoorVolume( GetLocalAngles(), m_angRotationOpenForward, &m_vecForwardBoundsMin, &m_vecForwardBoundsMax );
	CalculateDoorVolume( GetLocalAngles(), m_angRotationOpenBack, &m_vecBackBoundsMin, &m_vecBackBoundsMax );
}


//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
doorCheck_e CPropDoorRotating::GetOpenState( void )
{
	return ( m_angGoal == m_angRotationOpenForward ) ? DOOR_CHECK_FORWARD : DOOR_CHECK_BACKWARD;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CPropDoorRotating::OnDoorOpened( void )
{
	if ( m_hDoorBlocker != NULL )
	{
		// Allow passage through this blocker while open
		m_hDoorBlocker->AddSolidFlags( FSOLID_NOT_SOLID );

		if ( g_debug_doors.GetBool() )
		{
//			NDebugOverlay::Box( GetAbsOrigin(), m_hDoorBlocker->CollisionProp()->OBBMins(), m_hDoorBlocker->CollisionProp()->OBBMaxs(), 0, 255, 0, true, 1.0f );
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CPropDoorRotating::OnDoorClosed( void )
{
	if ( m_hDoorBlocker != NULL )
	{
		// Destroy the blocker that was preventing NPCs from getting in our way.
		UTIL_Remove( m_hDoorBlocker );
		
		if ( g_debug_doors.GetBool() )
		{
//			NDebugOverlay::Box( GetAbsOrigin(), m_hDoorBlocker->CollisionProp()->OBBMins(), m_hDoorBlocker->CollisionProp()->OBBMaxs(), 0, 255, 0, true, 1.0f );
		}
	}
}


//-----------------------------------------------------------------------------
// Purpose: Returns whether the way is clear for the door to close.
// Input  : state - Which sides to check, forward, backward, or both.
// Output : Returns true if the door can close, false if the way is blocked.
//-----------------------------------------------------------------------------
bool CPropDoorRotating::DoorCanClose( bool bAutoClose )
{
	if ( GetMaster() != NULL )
		return GetMaster()->DoorCanClose( bAutoClose );
	
	// Check all slaves
	if ( HasSlaves() )
	{
		int	numDoors = m_hDoorList.Count();

		CPropDoorRotating *pLinkedDoor = NULL;

		// Check all links as well
		for ( int i = 0; i < numDoors; i++ )
		{
			pLinkedDoor = dynamic_cast<CPropDoorRotating *>((CBasePropDoor *)m_hDoorList[i]);

			if ( pLinkedDoor != NULL )
			{
				if ( !pLinkedDoor->CheckDoorClear( bAutoClose ? DOOR_CHECK_FULL : pLinkedDoor->GetOpenState() ) )
					return false;
			}
		}
	}
	
	// See if our path of movement is clear to allow us to shut
	return CheckDoorClear( bAutoClose ? DOOR_CHECK_FULL : GetOpenState() );
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : closedAngles - 
//			openAngles - 
//			*destMins - 
//			*destMaxs - 
//-----------------------------------------------------------------------------
void CPropDoorRotating::CalculateDoorVolume( QAngle closedAngles, QAngle openAngles, Vector *destMins, Vector *destMaxs )
{
	// Save our current angles and move to our start angles
	QAngle	saveAngles = GetLocalAngles();
	SetLocalAngles( closedAngles );

	// Find our AABB at the closed state
	Vector	closedMins, closedMaxs;
//	CollisionProp()->WorldSpaceAABB( &closedMins, &closedMaxs );
	
	SetLocalAngles( openAngles );

	// Find our AABB at the open state
	Vector	openMins, openMaxs;
//	CollisionProp()->WorldSpaceAABB( &openMins, &openMaxs );

	// Reset our angles to our starting angles
	SetLocalAngles( saveAngles );

	// Find the minimum extents
	UTIL_ComputeAABBForBounds( closedMins, closedMaxs, openMins, openMaxs, destMins, destMaxs );
	
	// Move this back into local space
	*destMins -= GetAbsOrigin();
	*destMaxs -= GetAbsOrigin();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CPropDoorRotating::OnRestore( void )
{
	BaseClass::OnRestore();

	// Figure out our volumes of movement as this door opens
	CalculateDoorVolume( GetLocalAngles(), m_angRotationOpenForward, &m_vecForwardBoundsMin, &m_vecForwardBoundsMax );
	CalculateDoorVolume( GetLocalAngles(), m_angRotationOpenBack, &m_vecBackBoundsMin, &m_vecBackBoundsMax );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : forward - 
//			mask - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CPropDoorRotating::CheckDoorClear( doorCheck_e state )
{
	Vector moveMins;
	Vector moveMaxs;

	switch ( state )
	{
	case DOOR_CHECK_FORWARD:
		moveMins = m_vecForwardBoundsMin;
		moveMaxs = m_vecForwardBoundsMax;
		break;

	case DOOR_CHECK_BACKWARD:
		moveMins = m_vecBackBoundsMin;
		moveMaxs = m_vecBackBoundsMax;
		break;

	default:
	case DOOR_CHECK_FULL:
		UTIL_ComputeAABBForBounds( m_vecForwardBoundsMin, m_vecForwardBoundsMax, m_vecBackBoundsMin, m_vecBackBoundsMax, &moveMins, &moveMaxs );
		break;
	}

	// Look for blocking entities, ignoring ourselves and the entity that opened us.
	trace_t	tr;
	TraceHull_Door( this, GetAbsOrigin(), GetAbsOrigin(), moveMins, moveMaxs, MASK_SOLID, GetActivator(), COLLISION_GROUP_NONE, &tr );
	if ( tr.allsolid || tr.startsolid )
	{
		if ( g_debug_doors.GetBool() )
		{
			NDebugOverlay::Box( GetAbsOrigin(), moveMins, moveMaxs, 255, 0, 0, true, 10.0f );

			if ( tr.m_pEnt )
			{
//				NDebugOverlay::Box( tr.m_pEnt->GetAbsOrigin(), tr.m_pEnt->CollisionProp()->OBBMins(), tr.m_pEnt->CollisionProp()->OBBMaxs(), 220, 220, 0, true, 10.0f );
			}
		}

		return false;
	}

	if ( g_debug_doors.GetBool() )
	{
		NDebugOverlay::Box( GetAbsOrigin(), moveMins, moveMaxs, 0, 255, 0, true, 10.0f );
	}

	return true;
}


//-----------------------------------------------------------------------------
// Purpose: Puts the door in its appropriate position for spawning.
//-----------------------------------------------------------------------------
void CPropDoorRotating::DoorTeleportToSpawnPosition()
{
	QAngle angSpawn;

	// The Start Open spawnflag trumps the choices field
	if ( ( HasSpawnFlags( SF_DOOR_START_OPEN ) ) || ( m_eSpawnPosition == DOOR_SPAWN_OPEN_FORWARD ) )
	{
		angSpawn = m_angRotationOpenForward;
		SetDoorState( DOOR_STATE_OPEN );
	}
	else if ( m_eSpawnPosition == DOOR_SPAWN_OPEN_BACK )
	{
		angSpawn = m_angRotationOpenBack;
		SetDoorState( DOOR_STATE_OPEN );
	}
	else if ( m_eSpawnPosition == DOOR_SPAWN_CLOSED )
	{
		angSpawn = m_angRotationClosed;
		SetDoorState( DOOR_STATE_CLOSED );
	}
	else if ( m_eSpawnPosition == DOOR_SPAWN_AJAR )
	{
		angSpawn = m_angRotationAjar;
		SetDoorState( DOOR_STATE_AJAR );
	}
	else
	{
		// Bogus spawn position setting!
		Assert( false );
		angSpawn = m_angRotationClosed;
		SetDoorState( DOOR_STATE_CLOSED );
	}

	SetLocalAngles( angSpawn );

	// Doesn't relink; that's done in Spawn.
}


//-----------------------------------------------------------------------------
// Purpose: After rotating, set angle to exact final angle, call "move done" function.
//-----------------------------------------------------------------------------
void CPropDoorRotating::MoveDone()
{
	SetLocalAngles(m_angGoal);
	SetLocalAngularVelocity(vec3_angle);
	SetMoveDoneTime(-1);
	BaseClass::MoveDone();
}


//-----------------------------------------------------------------------------
// Purpose: Calculate m_vecVelocity and m_flNextThink to reach vecDest from
//			GetLocalOrigin() traveling at flSpeed. Just like LinearMove, but rotational.
// Input  : vecDestAngle - 
//			flSpeed - 
//-----------------------------------------------------------------------------
void CPropDoorRotating::AngularMove(const QAngle &vecDestAngle, float flSpeed)
{
	ASSERTSZ(flSpeed != 0, "AngularMove:  no speed is defined!");
	
	m_angGoal = vecDestAngle;

	// Already there?
	if (vecDestAngle == GetLocalAngles())
	{
		MoveDone();
		return;
	}
	
	// Set destdelta to the vector needed to move.
	QAngle vecDestDelta = vecDestAngle - GetLocalAngles();
	
	// Divide by speed to get time to reach dest
	float flTravelTime = vecDestDelta.Length() / flSpeed;

	// Call MoveDone when destination angles are reached.
	SetMoveDoneTime(flTravelTime);

	// Scale the destdelta vector by the time spent traveling to get velocity.
	SetLocalAngularVelocity(vecDestDelta * (1.0 / flTravelTime));
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CPropDoorRotating::BeginOpening(CBaseEntity *pOpenAwayFrom)
{
	// Determine the direction to open.
	QAngle angOpen = m_angRotationOpenForward;
	doorCheck_e eDirCheck = DOOR_CHECK_FORWARD;
	if (pOpenAwayFrom != NULL)
	{
		Vector vecForwardDoor;
		GetVectors(&vecForwardDoor, NULL, NULL);

		if (vecForwardDoor.Dot(pOpenAwayFrom->GetAbsOrigin()) > vecForwardDoor.Dot(GetAbsOrigin()))
		{
			angOpen = m_angRotationOpenBack;
			eDirCheck = DOOR_CHECK_BACKWARD;
		}
	}

	// If player is opening us and we're opening away from them, and we'll be
	// blocked if we open away from them, open toward them.
	if (IsPlayerOpening() && (pOpenAwayFrom && pOpenAwayFrom->IsPlayer()) && !CheckDoorClear(eDirCheck))
	{
		if (eDirCheck == DOOR_CHECK_FORWARD)
		{
			angOpen = m_angRotationOpenBack;
			eDirCheck = DOOR_CHECK_BACKWARD;
		}
		else
		{
			angOpen = m_angRotationOpenForward;
			eDirCheck = DOOR_CHECK_FORWARD;
		}
	}

	// Create the door blocker
	Vector mins, maxs;
	if ( eDirCheck == DOOR_CHECK_FORWARD )
	{
		mins = m_vecForwardBoundsMin;
		maxs = m_vecForwardBoundsMax;
	}
	else
	{
		mins = m_vecBackBoundsMin;
		maxs = m_vecBackBoundsMax;		
	}

	if ( m_hDoorBlocker != NULL )
	{
		UTIL_Remove( m_hDoorBlocker );
	}

	// Create a blocking entity to keep random entities out of our movement path
	m_hDoorBlocker = CEntityBlocker::Create( GetAbsOrigin(), mins, maxs, pOpenAwayFrom, false );
	
	Vector	volumeCenter = ((mins+maxs) * 0.5f) + GetAbsOrigin();

	// Ignoring the Z
	float volumeRadius = max( fabs(mins.x), maxs.x );
	volumeRadius = max( volumeRadius, max( fabs(mins.y), maxs.y ) );

	// Debug
	if ( g_debug_doors.GetBool() )
	{
		NDebugOverlay::Cross3D( volumeCenter, -Vector(volumeRadius,volumeRadius,volumeRadius), Vector(volumeRadius,volumeRadius,volumeRadius), 255, 0, 0, true, 1.0f );
	}

	// Make respectful entities move away from our path
//	CSoundEnt::InsertSound( SOUND_MOVE_AWAY, volumeCenter, volumeRadius, 0.5f, pOpenAwayFrom );

	// Do final setup
	if ( m_hDoorBlocker != NULL )
	{
		// Only block NPCs
//		m_hDoorBlocker->SetCollisionGroup( COLLISION_GROUP_DOOR_BLOCKER );

		// If we hit something while opening, just stay unsolid until we try again
		if ( CheckDoorClear( eDirCheck ) == false )
		{
			m_hDoorBlocker->AddSolidFlags( FSOLID_NOT_SOLID );
		}

		if ( g_debug_doors.GetBool() )
		{
//			NDebugOverlay::Box( GetAbsOrigin(), m_hDoorBlocker->CollisionProp()->OBBMins(), m_hDoorBlocker->CollisionProp()->OBBMaxs(), 255, 0, 0, true, 1.0f );
		}
	}

	AngularMove(angOpen, m_flSpeed);
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CPropDoorRotating::BeginClosing( void )
{
	if ( m_hDoorBlocker != NULL )
	{
		// Become solid again unless we're already being blocked
		if ( CheckDoorClear( GetOpenState() )  )
		{
			m_hDoorBlocker->RemoveSolidFlags( FSOLID_NOT_SOLID );
		}
		
		if ( g_debug_doors.GetBool() )
		{
//			NDebugOverlay::Box( GetAbsOrigin(), m_hDoorBlocker->CollisionProp()->OBBMins(), m_hDoorBlocker->CollisionProp()->OBBMaxs(), 255, 0, 0, true, 1.0f );
		}
	}

	AngularMove(m_angRotationClosed, m_flSpeed);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CPropDoorRotating::DoorStop( void )
{
	SetLocalAngularVelocity( vec3_angle );
	SetMoveDoneTime( -1 );
}

//-----------------------------------------------------------------------------
// Purpose: Restart a door moving that was temporarily paused
//-----------------------------------------------------------------------------
void CPropDoorRotating::DoorResume( void )
{
	// Restart our angular movement
	AngularMove( m_angGoal, m_flSpeed );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : vecMoveDir - 
//			opendata - 
//-----------------------------------------------------------------------------
void CPropDoorRotating::GetNPCOpenData(CAI_BaseNPC *pNPC, opendata_t &opendata)
{
	// dvs: TODO: finalize open position, direction, activity
	Vector vecForward;
	Vector vecRight;
	AngleVectors(GetAbsAngles(), &vecForward, &vecRight, NULL);

	//
	// Figure out where the NPC should stand to open this door,
	// and what direction they should face.
	//
	opendata.vecStandPos = GetAbsOrigin() - (vecRight * 24);
	opendata.vecStandPos.z -= 54;

	Vector vecNPCOrigin = pNPC->GetAbsOrigin();

	if (pNPC->GetAbsOrigin().Dot(vecForward) > GetAbsOrigin().Dot(vecForward))
	{
		// In front of the door relative to the door's forward vector.
		opendata.vecStandPos += vecForward * 64;
		opendata.vecFaceDir = -vecForward;
	}
	else
	{
		// Behind the door relative to the door's forward vector.
		opendata.vecStandPos -= vecForward * 64;
		opendata.vecFaceDir = vecForward;
	}

	opendata.eActivity = ACT_OPEN_DOOR;
}


//-----------------------------------------------------------------------------
// Purpose: Returns how long it will take this door to open.
//-----------------------------------------------------------------------------
float CPropDoorRotating::GetOpenInterval()
{
	// set destdelta to the vector needed to move
	QAngle vecDestDelta = m_angRotationOpenForward - GetLocalAngles();
	
	// divide by speed to get time to reach dest
	return vecDestDelta.Length() / m_flSpeed;
}


//-----------------------------------------------------------------------------
// Purpose: Draw any debug text overlays
// Output : Current text offset from the top
//-----------------------------------------------------------------------------
int CPropDoorRotating::DrawDebugTextOverlays(void) 
{
	int text_offset = BaseClass::DrawDebugTextOverlays();

	if (m_debugOverlays & OVERLAY_TEXT_BIT) 
	{
		char tempstr[512];
		Q_snprintf(tempstr, sizeof(tempstr),"Avelocity: %.2f %.2f %.2f", GetLocalAngularVelocity().x,  GetLocalAngularVelocity().y,  GetLocalAngularVelocity().z);
		NDebugOverlay::EntityText(entindex(), text_offset, tempstr, 0);
		text_offset++;

		if ( IsDoorOpen() )
		{
			Q_strncpy(tempstr, "DOOR STATE: OPEN", sizeof(tempstr));
		}
		else if ( IsDoorClosed() )
		{
			Q_strncpy(tempstr, "DOOR STATE: CLOSED", sizeof(tempstr));
		}
		else if ( IsDoorOpening() )
		{
			Q_strncpy(tempstr, "DOOR STATE: OPENING", sizeof(tempstr));
		}
		else if ( IsDoorClosing() )
		{
			Q_strncpy(tempstr, "DOOR STATE: CLOSING", sizeof(tempstr));
		}
		else if ( IsDoorAjar() )
		{
			Q_strncpy(tempstr, "DOOR STATE: AJAR", sizeof(tempstr));
		}
		NDebugOverlay::EntityText(entindex(), text_offset, tempstr, 0);
		text_offset++;
	}

	return text_offset;
}


// Debug sphere
class CPhysSphere : public CPhysicsProp
{
	DECLARE_CLASS( CPhysSphere, CPhysicsProp );
public:
	virtual bool OverridePropdata() { return true; }
	bool CreateVPhysics()
	{
		SetSolid( SOLID_BBOX );
		SetCollisionBounds( -Vector(12,12,12), Vector(12,12,12) );
		objectparams_t params = g_PhysDefaultObjectParams;
		params.pGameData = static_cast<void *>(this);
		IPhysicsObject *pPhysicsObject = physenv->CreateSphereObject( 12, 0, GetAbsOrigin(), GetAbsAngles(), &params, false );
		if ( pPhysicsObject )
		{
			VPhysicsSetObject( pPhysicsObject );
			SetMoveType( MOVETYPE_VPHYSICS );
			pPhysicsObject->Wake();
			Relink();
		}
	
		return true;
	}
};

LINK_ENTITY_TO_CLASS( prop_sphere, CPhysSphere );


