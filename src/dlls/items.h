//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef ITEMS_H
#define ITEMS_H
#pragma once

#include "entityoutput.h"

// Armor given by a battery
#define MAX_NORMAL_BATTERY	100

// Ammo counts given by ammo items
#define SIZE_AMMO_PISTOL			20
#define SIZE_AMMO_PISTOL_LARGE		100
#define SIZE_AMMO_SMG1				45
#define SIZE_AMMO_SMG1_LARGE		225
#define SIZE_AMMO_AR2				20
#define SIZE_AMMO_AR2_LARGE			100
#define SIZE_AMMO_RPG_ROUND			1
#define SIZE_AMMO_SMG1_GRENADE		1
#define SIZE_AMMO_BUCKSHOT			20
#define SIZE_AMMO_357				6
#define SIZE_AMMO_357_LARGE			20
#define SIZE_AMMO_CROSSBOW			6
#define	SIZE_AMMO_AR2_ALTFIRE		1

class CItem : public CBaseAnimating
{
public:
	DECLARE_CLASS( CItem, CBaseAnimating );

	void	Spawn( void );
	CBaseEntity*	Respawn( void );
	void	ItemTouch( CBaseEntity *pOther );
	void	Materialize( void );
	virtual bool MyTouch( CBasePlayer *pPlayer ) { return false; };

private:

	COutputEvent m_OnPlayerTouch;

	DECLARE_DATADESC();
};

#endif // ITEMS_H
