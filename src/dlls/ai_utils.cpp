//========= Copyright � 2003, Valve LLC, All rights reserved. ==========
//
// Purpose:
//
//=============================================================================

#include "cbase.h"
#include "ai_utils.h"

//-----------------------------------------------------------------------------

BEGIN_SIMPLE_DATADESC( CAI_MoveMonitor )
	DEFINE_FIELD( m_vMark, FIELD_POSITION_VECTOR ), 
	DEFINE_FIELD( m_flMarkTolerance, FIELD_FLOAT )
END_DATADESC()

//-----------------------------------------------------------------------------

BEGIN_SIMPLE_DATADESC( CAI_ShotRegulator )
	DEFINE_EMBEDDED( m_NextShotTimer ),
	DEFINE_FIELD( m_nShotsToTake, FIELD_INTEGER ),
	DEFINE_FIELD( m_nMinShots, FIELD_INTEGER ),
	DEFINE_FIELD( m_nMaxShots, FIELD_INTEGER ),
END_DATADESC()

//-----------------------------------------------------------------------------
