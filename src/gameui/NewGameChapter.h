//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef NewGameChapter_H
#define NewGameChapter_H
#ifdef _WIN32
#pragma once
#endif

#include "taskframe.h"
#include <vgui_controls/EditablePanel.h>
#include <vgui/MouseCode.h>
#include <vgui_controls/ImagePanel.h>

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class CNewGameChapter : public CTaskFrame
{
public:
	CNewGameChapter(vgui::Panel *parent);
	~CNewGameChapter();

	virtual void OnCommand( const char *command );
	virtual void OnClose();
	virtual void Getchapter( );
	virtual void Undechapter( );

	vgui::EditablePanel *ChapterPanel1;
	vgui::EditablePanel *ChapterPanel2;
	vgui::EditablePanel *ChapterPanel3;

protected:
	typedef vgui::Frame BaseClass;

	vgui::DHANDLE<vgui::Frame> m_GameChapterpanel;
};

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class CBitmapImagePanel;

class CGameChapterPanel : public vgui::EditablePanel
{
public:
	CGameChapterPanel(vgui::Panel *parent, const char *panelName);
	~CGameChapterPanel();

	virtual void OnMouseDoublePressed(vgui::MouseCode code);

protected:
	typedef vgui::EditablePanel BaseClass;

//   CBitmapImagePanel *m_pPicBorder;
//   CBitmapImagePanel *m_pLevelPic;

	vgui::ImagePanel        *m_pLevelPic;
	vgui::ImagePanel        *m_pLevelPicBorder;

	vgui::Label             *m_pChapterNameLabel;
	vgui::Label             *m_pChapterLabel;


};


#endif // NewGameChapter_H


