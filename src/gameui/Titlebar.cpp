//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include "Titlebar.h"
#include "ModInfo.h"

#include <vgui/IInput.h>
#include <vgui/IPanel.h>
#include <vgui/IScheme.h>
#include <vgui/ISystem.h>
#include <vgui/IVGui.h>
#include <vgui/ISurface.h>
#include <vgui_controls/ImagePanel.h>
#include <vgui_controls/Controls.h>
#include <tier1/keyvalues.h>

// memdbgon must be the last include file in a .cpp file!!!
#include <tier0/memdbgon.h>

using namespace vgui;

#define MOD_LOGO_INSET	24
#define BLACK_BAR_SIZE	64
#define BLACK_BAR_COLOR	Color(0, 0, 0, 128)

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CTitlebar::CTitlebar(Panel *parent, const char *panelName) : EditablePanel(parent, panelName)
{
	// create mod logo image panel
	m_pModLogo = new vgui::ImagePanel(this, "ModLogo");
}

//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
CTitlebar::~CTitlebar()
{
}

//-----------------------------------------------------------------------------
// Purpose: update the titlebar every frame
//-----------------------------------------------------------------------------
void CTitlebar::RunFrame()
{
	InvalidateLayout();
}

//-----------------------------------------------------------------------------
// Purpose: Lays out the position of the taskbar
//-----------------------------------------------------------------------------
void CTitlebar::PerformLayout()
{
	int wide, tall;
	vgui::surface()->GetScreenSize(wide, tall);

	// position self along top of screen
	SetPos(0, 0);
	SetSize(wide, BLACK_BAR_SIZE);
	SetBgColor(BLACK_BAR_COLOR);

	// set mod logo position
	m_pModLogo->SetPos(MOD_LOGO_INSET, 16);
	m_pModLogo->SetSize(wide, MOD_LOGO_INSET + 4);
	m_pModLogo->SetVisible(true);
}

//-----------------------------------------------------------------------------
// Purpose: Loads scheme information
//-----------------------------------------------------------------------------
void CTitlebar::ApplySchemeSettings(IScheme *pScheme)
{
	BaseClass::ApplySchemeSettings(pScheme);

	SetBorder(NULL);
	
	// set mod logo image
	m_pModLogo->SetImage( scheme()->GetImage( ModInfo().GetGameLogo(), false ) );

	// work out current focus - find the topmost panel
	SetBgColor(GetSchemeColor("MainMenu.Backdrop", pScheme));
	InvalidateLayout();
}
