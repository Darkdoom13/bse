//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef TITLEBAR_H
#define TITLEBAR_H
#ifdef _WIN32
#pragma once
#endif

#include <vgui_controls/EditablePanel.h>
#include <vgui_controls/ImagePanel.h>
#include "tier1/UtlVector.h"

//-----------------------------------------------------------------------------
// Purpose: Titlebar for mod logo
//-----------------------------------------------------------------------------
class CTitlebar : public vgui::EditablePanel
{
public:
	CTitlebar(vgui::Panel *parent, const char *panelName);
	~CTitlebar();

	// update the titlebar every frame
	void RunFrame();

private:
	virtual void PerformLayout();
	virtual void ApplySchemeSettings(vgui::IScheme *pScheme);

	vgui::ImagePanel *m_pModLogo;

	typedef vgui::Panel BaseClass;
};

extern CTitlebar *g_pTitlebar;

#endif // TITLEBAR_H
