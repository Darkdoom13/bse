//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include "NewGameChapter.h"

#include "EngineInterface.h"
#include <vgui_controls/Button.h>
#include <vgui_controls/CheckButton.h>
#include "BitmapImagePanel.h"
#include <tier1/keyvalues.h>
#include <vgui_controls/Label.h>
#include <vgui/ISurface.h>
#include <vgui_controls/ImagePanel.h>
#include <vgui/MouseCode.h>
#include <vgui_controls/RadioButton.h>
#include <stdio.h>
#include "ModInfo.h"

using namespace vgui;

#include "GameUI_Interface.h"
#include "taskframe.h"

// memdbgon must be the last include file in a .cpp file!!!
#include <tier0/memdbgon.h>

int chapter;

//-----------------------------------------------------------------------------
// Purpose: Basic help dialog
//-----------------------------------------------------------------------------
CNewGameChapter::CNewGameChapter(vgui::Panel *parent) : CTaskFrame(parent, "NewGameDialog")
{
	SetBounds(0, 0, 372, 160);
	SetSizeable( false );

	chapter = 0;

	MakePopup();
	g_pTaskbar->AddTask(GetVPanel());

	SetTitle("#GameUI_NewGame", true);

	vgui::Button *Next = new vgui::Button( this, "Next", "#gameui_next" );
	Next->SetCommand( "Next" );

	vgui::Button *Prev = new vgui::Button( this, "Prev", "#gameui_prev" );
	Next->SetCommand( "Prev" );

	vgui::Button *play = new vgui::Button( this, "Play", "#GameUI_Play" );
	play->SetCommand( "Play" );

	vgui::Button *cancel = new vgui::Button( this, "Cancel", "#GameUI_Cancel" );
	cancel->SetCommand( "Close" );

	Getchapter ( );

	LoadControlSettings("Resource\\NewzGameChapterPanel.res");
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
CNewGameChapter::~CNewGameChapter()
{
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *command - 
//-----------------------------------------------------------------------------
void CNewGameChapter::OnCommand( const char *command )
{
	if ( !stricmp( command, "Next" ) )
	{
		ChapterPanel1->OnDelete();
		ChapterPanel2->OnDelete();
		ChapterPanel3->OnDelete();

		Getchapter ( );
	}
	else if ( !stricmp( command, "Prev" ) )
	{
		if ( chapter > 3 )
		{
			ChapterPanel1->OnDelete();
			ChapterPanel2->OnDelete();
			ChapterPanel3->OnDelete();

			Undechapter ( );
		}
	}
	else if ( !stricmp( command, "Play" ) )
	{

	}
	else
	{
		BaseClass::OnCommand( command );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNewGameChapter::Undechapter( )
{
	chapter -= 5 ;

	ChapterPanel1 = new CGameChapterPanel(this, "ChapterPanel1");
	ChapterPanel1->SetBounds( 10, 50, 180, 160);

	chapter += 1 ;

	ChapterPanel2 = new CGameChapterPanel(this, "ChapterPanel2");
	ChapterPanel2->SetBounds( 185, 50, 180, 160);

	chapter += 1 ;

	ChapterPanel3 = new CGameChapterPanel(this, "ChapterPanel3");
	ChapterPanel3->SetBounds( 360, 50, 180, 160);

}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNewGameChapter::Getchapter( )
{
	chapter += 1 ;

	ChapterPanel1 = new CGameChapterPanel(this, "ChapterPanel1");

	ChapterPanel1->SetBounds( 10, 50, 180, 160);

	chapter += 1 ;

	ChapterPanel2 = new CGameChapterPanel(this, "ChapterPanel2");

	ChapterPanel2->SetBounds( 185, 50, 180, 160);

	chapter += 1 ;

	ChapterPanel3 = new CGameChapterPanel(this, "ChapterPanel3");

	ChapterPanel3->SetBounds( 360, 50, 180, 160);

}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNewGameChapter::OnClose()
{
	BaseClass::OnClose();
	MarkForDeletion();
}


//-----------------------------------------------------------------------------
// Purpose: Basic help dialog
//-----------------------------------------------------------------------------
CGameChapterPanel::CGameChapterPanel(Panel *parent, const char *panelName) : EditablePanel(parent, panelName)
{
	SetBounds( 10, 50, 180, 160);

	m_pLevelPic = new ImagePanel(this, "LevelPic");

	char texture[ 256 ];

	sprintf( texture, "chapters/chapter%d", chapter );

	m_pLevelPic->SetImage( scheme()->GetImage(texture, false) );

	m_pLevelPicBorder = new ImagePanel(this, "LevelPicBorder");

	m_pLevelPicBorder->AddActionSignalTarget( this );

	m_pLevelPicBorder->SetSize(152, 86);
	m_pLevelPicBorder->Repaint();

	char name[ 256 ];

	sprintf( name, "#Valve_Chapter%d_Title", chapter );

    m_pChapterNameLabel = new Label(this, "ChapterNameLabel", name);

	m_pChapterLabel = new Label(this, "ChapterLabel", "#GameUI_Chapter");

//	IScheme *pScheme =  scheme()->GetIScheme( scheme()->GetDefaultScheme() );

	LoadControlSettings("Resource\\NewGameChapterPanel.res");

}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
CGameChapterPanel::~CGameChapterPanel()
{
}

//-----------------------------------------------------------------------------
// Purpose: sets up mouse capture mode
//-----------------------------------------------------------------------------
void CGameChapterPanel::OnMouseDoublePressed(vgui::MouseCode code)
{
	switch ( code )
	{
		default:
		case vgui::MOUSE_LEFT:
			Msg( "test" );
			break;
	}

	BaseClass::OnMouseDoublePressed( code );
}