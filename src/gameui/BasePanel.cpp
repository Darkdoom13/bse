//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose:  
//
// $NoKeywords: $
//=============================================================================

#include <stdio.h>

#include "BasePanel.h"
#include "Taskbar.h"
#include "EngineInterface.h"

#include <vgui/IPanel.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui/IVGui.h>
#include <filesystem.h>
#include <tier1/keyvalues.h>
#include <vstdlib/random.h>

#include "GameConsole.h"

// memdbgon must be the last include file in a .cpp file!!!
#include <tier0/memdbgon.h>

using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CBasePanel::CBasePanel() : Panel(NULL, "BasePanel")
{
	m_eBackgroundState = BACKGROUND_NONE;
	kv = new KeyValues("chapters");

	m_bChapterLoads = LoadChapters();

	if( m_bChapterLoads )
	{
		m_iChapterID = RandomInt( 0, m_iChapterCount );
	}
	else
	{
		m_iChapterID = 0;
	}
}

//-----------------------------------------------------------------------------
// Purpose: Notifies the task bar that a new top-level panel has been created
//-----------------------------------------------------------------------------
void CBasePanel::OnChildAdded(VPANEL child)
{
	if (g_pTaskbar)
	{
		g_pTaskbar->AddTask(child);
	}
}

//-----------------------------------------------------------------------------
// Purpose: paints the main background image
//-----------------------------------------------------------------------------
void CBasePanel::PaintBackground()
{
	const char *levelName = engine->GetLevelName();
	if (levelName && levelName[0])
	{
		// render filled background in game
		int swide, stall;
		surface()->GetScreenSize(swide, stall);
//		surface()->DrawSetColor(0, 0, 0, 128);
		surface()->DrawSetColor(0, 0, 0, 0);
		surface()->DrawFilledRect(0, 0, swide, stall);		
		return;	
	}

	switch (m_eBackgroundState)
	{
	case BACKGROUND_BLACK:
		{
			// if the loading dialog is visible, draw the background black
			int swide, stall;
			surface()->GetScreenSize(swide, stall);
			surface()->DrawSetColor(0, 0, 0, 255);
			surface()->DrawFilledRect(0, 0, swide, stall);
			//DrawLoadingImage();
		}
		break;

	case BACKGROUND_LOADING:
		//DrawBackgroundImage();
		DrawLoadingImage();
		break;

	case BACKGROUND_DESKTOPIMAGE:
		DrawBackgroundImage();
		break;
		
	case BACKGROUND_LOADINGTRANSITION:
		{
		}
		break;

	case BACKGROUND_NONE:
	default:
		break;
	}
}

//-----------------------------------------------------------------------------
// Purpose: loads chapters file
//-----------------------------------------------------------------------------
bool CBasePanel::LoadChapters( void )
{
	m_iChapterCount = 0;

	if (kv->LoadFromFile(vgui::filesystem(), "scripts/ChapterBackgrounds.txt"))
	{
		// iterate the list loading all the servers
		for (KeyValues *srv = kv->GetFirstSubKey(); srv != NULL; srv = srv->GetNextKey())
		{
			m_iChapterCount++;
		}

		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// Purpose: get texture chapter name
//-----------------------------------------------------------------------------
const char *CBasePanel::GetChapterBackGround( int chapterid )
{
	int counter = 0;

	// iterate the list loading all the servers
	for (KeyValues *srv = kv->GetFirstSubKey(); srv != NULL; srv = srv->GetNextKey() )
	{
		if( chapterid == counter )
		{
			return srv->GetString();
		}

		counter++;
	}

	return srv->GetString();
}

//-----------------------------------------------------------------------------
// Purpose: loads background texture
//-----------------------------------------------------------------------------
void CBasePanel::ApplySchemeSettings(IScheme *pScheme)
{
	BaseClass::ApplySchemeSettings(pScheme);
	// turn on hardware filtering if we're scaling the images
	int wide, tall;
	surface()->GetScreenSize(wide, tall);
	bool hardwareFilter = false; //(wide != 800);

	bimage_t &bimage = m_ImageID[0][0];
	bimage.imageID = surface()->CreateNewTextureID();

	char filename[512];
	memset( filename, 0, sizeof(filename) );

	bool useWide = false;

	if ( wide == 1280 && tall == 720 )
	{
		useWide = true;
	}
	else if ( wide == 1080 && tall == 612 )
	{
		useWide = true;
	}

	if( m_bChapterLoads )
	{
		if ( useWide )
		{
			Msg( "Using widescreen background image...\n" );
			if ( vgui::filesystem()->FileExists( "materials/console/background01_widescreen.vmt" ) )
			{
				sprintf( filename, "console/%s_widescreen", GetChapterBackGround(m_iChapterID) );
			}
			else
			{	
				Msg( "console_background_wide not found! Using regular background.\n" );
				sprintf( filename, "console/%s", GetChapterBackGround(m_iChapterID) );
			}		
		}
		else
		{
			sprintf( filename, "console/%s", GetChapterBackGround(m_iChapterID) );
		}
	}
	else
	{
		if ( useWide )
		{
			Msg( "Using widescreen background image...\n" );
			if ( vgui::filesystem()->FileExists( "materials/console/background01_widescreen.vmt" ) )
			{
				sprintf( filename, "console/background01_widescreen" );
			}
			else
			{	
				Msg( "console_background_wide not found! Using regular background.\n" );
				sprintf( filename, "console/background01" );
			}		
		}
		else
		{
			sprintf(filename, "console/background01" );
		}
	}

	surface()->DrawSetTextureFile(bimage.imageID, filename, hardwareFilter, false);
	surface()->DrawGetTextureSize(bimage.imageID, bimage.width, bimage.height);

/*	BaseClass::ApplySchemeSettings(pScheme);
	// turn on hardware filtering if we're scaling the images
	int wide, tall;
	surface()->GetScreenSize(wide, tall);
	bool hardwareFilter = false; //(wide != 800);

	bimage_t &bimage = m_ImageID[0][0];
	bimage.imageID = surface()->CreateNewTextureID();

	char filename[512];
	bool useWide = false;

	if ( wide == 1280 && tall == 720 )
	{
		useWide = true;
	}
	else if ( wide == 1080 && tall == 612 )
	{
		useWide = true;
	}
	
	if ( useWide )
	{
		Msg( "Using widescreen background image...\n" );
		if ( vgui::filesystem()->FileExists( "materials/console/background01_widescreen.vmt" ) )
		{
			sprintf( filename, "console/background01_widescreen" );
		}
		else
		{	
			Msg( "console_background_wide not found! Using regular background.\n" );
			sprintf( filename, "console/background01" );
		}		
	}
	else
	{
		sprintf(filename, "console/background01" );
	}

	surface()->DrawSetTextureFile(bimage.imageID, filename, hardwareFilter, false);
	surface()->DrawGetTextureSize(bimage.imageID, bimage.width, bimage.height);
*/
}

//-----------------------------------------------------------------------------
// Purpose: sets how the game background should render
//-----------------------------------------------------------------------------
void CBasePanel::SetBackgroundRenderState(EBackgroundState state)
{
	m_eBackgroundState = state;

	switch (m_eBackgroundState)
	{
	case BACKGROUND_BLACK:
		{
			Msg("BLEQ BG\n");		
		}
		break;

	case BACKGROUND_LOADING:
		Msg( "LOADING BG\n" );
		break;

	case BACKGROUND_DESKTOPIMAGE:
		Msg( "DESKTOPIMAGE BG\n" );
		break;
		
	case BACKGROUND_LOADINGTRANSITION:
		{
		}
		break;

	case BACKGROUND_NONE:
	default:
		break;
	}
}

//-----------------------------------------------------------------------------
// Purpose: draws the background desktop image
//-----------------------------------------------------------------------------
void CBasePanel::DrawBackgroundImage()
{
//	int xpos, ypos;
	int wide, tall;
	GetSize(wide, tall);

	// work out scaling factors
	int swide, stall;
	surface()->GetScreenSize(swide, stall);
	float xScale, yScale;
//	xScale = swide / 800.0f;
//	yScale = stall / 600.0f;
	xScale = 1.0f;
	yScale = 1.0f;
/*
	// iterate and draw all the background pieces
	ypos = 0;
	for (int y = 0; y < BACKGROUND_ROWS; y++)
	{
		xpos = 0;
		for (int x = 0; x < BACKGROUND_COLUMNS; x++)
		{
			bimage_t &bimage = m_ImageID[y][x];

			int dx = (int)ceil(xpos * xScale);
			int dy = (int)ceil(ypos * yScale);
			int dw = (int)ceil((xpos + bimage.width) * xScale);
			int dt = (int)ceil((ypos + bimage.height) * yScale);

			if (x == 0)
			{
				dx = 0;
			}
			if (y == 0)
			{
				dy = 0;
			}

  */
			bimage_t &bimage = m_ImageID[0][0];
			// draw the color image only if the mono image isn't yet fully opaque
			surface()->DrawSetColor(255, 255, 255, 255);
			surface()->DrawSetTexture(bimage.imageID);
			surface()->DrawTexturedRect(0, 0, wide, tall);

		/*	xpos += bimage.width;
		}
		ypos += m_ImageID[y][0].height;
	}
	*/

	if( engine->IsBackGroundMap( ) && m_bChapterLoads )
	{
		char runcomand[512];
		memset( runcomand, 0, sizeof(runcomand) );
		sprintf( runcomand, "map_background %s", GetChapterBackGround(m_iChapterID) );
		engine->ClientCmd( runcomand );
	}
}

void CBasePanel::DrawLoadingImage()
{
//	int xpos, ypos;
	int wide, tall;

	GetSize(wide, tall);

	// work out scaling factors
	int swide, stall;
	surface()->GetScreenSize(swide, stall);
	float xScale, yScale;
//	xScale = swide / 800.0f;
//	yScale = stall / 600.0f;
	xScale = 1.0f;
	yScale = 1.0f;
/*
	// iterate and draw all the background pieces
	ypos = 0;
	for (int y = 0; y < BACKGROUND_ROWS; y++)
	{
		xpos = 0;
		for (int x = 0; x < BACKGROUND_COLUMNS; x++)
		{
			bimage_t &bimage = m_ImageID[y][x];

			int dx = (int)ceil(xpos * xScale);
			int dy = (int)ceil(ypos * yScale);
			int dw = (int)ceil((xpos + bimage.width) * xScale);
			int dt = (int)ceil((ypos + bimage.height) * yScale);

			if (x == 0)
			{
				dx = 0;
			}
			if (y == 0)
			{
				dy = 0;
			}

  */
			bimage_t &bimage = m_ImageID[0][0];

			char filename[512];
			sprintf(filename, "console/console_background_loading" );
			//bimage_t &bimage = m_ImageID[0][0];
			bimage.imageID = surface()->CreateNewTextureID();
			surface()->DrawSetTextureFile(bimage.imageID, filename, false, false);
			surface()->DrawGetTextureSize(bimage.imageID, bimage.width, bimage.height);
			// draw the color image only if the mono image isn't yet fully opaque
			surface()->DrawSetColor(255, 255, 255, 255);
			surface()->DrawSetTexture(bimage.imageID);
			surface()->DrawTexturedRect(0, 0, wide, tall);

			Msg( "Loading image drawn...\n" );
		/*	xpos += bimage.width;
		}
		ypos += m_ImageID[y][0].height;
	}
	*/
}
