//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef NewGameChapter_H
#define NewGameChapter_H
#ifdef _WIN32
#pragma once
#endif

#include "taskframe.h"
#include <vgui_controls/Panel.h>
#include <vgui_controls/ComboBox.h>
#include <vgui_controls/PropertyPage.h>

namespace vgui
{
class CheckButton;
class ComboBox;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class OptionsSubVideoAdvancedDlg : public CTaskFrame
{
public:
	OptionsSubVideoAdvancedDlg(vgui::Panel *parent);
	~OptionsSubVideoAdvancedDlg();

	virtual void OnCommand( const char *command );
	virtual void OnClose();
	virtual void OnApplyChanges();
	virtual void SetDefaultValue();

	vgui::ComboBox *m_pTextDetail;


protected:
	typedef vgui::Frame BaseClass;

};


#endif // NewGameChapter_H


