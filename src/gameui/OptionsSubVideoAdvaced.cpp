//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#include "OptionsSubVideoAdvaced.h"
#include "EngineInterface.h"
#include <vgui_controls/Button.h>
#include <vgui_controls/CheckButton.h>
#include "BitmapImagePanel.h"
#include <tier1/keyvalues.h>
#include <vgui_controls/Label.h>
#include <vgui/ISurface.h>
#include <vgui_controls/ImagePanel.h>
#include <vgui/MouseCode.h>
#include <vgui_controls/RadioButton.h>
#include <stdio.h>
#include "ModInfo.h"

using namespace vgui;

#include "GameUI_Interface.h"
#include "taskframe.h"

// memdbgon must be the last include file in a .cpp file!!!
#include <tier0/memdbgon.h>

//-----------------------------------------------------------------------------
// Purpose: Basic help dialog
//-----------------------------------------------------------------------------
OptionsSubVideoAdvancedDlg::OptionsSubVideoAdvancedDlg(vgui::Panel *parent) : CTaskFrame(parent, "OptionsSubVideoAdvancedDlg")
{

	m_pTextDetail = new ComboBox( this, "TextureDetail", 3, false );
	m_pTextDetail->AddItem("#gameui_high", NULL);
	m_pTextDetail->AddItem("#gameui_medium", NULL);
	m_pTextDetail->AddItem("#gameui_low", NULL);

	vgui::Button *AdButton = new vgui::Button( this, "Button1", "#GameUI_OK" );
	AdButton->SetCommand( "OK" );

	SetDefaultValue();

	MakePopup();

	LoadControlSettings("Resource\\OptionsSubVideoAdvancedDlg.res");
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
OptionsSubVideoAdvancedDlg::~OptionsSubVideoAdvancedDlg()
{
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *command - 
//-----------------------------------------------------------------------------
void OptionsSubVideoAdvancedDlg::OnCommand( const char *command )
{
	if ( !stricmp( command, "OK" ) )
	{
		OnApplyChanges();
	}
	else if ( !stricmp( command, "Prev" ) )
	{

	}
	else if ( !stricmp( command, "Play" ) )
	{

	}
	else
	{
		BaseClass::OnCommand( command );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void OptionsSubVideoAdvancedDlg::SetDefaultValue()
{
	ConVar const *TextDetVar = cvar->FindVar( "mat_picmip" );
	if ( TextDetVar )
	{
		m_pTextDetail->ActivateItemByRow(TextDetVar->GetInt());
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void OptionsSubVideoAdvancedDlg::OnApplyChanges()
{
	char szCmd[ 256 ];

	// Texture Detail
	sprintf( szCmd, "mat_picmip %d\n", m_pTextDetail->GetActiveItem() );
	engine->ClientCmd( szCmd );

	OnClose();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void OptionsSubVideoAdvancedDlg::OnClose()
{
	BaseClass::OnClose();
	MarkForDeletion();
}



//	ConVar const *var = cvar->FindVar( "sv_password" );
//	if ( var )
//	{
//		SetControlString("PasswordEdit", var->GetString() );
//	}