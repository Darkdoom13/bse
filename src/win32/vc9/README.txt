NOTE: You MUST build the entire project with this order:

			common.sln
			tools.sln
			dlls.sln
			engine.sln
			tracker.sln


--------------------------
COMPILATION REQUIPMENTS
--------------------------

Windows Platform SDK
DirectX SDK
AFX SDK ( for WorldCraft and other gui apps )
OpenAL SDK ( not now, maybe at a later date... ;) )
SDL ( not now, maybe at a later date... ;) )

Inevitab13: Don't try compiling projects outside the .sln's, instead, use batch build on the .sln's if you want to build an
individual project.