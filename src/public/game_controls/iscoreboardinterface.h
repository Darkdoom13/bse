//========= Copyright � 2008, DarkFire Team, All rights reserved. ============//
//
// $Purpose: 	  $
// 
// $Date:             $
// $Log: 	 $
//
// Purpose: 
//
// $Workfile:     $
// $Date:         $
//
//-----------------------------------------------------------------------------
// $Log: $
//
// $NoKeywords: $
//=============================================================================
#if !defined( ISCOREBOARDINTERFACE_H )
#define ISCOREBOARDINTERFACE_H
#ifdef _WIN32
#pragma once
#endif

#include <cl_dll/IVGuiClientDll.h>

//-----------------------------------------------------------------------------
// Purpose: interface for apps to update the scoreboard
//-----------------------------------------------------------------------------
class IScoreBoardInterface
{
public:
	// should be called on level change to reset the scoreboard
	virtual void Reset() = 0;

	virtual void Update( const char *servername, bool teamplay, bool spectator ) = 0;
	virtual void RebuildTeams( const char *servername, bool teamplay, bool spectator ) = 0;
	virtual void DeathMsg( int killer, int victim ) = 0;
	virtual void Activate( bool spectatorUIVisible ) = 0;
	virtual void MoveToFront() = 0;
	virtual bool IsVisible() = 0;
	virtual void SetVisible( bool state ) = 0;
	virtual void SetParent( vgui::VPANEL parent ) = 0;
	virtual void SetMouseInputEnabled( bool state ) = 0;
	virtual void SetTeamName(  int index,  const char *name) = 0;
	virtual void SetTeamDetails( int index, int frags, int deaths) = 0;
	virtual const char *GetTeamName( int playerIndex ) = 0;
	virtual VGuiLibraryTeamInfo_t GetPlayerTeamInfo( int playerIndex ) = 0;
};

#endif // ISCOREBOARDINTERFACE_H

