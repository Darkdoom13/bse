//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef VCOLLIDE_H
#define VCOLLIDE_H
#ifdef _WIN32
#pragma once
#endif

class CPhysCollide;

struct vcollide_t
{
	int		solidCount;
	// VPhysicsSolids
	CPhysCollide	**solids;
	char			*pKeyValues;
};

#define VPHY_HEADER_ID		(('Y'<<24)+('H'<<16)+('P'<<8)+'V')
#define VPHY_FLAG_HEAD		0x100
#define VPHY_FLAG_MOD		0x10000
struct vphyheader 
{
	unsigned int	fourcc;	//VPHY
	int				flag;	//0x100
	int				len;	//data length
	int				unk[4];
};
#endif // VCOLLIDE_H
