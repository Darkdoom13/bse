//========= Copyright � 2008, DarkFire Team, All rights reserved. ============//
//
// $Purpose: 	  $
// 
// $Date:             $
// $Log: 	 $
//
// Purpose: 
//
// $Header: $
// $NoKeywords: $
//=============================================================================

#ifndef SHADERDLL_H
#define SHADERDLL_H

#ifdef _WIN32
#pragma once
#endif

#include <materialsystem/IShader.h>

//-----------------------------------------------------------------------------
// forward declarations
//-----------------------------------------------------------------------------
class IShader;
struct PrecompiledShader_t;


//-----------------------------------------------------------------------------
// The standard implementation of CShaderDLL
//-----------------------------------------------------------------------------
class IShaderDLL
{
public:
	// Adds a shader to the list of shaders
	virtual void InsertShader( IShader *pShader ) = 0;

	// Adds a precompiled shader to the list of shaders
	virtual void InsertPrecompiledShader( PrecompiledShaderType_t type, const PrecompiledShader_t *pVertexShader ) = 0;
};


//-----------------------------------------------------------------------------
// Singleton interface
//-----------------------------------------------------------------------------
IShaderDLL *GetShaderDLL();


#endif // SHADERDLL_H
