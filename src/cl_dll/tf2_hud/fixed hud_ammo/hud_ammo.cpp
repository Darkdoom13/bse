//========= Copyright � 1996-2002, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

/*#include "cbase.h"
#include "hud_numeric.h"
#include "hud_ammo.h"
#include "hud.h"
#include "iclientmode.h"
#include <vgui_controls/AnimationController.h>
#include <KeyValues.h>

//-----------------------------------------------------------------------------
// Singleton
//-----------------------------------------------------------------------------
static CHudAmmo g_HudAmmo;
CHudAmmo* GetHudAmmo()
{
	return &g_HudAmmo;
}


//-----------------------------------------------------------------------------
// Accessor methods to set various state associated with the ammo display
//-----------------------------------------------------------------------------
void CHudAmmo::SetPrimaryAmmo( int nAmmoType, int nTotalAmmo, int nClipCount, int nMaxClipCount )
{
	m_nAmmoType1 = nAmmoType;
	m_nTotalAmmo1 = nTotalAmmo;
	m_nMaxClip1 = nMaxClipCount;
	m_nClip1 = nClipCount;
}

void CHudAmmo::SetSecondaryAmmo( int nAmmoType, int nTotalAmmo, int nClipCount, int nMaxClipCount )
{
	m_nAmmoType2 = nAmmoType;
	m_nTotalAmmo2 = nTotalAmmo;
	m_nMaxClip2 = nMaxClipCount;
	m_nClip2 = nClipCount;
}

bool CHudAmmo::ShouldShowPrimaryClip() const
{
	if ( m_nAmmoType1 <= 0 )
		return false;

	if ( m_nClip1 < 0 )
		return false;

	return true;
}

bool CHudAmmo::ShouldShowSecondary() const 
{
	if ( m_nAmmoType2 <= 0 )
		return false;

	if ( m_nTotalAmmo2 <= 0 )
		return false;

	return true;
}

void CHudAmmo::ShowHideHudControls()
{
	bool showClip = ShouldShowPrimaryClip();
	bool showSecondary = ShouldShowSecondary();

	if ( showClip )
	{
		if ( showSecondary )
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence( "ShowPrimaryAmmoClipShowSecondaryAmmo" );
		}
		else
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence( "ShowPrimaryAmmoClipHideSecondaryAmmo" );
		}
	}
	else
	{
		if ( showSecondary )
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence( "HidePrimaryAmmoClipShowSecondaryAmmo" );
		}
		else
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence( "HidePrimaryAmmoClipHideSecondaryAmmo" );
		}
	}
}

class CHudAmmoPrimary : public CHudNumeric
{
	DECLARE_CLASS_SIMPLE( CHudAmmoPrimary, CHudNumeric );
public:
	CHudAmmoPrimary( const char *pElementName ) : CHudNumeric( pElementName, "HudAmmoPrimary" )
	{
	}

	virtual const char *GetLabelText() { return m_szAmmoLabel; }
	virtual const char *GetPulseEvent( bool increment ) { return increment ? "PrimaryAmmoIncrement" : "PrimaryAmmoDecrement"; }

	virtual bool		GetValue( char *val, int maxlen )
	{
		if ( GetHudAmmo()->m_nAmmoType1 <= 0 )
			return false;

		int count = ( GetHudAmmo()->m_nClip1 >= 0 ) ? GetHudAmmo()->m_nClip1 : GetHudAmmo()->m_nTotalAmmo1;
		Q_snprintf( val, maxlen, "%i", count );
		return true;
	}

	virtual Color GetColor()
	{
		// Get our ratio bar information
		float	ammoPerc = 1.0f - ( (float) GetHudAmmo()->m_nClip1 ) / ( (float) GetHudAmmo()->m_nMaxClip1 );
		bool	ammoCaution = ( ammoPerc >= CLIP_PERC_THRESHOLD );

		if ( ammoCaution )
			return m_TextColorCritical;

		return m_TextColor;
	}

	virtual void ApplySchemeSettings(vgui::IScheme *scheme)
	{
		BaseClass::ApplySchemeSettings( scheme );

		SetPaintBackgroundEnabled( true );
	}

private:
	CPanelAnimationStringVar( 128, m_szAmmoLabel, "AmmoLabel", "Ammo" );
};

DECLARE_HUDELEMENT( CHudAmmoPrimary );

class CHudAmmoPrimaryClip : public CHudNumeric
{
	DECLARE_CLASS_SIMPLE( CHudAmmoPrimaryClip, CHudNumeric );

public:
	CHudAmmoPrimaryClip( const char *pElementName ) : BaseClass( pElementName, "HudAmmoPrimaryClip" )
	{
		SetDrawLabel( false );

		m_nPrevVisible = -1;
	}

	virtual const char *GetLabelText() { return m_szAmmoClipLabel; }
	virtual const char *GetPulseEvent( bool increment ) { return increment ? "PrimaryAmmoClipIncrement" : "PrimaryAmmoClipDecrement"; }

	virtual bool		GetValue( char *val, int maxlen )
	{
		int iret = _GetValue( val, maxlen ) ? 1 : 0;

		if ( iret != m_nPrevVisible )
		{
			GetHudAmmo()->ShowHideHudControls();
			m_nPrevVisible = iret;
		}

		return true;
	}

	virtual bool		_GetValue( char *val, int maxlen )
	{
		Q_snprintf( val, maxlen, "" );

		if ( !GetHudAmmo()->ShouldShowPrimaryClip() )
			return false;

		int count = GetHudAmmo()->m_nTotalAmmo1;
		Q_snprintf( val, maxlen, "%i", count );
		return true;
	}

	virtual Color GetColor()
	{
		if ( GetHudAmmo()->ShouldShowPrimaryClip() )
		{
			if ( GetHudAmmo()->m_nTotalAmmo1 <= GetHudAmmo()->m_nMaxClip1 )
			{
				return m_TextColorCritical;
			}
		}

		return m_TextColor;
	}

private:
	int		m_nPrevVisible;
	CPanelAnimationStringVar( 128, m_szAmmoClipLabel, "AmmoClipLabel", "PrimaryAmmoClip" );
};

DECLARE_HUDELEMENT( CHudAmmoPrimaryClip );

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class CHudAmmoSecondary : public CHudNumeric
{
	DECLARE_CLASS_SIMPLE( CHudAmmoSecondary, CHudNumeric );

public:
	CHudAmmoSecondary( const char *pElementName ) : CHudNumeric( pElementName, "HudAmmoSecondary" )
	{
		SetDrawLabel( false );

		m_nPrevVisible = -1;
	}

	virtual const char *GetLabelText() { return m_szAmmoSecondaryLabel; }
	virtual const char *GetPulseEvent( bool increment ) { return increment ? "SecondaryAmmoIncrement" : "SecondaryAmmoDecrement"; }

	virtual bool		GetValue( char *val, int maxlen )
	{
		int iret = _GetValue( val, maxlen ) ? 1 : 0;

		if ( iret != m_nPrevVisible )
		{
			// Shift primary and clip left/right as needed
			GetHudAmmo()->ShowHideHudControls();
			m_nPrevVisible = iret;
		}

		return iret ? true : false;
	}

	virtual bool		_GetValue( char *val, int maxlen )
	{
		if ( !GetHudAmmo()->ShouldShowSecondary() )
			return false;

		int count = GetHudAmmo()->m_nTotalAmmo2;
		Q_snprintf( val, maxlen, "%i", count );
		return true;
	}

	virtual Color GetColor()
	{
		if ( GetHudAmmo()->m_nAmmoType2 > 0 &&
			 GetHudAmmo()->m_nTotalAmmo2 == 1 )
		{
			return m_TextColorCritical;
		}

		return m_TextColor;
	}

private:
	int		m_nPrevVisible;
	CPanelAnimationStringVar( 128, m_szAmmoSecondaryLabel, "AmmoSecondaryLabel", "AmmoSecondary" );
};

DECLARE_HUDELEMENT( CHudAmmoSecondary );
*/
//========= Copyright � 1996-2003, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
//=============================================================================

#include "cbase.h"
#include "hud.h"
#include "hudelement.h"
#include "hud_macros.h"
#include "parsemsg.h"
#include "hud_numericdisplay.h"
#include "hud_ammo.h"
#include "iclientmode.h"

#include <vgui_controls/AnimationController.h>

//-----------------------------------------------------------------------------
// Purpose: Displays current ammunition level
//-----------------------------------------------------------------------------
/*class CHudAmmo : public CHudNumericDisplay, public CHudElement
{
	DECLARE_CLASS_SIMPLE( CHudAmmo, CHudNumericDisplay );

public:
	CHudAmmo( const char *pElementName );
	void Init( void );
	void VidInit( void );

	void SetAmmo(int ammo, bool playAnimation);
	void SetAmmo2(int ammo2, bool playAnimation);
		
protected:
	virtual void OnThink();
	
private:
	CHandle< C_BaseCombatWeapon > m_hCurrentActiveWeapon;
	int		m_iAmmo;
	int		m_iAmmo2;
};
*/
DECLARE_HUDELEMENT( CHudAmmo )


//-----------------------------------------------------------------------------
// Singleton
//-----------------------------------------------------------------------------
//static CHudAmmo g_HudAmmo;
//CHudAmmo* GetHudAmmo()
//{
//	return &g_HudAmmo;
//}

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudAmmo::CHudAmmo( const char *pElementName ) : BaseClass(NULL, "HudAmmo"), CHudElement( pElementName )
{

}


//DECLARE_HUDELEMENT( "CHudAmmo" )
//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudAmmo::Init( void )
{
	m_iAmmo		= -1;
	m_iAmmo2	= -1;

	SetLabelText(L"AMMO");
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudAmmo::VidInit( void )
{
}

//-----------------------------------------------------------------------------
// Purpose: called every frame to get ammo info from the weapon
//-----------------------------------------------------------------------------
void CHudAmmo::OnThink()
{
	C_BaseCombatWeapon *wpn = GetActiveWeapon();
	C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
	if (!wpn || !player || !wpn->UsesPrimaryAmmo())
	{
		SetPaintEnabled(false);
		SetPaintBackgroundEnabled(false);
		return;
	}
	else
	{
		SetPaintEnabled(true);
		SetPaintBackgroundEnabled(true);
	}

	// get the ammo in our clip
	int ammo1 = wpn->Clip1();
	int ammo2;
	if (ammo1 < 0)
	{
		// we don't use clip ammo, just use the total ammo count
		ammo1 = player->GetAmmoCount(wpn->GetPrimaryAmmoType());
		ammo2 = 0;
	}
	else
	{
		// we use clip ammo, so the second ammo is the total ammo
		ammo2 = player->GetAmmoCount(wpn->GetPrimaryAmmoType());
	}

	if (wpn == m_hCurrentActiveWeapon)
	{
		// same weapon, just update counts
		SetAmmo(ammo1, true);
		SetAmmo2(ammo2, true);
	}
	else
	{
		// diferent weapon, change without triggering
		SetAmmo(ammo1, false);
		SetAmmo2(ammo2, false);

		// update whether or not we show the total ammo display
		if (wpn->UsesClipsForAmmo1())
		{
			SetShouldDisplaySecondaryValue(true);
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponUsesClips");

		}
		else
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponDoesNotUseClips");
			SetShouldDisplaySecondaryValue(false);
		}

		if ( GetGameRestored() )
		{
			SetGameRestored( false );
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponChangedRestore");
		}
		else
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("WeaponChanged");
		}
		m_hCurrentActiveWeapon = wpn;
	}

	ammo1 = player->GetAmmoCount(wpn->GetPrimaryAmmoType());
	
}

//-----------------------------------------------------------------------------
// Purpose: Updates ammo display
//-----------------------------------------------------------------------------
void CHudAmmo::SetAmmo(int ammo, bool playAnimation)
{
	if (ammo != m_iAmmo)
	{
		if (ammo == 0)
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("AmmoEmpty");
		}
		else if (ammo < m_iAmmo)
		{
			// ammo has decreased
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("AmmoDecreased");
		}
		else
		{
			// ammunition has increased
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("AmmoIncreased");
		}

		m_iAmmo = ammo;
	}

	SetDisplayValue(ammo);

	//C_BaseCombatWeapon *wpn = GetActiveWeapon();

	const ConVar *wAmmo = cvar->FindVar( "cl_tf2_currentammo" );
	//Msg( "Set ammo to %i\n", wAmmo->GetInt() );

	wchar_t unicode[6];
	swprintf(unicode, L"%d", wAmmo->GetInt() );
	SetLabelText( unicode );
}

//-----------------------------------------------------------------------------
// Purpose: Updates 2nd ammo display
//-----------------------------------------------------------------------------
void CHudAmmo::SetAmmo2(int ammo2, bool playAnimation)
{
	if (ammo2 != m_iAmmo2)
	{
		if (ammo2 == 0)
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Ammo2Empty");
		}
		else if (ammo2 < m_iAmmo2)
		{
			// ammo has decreased
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Ammo2Decreased");
		}
		else
		{
			// ammunition has increased
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("Ammo2Increased");
		}

		m_iAmmo2 = ammo2;
	}

	SetSecondaryValue(ammo2);

	//const ConVar *wAmmo = cvar->FindVar( "cl_tf2_currentammo2" );
	//wchar_t unicode[6];
	//swprintf(unicode, L"%d", wAmmo->GetInt() );
	//SetLabelText( unicode );
	//const ConVar *wAmmo = cvar->FindVar( "cl_tf2_currentammo2" );
	//Msg( "Set ammo to %i\n", wAmmo->GetInt() );
}

//-----------------------------------------------------------------------------
// Purpose: Displays the secondary ammunition level
//-----------------------------------------------------------------------------
class CHudSecondaryAmmo : public CHudNumericDisplay, public CHudElement
{
	DECLARE_CLASS_SIMPLE( CHudSecondaryAmmo, CHudNumericDisplay );

public:
	CHudSecondaryAmmo( const char *pElementName ) : BaseClass( NULL, "HudAmmoSecondary" ), CHudElement( pElementName )
	{
		m_iAmmo = -1;
	}

	void Init( void )
	{
	}

	void VidInit( void )
	{
	}

	void SetAmmo( int ammo )
	{
		if (ammo != m_iAmmo)
		{
			if (ammo == 0)
			{
				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("AmmoSecondaryEmpty");
			}
			else if (ammo < m_iAmmo)
			{
				// ammo has decreased
				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("AmmoSecondaryDecreased");
			}
			else
			{
				// ammunition has increased
				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("AmmoSecondaryIncreased");
			}

			m_iAmmo = ammo;
		}
		SetDisplayValue( ammo );
	}

protected:
	virtual void OnThink( void )
	{
		// set whether or not the panel draws based on if we have a weapon that supports secondary ammo
		C_BaseCombatWeapon *wpn = GetActiveWeapon();
		C_BasePlayer *player = C_BasePlayer::GetLocalPlayer();
		if (!wpn || !player)
		{
			m_hCurrentActiveWeapon = NULL;
			SetPaintEnabled(false);
			SetPaintBackgroundEnabled(false);
			return;
		}
		else
		{
			SetPaintEnabled(true);
			SetPaintBackgroundEnabled(true);
		}

		if (wpn->UsesSecondaryAmmo())
		{
			SetAmmo(player->GetAmmoCount(wpn->GetSecondaryAmmoType()));
		}

		if ( m_hCurrentActiveWeapon != wpn )
		{
			bool restored = false;
			if ( GetGameRestored() )
			{
				SetGameRestored( false );
				restored = true;
			}

			if ( wpn->UsesSecondaryAmmo() )
			{
				// we've changed to a weapon that uses secondary ammo
				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence(
					restored ?
					"WeaponUsesSecondaryAmmoRestore" : 
					"WeaponUsesSecondaryAmmo");
			}
			else 
			{
				// we've changed away from a weapon that uses secondary ammo
				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence(
					restored ?
					"WeaponDoesNotUseSecondaryAmmoRestore" :
					"WeaponDoesNotUseSecondaryAmmo" );
			}
			m_hCurrentActiveWeapon = wpn;
		}
	}
	
private:
	CHandle< C_BaseCombatWeapon > m_hCurrentActiveWeapon;
	int		m_iAmmo;
};

DECLARE_HUDELEMENT( CHudSecondaryAmmo );


