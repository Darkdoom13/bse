//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: TF2's Hud health display
//
// $NoKeywords: $
//=============================================================================

#include "cbase.h"
#include "hud.h"
#include "hud_macros.h"
#include "view.h"
#include "parsemsg.h"

#include "hud_numeric.h"
#include "basetfvehicle.h"

#include "iclientmode.h"

#define PAIN_NAME "sprites/%d_pain.vmt"

#include <KeyValues.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui_controls/AnimationController.h>

extern void HudDamageIndicator_MsgFunc_Damage( const char *pszName, int iSize, void *pbuf );

using namespace vgui;

#include "hudelement.h"
#include "hud_numericdisplay.h"

#include "tier1/convar.h"

#include "tier0/memdbgon.h"

#define INIT_HEALTH -1

//-----------------------------------------------------------------------------
// Purpose: Health panel
//-----------------------------------------------------------------------------
class CHudHealth : public CHudElement, public CHudNumericDisplay
{
	DECLARE_CLASS_SIMPLE( CHudHealth, CHudNumericDisplay );

public:
	CHudHealth( const char *pElementName );
	virtual void Init( void );
	virtual void VidInit( void );
	virtual void Reset( void );
	virtual void			OnThink();
	void MsgFunc_Health(const char *pszName, int iSize, void *pbuf);

private:
	// old variables
	int		m_iHealth;
	
	float	m_fFade;
	int		m_bitsDamage;
	int		m_iGhostHealth;
};	

DECLARE_HUDELEMENT( CHudHealth );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudHealth::CHudHealth( const char *pElementName ) : CHudElement( pElementName ), CHudNumericDisplay(NULL, "HudHealth")
{

}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHealth::Init()
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHealth::Reset()
{
	m_iHealth		= INIT_HEALTH;
	m_iGhostHealth	= 100;
	m_fFade			= 0;
	m_bitsDamage	= 0;

	SetLabelText(L"HEALTH");
	SetDisplayValue(m_iHealth);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHealth::VidInit()
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudHealth::OnThink()
{
	int x = 0;
	C_BasePlayer *local = C_BasePlayer::GetLocalPlayer();
	if ( local )
	{
		// Never below zero
		x = max( local->GetHealth(), 0 );
	}

	// Only update the fade if we've changed health
	if ( x == m_iHealth )
	{
		return;
	}

	m_iGhostHealth = m_iHealth;
	m_fFade = 200;
	m_iHealth = x;

	bool restored = GetGameRestored();
	if ( restored )
	{
		SetGameRestored( false );
	}

	if ( m_iHealth >= 20 )
	{
		// Don't flash on save/load restoration
		if ( !restored )
		{
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("HealthIncreasedAbove20");
		}
	}
	else
	{
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("HealthIncreasedBelow20");
	}

	SetDisplayValue(m_iHealth);
}


//-----------------------------------------------------------------------------
// Purpose: Local player's vehicle health, if in one
//-----------------------------------------------------------------------------
class CHudVehicleHealth : public CHudNumeric
{
	DECLARE_CLASS_SIMPLE( CHudVehicleHealth, CHudNumeric );
	
public:
	CHudVehicleHealth( const char *pElementName );
	
	virtual const char *GetLabelText() { return m_szVehicleHealthLabel; }
	virtual const char *GetPulseEvent( bool increment ) { return increment ? "HealthVehicleIncrement" : "HealthVehicleDecrement"; }
	virtual bool		GetValue( char *val, int maxlen );
	
	virtual Color GetColor();
	virtual Color GetBoxColor();
	
private:
	bool				GetHealth( int& value );
	
	CPanelAnimationVar( float, m_flLingerTime, "exit_vehicle_linger_time", "2.0" );
	
	bool				m_bPrevHealth;
	float				m_flLingerFinish;
	int					m_nLastHealth;
	
	CPanelAnimationStringVar( 128, m_szVehicleHealthLabel, "VehicleHealthLabel", "Vehicle Health" );
};

DECLARE_HUDELEMENT( CHudVehicleHealth );

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
CHudVehicleHealth::CHudVehicleHealth( const char *pElementName ) :
CHudNumeric( pElementName, "HudVehicleHealth" )
{
	m_bPrevHealth = false;
	m_flLingerFinish = 0.0f;
	m_flLingerTime = 2.0f;
	m_nLastHealth = 0;
}

bool CHudVehicleHealth::GetHealth( int& value )
{
	C_BaseTFPlayer *pPlayer = C_BaseTFPlayer::GetLocalPlayer();
	if ( pPlayer )
	{
		// Draw the vehicle health:
		C_BaseTFVehicle *pVehicleEnt = ( C_BaseTFVehicle* )pPlayer->GetVehicle();
		if( pVehicleEnt )
		{
			value = pVehicleEnt->GetHealth();
			m_nLastHealth = value;
			bool changed = m_bPrevHealth != true;
			
			if ( changed )
			{
				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence(  "HealthVehicleEnterVehicle" );
			}
			
			m_bPrevHealth = true;
			return true;
		}
	}
	
	bool changed = m_bPrevHealth != false;
	if ( changed )
	{
		g_pClientMode->GetViewportAnimationController()->StartAnimationSequence( "HealthVehicleExitVehicle" );
		m_flLingerFinish = gpGlobals->curtime + m_flLingerTime;
	}
	
	m_bPrevHealth = false;
	
	if ( gpGlobals->curtime < m_flLingerFinish )
	{
		value = m_nLastHealth;
		return true;
	}
	
	m_flLingerFinish = 0.0f;
	return false;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : value - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CHudVehicleHealth::GetValue( char *val, int maxlen )
{
	int value = 0;
	bool show = GetHealth( value );
	if ( !show )
	{
		return false;
	}
	
	Q_snprintf( val, maxlen,  "%i", value );
	return true;
}

Color CHudVehicleHealth::GetColor()
{
	int value = 0;
	if ( !GetHealth( value ) || value > 75 )
		return m_TextColor;
	
	return m_TextColorCritical;
}

Color CHudVehicleHealth::GetBoxColor()
{
	int value = 0;
	if ( !GetHealth( value ) || value > 75 )
		return m_BoxColor;
	return m_BoxColorCritical;
}









//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
/*

void CHudHealth::MsgFunc_Damage(const char *pszName, int iSize, void *pbuf )
{
	BEGIN_READ( pbuf, iSize );

	int armor = READ_BYTE();	// armor
	int damageTaken = READ_BYTE();	// health
	long bitsDamage = READ_LONG(); // damage bits
	bitsDamage; // variable still sent but not used

	Vector vecFrom;

	vecFrom.x = READ_COORD();
	vecFrom.y = READ_COORD();
	vecFrom.z = READ_COORD();

	// Actually took damage?
	if ( damageTaken > 0 || armor > 0 )
	{
		if ( damageTaken > 0 )
		{
			// start the animation
			g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("HealthDamageTaken");

			// see if our health is low
//			if ( m_iHealth < 20 )
//			{
//				g_pClientMode->GetViewportAnimationController()->StartAnimationSequence("HealthLow");
//			}
		}
	}

	// notify damage indicator of damage
	HudDamageIndicator_MsgFunc_Damage( pszName, iSize, pbuf );
}

*/

