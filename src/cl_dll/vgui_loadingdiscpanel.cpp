//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $Workfile:     $
// $Date:         $
// $NoKeywords: $
//=============================================================================//
#include "cbase.h"
#include "iloadingdisc.h"
#include "vgui_BasePanel.h"
#include "vgui_controls/Frame.h"
#include "vgui_controls/Label.h"
#include "vgui/ISurface.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#if 0
//-----------------------------------------------------------------------------
// Purpose: Displays the loading plaque
//-----------------------------------------------------------------------------
class CLoadingDiscPanel : public vgui::Panel
{
	typedef vgui::Panel BaseClass;
public:
					CLoadingDiscPanel( vgui::VPANEL parent );
	virtual			~CLoadingDiscPanel( void );

	virtual void	Paint();

private:
	int				m_nTextureID;
};

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *parent - 
//-----------------------------------------------------------------------------
CLoadingDiscPanel::CLoadingDiscPanel( vgui::VPANEL parent ) : 
	BaseClass( NULL, "CLoadingDiscPanel" )
{
	SetParent( parent );
	SetVisible( false );
	SetCursor( null );

	SetFgColor( Color( 0, 0, 0, 255 ) );
	SetPaintBackgroundEnabled( false );

	m_nTextureID = vgui::surface()->CreateNewTextureID();
	
	//if we have not uploaded yet, lets go ahead and do so
	vgui::surface()->DrawSetTextureFile(m_nTextureID,( const char * )"console/loading",true, true);
	int wide, tall;
	vgui::surface()->DrawGetTextureSize( m_nTextureID, wide, tall );

	SetSize( wide, tall );
	SetPos( ( ScreenWidth() - wide ) / 2, ( ScreenHeight() - tall ) / 2 ); 

}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
CLoadingDiscPanel::~CLoadingDiscPanel( void )
{
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CLoadingDiscPanel::Paint() 
{
	int r,g,b,a;
	r = 255;
	g = 255;
	b = 255;
	a = 255;

	//set the texture current, set the color, and draw the biatch
	vgui::surface()->DrawSetTexture(m_nTextureID);
	vgui::surface()->DrawSetColor(r,g,b,a);

	int wide, tall;
	GetSize( wide, tall );
	vgui::surface()->DrawTexturedRect(0, 0, wide, tall );
}

class CLoadingDisc : public ILoadingDisc
{
private:
	CLoadingDiscPanel *loadingDiscPanel;
public:
	CLoadingDisc( void )
	{
		loadingDiscPanel = NULL;
	}

	void Create( vgui::VPANEL parent )
	{
		loadingDiscPanel = new CLoadingDiscPanel( parent );
	}

	void Destroy( void )
	{
		if ( loadingDiscPanel )
		{
			loadingDiscPanel->SetParent( (vgui::Panel *)NULL );
			delete loadingDiscPanel;
		}
	}

	void SetVisible( bool bVisible )
	{
		loadingDiscPanel->SetVisible( bVisible );
	}

};
#else
//-----------------------------------------------------------------------------
// Purpose: Displays the loading plaque
//-----------------------------------------------------------------------------
class CLoadingDiscPanel : public vgui::EditablePanel
{
	typedef vgui::EditablePanel BaseClass;
public:
	CLoadingDiscPanel( vgui::VPANEL parent );
	~CLoadingDiscPanel();

	virtual void ApplySchemeSettings( vgui::IScheme *pScheme )
	{
		BaseClass::ApplySchemeSettings( pScheme );
	}

	virtual void PaintBackground()
	{
		SetBgColor( Color(0, 0, 0, 128) );
		SetPaintBackgroundType( 2 );
		BaseClass::PaintBackground();
	}

	virtual void SetText( const char *text )
	{
		m_pLoadingLabel->SetText( text );
	}

private:
	vgui::Label *m_pLoadingLabel;
};

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CLoadingDiscPanel::CLoadingDiscPanel( vgui::VPANEL parent ) : BaseClass( NULL, "CLoadingDiscPanel" )
{
	SetParent( parent );
	SetProportional( true );
	SetScheme( "ClientScheme" );
	SetVisible( false );
	SetCursor( NULL );

	m_pLoadingLabel = vgui::SETUP_PANEL(new vgui::Label( this, "LoadingLabel", "" ));
	m_pLoadingLabel->SetPaintBackgroundEnabled( false );

	LoadControlSettings( "resource/LoadingDiscPanel.res" );

	// center the dialog
	int wide, tall;
	GetSize( wide, tall );
	SetPos( ( ScreenWidth() - wide ) / 2, ( ScreenHeight() - tall ) / 2 );
}

//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
CLoadingDiscPanel::~CLoadingDiscPanel()
{
}

class CLoadingDisc : public ILoadingDisc
{
private:
	CLoadingDiscPanel *loadingDiscPanel;
	vgui::VPANEL m_hParent;

public:
	CLoadingDisc( void )
	{
		loadingDiscPanel = NULL;
	}

	void Create( vgui::VPANEL parent )
	{
		// don't create now, only when it's needed
		m_hParent = parent;
	}

	void Destroy( void )
	{
		if ( loadingDiscPanel )
		{
			loadingDiscPanel->SetParent( (vgui::Panel *)NULL );
			delete loadingDiscPanel;
		}
	}

	void SetVisible( bool bVisible )
	{
		// demand-create the dialog
		if ( bVisible && !loadingDiscPanel )
		{
			loadingDiscPanel = vgui::SETUP_PANEL(new CLoadingDiscPanel( m_hParent ) );
		}

		if ( loadingDiscPanel )
		{
			loadingDiscPanel->SetVisible( bVisible );
		}
	}
};
#endif

static CLoadingDisc g_LoadingDisc;
ILoadingDisc *loadingdisc = ( ILoadingDisc * )&g_LoadingDisc;