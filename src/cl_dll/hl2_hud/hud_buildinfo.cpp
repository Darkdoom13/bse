//========= Copyright � 1996-2003, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
//=============================================================================

#include "cbase.h"
#include "hudelement.h"
#include "hud_numericdisplay.h"
#include <vgui_controls/Panel.h>
#include "hud.h"
#include "parsemsg.h"
#include "hud_suitpower.h"
#include "hud_macros.h"
#include "iclientmode.h"
#include <vgui_controls/AnimationController.h>
#include <vgui/ISurface.h>
#include <vgui/ILocalize.h>

#if 0

//-----------------------------------------------------------------------------
// Purpose: Shows the build info
//-----------------------------------------------------------------------------
class CHudBuildInfo : public CHudElement, public vgui::Panel
{
	DECLARE_CLASS_SIMPLE( CHudBuildInfo, vgui::Panel );

public:
	CHudBuildInfo( const char *pElementName );
	virtual void Init( void );

protected:
	virtual void Paint();

private:
	CPanelAnimationVar( vgui::HFont, m_hTextFont, "TextFont", "DefaultSmall" );
	CPanelAnimationVar( Color, m_TextColor, "TextColor", "FgColor" );
	CPanelAnimationVarAliasType( float, text_xpos, "text_xpos", "8", "proportional_float" );
	CPanelAnimationVarAliasType( float, text_ypos, "text_ypos", "20", "proportional_float" );
};	

using namespace vgui;

DECLARE_HUDELEMENT( CHudBuildInfo );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudBuildInfo::CHudBuildInfo( const char *pElementName ) : CHudElement( pElementName ), BaseClass( NULL, "HudBuildInfo" )
{
	vgui::Panel *pParent = g_pClientMode->GetViewport();
	SetParent( pParent );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudBuildInfo::Init()
{
}

//-----------------------------------------------------------------------------
// Purpose: draws the flashlight icon
//-----------------------------------------------------------------------------
void CHudBuildInfo::Paint()
{
	C_BasePlayer *pPlayer = C_BasePlayer::GetLocalPlayer();
	if ( !pPlayer )
		return;

	// draw build info
#if 0
	char buildString[256];
	char gameString[128];
	wchar_t *unicodeString;

#ifdef HL1_CLIENT_DLL
	Q_snprintf(gameString, sizeof(gameString), "Half Life Internal Build");
#elif HL2_CLIENT_DLL
	Q_snprintf(gameString, sizeof(gameString), "Half Life 2 Internal Build");
#elif TF2_CLIENT_DLL
	Q_snprintf(gameString, sizeof(gameString), "Team Fortress 2 Internal Build");
#elif CSTRIKE_DLL
	Q_snprintf(gameString, sizeof(gameString), "Counter Strike Internal Build");
#else
#error WTF
#endif

	Q_snprintf(buildString, sizeof(buildString), "Beta Source Engine\n%s\n built %s at %s", gameString, __DATE__, __TIME__);

	vgui::localize()->ConvertANSIToUnicode( gameString, unicodeString, sizeof(unicodeString) );

	surface()->DrawSetTextFont(m_hTextFont);
	surface()->DrawSetTextColor(m_TextColor);
	surface()->DrawSetTextPos(text_xpos, text_ypos);
	surface()->DrawPrintText(unicodeString, wcslen(unicodeString));
#else
	wchar_t *text = L"BSE - Jan '09 build";
	surface()->DrawSetTextFont(m_hTextFont);
	surface()->DrawSetTextColor(m_TextColor);
	surface()->DrawSetTextPos(text_xpos, text_ypos);
	for (wchar_t *wch = text; *wch != 0; wch++)
	{
		surface()->DrawUnicodeChar(*wch);
	}
#endif
}

#endif
