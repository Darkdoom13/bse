//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef IADMINSERVER_H
#define IADMINSERVER_H
#ifdef _WIN32
#pragma once
#endif

#include "tier1/interface.h"

// handle to a game window
typedef unsigned int GameHandle_t;

//-----------------------------------------------------------------------------
// Purpose: Interface to server adminserver module
//-----------------------------------------------------------------------------
class IAdminServer
{
public:
	// activates the admin server window, brings it to the foreground
	virtual bool Activate() = 0;

	// opens a game info dialog to watch the specified server; associated with the friend 'userName'
	virtual GameHandle_t OpenGameInfoDialog(unsigned int gameIP, unsigned int gamePort, const char *userName);

	// joins a specified game - game info dialog will only be opened if the server is fully or passworded
	virtual GameHandle_t JoinGame(unsigned int gameIP, unsigned int gamePort, const char *userName);

	// changes the game info dialog to watch a new server; normally used when a friend changes games
	virtual void UpdateGameInfoDialog(GameHandle_t gameDialog, unsigned int gameIP, unsigned int gamePort);

	// forces the game info dialog closed
	virtual void CloseGameInfoDialog(GameHandle_t gameDialog);
};

#define ADMINSERVER_INTERFACE_VERSION "AdminServer001"



#endif // IADMINSERVER_H
