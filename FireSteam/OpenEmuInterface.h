#pragma once

#include ".\SteamInterface\SteamInterface003.h"
#include ".\SteamInterface\SteamInterface004.h"
#include ".\SteamInterface\SteamInterface005.h"
#include ".\SteamInterface\SteamInterface006.h"
#include ".\SteamDLLAppsystem\SteamDLLAppsystem001.h"

OPENEMU_API unsigned int OPENEMU_CALL CreateInterface(const char* cszSteamDLLAppsystemInterfaceVersion, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("CreateInterface SteamDLLAppsystem version: %s\n", cszSteamDLLAppsystemInterfaceVersion);
#endif
	static CSteamDLLAppsystem001 SteamDLLAppsystem001;

	if(cszSteamDLLAppsystemInterfaceVersion != NULL)
	{
		if(strcmp(cszSteamDLLAppsystemInterfaceVersion,"SteamDLLAppsystem001") == 0)
		{
			SteamClearError(pError);
			return (unsigned int)&SteamDLLAppsystem001;
		}
	}
	return 0;
}

OPENEMU_API unsigned int OPENEMU_CALL _f(const char* cszSteamInterfaceVersion)
{
#ifdef DEBUG
	Log->Write("_f SteamInterface version: %s\n", cszSteamInterfaceVersion);
#endif

	static CSteamInterface003 SteamInterface003;
	static CSteamInterface004 SteamInterface004;
	static CSteamInterface005 SteamInterface005;
	static CSteamInterface006 SteamInterface006;
	
	if(cszSteamInterfaceVersion != NULL)
	{
		if(strcmp(cszSteamInterfaceVersion,"Steam003") == 0)
		{
			return (unsigned int)&SteamInterface003;
		}
		else if(strcmp(cszSteamInterfaceVersion,"Steam004") == 0)
		{
			return (unsigned int)&SteamInterface004;
		}
		else if(strcmp(cszSteamInterfaceVersion,"Steam005") == 0)
		{
			return (unsigned int)&SteamInterface005;
		}
		else if(strcmp(cszSteamInterfaceVersion,"Steam006") == 0)
		{
			return (unsigned int)&SteamInterface006;
		}
	}
	return 0;
}