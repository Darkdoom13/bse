#pragma once

/*
** Asynchrounous call handling
*/

OPENEMU_API int OPENEMU_CALL SteamProcessCall(SteamCallHandle_t handle, TSteamProgress *pProgress, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamProcessCall\n");
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamAbortCall(SteamCallHandle_t handle, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamAbortCall\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamBlockingCall(SteamCallHandle_t handle, unsigned int uiProcessTickMS, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamBlockingCall\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamSetMaxStallCount(unsigned int uNumStalls, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamSetMaxStallCount\n");
#endif
	return 1;
}