/*
 * HLLib
 * Copyright (C) 2006 Ryan Gregg

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later
 * version.
 */

//#include "HLLib.h"
#include "StreamMapping.h"

using namespace HLLib;
using namespace HLLib::Mapping;

CStreamMapping::CStreamMapping(Streams::IStream &Stream) : Stream(Stream)
{
	this->Stream.Close();
}

CStreamMapping::~CStreamMapping()
{
	this->Close();
}

HLMappingType CStreamMapping::GetType() const
{
	return HL_MAPPING_STREAM;
}


const Streams::IStream& CStreamMapping::GetStream() const
{
	return this->Stream;
}

const hlChar *CStreamMapping::GetFileName() const
{
	return this->Stream.GetFileName();
}

hlBool CStreamMapping::GetOpened() const
{
	return this->Stream.GetOpened();
}

hlUInt CStreamMapping::GetMode() const
{
	return this->Stream.GetMode();
}

hlBool CStreamMapping::OpenInternal(hlUInt uiMode)
{
	assert(!this->GetOpened());

	if((uiMode & HL_MODE_READ) == 0 && (uiMode & HL_MODE_WRITE) == 0)
	{
//		LastError.SetErrorMessageFormated("Invalid open mode (%#.8x).", uiMode);
		return hlFalse;
	}

	return this->Stream.Open(uiMode);
}

hlVoid CStreamMapping::CloseInternal()
{
	this->Stream.Close();
}

hlUInt CStreamMapping::GetMappingSize() const
{
	return this->Stream.GetStreamSize();
}

hlBool CStreamMapping::MapInternal(CView *&pView, hlUInt uiOffset, hlUInt uiLength)
{
	assert(this->GetOpened());

	if(uiOffset + uiLength > this->Stream.GetStreamSize())
	{
//		LastError.SetErrorMessageFormated("Requested view (%u, %u) does not fit inside mapping, (%u, %u).", uiOffset, uiLength, 0, this->Stream.GetStreamSize());
		return hlFalse;
	}

	if(Stream.Seek((hlLong)uiOffset, HL_SEEK_BEGINNING) != uiOffset)
	{
		return hlFalse;
	}

	hlByte *lpData = new hlByte[uiLength];

	if(Stream.Read(lpData, uiLength) != uiLength)
	{
		delete []lpData;
		return hlFalse;
	}

	pView = new CView(this, lpData, uiOffset, uiLength);

	return hlTrue;
}

hlVoid CStreamMapping::UnmapInternal(CView &View)
{
	assert(this->GetOpened());
	assert(View.GetMapping() == this);

	delete [](hlByte *)View.GetAllocationView();
}

hlBool CStreamMapping::CommitInternal(CView &View, hlUInt uiOffset, hlUInt uiLength)
{
	assert(this->GetOpened());

	hlUInt uiFileOffset = View.GetAllocationOffset() + View.GetOffset() + uiOffset;

	if(Stream.Seek((hlLong)uiFileOffset, HL_SEEK_BEGINNING) != uiFileOffset)
	{
		return hlFalse;
	}

	if(Stream.Write((const hlByte *)View.GetView() + uiOffset, uiLength) != uiLength)
	{
		return hlFalse;
	}

	return hlTrue;
}
