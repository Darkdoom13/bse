/*
 * HLLib
 * Copyright (C) 2006 Ryan Gregg

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your Option) any later
 * version.
 */
#pragma warning(disable:4996)//disable deprecated warnings

#include "HLLib.h"

using namespace HLLib;

namespace HLLib
{
	POpenProc pOpenProc = 0;
	PCloseProc pCloseProc = 0;
	PReadProc pReadProc = 0;
	PWriteProc pWriteProc = 0;
	PSeekProc pSeekProc = 0;
	PTellProc pTellProc = 0;
	PSizeProc pSizeProc = 0;

	PExtractItemStartProc pExtractItemStartProc = 0;
	PExtractItemEndProc pExtractItemEndProc = 0;
	PExtractFileProgressProc pExtractFileProgressProc = 0;
	PValidateFileProgressProc pValidateFileProgressProc = 0;
	PDefragmentProgressProc pDefragmentProgressProc = 0;

	hlBool bOverwriteFiles = hlTrue;
	hlBool bReadEncrypted = hlTrue;
	hlBool bForceDefragment = hlFalse;
}

hlBool hlAttributeGetBoolean(HLAttribute *pAttribute)
{
	if(pAttribute->eAttributeType != HL_ATTRIBUTE_BOOLEAN)
	{
		return hlFalse;
	}

	return pAttribute->Value.Boolean.bValue;
}

hlVoid hlAttributeSetBoolean(HLAttribute *pAttribute, const hlChar *lpName, hlBool bValue)
{
	pAttribute->eAttributeType = HL_ATTRIBUTE_BOOLEAN;
	if(lpName != 0)
	{
		strncpy(pAttribute->lpName, lpName, sizeof(pAttribute->lpName));
		pAttribute->lpName[sizeof(pAttribute->lpName) - 1] = '\0';
	}
	pAttribute->Value.Boolean.bValue = bValue;
}

hlInt hlAttributeGetInteger(HLAttribute *pAttribute)
{
	if(pAttribute->eAttributeType != HL_ATTRIBUTE_INTEGER)
	{
		return 0;
	}

	return pAttribute->Value.Integer.iValue;
}

hlVoid hlAttributeSetInteger(HLAttribute *pAttribute, const hlChar *lpName, hlInt iValue)
{
	pAttribute->eAttributeType = HL_ATTRIBUTE_INTEGER;
	if(lpName != 0)
	{
		strncpy(pAttribute->lpName, lpName, sizeof(pAttribute->lpName));
		pAttribute->lpName[sizeof(pAttribute->lpName) - 1] = '\0';
	}
	pAttribute->Value.Integer.iValue = iValue;
}

hlUInt hlAttributeGetUnsignedInteger(HLAttribute *pAttribute)
{
	if(pAttribute->eAttributeType != HL_ATTRIBUTE_UNSIGNED_INTEGER)
	{
		return 0;
	}

	return pAttribute->Value.UnsignedInteger.uiValue;
}

hlVoid hlAttributeSetUnsignedInteger(HLAttribute *pAttribute, const hlChar *lpName, hlUInt uiValue, hlBool bHexadecimal)
{
	pAttribute->eAttributeType = HL_ATTRIBUTE_UNSIGNED_INTEGER;
	if(lpName != 0)
	{
		strncpy(pAttribute->lpName, lpName, sizeof(pAttribute->lpName));
		pAttribute->lpName[sizeof(pAttribute->lpName) - 1] = '\0';
	}
	pAttribute->Value.UnsignedInteger.uiValue = uiValue;
	pAttribute->Value.UnsignedInteger.bHexadecimal = bHexadecimal;
}

hlFloat hlAttributeGetFloat(HLAttribute *pAttribute)
{
	if(pAttribute->eAttributeType != HL_ATTRIBUTE_FLOAT)
	{
		return 0.0f;
	}

	return pAttribute->Value.Float.fValue;
}

hlVoid hlAttributeSetFloat(HLAttribute *pAttribute, const hlChar *lpName, hlFloat fValue)
{
	pAttribute->eAttributeType = HL_ATTRIBUTE_FLOAT;
	if(lpName != 0)
	{
		strncpy(pAttribute->lpName, lpName, sizeof(pAttribute->lpName));
		pAttribute->lpName[sizeof(pAttribute->lpName) - 1] = '\0';
	}
	pAttribute->Value.Float.fValue = fValue;
}

const hlChar *hlAttributeGetString(HLAttribute *pAttribute)
{
	if(pAttribute->eAttributeType != HL_ATTRIBUTE_STRING)
	{
		return "";
	}

	return pAttribute->Value.String.lpValue;
}

hlVoid hlAttributeSetString(HLAttribute *pAttribute, const hlChar *lpName, const hlChar *lpValue)
{
	pAttribute->eAttributeType = HL_ATTRIBUTE_STRING;
	if(lpName != 0)
	{
		strncpy(pAttribute->lpName, lpName, sizeof(pAttribute->lpName));
		pAttribute->lpName[sizeof(pAttribute->lpName) - 1] = '\0';
	}
	if(lpValue != 0)
	{
		strncpy(pAttribute->Value.String.lpValue, lpValue, sizeof(pAttribute->Value.String.lpValue));
		pAttribute->Value.String.lpValue[sizeof(pAttribute->Value.String.lpValue) - 1] = '\0';
	}
	else
	{
		*pAttribute->Value.String.lpValue = '\0';
	}
}


