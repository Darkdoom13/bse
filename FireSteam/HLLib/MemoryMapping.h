/*
 * HLLib
 * Copyright (C) 2006 Ryan Gregg

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later
 * version.
 */

#ifndef MEMORYMAPPING_H
#define MEMORYMAPPING_H

#include "stdafx.h"
#include "Mapping.h"

namespace HLLib
{
	namespace Mapping
	{
		class CMemoryMapping : public CMapping
		{
		private:
			hlBool bOpened;
			hlUInt uiMode;

			hlVoid *lpData;
			hlUInt uiBufferSize;

		public:
			CMemoryMapping(hlVoid *lpData, hlUInt uiBufferSize);
			virtual ~CMemoryMapping();

			virtual HLMappingType GetType() const;

			const hlVoid *GetBuffer() const;
			hlUInt GetBufferSize() const;

			virtual hlBool GetOpened() const;
			virtual hlUInt GetMode() const;

			virtual hlUInt GetMappingSize() const;

		private:
			virtual hlBool OpenInternal(hlUInt uiMode);
			virtual hlVoid CloseInternal();

			virtual hlBool MapInternal(CView *&pView, hlUInt uiOffset, hlUInt uiLength);
		};
	}
}

#endif
