/*
 * HLLib
 * Copyright (C) 2006 Ryan Gregg

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later
 * version.
 */

#ifndef MAPPING_H
#define MAPPING_H

#include "stdafx.h"

namespace HLLib
{
	namespace Mapping
	{
		class CMapping;

		class CView
		{
			friend class CMapping;

		private:
			CMapping *pMapping;

			hlVoid *lpView;
			hlUInt uiOffset;
			hlUInt uiLength;
			hlUInt uiAllocationOffset;
			hlUInt uiAllocationLength;

		public:
			CView(CMapping *pMapping, hlVoid *lpView, hlUInt uiAllocationOffset, hlUInt uiAllocationLength, hlUInt uiOffset = 0, hlUInt uiLength = 0);
			~CView();

			CMapping *GetMapping() const;

			const hlVoid *GetView() const;
			hlUInt GetOffset() const;
			hlUInt GetLength() const;
			const hlVoid *GetAllocationView() const;
			hlUInt GetAllocationOffset() const;
			hlUInt GetAllocationLength() const;
		};

		typedef std::list<CView *> CViewList;

		class CMapping
		{
		private:
			CViewList *pViews;

		public:
			CMapping();
			virtual ~CMapping();

			virtual HLMappingType GetType() const = 0;

			virtual const hlChar *GetFileName() const;

			virtual hlBool GetOpened() const = 0;
			virtual hlUInt GetMode() const = 0;

			hlUInt GetTotalAllocations() const;
			hlUInt GetTotalMemoryAllocated() const;
			hlUInt GetTotalMemoryUsed() const;

			hlBool Open(hlUInt uiMode);
			hlVoid Close();

			virtual hlUInt GetMappingSize() const = 0;

			hlBool Map(CView *&pView, hlUInt uiOffset, hlUInt uiLength);
			hlBool Unmap(CView *&pView);

			hlBool Commit(CView &View);
			hlBool Commit(CView &View, hlUInt uiOffset, hlUInt uiLength);

		private:
			virtual hlBool OpenInternal(hlUInt uiMode) = 0;
			virtual hlVoid CloseInternal() = 0;

			virtual hlBool MapInternal(CView *&pView, hlUInt uiOffset, hlUInt uiLength) = 0;
			virtual hlVoid UnmapInternal(CView &View);

			virtual hlBool CommitInternal(CView &View, hlUInt uiOffset, hlUInt uiLength);
		};
	}
}

#endif
