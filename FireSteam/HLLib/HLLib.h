/*
 * HLLib
 * Copyright (C) 2006 Ryan Gregg

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your Option) any later
 * version.
 */

#ifndef HLLIB_H
#define HLLIB_H

#include "stdafx.h"

namespace HLLib
{
	extern hlBool bInitialized;

	extern POpenProc pOpenProc;
	extern PCloseProc pCloseProc;
	extern PReadProc pReadProc;
	extern PWriteProc pWriteProc;
	extern PSeekProc pSeekProc;
	extern PTellProc pTellProc;
	extern PSizeProc pSizeProc;

	extern PExtractItemStartProc pExtractItemStartProc;
	extern PExtractItemEndProc pExtractItemEndProc;
	extern PExtractFileProgressProc pExtractFileProgressProc;
	extern PValidateFileProgressProc pValidateFileProgressProc;
	extern PDefragmentProgressProc pDefragmentProgressProc;

	extern hlBool bOverwriteFiles;
	extern hlBool bReadEncrypted;
	extern hlBool bForceDefragment;
}

#ifdef __cplusplus
extern "C" {
#endif
//
// Attributes
//

hlBool hlAttributeGetBoolean(HLAttribute *pAttribute);
hlVoid hlAttributeSetBoolean(HLAttribute *pAttribute, const hlChar *lpName, hlBool bValue);

hlInt hlAttributeGetInteger(HLAttribute *pAttribute);
hlVoid hlAttributeSetInteger(HLAttribute *pAttribute, const hlChar *lpName, hlInt iValue);

hlUInt hlAttributeGetUnsignedInteger(HLAttribute *pAttribute);
hlVoid hlAttributeSetUnsignedInteger(HLAttribute *pAttribute, const hlChar *lpName, hlUInt uiValue, hlBool bHexadecimal);

hlFloat hlAttributeGetFloat(HLAttribute *pAttribute);
hlVoid hlAttributeSetFloat(HLAttribute *pAttribute, const hlChar *lpName, hlFloat fValue);

const hlChar *hlAttributeGetString(HLAttribute *pAttribute);
hlVoid hlAttributeSetString(HLAttribute *pAttribute, const hlChar *lpName, const hlChar *lpValue);

#ifdef __cplusplus
}
#endif

#endif
