#pragma once

/*
*	Account
*/

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamCreateAccount(const char *cszUser, const char *cszPassphrase, const char *cszCreationKey, const char *cszPersonalQuestion, const char *cszAnswerToQuestion, int *pbCreated, unsigned int uUnknown, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamCreateAccount\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamDeleteAccount(TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamDeleteAccount\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamChangeAccountName()
{
#ifdef DEBUG
	Log->Write("SteamChangeAccountName\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamChangeEmailAddress(const char *cszNewEmailAddress, int *pbChanged, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamChangeEmailAddress\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamChangeForgottenPassword()
{
#ifdef DEBUG
	Log->Write("SteamChangeForgottenPassword\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamChangePassword(const char *cszCurrentPassphrase, const char *cszNewPassphrase, int *pbChanged, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamChangePassword\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamChangePersonalQA(const char *cszCurrentPassphrase, const char *cszNewPersonalQuestion, const char *cszNewAnswerToQuestion, int *pbChanged, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamChangePersonalQA\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamCheckAppOwnership()
{
#ifdef DEBUG
	Log->Write("SteamCheckAppOwnership\n");
#endif
	return 1;
}




OPENEMU_API int OPENEMU_CALL SteamDefragCaches()
{
#ifdef DEBUG
	Log->Write("SteamDefragCaches\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamFindFirst64()
{
#ifdef DEBUG
	Log->Write("SteamFindFirst64\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamFindNext64()
{
#ifdef DEBUG
	Log->Write("SteamFindNext64\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamGetCachePercentFragmentation()
{
#ifdef DEBUG
	Log->Write("SteamGetCachePercentFragmentation\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamIsFileNeededByApp()
{
#ifdef DEBUG
	Log->Write("SteamIsFileNeededByApp\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamLoadFileToApp()
{
#ifdef DEBUG
	Log->Write("SteamLoadFileToApp\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamMiniDumpInit()
{
#ifdef DEBUG
	Log->Write("SteamMiniDumpInit\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamOpenFile64()
{
#ifdef DEBUG
	Log->Write("SteamOpenFile64\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamRefreshAccountInfo2()
{
#ifdef DEBUG
	Log->Write("SteamRefreshAccountInfo2\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamSeekFile64()
{
#ifdef DEBUG
	Log->Write("SteamSeekFile64\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamSizeFile64()
{
#ifdef DEBUG
	Log->Write("SteamSizeFile64\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamStat64()
{
#ifdef DEBUG
	Log->Write("SteamStat64\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamTellFile64()
{
#ifdef DEBUG
	Log->Write("SteamTellFile64\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamWaitForAppResources()
{
#ifdef DEBUG
	Log->Write("SteamWaitForAppResources\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamWasBlobRegistryDeleted()
{
#ifdef DEBUG
	Log->Write("SteamWasBlobRegistryDeleted\n");
#endif
	return 1;
}


OPENEMU_API int OPENEMU_CALL SteamWriteMiniDumpWithAppID()
{
#ifdef DEBUG
	Log->Write("SteamWriteMiniDumpWithAppID\n");
#endif
	return 1;
}



OPENEMU_API int OPENEMU_CALL SteamEnumerateApp(unsigned int AppId, TSteamApp *pApp, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamEnumerateApp: %u\n", AppId);
#endif
	if(CAppRecord* AppRecord = CDR->GetAppRecordById(AppId))
	{
		strcpy(pApp->szName, AppRecord->Name);
		//pApp->szLatestVersionLabel;
		//pApp->szCurrentVersionLabel;
		strcpy(pApp->szCacheFile, AppRecord->InstallDirName);
		pApp->uId = AppRecord->AppId;
		pApp->uLatestVersionId = AppRecord->TrickleVersionId;
		pApp->uCurrentVersionId = AppRecord->CurrentVersionId;
		pApp->uMinCacheFileSizeMB = AppRecord->MinCacheFileSizeMB;
		pApp->uMaxCacheFileSizeMB = AppRecord->MaxCacheFileSizeMB;
		pApp->uNumLaunchOptions = AppRecord->LaunchOptionsRecord.size();
		pApp->uNumIcons = AppRecord->IconsRecord.size();
		pApp->uNumVersions = AppRecord->VersionsRecord.size();
		pApp->numDependencies = AppRecord->FilesystemsRecord.size();
		SteamClearError(pError);
		return 1;
	}
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamEnumerateAppDependency(unsigned int AppId, unsigned int uDependency, TSteamAppDependencyInfo *pDependencyInfo, TSteamError *pError)
{
	if(pDependencyInfo && pError)
	{
		if(CAppRecord* AppRecord = CDR->GetAppRecordById(AppId))
		{
#ifdef DEBUG
			Log->Write("SteamEnumerateAppDependency: %u %u\n", AppId, uDependency);
#endif
			pDependencyInfo->AppId = AppRecord->FilesystemsRecord[uDependency]->AppId;
			pDependencyInfo->IsRequired = (AppRecord->FilesystemsRecord[uDependency]->IsOptional ? 1 : 0);
			strcpy(pDependencyInfo->szMountName, AppRecord->FilesystemsRecord[uDependency]->MountName);
			SteamClearError(pError);
			return 1;
		}
	}
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamEnumerateAppIcon(unsigned int uAppId, unsigned int uIconIndex, unsigned char *pIconData, unsigned int uIconDataBufSize, unsigned int *puSizeOfIconData, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamEnumerateAppIcon\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamEnumerateAppLaunchOption(unsigned int uAppId, unsigned int uLaunchOptionIndex, TSteamAppLaunchOption *pLaunchOption, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamEnumerateAppLaunchOption\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamEnumerateAppVersion(unsigned int uAppId, unsigned int uVersionIndex, TSteamAppVersion *pAppVersion, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamEnumerateAppVersion\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamEnumerateSubscription(unsigned int uSubId, TSteamSubscription *pSubscription, TSteamError *pError)
{
	if(pSubscription && pError)
	{
#ifdef DEBUG
		Log->Write("SteamEnumerateSubscription: %u\n", uSubId);
#endif
		CSubscriptionRecord* SubRecord = CDR->GetSubRecordById(uSubId);
		pSubscription->uId = SubRecord->SubscriptionId;
		strcpy(pSubscription->szName, SubRecord->Name);
		pSubscription->bIsAutoRenewing = 0;
		pSubscription->uCostInCents = SubRecord->CostInCents;
		pSubscription->uDurationInSeconds = SubRecord->PeriodInMinutes;
		pSubscription->uMaxNameChars = CDR->uSubMaxNameChars;
		pSubscription->uMaxAppIds = CDR->uSubMaxApps;
		for(unsigned int i = 0;i < SubRecord->AppIds.size();i++)
		{
			pSubscription->puAppIds[i] = SubRecord->AppIds[i];
		}
		SteamClearError(pError);
		return 1;
	}
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamEnumerateSubscriptionDiscount()
{
#ifdef DEBUG
	Log->Write("SteamEnumerateSubscriptionDiscount\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamEnumerateSubscriptionDiscountQualifier()
{
#ifdef DEBUG
	Log->Write("SteamEnumerateSubscriptionDiscountQualifier\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGenerateSuggestedAccountNames()
{
#ifdef DEBUG
	Log->Write("SteamGenerateSuggestedAccountNames\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetAccountStatus()
{
#ifdef DEBUG
	Log->Write("SteamGetAccountStatus\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamGetAppCacheSize(unsigned int uAppId, unsigned int *pCacheSizeInMb, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetAppCacheSize\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetAppDependencies()
{
#ifdef DEBUG
	Log->Write("SteamGetAppDependencies\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetAppDir()
{
#ifdef DEBUG
	Log->Write("SteamGetAppDir\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetAppIds(unsigned int *puAppIds, unsigned int uMaxIds, TSteamError *pError)
{
	if(puAppIds && pError)
	{
#ifdef DEBUG
		Log->Write("SteamGetAppIds: %u\n", uMaxIds);
#endif
		for(unsigned int i = 0;i < uMaxIds;i++)
		{
			puAppIds[i] = CDR->ApplicationRecords[i]->AppId;
		}
		SteamClearError(pError);
		return 1;
	}
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamGetAppPurchaseCountry(int appID, char* szCountryCode, unsigned int a3, unsigned int* pPurchaseTime, TSteamError* pError)
{
#ifdef DEBUG
	Log->Write("SteamGetAppPurchaseCountry\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetAppStats(TSteamAppStats *pAppStats, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetAppStats\n");
#endif
	if(pAppStats && pError)
	{
		pAppStats->uNumApps = CDR->ApplicationRecords.size();
		pAppStats->uMaxNameChars = CDR->uAppMaxNameChars;
		pAppStats->uMaxVersionLabelChars = CDR->uAppMaxVersionLabelChars;
		pAppStats->uMaxLaunchOptions = CDR->uAppMaxLaunchOptions;
		pAppStats->uMaxLaunchOptionDescChars = CDR->uAppMaxLaunchOptionDescChars;
		pAppStats->uMaxLaunchOptionCmdLineChars = CDR->uAppMaxLaunchOptionCmdLineChars;
		pAppStats->uMaxNumIcons = CDR->uAppMaxNumIcons;
		pAppStats->uMaxIconSize = CDR->uAppMaxIconSize;
		SteamClearError(pError);
		return 1;
	}
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamGetAppUpdateStats(unsigned int uAppId, unsigned int uStatType, TSteamUpdateStats *pUpdateStats, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetAppUpdateStats: %u\n", uAppId);
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamGetAppUserDefinedInfo(unsigned int uAppId, const char *cszPropertyName, char *szPropertyValue, unsigned int uBufSize, unsigned int *puPropertyValueLength, TSteamError *pError)
{
	if(cszPropertyName && szPropertyValue && pError)
	{
		CAppRecord* AppRecord = CDR->GetAppRecordById(uAppId);
		if(char* szpropertyvalue = AppRecord->UserDefinedRecords[(char*)cszPropertyName])
		{
			size_t propertyvaluelength = strlen(szpropertyvalue);
			if(propertyvaluelength+1 <= uBufSize)
			{
				strcpy(szPropertyValue, szpropertyvalue);
				*puPropertyValueLength = propertyvaluelength;
#ifdef DEBUG
				Log->Write("SteamGetAppUserDefinedInfo: (%u) %s = %s\n", uAppId, cszPropertyName, szPropertyValue);
#endif
				SteamClearError(pError);
				return 1;
			}
		}
	}
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamGetAppUserDefinedRecord(unsigned int uAppId, unsigned int arg2, unsigned int arg3, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetAppUserDefinedRecord: %u\n", uAppId);
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamGetCurrentEmailAddress(char *szEmailaddress, unsigned int uBufSize, unsigned int *puEmailaddressChars, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetCurrentEmailAddress\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetNumAccountsWithEmailAddress()
{
#ifdef DEBUG
	Log->Write("SteamGetNumAccountsWithEmailAddress\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetSponsorUrl(unsigned int uAppId, char *szUrl, unsigned int uBufSize, unsigned int *pUrlChars, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetSponsorUrl\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetSubscriptionExtendedInfo()
{
#ifdef DEBUG
	Log->Write("SteamGetSubscriptionExtendedInfo\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetSubscriptionIds(unsigned int *puSubIds, unsigned int uMaxIds, TSteamError *pError)
{
	if(puSubIds && pError)
	{
#ifdef DEBUG
		Log->Write("SteamGetSubscriptionIds: %u\n", uMaxIds);
#endif
		for(unsigned int i = 0; i < uMaxIds; i++)
		{
			puSubIds[i] = CDR->SubscriptionsRecord[i]->SubscriptionId;
		}
		SteamClearError(pError);
		return 1;
	}
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamGetSubscriptionPurchaseCountry()
{
#ifdef DEBUG
	Log->Write("SteamGetSubscriptionPurchaseCountry\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetSubscriptionReceipt()
{
#ifdef DEBUG
	Log->Write("SteamGetSubscriptionReceipt\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetSubscriptionStats(TSteamSubscriptionStats *pSubscriptionStats, TSteamError *pError)
{
	if(pSubscriptionStats && pError)
	{
#ifdef DEBUG
		Log->Write("SteamGetSubscriptionStats\n");
#endif
		pSubscriptionStats->uNumSubscriptions = CDR->SubscriptionsRecord.size();
		pSubscriptionStats->uMaxNameChars = CDR->uSubMaxNameChars;
		pSubscriptionStats->uMaxApps = CDR->uSubMaxApps;
		SteamClearError(pError);
		return 1;
	}
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamGetTotalUpdateStats(TSteamUpdateStats *pUpdateStats, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetTotalUpdateStats\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetUser(char *szUser, unsigned int uBufSize, unsigned int *puUserChars, int bIsSecureComputer, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetUser\n");
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamGetUserType()
{
#ifdef DEBUG
	Log->Write("SteamGetUserType\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamIsAccountNameInUse()
{
#ifdef DEBUG
	Log->Write("SteamIsAccountNameInUse\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamIsAppSubscribed(unsigned int uAppId, int *pbIsAppSubscribed, int *pReserved, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamIsAppSubscribed: %u\n", uAppId);
#endif
	if(pbIsAppSubscribed && pReserved && pError)
	{
		*pbIsAppSubscribed = 1;
		*pReserved = 0;
		SteamClearError(pError);
		return 1;
	}
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamIsLoggedIn(int *pbIsLoggedIn, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamIsLoggedIn %d\n", *pbIsLoggedIn);
#endif
	
	if (pbIsLoggedIn && pError)
	{
		*pbIsLoggedIn = 1;
		SteamClearError(pError);
		return 1;
	}

	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamIsSecureComputer(int *pbIsSecure, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamIsSecureComputer\n");
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamIsSubscribed(unsigned int uSubscriptionId, int *pbIsSubscribed, int *pReserved, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamIsSubscribed: %u\n", uSubscriptionId);
#endif
	if(pbIsSubscribed && pReserved && pError)
	{
		*pbIsSubscribed = 1;
		*pReserved = 0;
		SteamClearError(pError);
		return 1;
	}
	return 0;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamLaunchApp(unsigned int uAppId, unsigned int uLaunchOption, const char *cszArgs, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamLaunchApp: %u\n", uAppId);
#endif
	return 0;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamLogin(const char *cszUser, const char *cszPassphrase, int bIsSecureComputer, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamLogin\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamLogout(TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamLogout\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamMoveApp(unsigned int uAppId, const char *szPath, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamMoveApp\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamRefreshAccountInfo()
{
#ifdef DEBUG
	Log->Write("SteamRefreshAccountInfo\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamRefreshAccountInfoEx()
{
#ifdef DEBUG
	Log->Write("SteamRefreshAccountInfoEx\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamRefreshLogin(const char *cszPassphrase, int bIsSecureComputer, TSteamError * pError)
{
#ifdef DEBUG
	Log->Write("SteamRefreshLogin\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamRequestAccountsByCdKeyEmail()
{
#ifdef DEBUG
	Log->Write("SteamRequestAccountsByCdKeyEmail\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamRequestAccountsByEmailAddressEmail()
{
#ifdef DEBUG
	Log->Write("SteamRequestAccountsByEmailAddressEmail\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamRequestEmailAddressVerificationEmail()
{
#ifdef DEBUG
	Log->Write("SteamRequestEmailAddressVerificationEmail\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamRequestForgottenPasswordEmail()
{
#ifdef DEBUG
	Log->Write("SteamRequestForgottenPasswordEmail\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamSetUser(const char *cszUser, int *pbUserSet, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamSetUser: %s\n", cszUser);
#endif
	return 0;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamSubscribe(unsigned int uSubscriptionId, const TSteamSubscriptionBillingInfo *pSubscriptionBillingInfo, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamSubscribe\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamUnsubscribe(unsigned int uSubscriptionId, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamUnsubscribe\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamUpdateAccountBillingInfo(const TSteamPaymentCardInfo *pPaymentCardInfo, int *pbChanged, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamUpdateAccountBillingInfo\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamUpdateSubscriptionBillingInfo(unsigned int uSubscriptionId, const TSteamSubscriptionBillingInfo *pSubscriptionBillingInfo, int *pbChanged, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamUpdateSubscriptionBillingInfo\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamVerifyEmailAddress()
{
#ifdef DEBUG
	Log->Write("SteamVerifyEmailAddress\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamVerifyPassword()
{
#ifdef DEBUG
	Log->Write("SteamVerifyPassword\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamWaitForAppReadyToLaunch(unsigned int uAppId, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamWaitForAppReadyToLaunch\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamAckSubscriptionReceipt()
{
#ifdef DEBUG
	Log->Write("SteamAckSubscriptionReceipt\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamRemoveAppDependency()
{
#ifdef DEBUG
	Log->Write("SteamRemoveAppDependency\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamSetAppCacheSize(unsigned int uAppId, unsigned int nCacheSizeInMb, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamSetAppCacheSize\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamSetAppVersion(unsigned int uAppId, unsigned int uAppVersionId, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamSetAppVersion\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamInsertAppDependency()
{
#ifdef DEBUG
	Log->Write("SteamInsertAppDependency\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamNumAppsRunning()
{
#ifdef DEBUG
	Log->Write("SteamNumAppsRunning\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamFindApp()
{
#ifdef DEBUG
	Log->Write("SteamFindApp\n");
#endif
	return 1;
}