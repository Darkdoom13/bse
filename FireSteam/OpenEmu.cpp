// OpenEmu.cpp : Defines the entry point for the DLL application.
//
#pragma warning(disable:4996)//disable deprecated warnings

#include <sys/stat.h>
#include <windows.h>
#include <cstdio>
#include <io.h>

#include ".\OpenEmu.h"

#include "CommandLine.h"
static CCommandLine* CommandLine;

#ifdef DEBUG
#include "LogFile.h"
static CLogFile* Log;
#endif

#include "IniFile.h"
static CIniFile* Ini;

#include ".\SteamBlobFileSystem\SteamBlobFileSystem.h"
#include ".\SteamBlobFileSystem\ContentDescriptionRecord.h"
static CContentDescriptionRecord* CDR;

char szSteamDLLPath[MAX_PATH];
char szSteamAppsDir[MAX_PATH];

const char OpenEmuError[] = "OpenEmu Error:";

#include ".\CacheManager.h"
CCacheManager* CM;

unsigned int ulAppID; // Current appid

void InitGlobalVaribles(HMODULE hModule)
{
	if(GetModuleFileNameA(hModule, szSteamDLLPath, MAX_PATH))
	{


		FILE *file = fopen ("midump.txt", "wt");
		if (file)
		{



		char* lastslash = strrchr(szSteamDLLPath, '\\');
		lastslash++;
		*lastslash = 0;
		char szFilePath[MAX_PATH];


#ifdef DEBUG
		strcpy(szFilePath, szSteamDLLPath);
		strcat(szFilePath, "OpenEmu.log");
		Log = new CLogFile(szFilePath);
		Log->Clear();
#endif
		strcpy(szFilePath, szSteamDLLPath);
		strcat(szFilePath, "OpenEmu.ini");
		Ini = new CIniFile(szFilePath);

//		fprintf(file, "Path %s\n", szFilePath);


		if(char* SteamAppsDir = Ini->IniReadValue("Common", "SteamAppsDir"))
		{
			strcpy(szSteamAppsDir, SteamAppsDir);
			delete SteamAppsDir;
		}
		else
		{
			strcpy(szSteamAppsDir, szSteamDLLPath);
			strcat(szSteamAppsDir, "SteamApps");
		}

		if(szSteamAppsDir[strlen(szSteamAppsDir)-1] != '\\')
		{
			strcat(szSteamAppsDir, "\\");
		}

		CommandLine = new CCommandLine();
		CommandLine->CreateCmdLine(GetCommandLine());
		ulAppID = CommandLine->ParmValue("-appid", 0);

		CM = new CCacheManager();

		CSteamBlobFileSystem* ClientRegistryBlob = new CSteamBlobFileSystem();
		char crb[MAX_PATH];
		strcpy(crb, szSteamDLLPath);
		strcat(crb, "ClientRegistry.blob");

//		fprintf(file, "Path %s\n", crb);

		
		if(ClientRegistryBlob->Open(crb))
		{

			fprintf(file, "Open ClientRegistryBlob");


			if(CSteamBlobNode *CDRNode = ClientRegistryBlob->GetNodeByPath("ContentDescriptionRecord"))
			{


				CDR = new CContentDescriptionRecord();
				if(CDR->Enumerate(CDRNode->KeyValue->Value))
				{
					ClientRegistryBlob->Close();
					return;
				}
			}
			else
			{
#ifdef DEBUG

				fprintf(file, "GetNodeByPath: Path not exist!\n");
#endif
				MessageBoxA(NULL, "ContentDescriptionRecord not found in ClientRegistry.blob.", OpenEmuError, (MB_OK|MB_APPLMODAL|MB_ICONERROR));
				ExitProcess(0);
			}
			ClientRegistryBlob->Close();
		}
		delete ClientRegistryBlob;
		MessageBoxA(NULL, "Can\'t open ClientRegistry.blob.", OpenEmuError, (MB_OK|MB_APPLMODAL|MB_ICONERROR));


			fclose (file);

		}


		}
	ExitProcess(0);
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{

			InitGlobalVaribles(hModule);


	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			InitGlobalVaribles(hModule);
			break;
		case DLL_THREAD_ATTACH:
			break;
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
			break;
	}
    return TRUE;
}

#include ".\OpenEmuInterface.h"			//Interface
#include ".\OpenEmuInit.h"				//Initialization
#include ".\OpenEmuAsyncCallHandling.h" //Asynchrounous call handling
#include ".\OpenEmuLogging.h"			//Logging
#include ".\OpenEmuFilesystem.h"		//Filesystem
#include ".\OpenEmuAccount.h"			//Account
#include ".\OpenEmuUserIDValidation.h"	//User ID validation
#include ".\OpenEmuMiniDump.h"			//Minidump
#include ".\OpenEmuMisc.h"				//Misc