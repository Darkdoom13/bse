#pragma once

/*
** User ID validation
*/

OPENEMU_API ESteamError OPENEMU_CALL SteamGetEncryptedUserIDTicket(const void *pEncryptionKeyReceivedFromAppServer, unsigned int uEncryptionKeyLength, void *pOutputBuffer, unsigned int uSizeOfOutputBuffer, unsigned int *pReceiveSizeOfEncryptedTicket, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetEncryptedUserIDTicket: %u\n", uSizeOfOutputBuffer);
#endif
	return eSteamErrorNone;
}

OPENEMU_API ESteamError OPENEMU_CALL SteamInitializeUserIDTicketValidator(const char * pszOptionalPublicEncryptionKeyFilename, const char *	pszOptionalPrivateDecryptionKeyFilename, unsigned int ClientClockSkewToleranceInSeconds, unsigned int ServerClockSkewToleranceInSeconds, unsigned int MaxNumLoginsWithinClientClockSkewTolerancePerClient, unsigned int	HintPeakSimultaneousValidations, unsigned int AbortValidationAfterStallingForNProcessSteps)
{
#ifdef DEBUG
	Log->Write("SteamInitializeUserIDTicketValidator\n");
#endif
	return eSteamErrorNone;
}

OPENEMU_API ESteamError OPENEMU_CALL SteamShutdownUserIDTicketValidator()
{
#ifdef DEBUG
	Log->Write("SteamShutdownUserIDTicketValidator\n");
#endif
	return eSteamErrorNone;
}

OPENEMU_API const unsigned char* OPENEMU_CALL SteamGetEncryptionKeyToSendToNewClient(unsigned int * pReceiveSizeOfEncryptionKey)
{
#ifdef DEBUG
	Log->Write("SteamGetEncryptionKeyToSendToNewClient\n");
#endif
	return m_key;
}

OPENEMU_API ESteamError OPENEMU_CALL SteamStartValidatingUserIDTicket(void *pEncryptedUserIDTicketFromClient, unsigned int uSizeOfEncryptedUserIDTicketFromClient, unsigned int ObservedClientIPAddr, SteamUserIDTicketValidationHandle_t *pReceiveHandle)
{
#ifdef DEBUG
	Log->Write("SteamStartValidatingUserIDTicket\n");
#endif
	return eSteamErrorNone;
}

OPENEMU_API ESteamError OPENEMU_CALL SteamStartValidatingNewValveCDKey(void *pEncryptedNewValveCDKeyFromClient, unsigned int uSizeOfEncryptedNewValveCDKeyFromClient, unsigned int ObservedClientIPAddr, struct sockaddr *pPrimaryValidateNewCDKeyServerSockAddr, struct sockaddr *pSecondaryValidateNewCDKeyServerSockAddr, SteamUserIDTicketValidationHandle_t *pReceiveHandle)
{
#ifdef DEBUG
	Log->Write("SteamStartValidatingNewValveCDKey\n");
#endif
	return eSteamErrorNone;
}

OPENEMU_API ESteamError OPENEMU_CALL SteamProcessOngoingUserIDTicketValidation(SteamUserIDTicketValidationHandle_t Handle, TSteamGlobalUserID *pReceiveValidSteamGlobalUserID, unsigned int *pReceiveClientLocalIPAddr, unsigned char *pOptionalReceiveProofOfAuthenticationToken, size_t SizeOfOptionalAreaToReceiveProofOfAuthenticationToken, size_t *pOptionalReceiveSizeOfProofOfAuthenticationToken)
{
#ifdef DEBUG
	Log->Write("SteamProcessOngoingUserIDTicketValidation\n");
#endif
	return eSteamErrorNone;
}

OPENEMU_API void OPENEMU_CALL SteamAbortOngoingUserIDTicketValidation(SteamUserIDTicketValidationHandle_t Handle)
{
#ifdef DEBUG
	Log->Write("SteamAbortOngoingUserIDTicketValidation\n");
#endif
}

OPENEMU_API ESteamError OPENEMU_CALL SteamOptionalCleanUpAfterClientHasDisconnected(unsigned int ObservedClientIPAddr, unsigned int ClientLocalIPAddr)
{
#ifdef DEBUG
	Log->Write("SteamOptionalCleanUpAfterClientHasDisconnected\n");
#endif
	return eSteamErrorNone;
}