#pragma once

/*
** Misc
*/

OPENEMU_API void OPENEMU_CALL SteamClearError(TSteamError *pError)
{
	if(pError)
	{
#ifdef DEBUG
		//Log->Write("SteamClearError\n");
#endif
		pError->eSteamError = eSteamErrorNone;
		pError->eDetailedErrorType = eNoDetailedErrorAvailable;
		pError->nDetailedErrorCode = 0;
		pError->szDesc[0] = 0;
	}
}

OPENEMU_API int OPENEMU_CALL InternalSteamNumClientsConnectedToEngine()
{
#ifdef DEBUG
	Log->Write("InternalSteamNumClientsConnectedToEngine\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL InternalSteamShouldShutdownEngine2()
{
#ifdef DEBUG
	Log->Write("InternalSteamShouldShutdownEngine2\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetLocalClientVersion()
{
#ifdef DEBUG
	Log->Write("SteamGetLocalClientVersion\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamChangeOfflineStatus()
{
#ifdef DEBUG
	Log->Write("SteamChangeOfflineStatus\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetOfflineStatus(unsigned int* buIsOffline, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetOfflineStatus\n");
#endif

	if (buIsOffline && pError)
	{
		SteamClearError(pError);
		return 1;
	}

	return 0;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamUninstall(TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamUninstall\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamWeakVerifyNewValveCDKey()
{
#ifdef DEBUG
	Log->Write("SteamWeakVerifyNewValveCDKey\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetEncryptedNewValveCDKey()
{
#ifdef DEBUG
	Log->Write("SteamGetEncryptedNewValveCDKey\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamDecryptDataForThisMachine()
{
#ifdef DEBUG
	Log->Write("SteamDecryptDataForThisMachine\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamEncryptDataForThisMachine()
{
#ifdef DEBUG
	Log->Write("SteamEncryptDataForThisMachine\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamFindServersGetErrorString()
{
#ifdef DEBUG
	Log->Write("SteamFindServersGetErrorString\n");
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamFindServersIterateServer(int arg1, int arg2, char *szServerAddress, unsigned int uServerAddressChars)
{
#ifdef DEBUG
	Log->Write("SteamFindServersIterateServer\n");
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamFindServersNumServers(unsigned int arg1)
{
#ifdef DEBUG
	Log->Write("SteamFindServersNumServers %u\n", arg1);
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamGetContentServerInfo()
{
#ifdef DEBUG
	Log->Write("SteamGetContentServerInfo\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamRefreshMinimumFootprintFiles()
{
#ifdef DEBUG
	Log->Write("SteamRefreshMinimumFootprintFiles\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamSetNotificationCallback(SteamNotificationCallback_t pCallbackFunction, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamSetNotificationCallback\n");
#endif
	return 1;
}