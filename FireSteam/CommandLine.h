#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <malloc.h> 

static const int MAX_PARAMETER_LEN = 128;

class CCommandLine
{
private:
	enum
	{
		MAX_PARAMETER_LEN = 128,
		MAX_PARAMETERS = 256,
	};

	// Copy of actual command line
	char *m_pszCmdLine;

	// Pointers to each argument...
	int m_nParmCount;
	char *m_ppParms[MAX_PARAMETERS];

public:
	CCommandLine();
	~CCommandLine();

	void LoadParametersFromFile( char *&pSrc, char *&pDst );
	void CreateCmdLine( int argc, char **argv );
	void CreateCmdLine( const char *commandline );
	void RemoveParm( const char *pszParm );
	void AppendParm( const char *pszParm, const char *pszValues );
	const char *GetCmdLine( void ) const;
	char **GetCmdLine(int *nParmCount) const;
	const char *CheckParm( const char *psz, const char **ppszValue ) const;
	void AddArgument( const char *pFirst, const char *pLast );
	void ParseCommandLine();
	void CleanUpParms();
	int ParmCount() const;
	int FindParm( const char *psz ) const;
	const char *GetParm( int nIndex ) const;
	const char *ParmValue( const char *psz, const char *pDefaultVal ) const;
	int	ParmValue( const char *psz, int nDefaultVal ) const;
	double ParmValue( const char *psz, double flDefaultVal ) const;
};
