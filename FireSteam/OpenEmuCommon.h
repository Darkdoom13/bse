
//*********** (C) Copyright 2002 Valve, L.L.C. All rights reserved. ***********
//
// The copyright to the contents herein is the property of Valve, L.L.C.
// The contents may be used and/or copied only with the written permission of
// Valve, L.L.C., or in accordance with the terms and conditions stipulated in
// the agreement/contract under which the contents have been supplied.
//
//*****************************************************************************
//
// Contents:
//
//		SteamUserID types.
//
//		This file is distributed to Steam application developers.
//
// Authors:	
//
// Target restrictions:
//
// Tool restrictions:
//
// Things to do:
//
//		
//
//*****************************************************************************

#ifndef INCLUDED_OPENEMU_COMMON_OPENEMUCOMMON_H
#define INCLUDED_OPENEMU_COMMON_OPENEMUCOMMON_H

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

/* Applications should not define STEAM_EXPORTS. */

#if defined ( WIN32 )

#ifdef OPENEMU_EXPORTS
#define OPENEMU_API __declspec(dllexport)
#else
#define OPENEMU_API __declspec(dllimport)
#endif

#define OPENEMU_CALL __cdecl

#else

#define OPENEMU_API /* */
#define OPENEMU_CALL /* */

#endif


/******************************************************************************
**
** Exported macros and constants
**
******************************************************************************/

#define STEAM_USING_FILESYSTEM							(0x00000001)
#define STEAM_USING_LOGGING								(0x00000002)
#define STEAM_USING_USERID								(0x00000004)
#define STEAM_USING_ACCOUNT								(0x00000008)
#define STEAM_USING_ALL									(0x0000000f)
#define STEAM_MAX_PATH									(255)
#define STEAM_QUESTION_MAXLEN							(255)
#define STEAM_SALT_SIZE									(8)

#define STEAM_CARD_NUMBER_SIZE							(16)
#define STEAM_CARD_HOLDERNAME_SIZE						(80)
#define STEAM_CARD_EXPYEAR_SIZE							(4)
#define STEAM_CARD_EXPMONTH_SIZE						(2)
#define STEAM_CARD_CVV2_SIZE							(10)
#define STEAM_BILLING_ADDRESS1_SIZE						(80)
#define STEAM_BILLING_ADDRESS2_SIZE						(80)
#define STEAM_BILLING_CITY_SIZE							(80)
#define STEAM_BILLING_ZIP_SIZE							(10)
#define STEAM_BILLING_STATE_SIZE						(40)
#define STEAM_BILLING_COUNTRY_SIZE						(40)
#define STEAM_BILLING_PHONE_SIZE						(20)
#define STEAM_TYPE_OF_PROOF_OF_PURCHASE_SIZE			(20)
#define STEAM_PROOF_OF_PURCHASE_TOKEN_SIZE				(200)
#define STEAM_EXTERNAL_ACCOUNTNAME_SIZE					(80)
#define STEAM_EXTERNAL_ACCOUNTPASSWORD_SIZE				(80)

/******************************************************************************
**
** Scalar type and enumerated type definitions.
**
******************************************************************************/

typedef unsigned int SteamHandle_t;

typedef void * SteamUserIDTicketValidationHandle_t;

typedef unsigned int SteamCallHandle_t;

#if defined(_MSC_VER)
typedef unsigned __int64	SteamUnsigned64_t;
#else
typedef unsigned long long	SteamUnsigned64_t;
#endif

typedef enum
{
	eSteamSeekMethodSet = 0,
	eSteamSeekMethodCur = 1,
	eSteamSeekMethodEnd = 2
} ESteamSeekMethod;

typedef enum
{
	eSteamBufferMethodFBF = 0,
	eSteamBufferMethodNBF = 1
} ESteamBufferMethod;

typedef enum
{
	eSteamErrorNone													= 0,
	eSteamErrorUnknown												= 1,
	eSteamErrorLibraryNotInitialized								= 2,
	eSteamErrorLibraryAlreadyInitialized							= 3,
	eSteamErrorConfig												= 4,
	eSteamErrorContentServerConnect									= 5,
	eSteamErrorBadHandle											= 6,
	eSteamErrorHandlesExhausted										= 7,
	eSteamErrorBadArg												= 8,
	eSteamErrorNotFound												= 9,
	eSteamErrorRead													= 10,
	eSteamErrorEOF													= 11,
	eSteamErrorSeek													= 12,
	eSteamErrorCannotWriteNonUserConfigFile							= 13,
	eSteamErrorCacheOpen											= 14,
	eSteamErrorCacheRead											= 15,
	eSteamErrorCacheCorrupted										= 16,
	eSteamErrorCacheWrite											= 17,
	eSteamErrorCacheSession											= 18,
	eSteamErrorCacheInternal										= 19,
	eSteamErrorCacheBadApp											= 20,
	eSteamErrorCacheVersion											= 21,
	eSteamErrorCacheBadFingerPrint									= 22,

	eSteamErrorNotFinishedProcessing								= 23,
	eSteamErrorNothingToDo											= 24,
	eSteamErrorCorruptEncryptedUserIDTicket							= 25,
	eSteamErrorSocketLibraryNotInitialized							= 26,
	eSteamErrorFailedToConnectToUserIDTicketValidationServer		= 27,
	eSteamErrorBadProtocolVersion									= 28,
	eSteamErrorReplayedUserIDTicketFromClient						= 29,
	eSteamErrorReceiveResultBufferTooSmall							= 30,
	eSteamErrorSendFailed											= 31,
	eSteamErrorReceiveFailed										= 32,
	eSteamErrorReplayedReplyFromUserIDTicketValidationServer		= 33,
	eSteamErrorBadSignatureFromUserIDTicketValidationServer			= 34,
	eSteamErrorValidationStalledSoAborted							= 35,
	eSteamErrorInvalidUserIDTicket									= 36,
	eSteamErrorClientLoginRateTooHigh								= 37,
	eSteamErrorClientWasNeverValidated								= 38,
	eSteamErrorInternalSendBufferTooSmall							= 39,
	eSteamErrorInternalReceiveBufferTooSmall						= 40,
	eSteamErrorUserTicketExpired									= 41,
	eSteamErrorCDKeyAlreadyInUseOnAnotherClient						= 42,

	eSteamErrorNotLoggedIn											= 101,
	eSteamErrorAlreadyExists										= 102,
	eSteamErrorAlreadySubscribed									= 103,
	eSteamErrorNotSubscribed										= 104,
	eSteamErrorAccessDenied											= 105,
	eSteamErrorFailedToCreateCacheFile								= 106,
	eSteamErrorCallStalledSoAborted									= 107,
	eSteamErrorEngineNotRunning										= 108,
	eSteamErrorEngineConnectionLost									= 109,
	eSteamErrorLoginFailed											= 110,
	eSteamErrorAccountPending										= 111,
	eSteamErrorCacheWasMissingRetry									= 112,
	eSteamErrorLocalTimeIncorrect									= 113,

	eSteamErrorNetwork												= 200


} ESteamError;


typedef enum
{
	eNoDetailedErrorAvailable,
	eStandardCerrno,
	eWin32LastError,
	eWinSockLastError,
	eDetailedPlatformErrorCount
} EDetailedPlatformErrorType;

typedef enum						/* Filter elements returned by SteamFind{First,Next} */
{
	eSteamFindLocalOnly,			/* limit search to local filesystem */
	eSteamFindRemoteOnly,			/* limit search to remote repository */
	eSteamFindAll					/* do not limit search (duplicates allowed) */
} ESteamFindFilter;


/******************************************************************************
**
** Exported structure and complex type definitions.
**
******************************************************************************/


typedef struct
{
	ESteamError eSteamError;
	EDetailedPlatformErrorType eDetailedErrorType;
	int nDetailedErrorCode;
	char szDesc[STEAM_MAX_PATH];
} TSteamError;



typedef struct
{
	int bIsDir;						/* If non-zero, element is a directory; if zero, element is a file */
	unsigned int uSizeOrCount;		/* If element is a file, this contains size of file in bytes */
	int bIsLocal;					/* If non-zero, reported item is a standalone element on local filesystem */
	char cszName[STEAM_MAX_PATH];	/* Base element name (no path) */
	long lLastAccessTime;			/* Seconds since 1/1/1970 (like time_t) when element was last accessed */
	long lLastModificationTime;		/* Seconds since 1/1/1970 (like time_t) when element was last modified */
	long lCreationTime;				/* Seconds since 1/1/1970 (like time_t) when element was created */
} TSteamElemInfo;


typedef struct
{
	unsigned int uNumSubscriptions;
	unsigned int uMaxNameChars;
	unsigned int uMaxApps;

} TSteamSubscriptionStats;


typedef struct
{
    unsigned int AppId;
    unsigned int IsRequired;
	char szMountName[STEAM_MAX_PATH];
} TSteamAppDependencyInfo;

typedef struct
{
	unsigned int uNumApps;
	unsigned int uMaxNameChars;
	unsigned int uMaxVersionLabelChars;
	unsigned int uMaxLaunchOptions;
	unsigned int uMaxLaunchOptionDescChars;
	unsigned int uMaxLaunchOptionCmdLineChars;
	unsigned int uMaxNumIcons;
	unsigned int uMaxIconSize;

} TSteamAppStats;

typedef struct
{
	char *szLabel;
	unsigned int uMaxLabelChars;
	unsigned int uVersionId;
	int bIsNotAvailable;
} TSteamAppVersion;

typedef struct
{
	char *szDesc;
	unsigned int uMaxDescChars;
	char *szCmdLine;
	unsigned int uMaxCmdLineChars;
	unsigned int uIndex;
	unsigned int uIconIndex;
	int bNoDesktopShortcut;
	int bNoStartMenuShortcut;
	int bIsLongRunningUnattended;

} TSteamAppLaunchOption;


typedef struct
{
	char *szName;
	unsigned int uMaxNameChars;
	char *szLatestVersionLabel;
	unsigned int uMaxLatestVersionLabelChars;
	char *szCurrentVersionLabel;
	unsigned int uMaxCurrentVersionLabelChars;
	char *szCacheFile;
    unsigned int uMaxCacheFileChars;
	unsigned int uId;
	unsigned int uLatestVersionId;
	unsigned int uCurrentVersionId;
	unsigned int uMinCacheFileSizeMB;
	unsigned int uMaxCacheFileSizeMB;
	unsigned int uNumLaunchOptions;
	unsigned int uNumIcons;
	unsigned int uNumVersions;
	unsigned int numDependencies;
} TSteamApp;

typedef struct
{
	char *szName;
	unsigned int uMaxNameChars;
	unsigned int *puAppIds;
	unsigned int uMaxAppIds;
	unsigned int uId;
	unsigned int uNumApps;
	unsigned int uDurationInSeconds;
	unsigned int uCostInCents;
	unsigned int bIsAutoRenewing;

} TSteamSubscription;

typedef struct TSteamProgress
{
	int bValid;                      // non-zero if call provides progress info
	unsigned int uPercentDone;       // 0 to 100 if bValid
	char szProgress[STEAM_MAX_PATH]; // additional progress info
} TSteamProgress;

typedef enum
{
	eSteamNotifyTicketsWillExpire,
	eSteamNotifyAccountInfoChanged,
	eSteamNotifyContentDescriptionChanged,
	eSteamNotifyPleaseShutdown,
	eSteamNotifyNewSponsorUrl,
	eSteamNotifyAppVersionChanged
} ESteamNotificationCallbackEvent;


typedef void(*SteamNotificationCallback_t)(ESteamNotificationCallbackEvent eEvent, unsigned int nData);


typedef char SteamPersonalQuestion_t[ STEAM_QUESTION_MAXLEN + 1 ];

typedef struct
{
	unsigned char uchSalt[STEAM_SALT_SIZE];
} SteamSalt_t;

typedef enum
{
	eVisa				= 1,
	eMaster				= 2,
	eAmericanExpress	= 3,
	eDiscover			= 4,
	eDinnersClub		= 5
} ESteamPaymentCardType;

typedef struct
{
	ESteamPaymentCardType eCardType;
	char szCardNumber[ STEAM_CARD_NUMBER_SIZE +1 ];
	char szCardHolderName[ STEAM_CARD_HOLDERNAME_SIZE + 1];
	char szCardExpYear[ STEAM_CARD_EXPYEAR_SIZE + 1 ];
	char szCardExpMonth[ STEAM_CARD_EXPMONTH_SIZE+ 1 ];
	char szCardCVV2[ STEAM_CARD_CVV2_SIZE + 1 ];
	char szBillingAddress1[ STEAM_BILLING_ADDRESS1_SIZE + 1 ];
	char szBillingAddress2[ STEAM_BILLING_ADDRESS2_SIZE + 1 ];
	char szBillingCity[ STEAM_BILLING_CITY_SIZE + 1 ];
	char szBillingZip[ STEAM_BILLING_ZIP_SIZE + 1 ];
	char szBillingState[ STEAM_BILLING_STATE_SIZE + 1 ];
	char szBillingCountry[ STEAM_BILLING_COUNTRY_SIZE + 1 ];
	char szBillingPhone[ STEAM_BILLING_PHONE_SIZE + 1 ];
} TSteamPaymentCardInfo;

typedef struct
{
	char					szTypeOfProofOfPurchase[ STEAM_TYPE_OF_PROOF_OF_PURCHASE_SIZE + 1 ];

	// A ProofOfPurchase token is not necessarily a nul-terminated string; it may be binary data
	// (perhaps encrypted). Hence we need a length and an array of bytes.
	unsigned int			uLengthOfBinaryProofOfPurchaseToken;	
	char					cBinaryProofOfPurchaseToken[ STEAM_PROOF_OF_PURCHASE_TOKEN_SIZE + 1 ];
} TSteamPrepurchaseInfo;

typedef struct
{
	char szAccountName[ STEAM_EXTERNAL_ACCOUNTNAME_SIZE + 1 ];
	char szPassword[ STEAM_EXTERNAL_ACCOUNTPASSWORD_SIZE + 1 ];
} TSteamExternalBillingInfo;

typedef enum
{
	ePaymentCardInfo		= 1,
	ePrepurchasedInfo		= 2,
	eAccountBillingInfo		= 3,
	eExternalBillingInfo	= 4		// indirect billing via ISP etc (not supported yet)
} ESteamSubscriptionBillingInfoType;

typedef struct
{
	ESteamSubscriptionBillingInfoType	eBillingInfoType;
	union {
		TSteamPaymentCardInfo			PaymentCardInfo;
		TSteamPrepurchaseInfo			PrepurchaseInfo;
		TSteamExternalBillingInfo		ExternalBillingInfo;
		char							bUseAccountBillingInfo;
	};

} TSteamSubscriptionBillingInfo;

typedef struct
{
	SteamUnsigned64_t uPhysicalBytesReceived;
} TSteamUpdateStats;

/******************************************************************************
**
** More exported constants
**
******************************************************************************/


#ifdef __cplusplus

const SteamHandle_t										STEAM_INVALID_HANDLE = 0;
const SteamCallHandle_t									STEAM_INVALID_CALL_HANDLE = 0;
const SteamUserIDTicketValidationHandle_t				STEAM_INACTIVE_USERIDTICKET_VALIDATION_HANDLE = 0;
const unsigned int										STEAM_USE_LATEST_VERSION = 0xFFFFFFFF;

#else

#define STEAM_INVALID_HANDLE							((SteamHandle_t)(0))
#define STEAM_INVALID_CALL_HANDLE						((SteamCallHandle_t)(0))
#define	STEAM_INACTIVE_USERIDTICKET_VALIDATION_HANDLE	((SteamUserIDTicketValidationHandle_t)(0))
#define STEAM_USE_LATEST_VERSION						(0xFFFFFFFFu);

#endif


// Each Steam instance (licensed Steam Service Provider) has a unique SteamInstanceID_t.
//
// Each Steam instance as its own DB of users.
// Each user in the DB has a unique SteamLocalUserID_t (a serial number, with possible 
// rare gaps in the sequence).

typedef	unsigned short		SteamInstanceID_t;		// MUST be 16 bits


#if defined ( WIN32 )
typedef	unsigned __int64	SteamLocalUserID_t;		// MUST be 64 bits
#else
typedef	unsigned long long	SteamLocalUserID_t;		// MUST be 64 bits
#endif


// Applications need to be able to authenticate Steam users from ANY instance.
// So a SteamIDTicket contains SteamGlobalUserID, which is a unique combination of 
// instance and user id.

// SteamLocalUserID is an unsigned 64-bit integer.
// For platforms without 64-bit int support, we provide access via a union that splits it into 
// high and low unsigned 32-bit ints.  Such platforms will only need to compare LocalUserIDs 
// for equivalence anyway - not perform arithmetic with them.
typedef struct	
{
	unsigned int	Low32bits;
	unsigned int	High32bits;
}	TSteamSplitLocalUserID;

typedef struct
{
	SteamInstanceID_t		m_SteamInstanceID;

	union
	{
		SteamLocalUserID_t		As64bits;
		TSteamSplitLocalUserID	Split;
	}						m_SteamLocalUserID;

} TSteamGlobalUserID;

const unsigned char m_key[160] = {
		0x30, 0x81, 0x9D, 0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86,
        0xF7, 0x0D, 0x01, 0x01, 0x01, 0x05, 0x00, 0x03, 0x81, 0x8B, 0x00,
        0x30, 0x81, 0x87, 0x02, 0x81, 0x81, 0x00, 0xC1, 0x7E, 0xE4, 0xCC,
        0x16, 0x61, 0xB4, 0x19, 0x1F, 0x6A, 0x88, 0xDA, 0x8D, 0xC9, 0x5F,
        0x68, 0x32, 0x53, 0x00, 0x7F, 0xF9, 0x46, 0x5B, 0x89, 0x10, 0xC6,
        0xCB, 0x30, 0xBD, 0x7B, 0x95, 0xD6, 0xB4, 0xBA, 0x52, 0xF1, 0x77,
        0x1F, 0x41, 0x2E, 0x10, 0x13, 0xF2, 0x12, 0x6E, 0x88, 0x45, 0x4D,
        0x97, 0x57, 0x5C, 0x78, 0x76, 0x44, 0xBE, 0xD2, 0xEB, 0x4A, 0xF2,
        0xD9, 0x04, 0x76, 0x72, 0x7D, 0xA2, 0x12, 0xB2, 0xAF, 0xB3, 0x3E,
        0x60, 0xE4, 0xE1, 0x17, 0x13, 0x78, 0xCA, 0x9F, 0x06, 0x08, 0x19,
        0x76, 0xEF, 0x89, 0x98, 0x5A, 0xDA, 0xB3, 0x03, 0xE8, 0x51, 0x33,
        0xB2, 0x34, 0x28, 0xA0, 0x96, 0x1F, 0x66, 0xE4, 0x99, 0xA2, 0x86,
        0x97, 0xE6, 0xEF, 0xE1, 0x5E, 0x81, 0xAF, 0xE8, 0x38, 0x02, 0xCE,
        0x61, 0x54, 0x7A, 0xC5, 0x95, 0x4B, 0x87, 0x6D, 0xA2, 0x46, 0xDF,
        0x19, 0x57, 0xE9, 0x02, 0x01, 0x11
};

#ifdef __cplusplus
}
#endif


#endif
