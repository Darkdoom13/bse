#pragma once

/*
** Filesystem
*/

OPENEMU_API int OPENEMU_CALL SteamMountAppFilesystem(TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamMountAppFilesystem\n");
#endif
	char szCacheFileName[MAX_PATH];
	char gcfMountPath[MAX_PATH];

	BOOL IsAllRequiredCacheMounted = TRUE;

	CAppRecord* AppRecord = CDR->GetAppRecordById(ulAppID);

	if (AppRecord && pError)
	{
		for(unsigned int i = 0;i< AppRecord->FilesystemsRecord.size();i++)
		{
			CAppRecord* CacheToMount = CDR->GetAppRecordById(AppRecord->FilesystemsRecord[i]->AppId);
			strcpy(szCacheFileName, szSteamAppsDir);
			strcat(szCacheFileName, CacheToMount->InstallDirName);
			strcat(szCacheFileName, (CacheToMount->ManifestOnlyApp ? ".ncf" : ".gcf"));

			if (strlen(AppRecord->FilesystemsRecord[i]->MountName) > 0)
				sprintf(gcfMountPath, "%s%s\\", szSteamDLLPath, AppRecord->FilesystemsRecord[i]->MountName);
			else
				strcpy(gcfMountPath, szSteamDLLPath);

			bool IsMounted = CM->MountGCFFile(szCacheFileName, gcfMountPath);
			if(!IsMounted && !AppRecord->FilesystemsRecord[i]->IsOptional)
			{
#ifdef DEBUG
				Log->Write("Unable to mount required cache file: %u %s\n", AppRecord->FilesystemsRecord[i]->AppId, szCacheFileName);
#endif
				IsAllRequiredCacheMounted = FALSE;
				break;
			}
#ifdef DEBUG
			else if(IsMounted && !AppRecord->FilesystemsRecord[i]->IsOptional) 
			{
				Log->Write("Required cache file mounted: %u %s\n", AppRecord->FilesystemsRecord[i]->AppId, szCacheFileName);
			}
			else if(!IsMounted && AppRecord->FilesystemsRecord[i]->IsOptional) 
			{
				Log->Write("Unable to mount optional cache file: %u %s\n", AppRecord->FilesystemsRecord[i]->AppId, szCacheFileName);
			}
			else if(IsMounted && AppRecord->FilesystemsRecord[i]->IsOptional) 
			{
				Log->Write("Optional cache file mounted: %u %s\n", AppRecord->FilesystemsRecord[i]->AppId, szCacheFileName);
			}
#endif
		}
		SteamClearError(pError);
		return IsAllRequiredCacheMounted;
	}

	return IsAllRequiredCacheMounted;
}

OPENEMU_API int OPENEMU_CALL SteamUnmountAppFilesystem(TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamUnmountAppFilesystem\n");
#endif

	if (pError)
	{
		CM->UnMountAll();
		SteamClearError(pError);
		return 1;
	}

	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamMountFilesystem(unsigned int uAppId, const char *szMountPath, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamMountFilesystem AppId: %u Path: %s\n", uAppId, szMountPath);
#endif	
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamUnmountFilesystem(const char *szMountPath, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamUnmountFilesystem\n");
#endif
	return 1;
}

OPENEMU_API SteamHandle_t OPENEMU_CALL SteamOpenFileEx(const char *cszFileName, const char *cszMode, unsigned int *puSize, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamOpenFileEx: %s\n", cszFileName);
#endif

	if(cszFileName && cszMode && puSize && pError)
	{
		if(FILE* pFile = fopen(cszFileName, cszMode))
		{
			fseek(pFile, 0, SEEK_END);
			*puSize = ftell(pFile);						// Return the size of file

			fseek(pFile, 0, SEEK_SET);					// Seek to the beginning

			SteamClearError(pError);
			return (SteamHandle_t) pFile;				// return the adress of pointer to opened FILE
		}
		else
		{
			if (HLLib::Streams::IStream* pCFile = CM->OpenFile(cszFileName, "", puSize))
			{
				SteamClearError(pError);
				return ((SteamHandle_t)pCFile | 0x80000000);
			}
			else
			{
#ifdef DEBUG
				Log->Write("Failed to open file: %s\n", cszFileName);
#endif
			}
		}
	}
    return 0;
}

OPENEMU_API SteamHandle_t OPENEMU_CALL SteamOpenFile(const char *cszFileName, const char *cszMode, TSteamError *pError)
{
	unsigned int *puSize = new unsigned int;
	SteamHandle_t res = SteamOpenFileEx(cszFileName, cszMode, puSize, pError);
	delete puSize;
	return res;
}

OPENEMU_API SteamHandle_t OPENEMU_CALL SteamOpenTmpFile(TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamOpenTmpFile\n");
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamFlushFile(SteamHandle_t hFile, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamFlushFile\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamCloseFile(SteamHandle_t hFile, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamCloseFile\n");
#endif

	if ((hFile & 0x80000000) == 0x80000000)
	{
		HLLib::Streams::IStream* pCFile = (HLLib::Streams::IStream*) (hFile ^ 0x80000000);
		CM->CloseFile(pCFile);
		SteamClearError(pError);
		return 0;
	}
	else
	{
		FILE* pFile = (FILE*) hFile;			// Retrieve our file pointer

		if(pFile != NULL && (fclose(pFile) == 0))
		{
			SteamClearError(pError);
			return 0;
		}
		return -1;
	}
}

OPENEMU_API int OPENEMU_CALL SteamSetvBuf(SteamHandle_t hFile, void* pBuf, ESteamBufferMethod eMethod, unsigned int uBytes, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamSetvBuf\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetc(SteamHandle_t hFile, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetc\n");
#endif

	FILE* pFile = (FILE*) hFile;

	if(pFile && pError)
	{
		SteamClearError(pError);
		return fgetc(pFile);
	}
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamPutc(int cChar, SteamHandle_t hFile, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamPutc\n");
#endif

	FILE* pFile = (FILE*) hFile;

	if(pFile && pError)
	{
		SteamClearError(pError);
		return fputc(cChar, pFile);
	}
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamPrintFile(SteamHandle_t hFile, TSteamError *pError, const char *cszFormat, ...)
{
#ifdef DEBUG
	Log->Write("SteamPrintFile\n");
#endif
	return 1;
}

OPENEMU_API unsigned int OPENEMU_CALL SteamReadFile(void *pBuf, unsigned int uSize, unsigned int uCount, SteamHandle_t hFile, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamReadFile 0x%08x 0x%08x\n", hFile, uSize * uCount);
#endif
	if ((hFile & 0x80000000) == 0x80000000)
	{
		HLLib::Streams::IStream* pCFile = (HLLib::Streams::IStream*) (hFile ^ 0x80000000);
		SteamClearError(pError);
		return pCFile->Read(pBuf, uSize * uCount);
	}
	else
	{
		FILE* pFile = (FILE*) hFile;

		if(pFile && pBuf && pError)
		{
			int nBytes = fread(pBuf, uSize, uCount, pFile);

			if(nBytes > 0)
			{
				SteamClearError(pError);
				return nBytes;
			}
		}
		return 0;
	}
}

OPENEMU_API unsigned int OPENEMU_CALL SteamWriteFile(const void *pBuf, unsigned int uSize, unsigned int uCount, SteamHandle_t hFile, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamWriteFile\n");
#endif

	FILE* pFile = (FILE*) hFile;

	if(pFile && pBuf && pError)
	{
		int nBytes = fwrite(pBuf, uSize, uCount, (FILE*)hFile);

		if(nBytes > 0)
		{
			SteamClearError(pError);
			return nBytes;
		}
	}
    return 0;
}

OPENEMU_API int OPENEMU_CALL SteamSeekFile(SteamHandle_t hFile, long lOffset, ESteamSeekMethod esMethod, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamSeekFile: 0x%08X\n", lOffset);
#endif
	if ((hFile & 0x80000000) == 0x80000000)
	{
		HLLib::Streams::IStream* pCFile = (HLLib::Streams::IStream*) (hFile ^ 0x80000000);
		SteamClearError(pError);
		pCFile->Seek(lOffset, (HLSeekMode)esMethod);
		return 0;
	}
	else
	{
		FILE* pFile = (FILE*) hFile;			// Retrieve our file pointer

		if(pFile && !fseek(pFile, lOffset, esMethod))
		{
			SteamClearError(pError);
			return 0;
		}

		return -1;
	}
}

OPENEMU_API long OPENEMU_CALL SteamSizeFile(SteamHandle_t hFile, TSteamError *pError)
{

	FILE* pFile = (FILE*) hFile;
#ifdef DEBUG
		Log->Write("SteamSizeFile: %d\n", hFile);
#endif
	if(pFile && pError)
	{
		size_t ufilepos = ftell(pFile);
		fseek(pFile, 0, SEEK_END);
		size_t uSize = ftell(pFile);
		fseek(pFile, ufilepos, SEEK_SET);
#ifdef DEBUG
		Log->Write("SteamSizeFile: 0x%08x\n", uSize);
#endif
		SteamClearError(pError);
		return uSize;
	}
	return 0;
}

OPENEMU_API long OPENEMU_CALL SteamTellFile(SteamHandle_t hFile, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamTellFile\n");
#endif
	if ((hFile & 0x80000000) == 0x80000000)
	{
		HLLib::Streams::IStream* pCFile = (HLLib::Streams::IStream*) (hFile ^ 0x80000000);
		SteamClearError(pError);
		return pCFile->GetStreamPointer();
	}
	else
	{
		FILE* pFile = (FILE*) hFile;

		if(pFile && pError)
		{
			SteamClearError(pError);
			return ftell((FILE*)hFile);
		}
		return -1L;
	}
}

OPENEMU_API int OPENEMU_CALL SteamStat(const char *cszFileName, TSteamElemInfo *pInfo, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamStat %s\n", cszFileName);
#endif

	if(pInfo && pError)
	{
		struct _stat mystat;

		if(_stat(cszFileName, &mystat) == 0)
		{
			pInfo->bIsDir = ((mystat.st_mode & S_IFMT) == S_IFDIR);

			pInfo->bIsLocal = 1;

			char* Period = (char*) strrchr(cszFileName, '\\');
			strcpy( pInfo->cszName, Period + 1 );

			pInfo->lCreationTime = mystat.st_ctime;
			pInfo->lLastAccessTime = mystat.st_atime;
			pInfo->lLastModificationTime = mystat.st_mtime;
			pInfo->uSizeOrCount = mystat.st_size;

			SteamClearError(pError);
			return 0;
		}
		else
		{
			if (CM->FileStat(cszFileName, pInfo))
			{
				SteamClearError(pError);
				return 0;
			}
		}
	}
	return -1;
}

OPENEMU_API int OPENEMU_CALL SteamFindClose(SteamHandle_t hFind, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamFindClose\n");
#endif
	int retval = -1;
	if(hFind && pError)
	{
		TFindRecord* pFind = (TFindRecord*) hFind;
		if (!pFind->bLocalSearch)
		{
			CM->FindClose(pFind);
			SteamClearError(pError);
			retval = 1;
		}
		else
		{
			SteamClearError(pError);
			retval = _findclose(pFind->hFind);
			delete pFind;
		}
	}
	return retval;
}

OPENEMU_API SteamHandle_t OPENEMU_CALL SteamFindFirst(const char *cszPattern, ESteamFindFilter eFilter, TSteamElemInfo *pFindInfo, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamFindFirst: %s\n", cszPattern);
#endif
	if(cszPattern && pFindInfo && pError)
	{
		_finddata_t finddata;
		int retval = _findfirst(cszPattern, &finddata);
		if(retval > 0)
		{
			pFindInfo->uSizeOrCount = finddata.size;
			pFindInfo->bIsDir = (0!=(finddata.attrib&_S_IFDIR));
			pFindInfo->bIsLocal = 1;
			pFindInfo->lCreationTime = (long)finddata.time_create;
			pFindInfo->lLastAccessTime = (long)finddata.time_access;
			pFindInfo->lLastModificationTime = (long)finddata.time_write;
			strcpy(pFindInfo->cszName, finddata.name);

			TFindRecord* pFind = new TFindRecord();
			pFind->bLocalSearch = true;
			pFind->hFind = retval;

			SteamClearError(pError);
			return (int) pFind;
		}
		else
		{
			retval = (int) CM->FindFirst(cszPattern, pFindInfo);
			if (retval)
			{
				SteamClearError(pError);
				return retval;
			}
		}
	}
	return STEAM_INVALID_HANDLE;
}

OPENEMU_API int OPENEMU_CALL SteamFindNext(SteamHandle_t hFind, TSteamElemInfo *pFindInfo, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamFindNext\n");
#endif

	int retval = -1;

	if(hFind && pFindInfo && pError)
	{
		TFindRecord* pFind = (TFindRecord*) hFind;

		if (!pFind->bLocalSearch)
		{
			if (CM->FindNext(pFind, pFindInfo))
			{
				retval = 0;
				SteamClearError(pError);
			}
		}
		else
		{
			struct _finddata_t finddata;
			retval = _findnext(pFind->hFind, &finddata);
			if(retval == 0)
			{
				pFindInfo->uSizeOrCount = finddata.size;
				pFindInfo->bIsDir = (0!=(finddata.attrib&_S_IFDIR));
				pFindInfo->bIsLocal = 1;
				pFindInfo->lCreationTime = (long)finddata.time_create;
				pFindInfo->lLastAccessTime = (long)finddata.time_access;
				pFindInfo->lLastModificationTime = (long)finddata.time_write;
				strcpy(pFindInfo->cszName, finddata.name);

				SteamClearError(pError);
			}
		}
	}
	return retval;
}

OPENEMU_API int OPENEMU_CALL SteamGetLocalFileCopy(const char *cszFileName, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetLocalFileCopy: %s\n", cszFileName);
#endif

	if(cszFileName[1] != ':')
	{
		char pathbuffer[MAX_PATH];
		pathbuffer[0] = 0;
		_searchenv(cszFileName, "PATH", pathbuffer);
		if(pathbuffer[0] != 0)
		{
			SteamClearError(pError);
			return 1;
		}
	}
	else if(FILE* file = fopen(cszFileName, "r"))
	{
		SteamClearError(pError);
		fclose(file);
		return 1;
	}
	else if (CM->ExtractFile(cszFileName))
	{
		SteamClearError(pError);
		return 1;
	}
#ifdef DEBUG
	Log->Write("SteamGetLocalFileCopy: %s Failed\n", cszFileName);
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamIsFileImmediatelyAvailable(const char *cszName, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamIsFileImmediatelyAvailable\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamHintResourceNeed(const char *cszHintList, int bForgetEverything, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamHintResourceNeed: %s, %d\n", cszHintList, bForgetEverything);
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamForgetAllHints(const char *cszMountPath, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamForgetAllHints\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamPauseCachePreloading(const char *cszMountPath, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamPauseCachePreloading\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamResumeCachePreloading(const char *cszMountPath, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamResumeCachePreloading\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamWaitForResources(const char *cszMasterList, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamWaitForResources\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamFlushCache(unsigned int uAppId, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamFlushCache\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetCacheDecryptionKey()
{
#ifdef DEBUG
	Log->Write("SteamGetCacheDecryptionKey\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetCacheDefaultDirectory(char *szPath, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamGetCacheDefaultDirectory\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamSetCacheDefaultDirectory(const char *szPath, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamSetCacheDefaultDirectory\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetCacheFilePath()
{
#ifdef DEBUG
	Log->Write("SteamGetCacheFilePath\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamIsFileNeededByCache()
{
#ifdef DEBUG
	Log->Write("SteamIsFileNeededByCache\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamRepairOrDecryptCaches()
{
#ifdef DEBUG
	Log->Write("SteamRepairOrDecryptCaches\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamCreateCachePreloaders(TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamCreateCachePreloaders\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamIsCacheLoadingEnabled(unsigned int uAppId, int *pbIsLoading, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamIsCacheLoadingEnabled\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamLoadCacheFromDir(unsigned int uAppId, const char *szPath, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamLoadCacheFromDir\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamLoadFileToCache()
{
#ifdef DEBUG
	Log->Write("SteamLoadFileToCache\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamStartLoadingCache(unsigned int uAppId, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamStartLoadingCache\n");
#endif
	return 1;
}

OPENEMU_API SteamCallHandle_t OPENEMU_CALL SteamStopLoadingCache(unsigned int uAppId, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamStopLoadingCache\n");
#endif
	return 1;
}