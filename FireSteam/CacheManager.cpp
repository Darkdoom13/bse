#include "CacheManager.h"
#include "HLLib/GCFFile.h"
#include "HLLib/Utility.h"

using namespace HLLib;

CCacheManager::CCacheManager(void)
{
}

CCacheManager::~CCacheManager(void)
{
	UnMountAll();
}

bool CCacheManager::MountGCFFile(const char* cszFileName, const char* cszMountPath)
{
	CGCFFile* newGCF = new CGCFFile();
	if (newGCF->Open(cszFileName, HL_MODE_READ))
	{
		newGCF->SetMountPath(cszMountPath);
		mCaches.push_back(newGCF);
		return true;
	}
	else
	{
		delete newGCF;
		return false;
	}
}

HLLib::Streams::IStream* CCacheManager::OpenFile(const char *cszFileName, const char *cszMode, unsigned int *puSize)
{
	CDirectoryFile* item = 0;
	unsigned int i = 0;

	while (i < mCaches.size() && !item)
	{
		item = (CDirectoryFile*) mCaches[i]->GetItemFromPath(cszFileName, HL_FIND_FILES);
		i++;
	}
	
	if (item)
	{
		i--;
		Streams::IStream* stream = 0;
		mCaches[i]->CreateStream(item, stream);

		if (stream)
		{
			stream->Open(HL_MODE_READ);
			*puSize = stream->GetStreamSize();
			return stream;
		}
	}

	return 0;
}

void CCacheManager::CloseFile(HLLib::Streams::IStream* pFile)
{
	pFile->Close();
	for (unsigned int i = 0; i < mCaches.size(); i++)
		mCaches[i]->ReleaseStream(pFile);
}

bool CCacheManager::FileStat(const char *cszFileName, TSteamElemInfo *pInfo)
{
	CDirectoryItem* item = 0;
	unsigned int i = 0;

	while (i < mCaches.size() && !item)
	{
		item = mCaches[i]->GetItemFromPath(cszFileName);
		i++;
	}

	if (item)
	{
		fillInfo(item, pInfo);
		return true;
	}

	return false;
}

TFindRecord* CCacheManager::FindFirst(const char *cszPattern, TSteamElemInfo *pFindInfo)
{
	TFindRecord* pFind = new TFindRecord();
	pFind->bLocalSearch = false;
	pFind->currCache = mCaches.begin();

	strcpy_s(pFind->cszFindPath, cszPattern);
	char* Period = strrchr(pFind->cszFindPath, '\\');
	strcpy_s(pFind->cszPattern, Period + 1);
	Period[0] = '\0';
	pFind->currItm = 0;

	while (pFind->currCache != mCaches.end() && !pFind->currItm)
	{
		CDirectoryFolder* searchFolder = (CDirectoryFolder*) (*pFind->currCache)->GetItemFromPath(pFind->cszFindPath, HL_FIND_FOLDERS);
		if (searchFolder)
			pFind->currItm = searchFolder->FindFirst(pFind->cszPattern);
		if (!pFind->currItm)
			pFind->currCache++;
	}

	if (!pFind->currItm)
	{
		delete pFind;
		return 0;
	}
	else
	{
		fillInfo(pFind->currItm, pFindInfo);
		return pFind;
	}
}

bool CCacheManager::FindNext(TFindRecord* pFind, TSteamElemInfo *pFindInfo)
{
	CDirectoryFolder* searchFolder = (CDirectoryFolder*) (*pFind->currCache)->GetItemFromPath(pFind->cszFindPath, HL_FIND_FOLDERS);
	if (searchFolder)
		pFind->currItm = searchFolder->FindNext(pFind->currItm, pFind->cszPattern);
	else
		pFind->currItm = 0;
	
	if (!pFind->currItm)
		pFind->currCache++;

	while (pFind->currCache != mCaches.end() && !pFind->currItm)
	{
		CDirectoryFolder* searchFolder = (CDirectoryFolder*) (*pFind->currCache)->GetItemFromPath(pFind->cszFindPath, HL_FIND_FOLDERS);
		if (searchFolder)
			pFind->currItm = searchFolder->FindFirst(pFind->cszPattern);
		pFind->currCache++;
	}
	
	if (pFind->currItm)
	{
		fillInfo(pFind->currItm, pFindInfo);
		return true;
	}
	else
		return false;
}

void CCacheManager::FindClose(TFindRecord* pFind)
{
	delete pFind;
}

void CCacheManager::fillInfo(CDirectoryItem *pItem, TSteamElemInfo *pInfo)
{
	if (pItem->GetType() == HL_ITEM_FILE)
	{
		pInfo->bIsDir = 0;
		pInfo->uSizeOrCount = ((CDirectoryFile*) pItem)->GetSize();
	}
	else
	{
		pInfo->bIsDir = 1;
		pInfo->uSizeOrCount = 0;
	}

	pInfo->bIsLocal = 0;

	strcpy_s(pInfo->cszName, pItem->GetName());

	pInfo->lCreationTime = 0x12341234;
	pInfo->lLastAccessTime = 0x12341234;
	pInfo->lLastModificationTime = 0x12341234;
}

void CCacheManager::UnMountAll()
{
	for (unsigned int i = 0; i < mCaches.size(); i++)
		delete mCaches[i];
	mCaches.clear();
}

bool CCacheManager::ExtractFile(const char* cszFileName)
{
	CDirectoryFile* item = 0;
	unsigned int i = 0;
	char path[MAX_PATH];

	// Find the file to extract in caches
	while (i < mCaches.size() && !item)
	{
		item = (CDirectoryFile*) mCaches[i]->GetItemFromPath(cszFileName, HL_FIND_FILES);
		i++;
	}

	if (item && item->GetExtractable())
	{
		// Extract the path part of the filename
		strcpy_s(path, cszFileName);
		char* delim = strrchr(path, '\\');
		delim[1] = '\0';

		// Make sure the path exists
		if (!GetFolderExists(path))
			for (unsigned int i = 0; i < strlen(path); i++)
				if (path[i] == '\\')
				{
					char temppath[MAX_PATH];
					strcpy_s(temppath, path);
					temppath[i] = '\0';
					if (!GetFolderExists(temppath))
						CreateFolder(temppath);
				}

		// Extract the file
		if (item->Extract(path))
			return true;
	}
	return false;
}