#pragma once

#include <vector>
#include <windows.h>
#include "HLLib/Package.h"
#include "OpenEmuCommon.h"

typedef std::vector< HLLib::CPackage* > CCacheList;

struct TFindRecord
{
	bool bLocalSearch;
	char cszPattern[MAX_PATH];
	char cszFindPath[MAX_PATH];
	HLLib::CDirectoryItem* currItm;
	CCacheList::iterator currCache;
	SteamHandle_t hFind;
};

class CCacheManager
{
public:
	CCacheManager(void);
	~CCacheManager(void);

	bool MountGCFFile(const char* cszFileName, const char* cszMountPath);
	HLLib::Streams::IStream* OpenFile(const char *cszFileName, const char *cszMode, unsigned int *puSize);
	void CloseFile(HLLib::Streams::IStream* pFile);
	bool FileStat(const char *cszFileName, TSteamElemInfo *pInfo);
	TFindRecord* FindFirst(const char *cszPattern, TSteamElemInfo *pFindInfo);
	bool FindNext(TFindRecord* pFind, TSteamElemInfo *pFindInfo);
	void FindClose(TFindRecord* pFind);
	void UnMountAll();
	bool ExtractFile(const char* cszFileName);

private:
	void fillInfo(HLLib::CDirectoryItem *pItem, TSteamElemInfo *pInfo);

	CCacheList mCaches;
};
