#include ".\ContentDescriptionRecord.h"

CContentDescriptionRecord::CContentDescriptionRecord()
{
	VersionNumber = 0;

	uAppMaxNameChars = 0;
	uAppMaxVersionLabelChars = 0;
	uAppMaxLaunchOptions = 0;
	uAppMaxLaunchOptionDescChars = 0;
	uAppMaxLaunchOptionCmdLineChars = 0;
	uAppMaxNumIcons = 0;
	uAppMaxIconSize = 0;

	uSubMaxNameChars = 0;
	uSubMaxApps = 0;
}

CContentDescriptionRecord::~CContentDescriptionRecord(){}

bool CContentDescriptionRecord::Enumerate(char* CDRBinary)
{
	char* CDRBinaryComp = NULL;

	TNodeHeader *NodeHeader = (TNodeHeader*)CDRBinary;
	if(NodeHeader->magic == NodeMagicNumComp)
	{
		CDRBinary += sizeof(TNodeHeader);
		TCompressedNode *CNode = (TCompressedNode*) CDRBinary;
		CDRBinary += sizeof(TCompressedNode);
		unsigned int compressedsize = NodeHeader->datalength - (sizeof(TNodeHeader) + sizeof(TCompressedNode));
		unsigned int uncopressedsize = CNode->uncompressedsize;
		CDRBinaryComp = new char[uncopressedsize];
		uncompress((Bytef*)CDRBinaryComp, (uLongf*)&uncopressedsize, (Bytef*)CDRBinary, compressedsize);
		CDRBinary = CDRBinaryComp;
		NodeHeader = (TNodeHeader*)CDRBinary;
	}

	if(NodeHeader->magic != NodeMagicNum)
	{
		return false;
	}
	else
	{
		char* NodeEnd = CDRBinary + NodeHeader->datalength;
		CDRBinary += sizeof(TNodeHeader);
		while(CDRBinary < NodeEnd)
		{
			TDescriptorNode *DNode = (TDescriptorNode*)CDRBinary;
			CDRBinary += sizeof(TDescriptorNode);
			switch (DNode->type)
			{
				case eCRDVersionNumber:
					{
						VersionNumber = *(unsigned short*)CDRBinary;
						CDRBinary += DNode->datalength;
						break;
					}
				case eCRDApplicationsRecord:
					{
						TNodeHeader *AppNodeHeader = (TNodeHeader*)CDRBinary;
						if(AppNodeHeader->magic != NodeMagicNum)
						{
						}
						else
						{
							char* AppRBinary = CDRBinary;
							char* AppNodeEnd = AppRBinary + AppNodeHeader->datalength;
							AppRBinary += sizeof(TNodeHeader);
							while(AppRBinary < AppNodeEnd)
							{
								AppRBinary += sizeof(TDescriptorNode);
								CAppRecord* newApp = new CAppRecord();
								AppRBinary = newApp->Enumerate(AppRBinary);
								ApplicationRecords.push_back(newApp);
							}
						}
						CDRBinary += DNode->datalength;
						break;
					}
				case eCRDSubscriptionsRecord :
					{
						TNodeHeader *SubNodeHeader = (TNodeHeader*)CDRBinary;
						if(SubNodeHeader->magic != NodeMagicNum)
						{
						}
						else
						{
							char* SubRBinary = CDRBinary;
							char* SubNodeEnd = SubRBinary + SubNodeHeader->datalength;
							SubRBinary += sizeof(TNodeHeader);
							while(SubRBinary < SubNodeEnd)
							{
								SubRBinary += sizeof(TDescriptorNode);
								CSubscriptionRecord* newSub = new CSubscriptionRecord();
								SubRBinary = newSub->Enumerate(SubRBinary);
								SubscriptionsRecord.push_back(newSub);
							}
						}
						CDRBinary += DNode->datalength;
						break;
					}
				case eCRDLastChangedExistingAppOrSubscriptionTime:
					CDRBinary += DNode->datalength;
					break;
				case eCRDIndexAppIdToSubscriptionIdsRecord:
					CDRBinary += DNode->datalength;
					break;
				case eCRDAllAppsPublicKeysRecord:
					{
						TNodeHeader *APKRNodeHeader = (TNodeHeader*)CDRBinary;
						if(APKRNodeHeader->magic != NodeMagicNum)
						{
						}
						else
						{
							char* APKRBinary = CDRBinary;
							char* APKRNodeEnd = APKRBinary + APKRNodeHeader->datalength;
							APKRBinary += sizeof(TNodeHeader);
							while(APKRBinary < APKRNodeEnd)
							{
								TDescriptorNode *APKRDNode = (TDescriptorNode*)APKRBinary;
								APKRBinary += sizeof(TDescriptorNode);
								unsigned int uAppId = APKRDNode->type;
								char* szKeyData = new char[APKRDNode->datalength];
								memcpy(szKeyData, APKRBinary, APKRDNode->datalength);
								APKRBinary += APKRDNode->datalength;
								AllAppsPublicKeysRecord[uAppId] = szKeyData;
							}
						}
						CDRBinary += DNode->datalength;
						break;
					}
				case eCRDAllAppsEncryptedPrivateKeysRecord:
					CDRBinary += DNode->datalength;
					break;
				default:
					CDRBinary += DNode->datalength;
					break;
			}
		}
	}
	GetMaxValues();
	if(CDRBinaryComp)
	{
		delete CDRBinaryComp;
	}
	return true;
}

CAppRecord* CContentDescriptionRecord::GetAppRecordById(unsigned int AppId)
{
	std::vector<CAppRecord*>::iterator AppRecordIterator = ApplicationRecords.begin();
	std::vector<CAppRecord*>::iterator AppRecordEnd = ApplicationRecords.end();
	for(; AppRecordIterator != AppRecordEnd; AppRecordIterator++ ) {
		if(((CAppRecord*)*AppRecordIterator)->AppId == AppId)
		{
			return (CAppRecord*)*AppRecordIterator;
		}
	}
	return NULL;
}

CSubscriptionRecord* CContentDescriptionRecord::GetSubRecordById(unsigned int SubscriptionId)
{
	std::vector<CSubscriptionRecord*>::iterator SubRecordIterator = SubscriptionsRecord.begin();
	std::vector<CSubscriptionRecord*>::iterator SubRecordEnd = SubscriptionsRecord.end();
	for(; SubRecordIterator != SubRecordEnd; SubRecordIterator++ ) {
		if(((CSubscriptionRecord*)*SubRecordIterator)->SubscriptionId == SubscriptionId)
		{
			return (CSubscriptionRecord*)*SubRecordIterator;
		}
	}
	return NULL;
}

void CContentDescriptionRecord::GetMaxValues()
{
	unsigned int lenght = 0;
	for(unsigned int i = 0;i < this->ApplicationRecords.size();i++)
	{
		for(unsigned int j = 0; j < this->ApplicationRecords[i]->VersionsRecord.size();j++)
		{
			lenght = (unsigned int)strlen(this->ApplicationRecords[i]->VersionsRecord[j]->Description) + 1;
			if(lenght > this->uAppMaxVersionLabelChars)
			{
				this->uAppMaxVersionLabelChars = lenght;
			}
		}
		for(unsigned int j = 0; j < this->ApplicationRecords[i]->LaunchOptionsRecord.size();j++)
		{
			lenght = (unsigned int)this->ApplicationRecords[i]->LaunchOptionsRecord.size();
			if(lenght > this->uAppMaxLaunchOptions)
			{
				this->uAppMaxLaunchOptions = lenght;
			}
			lenght = (unsigned int)strlen(this->ApplicationRecords[i]->LaunchOptionsRecord[j]->Description) + 1;
			if(lenght > this->uAppMaxLaunchOptionDescChars)
			{
				this->uAppMaxLaunchOptionDescChars = lenght;
			}
			lenght = (unsigned int)strlen(this->ApplicationRecords[i]->LaunchOptionsRecord[j]->CommandLine) + 1;
			if(lenght > this->uAppMaxLaunchOptionCmdLineChars)
			{
				this->uAppMaxLaunchOptionCmdLineChars = lenght;
			}
		}
		for(unsigned int j = 0; j < this->ApplicationRecords[i]->IconsRecord.size();j++)
		{
			lenght = (unsigned int)this->ApplicationRecords[i]->IconsRecord.size();
			if(lenght > this->uAppMaxNumIcons)
			{
				this->uAppMaxNumIcons = lenght;
			}
		}
		lenght = (unsigned int)strlen(this->ApplicationRecords[i]->Name) + 1;
		if(lenght > this->uAppMaxNameChars)
		{
			this->uAppMaxNameChars = lenght;
		}
	}
	for(unsigned int i = 0;i < this->SubscriptionsRecord.size();i++)
	{
		lenght = (unsigned int)strlen(this->SubscriptionsRecord[i]->Name) + 1;
		if(lenght > this->uSubMaxNameChars)
		{
			this->uSubMaxNameChars = lenght;
		}
		lenght = (unsigned int)this->SubscriptionsRecord[i]->AppIds.size();
		if(lenght > this->uSubMaxApps)
		{
			this->uSubMaxApps = lenght;
		}
	}
}
