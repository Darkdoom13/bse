#include ".\ContentDescriptionRecord\AppRecord.h"
#include ".\ContentDescriptionRecord\SubscriptionRecord.h"

#include "..\ZipLib\zlib.h"

#pragma once

typedef enum
{
	eCRDVersionNumber,
	eCRDApplicationsRecord,
	eCRDSubscriptionsRecord,
	eCRDLastChangedExistingAppOrSubscriptionTime,
	eCRDIndexAppIdToSubscriptionIdsRecord,
	eCRDAllAppsPublicKeysRecord,
	eCRDAllAppsEncryptedPrivateKeysRecord
}ECDRType;

class CContentDescriptionRecord
{
public:
	unsigned short VersionNumber;
	std::vector<CAppRecord*> ApplicationRecords;
	std::vector<CSubscriptionRecord*> SubscriptionsRecord;
	std::map<unsigned int, char*> AllAppsPublicKeysRecord;

	//AppMaxValues
	unsigned int uAppMaxNameChars;
	unsigned int uAppMaxVersionLabelChars;
	unsigned int uAppMaxLaunchOptions;
	unsigned int uAppMaxLaunchOptionDescChars;
	unsigned int uAppMaxLaunchOptionCmdLineChars;
	unsigned int uAppMaxNumIcons;
	unsigned int uAppMaxIconSize;

	//SubMaxValues
	unsigned int uSubMaxNameChars;
	unsigned int uSubMaxApps;

	CContentDescriptionRecord();
	~CContentDescriptionRecord();

	bool Enumerate(char* CDRBinary);
	CAppRecord* GetAppRecordById(unsigned int AppId);
	CSubscriptionRecord* GetSubRecordById(unsigned int SubscriptionId);

private:
	void GetMaxValues();
};
