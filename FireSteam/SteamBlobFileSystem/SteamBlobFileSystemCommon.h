#pragma once

typedef enum
{
	Key = 1,
	Value = 2
}ESteamBlobNodeType;

typedef enum
{
	String,
	dword,
	RawBinaryData
}ESteamBlobValueType;

#pragma pack (push, 2)
typedef struct
{
    unsigned short magic;
    unsigned int datalength;
    unsigned int nullpadding;
}TNodeHeader;

typedef struct
{
    unsigned short descriptorlength;
    unsigned int datalength;
	ESteamBlobNodeType type;
}TDescriptorNode;

typedef struct
{
	unsigned int uncompressedsize;
	unsigned int unknown1;
	unsigned short unknown2;
}TCompressedNode;

typedef struct
{
    unsigned short descriptorlength;
    unsigned int  datalength;
}TNode;
#pragma pack (pop)

typedef struct
{
	ESteamBlobValueType Type;
	unsigned int ValueSize;
	char *Value;
}TNodeValue;

struct strCmp {
	bool operator()( const char* s1, const char* s2 ) const {
	  return strcmp( s1, s2 ) < 0;
	}
};

const unsigned short NodeMagicNum = 0x5001;
const unsigned short NodeMagicNumComp = 0x4301;