#pragma once

#include <vector>
#include ".\SteamBlobFileSystemCommon.h"

class CSteamBlobNode
{
public:
	char *Name;
	ESteamBlobNodeType Type;
	std::vector<CSteamBlobNode*> Nodes;
	TNodeValue *KeyValue;

	CSteamBlobNode();
	~CSteamBlobNode();

	void Populate(char *NodeBinary);
	char* SubKeys(char *NodeBinary);
	char* KeyEntry(char *NodeBinary);
	char* KeyEntries(char *NodeBinary);
	char* ValueProperties(char *NodeBinary);
	char* ValueEntry(char *NodeBinary);
	char* ValueEntries(char *NodeBinary);
};