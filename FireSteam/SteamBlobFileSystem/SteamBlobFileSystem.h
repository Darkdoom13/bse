#pragma once

#include <stdio.h>
#include <windows.h>

#include ".\SteamBlobNode.h"

class CSteamBlobFileSystem
{
public:
	FILE* BlobFile;
	CSteamBlobNode *TopKey;
private:
	char *BlobBinary;
public:
	CSteamBlobFileSystem();
	~CSteamBlobFileSystem();

	bool Open(const char* cszFileName);
	bool Close();
	CSteamBlobNode *GetNodeByPath(const char* cszNodePath);
};
