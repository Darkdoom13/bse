#pragma once

#include <vector>
#include ".\SubscriptionDiscountQualifier.h"

typedef enum
{
	eSDRName = 1,
	eSDRDiscountInCents = 2,
	eSDRDiscountQualifiers = 3
}ESubDiscountFields;

class CSubscriptionDiscountRecord
{
public:
	unsigned int DiscountId;
	char* Name;
	unsigned int DiscountInCents;
	std::vector<CSubscriptionDiscountQualifier*> DiscountQualifiers;

	CSubscriptionDiscountRecord();
	~CSubscriptionDiscountRecord();

	char* Enumerate(char* SDRBinary);
};
