#include ".\SubscriptionDiscountQualifier.h"

CSubscriptionDiscountQualifier::CSubscriptionDiscountQualifier()
{
	QualifierId = 0;
	Name = NULL;
	SubscriptionId = 0;
}
CSubscriptionDiscountQualifier::~CSubscriptionDiscountQualifier(){}

char* CSubscriptionDiscountQualifier::Enumerate(char* SDQBinary)
{
	TNodeHeader *NodeHeader = (TNodeHeader*)SDQBinary;
	if(NodeHeader->magic != NodeMagicNum)
	{
		return NULL;
	}
	else
	{
		char* NodeEnd = SDQBinary + NodeHeader->datalength;
		SDQBinary += sizeof(TNodeHeader);
		while(SDQBinary < NodeEnd)
		{
			TDescriptorNode *DNode = (TDescriptorNode*)SDQBinary;
			SDQBinary += sizeof(TDescriptorNode);
			switch (DNode->type)
			{
				case eSDQName:
					this->Name = new char[DNode->datalength];
					memcpy(this->Name, SDQBinary, DNode->datalength);
					SDQBinary += DNode->datalength;
					break;
				case eSDQSubscriptionId:
					this->SubscriptionId = *(unsigned int*)SDQBinary;
					SDQBinary += DNode->datalength;
					break;
				default:
					SDQBinary += DNode->datalength;
					break;
			}
		}
		return (SDQBinary + NodeHeader->nullpadding);
	}
}
