#pragma once

#include <vector>
#include <map>
#include ".\SubscriptionDiscountRecord.h"

typedef enum
{
	eSubSubscriptionId = 1,
	eSubName = 2,
	eSubBillingType = 3,
	eSubCostInCents = 4,
	eSubPeriodInMinutes = 5,
	eSubAppIds = 6,
	eSubRunAppId = 7,
	eSubOnSubscribeRunLaunchOptionIndex = 8,
	eSubRateLimitRecord = 9,
	eSubDiscounts = 10,
	eSubIsPreorder = 11,
	eSubRequiresShippingAddress = 12,
	eSubDomesticCostInCents = 13,
	eSubInternationalCostInCents = 14,
	eSubRequiredKeyType = 15,
	eSubIsCyberCafe = 16,
	eSubGameCode = 17,
	eSubGameCodeDescription = 18,
	eSubIsDisabled = 19,
	eSubRequiresCD = 20,
	eSubTerritoryCode = 21,
	eSubIsSteam3Subscription = 22,
	eSubExtendedInfoRecords = 23
}ESubscriptionFields;

typedef enum
{
	eSBTNoCost,
	eSBTBillOnceOnly,
	eSBTBillMonthly,
	eSBTProofOfPrepurchaseOnly,
	eSBTGuestPass,
	eSBTHardwarePromo
}ESubBillingType;

class CSubscriptionRecord
{
public:
	unsigned int SubscriptionId;
	char* Name;
	ESubBillingType BillingType;
	unsigned int CostInCents;
	int PeriodInMinutes;
	std::vector<unsigned int> AppIds;
	int RunAppId;
	int OnSubscribeRunLaunchOptionIndex;
	//CRateLimitRecord* RateLimitRecord;
	std::vector<CSubscriptionDiscountRecord*> Discounts;
	bool IsPreorder;
	bool RequiresShippingAddress;
	unsigned int DomesticCostInCents;
	unsigned int InternationalCostInCents;
	unsigned int RequiredKeyType;
	bool IsCyberCafe;
	int GameCode;
	char* GameCodeDescription;
	bool IsDisabled;
	bool RequiresCD;
	unsigned int TerritoryCode;
	bool IsSteam3Subscription;
	std::map<char*, char*, strCmp> ExtendedInfoRecords;

	CSubscriptionRecord();
	~CSubscriptionRecord();

	char* Enumerate(char* SubRBinary);
};
