#include ".\AppVersionRecord.h"

CAppVersionRecord::CAppVersionRecord()
{
	Description = NULL;
	VersionId = 0;
	IsNotAvailable = false;
	DepotEncryptionKey = NULL;
	IsEncryptionKeyAvailable = false;
	IsRebased = false;
	IsLongVersionRoll = false;
}
CAppVersionRecord::~CAppVersionRecord(){}

char* CAppVersionRecord::Enumerate(char* VRBinary)
{
	TNodeHeader *NodeHeader = (TNodeHeader*)VRBinary;
	if(NodeHeader->magic != NodeMagicNum)
	{
		return NULL;
	}
	else
	{
		char* NodeEnd = VRBinary + NodeHeader->datalength;
		VRBinary += sizeof(TNodeHeader);
		while(VRBinary < NodeEnd)
		{
			TDescriptorNode *DNode = (TDescriptorNode*)VRBinary;
			VRBinary += sizeof(TDescriptorNode);
			switch (DNode->type)
			{
				case eVRDescription:
					this->Description = new char[DNode->datalength];
					memcpy(this->Description, VRBinary, DNode->datalength);
					VRBinary += DNode->datalength;
					break;
				case eVRVersionId:
					this->VersionId = *(unsigned int*)VRBinary;
					VRBinary += DNode->datalength;
					break;
				case eVRIsNotAvailable:
					this->IsNotAvailable = *(bool*)VRBinary;
					VRBinary += DNode->datalength;
					break;
				case eVRLaunchOptionIdsRecord:
					{
						TNodeHeader *LOIRNodeHeader = (TNodeHeader*)VRBinary;
						if(LOIRNodeHeader->magic != NodeMagicNum)
						{
						}
						else
						{
							char* LOIRBinary = VRBinary;
							char* LOIRNodeEnd = LOIRBinary + LOIRNodeHeader->datalength;
							LOIRBinary += sizeof(TNodeHeader);
							while(LOIRBinary < LOIRNodeEnd)
							{
								LOIRBinary += sizeof(TDescriptorNode);
								unsigned int newLOIR = *(unsigned int*)LOIRBinary;
								LaunchOptionIdsRecord.push_back(newLOIR);
							}
						}
						VRBinary += DNode->datalength;
						break;
					}
				case eVRDepotEncryptionKey:
					this->DepotEncryptionKey = new char[DNode->datalength];
					memcpy(this->DepotEncryptionKey, VRBinary, DNode->datalength);
					VRBinary += DNode->datalength;
					break;
				case eVRIsEncryptionKeyAvailable:
					this->IsEncryptionKeyAvailable = *(bool*)VRBinary;
					VRBinary += DNode->datalength;
					break;
				case eVRIsRebased:
					this->IsRebased = *(bool*)VRBinary;
					VRBinary += DNode->datalength;
					break;
				case eVRIsLongVersionRoll:
					this->IsLongVersionRoll = *(bool*)VRBinary;
					VRBinary += DNode->datalength;
					break;
			}
		}
		return (VRBinary + NodeHeader->nullpadding);
	}
}
