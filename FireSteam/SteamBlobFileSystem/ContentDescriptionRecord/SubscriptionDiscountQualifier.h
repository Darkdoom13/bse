#pragma once

#include <windows.h>
#include "..\SteamBlobFileSystemCommon.h"

typedef enum
{
	eSDQName = 1,
	eSDQSubscriptionId = 2
}ESubDiscountQualifierFields;

class CSubscriptionDiscountQualifier
{
public:
	unsigned int QualifierId;
	char* Name;
	unsigned int SubscriptionId;

	CSubscriptionDiscountQualifier();
	~CSubscriptionDiscountQualifier();

	char* Enumerate(char* SDQBinary);
};
