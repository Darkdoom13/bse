#include <windows.h>
#include ".\AppLaunchOptionRecord.h"

CAppLaunchOptionRecord::CAppLaunchOptionRecord()
{
	Description = NULL;
    CommandLine = NULL;
	IconIndex = 0;
	NoDesktopShortcut = false;
	NoStartMenuShortcut = false;
	LongRunningUnattended = false;
}

CAppLaunchOptionRecord::~CAppLaunchOptionRecord(){}

char* CAppLaunchOptionRecord::Enumerate(char* LOBinary)
{
	TNodeHeader *NodeHeader = (TNodeHeader*)LOBinary;
	if(NodeHeader->magic != NodeMagicNum)
	{
		return NULL;
	}
	else
	{
		char* NodeEnd = LOBinary + NodeHeader->datalength;
		LOBinary += sizeof(TNodeHeader);
		while(LOBinary < NodeEnd)
		{
			TDescriptorNode *DNode = (TDescriptorNode*)LOBinary;
			LOBinary += sizeof(TDescriptorNode);
			switch (DNode->type)
			{
				case eLODescription:
					this->Description = new char[DNode->datalength];
					memcpy(this->Description, LOBinary, DNode->datalength);
					LOBinary += DNode->datalength;
					break;
				case eLOCommandLine:
					this->CommandLine = new char[DNode->datalength];
					memcpy(this->CommandLine, LOBinary, DNode->datalength);
					LOBinary += DNode->datalength;
					break;
				case eLOIconIndex:
					this->IconIndex = *(int*)LOBinary;
					LOBinary += DNode->datalength;
					break;
				case eLONoDesktopShortcut:
					this->NoDesktopShortcut = *(bool*)LOBinary;
					LOBinary += DNode->datalength;
					break;
				case eLONoStartMenuShortcut:
					this->NoStartMenuShortcut = *(bool*)LOBinary;
					LOBinary += DNode->datalength;
					break;
				case eLOLongRunningUnattended:
					this->LongRunningUnattended = *(bool*)LOBinary;
					LOBinary += DNode->datalength;
					break;
				default:
					LOBinary += DNode->datalength;
					break;
			}
		}
		return (LOBinary + NodeHeader->nullpadding);
	}
}
