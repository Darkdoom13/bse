#include ".\SubscriptionRecord.h"

CSubscriptionRecord::CSubscriptionRecord()
{
	SubscriptionId = 0;
	Name = NULL;
	BillingType = eSBTNoCost;
	CostInCents = 0;
	PeriodInMinutes = -1;
	RunAppId = -1;
	OnSubscribeRunLaunchOptionIndex = -1;
	//RateLimitRecord;
	IsPreorder = false;
	RequiresShippingAddress = false;
	DomesticCostInCents = 0;
	InternationalCostInCents = 0;
	RequiredKeyType = 0;
	IsCyberCafe = false;
	GameCode = -1;
	GameCodeDescription = NULL;
	IsDisabled = false;
	RequiresCD = false;
	TerritoryCode = 0;
	IsSteam3Subscription = false;
}
CSubscriptionRecord::~CSubscriptionRecord(){}

char* CSubscriptionRecord::Enumerate(char* SubRBinary)
{
	TNodeHeader *NodeHeader = (TNodeHeader*)SubRBinary;
	if(NodeHeader->magic != NodeMagicNum)
	{
		return NULL;
	}
	else
	{
		char* NodeEnd = SubRBinary + NodeHeader->datalength;
		SubRBinary += sizeof(TNodeHeader);
		while(SubRBinary < NodeEnd)
		{
			TDescriptorNode *DNode = (TDescriptorNode*)SubRBinary;
			SubRBinary += sizeof(TDescriptorNode);
			switch (DNode->type)
			{
				case eSubSubscriptionId:
					this->SubscriptionId = *(unsigned int*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubName:
					this->Name = new char[DNode->datalength];
					memcpy(this->Name, SubRBinary, DNode->datalength);
					SubRBinary += DNode->datalength;
					break;
				case eSubBillingType:
					{
					short billingtype = *(unsigned short*)SubRBinary;
					this->BillingType = (ESubBillingType)billingtype;
					SubRBinary += DNode->datalength;
					break;
					}
				case eSubCostInCents:
					this->CostInCents = *(unsigned int*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubPeriodInMinutes:
					this->PeriodInMinutes = *(int*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubAppIds:
					{
						TNodeHeader *SAINodeHeader = (TNodeHeader*)SubRBinary;
						if(SAINodeHeader->magic != NodeMagicNum)
						{
						}
						else
						{
							char* SAIBinary = SubRBinary;
							char* SAINodeEnd = SAIBinary + SAINodeHeader->datalength;
							SAIBinary += sizeof(TNodeHeader);
							while(SAIBinary < SAINodeEnd)
							{
								TDescriptorNode *SAIDNode = (TDescriptorNode*)SAIBinary;
								SAIBinary += sizeof(TDescriptorNode);
								unsigned int newSAI = SAIDNode->type;
								AppIds.push_back(newSAI);
							}
						}
						SubRBinary += DNode->datalength;
						break;
					}
				case eSubRunAppId:
					this->RunAppId = *(int*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubOnSubscribeRunLaunchOptionIndex:
					this->OnSubscribeRunLaunchOptionIndex = *(int*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubRateLimitRecord:
					SubRBinary += DNode->datalength;
					break;
				case eSubDiscounts:
					{
						TNodeHeader *SDNodeHeader = (TNodeHeader*)SubRBinary;
						if(SDNodeHeader->magic != NodeMagicNum)
						{
						}
						else
						{
							char* SDBinary = SubRBinary;
							char* SDNodeEnd = SDBinary + SDNodeHeader->datalength;
							SDBinary += sizeof(TNodeHeader);
							while(SDBinary < SDNodeEnd)
							{
								CSubscriptionDiscountRecord* newSD = new CSubscriptionDiscountRecord();
								TDescriptorNode* SDDNode = (TDescriptorNode*)SDBinary;
								newSD->DiscountId = SDDNode->type;
								SDBinary += sizeof(TDescriptorNode);
								SDBinary = newSD->Enumerate(SDBinary);
								Discounts.push_back(newSD);
							}
						}
						SubRBinary += DNode->datalength;
						break;
					}
				case eSubIsPreorder:
					this->IsPreorder = *(bool*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubRequiresShippingAddress:
					this->RequiresShippingAddress = *(bool*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubDomesticCostInCents:
					this->DomesticCostInCents = *(unsigned int*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubInternationalCostInCents:
					this->InternationalCostInCents = *(unsigned int*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubRequiredKeyType:
					this->RequiredKeyType = *(unsigned int*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubIsCyberCafe:
					this->IsCyberCafe = *(bool*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubGameCode:
					this->GameCode = *(int*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubGameCodeDescription:
					this->GameCodeDescription = new char[DNode->datalength];
					memcpy(this->GameCodeDescription, SubRBinary, DNode->datalength);
					SubRBinary += DNode->datalength;
					break;
				case eSubIsDisabled:
					this->IsDisabled = *(bool*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubRequiresCD:
					this->RequiresCD = *(bool*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubTerritoryCode:
					this->TerritoryCode = *(unsigned int*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubIsSteam3Subscription:
					this->IsSteam3Subscription = *(bool*)SubRBinary;
					SubRBinary += DNode->datalength;
					break;
				case eSubExtendedInfoRecords:
					{
						TNodeHeader *EIRNodeHeader = (TNodeHeader*)SubRBinary;
						if(EIRNodeHeader->magic != NodeMagicNum)
						{
						}
						else
						{
							char* EIRBinary = SubRBinary;
							char* EIRNodeEnd = EIRBinary + EIRNodeHeader->datalength;
							EIRBinary += sizeof(TNodeHeader);
							while(EIRBinary < EIRNodeEnd)
							{
								TNode *EIRNode = (TNode*)EIRBinary;
								EIRBinary += sizeof(TNode);
								char* szKeyName = new char[EIRNode->descriptorlength + 1];
								memset(szKeyName, 0, EIRNode->descriptorlength + 1);
								memcpy(szKeyName, EIRBinary, EIRNode->descriptorlength);
								EIRBinary += EIRNode->descriptorlength;
								char* szValue = new char[EIRNode->datalength];
								memcpy(szValue, EIRBinary, EIRNode->datalength);
								EIRBinary += EIRNode->datalength;
								ExtendedInfoRecords[szKeyName] = szValue;
							}
						}
						SubRBinary += DNode->datalength;
						break;
					}
				default:
					SubRBinary += DNode->datalength;
					break;
			}
		}
		return (SubRBinary + NodeHeader->nullpadding);
	}
}
