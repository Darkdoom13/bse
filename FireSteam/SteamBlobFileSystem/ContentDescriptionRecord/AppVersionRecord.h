#pragma once

#include <vector>
#include "..\SteamBlobFileSystemCommon.h"

typedef enum
{
	eVRDescription = 1,
	eVRVersionId = 2,
	eVRIsNotAvailable = 3,
	eVRLaunchOptionIdsRecord = 4,
	eVRDepotEncryptionKey = 5,
	eVRIsEncryptionKeyAvailable = 6,
	eVRIsRebased = 7,
	eVRIsLongVersionRoll = 8
}EVersionRecordFields;

class CAppVersionRecord
{
public:
	char* Description;
	unsigned int VersionId;
	bool IsNotAvailable;
	std::vector<unsigned int> LaunchOptionIdsRecord;
	char* DepotEncryptionKey;
	bool IsEncryptionKeyAvailable;
	bool IsRebased;
	bool IsLongVersionRoll;

	CAppVersionRecord();
	~CAppVersionRecord();

	char* Enumerate(char* VRBinary);
};
