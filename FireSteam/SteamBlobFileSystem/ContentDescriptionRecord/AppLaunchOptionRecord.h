#pragma once

#include "..\SteamBlobFileSystemCommon.h"

typedef enum
{
	eLODescription = 1,
	eLOCommandLine = 2,
	eLOIconIndex = 3,
	eLONoDesktopShortcut = 4,
	eLONoStartMenuShortcut = 5,
	eLOLongRunningUnattended = 6
}ELaunchOptionFields;

class CAppLaunchOptionRecord
{
public:
	char* Description;
	char* CommandLine;
	int IconIndex;
	bool NoDesktopShortcut;
	bool NoStartMenuShortcut;
	bool LongRunningUnattended;

	CAppLaunchOptionRecord();
	~CAppLaunchOptionRecord();

	char* Enumerate(char* LOBinary);
};
