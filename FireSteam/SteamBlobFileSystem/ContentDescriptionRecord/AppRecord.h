#pragma once

#include <vector>
#include <map>

#include "AppLaunchOptionRecord.h"
#include "AppIconRecord.h"
#include "AppVersionRecord.h"
#include "AppFilesystemRecord.h"

typedef enum
{
	eAppAppId = 1,
	eAppName = 2,
	eAppInstallDirName = 3,
	eAppMinCacheFileSizeMB = 4,
	eAppMaxCacheFileSizeMB = 5,
	eAppLaunchOptionsRecord = 6,
	eAppAppIconsRecord = 7,
	eAppOnFirstLaunch = 8,
	eAppIsBandwidthGreedy = 9,
	eAppVersionsRecord = 10,
	eAppCurrentVersionId = 11,
	eAppFilesystemRecords = 12,
	eAppTrickleVersionId = 13,
	eAppUserDefinedRecords = 14,
	eAppBetaVersionPassword = 15,
	eAppBetaVersionId = 16,
	eAppLegacyInstallDirName = 17,
	eAppSkipMFPOverwrite = 18,
	eAppUseFilesystemDvr = 19,
	eAppManifestOnlyApp = 20,
	eAppAppOfManifestOnlyCache = 21,
}EApplicationFields;

class CAppRecord
{
public:
	unsigned int AppId;
	char* Name;
	char* InstallDirName;
	unsigned int MinCacheFileSizeMB;
	unsigned int MaxCacheFileSizeMB;
	std::vector<CAppLaunchOptionRecord*> LaunchOptionsRecord;
	std::vector<CAppIconRecord*> IconsRecord;
	int OnFirstLaunch;
	bool IsBandwidthGreedy;
	std::vector<CAppVersionRecord*> VersionsRecord;
	unsigned int CurrentVersionId;
	std::vector<CAppFilesystemRecord*> FilesystemsRecord;
	int TrickleVersionId;
	std::map<char*, char*, strCmp> UserDefinedRecords;
	char* BetaVersionPassword;
	int BetaVersionId;
	char* LegacyInstallDirName;
	bool SkipMFPOverwrite;
	bool UseFilesystemDvr;
	bool ManifestOnlyApp;
	unsigned int AppOfManifestOnlyCache;

	CAppRecord();
	~CAppRecord();

	char* Enumerate(char* AppRBinary);
};
