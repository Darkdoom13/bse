#pragma once

#include "..\SteamBlobFileSystemCommon.h"

typedef enum
{
	eFSRAppId = 1,
	eFSRMountName = 2,
	eFSRIsOptional = 3
}EFileSystemFields;

class CAppFilesystemRecord
{
public:
	unsigned int AppId;
	char* MountName;
	bool IsOptional;
public:
	CAppFilesystemRecord();
	~CAppFilesystemRecord();

	char* Enumerate(char* FSRBinary);
};
