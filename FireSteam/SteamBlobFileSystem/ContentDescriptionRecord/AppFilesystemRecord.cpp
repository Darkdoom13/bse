#include <windows.h>
#include ".\AppFilesystemRecord.h"

CAppFilesystemRecord::CAppFilesystemRecord()
{
	AppId = 0;
	MountName = NULL;
	IsOptional = false;
}
CAppFilesystemRecord::~CAppFilesystemRecord(){}

char* CAppFilesystemRecord::Enumerate(char* FSRBinary)
{
	TNodeHeader *NodeHeader = (TNodeHeader*)FSRBinary;
	if(NodeHeader->magic != NodeMagicNum)
	{
		return NULL;
	}
	else
	{
		char* NodeEnd = FSRBinary + NodeHeader->datalength;
		FSRBinary += sizeof(TNodeHeader);
		while(FSRBinary < NodeEnd)
		{
			TDescriptorNode *DNode = (TDescriptorNode*)FSRBinary;
			FSRBinary += sizeof(TDescriptorNode);
			switch (DNode->type)
			{
				case eFSRAppId:
					this->AppId = *(unsigned int*)FSRBinary;
					FSRBinary += DNode->datalength;
					break;
				case eFSRMountName:
					this->MountName = new char[DNode->datalength];
					memcpy(this->MountName, FSRBinary, DNode->datalength);
					FSRBinary += DNode->datalength;
					break;
				case eFSRIsOptional:
					this->IsOptional = *(bool*)FSRBinary;
					FSRBinary += DNode->datalength;
					break;
				default:
					FSRBinary += DNode->datalength;
					break;
			}
		}
		return (FSRBinary + NodeHeader->nullpadding);
	}
}
