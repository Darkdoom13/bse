#pragma warning(disable:4996)//disable deprecated warnings

#include ".\SteamBlobFileSystem.h"

CSteamBlobFileSystem::CSteamBlobFileSystem(){}
CSteamBlobFileSystem::~CSteamBlobFileSystem(){}

bool CSteamBlobFileSystem::Open(const char* cszFileName)
{
	if(BlobFile = fopen(cszFileName, "rb"))
	{
		fseek(BlobFile,0, SEEK_END);
		int BlobFileSize = ftell(BlobFile);
		fseek(BlobFile,0, SEEK_SET);
		BlobBinary = new char[BlobFileSize];
		if(fread(BlobBinary, 1, BlobFileSize, BlobFile))
		{
			TopKey = new CSteamBlobNode();
			TopKey->Populate(BlobBinary);
		}
		Close();
		return true;
	}
	return false;
}

bool CSteamBlobFileSystem::Close()
{
	if(fclose(BlobFile) == 0)
	{
		delete BlobBinary;
		return true;
	}
	return false;
}

CSteamBlobNode *CSteamBlobFileSystem::GetNodeByPath(const char* cszNodePath)
{
	char* szTempNodePath = new char[strlen(cszNodePath)+1];
	strcpy(szTempNodePath, cszNodePath);
	char* szNodeName = strtok(szTempNodePath, "\\");
	std::vector<CSteamBlobNode*>::iterator NodesIterator = TopKey->Nodes.begin();
	std::vector<CSteamBlobNode*>::iterator NodesEnd = TopKey->Nodes.end();
	for(; NodesIterator != NodesEnd; NodesIterator++ ) {
		if(strcmp(((CSteamBlobNode*)*NodesIterator)->Name, szNodeName) == 0 && szNodeName != NULL)
		{
			szNodeName = strtok(NULL, "\\");
			if(szNodeName == NULL)
			{
				delete szTempNodePath;
				return (CSteamBlobNode*)*NodesIterator;
			}
			if(((CSteamBlobNode*)*NodesIterator)->Nodes.size() != 0)
			{
				std::vector<CSteamBlobNode*>::iterator NodesIterator = ((CSteamBlobNode*)*NodesIterator)->Nodes.begin();
				std::vector<CSteamBlobNode*>::iterator NodesEnd = ((CSteamBlobNode*)*NodesIterator)->Nodes.end();
			}
			else
			{
				break;
			}
		}
	}
	delete szTempNodePath;
	return NULL;
}
