#pragma once

/*
** MiniDump
*/

OPENEMU_API int OPENEMU_CALL SteamWriteMiniDumpFromAssert()
{
#ifdef DEBUG
	Log->Write("SteamWriteMiniDumpFromAssert\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamWriteMiniDumpSetComment(const char *cszComment)
{
#ifdef DEBUG
	Log->Write("SteamWriteMiniDumpSetComment:\n%s\n", cszComment);
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamWriteMiniDumpUsingExceptionInfo()
{
#ifdef DEBUG
	Log->Write("SteamWriteMiniDumpUsingExceptionInfo\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamWriteMiniDumpUsingExceptionInfoWithBuildId()
{
#ifdef DEBUG
	Log->Write("SteamWriteMiniDumpUsingExceptionInfoWithBuildId\n");
#endif
	return 1;
}