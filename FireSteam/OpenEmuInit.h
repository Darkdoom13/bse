#pragma once

/*
** Initialization
*/

OPENEMU_API int OPENEMU_CALL SteamStartEngine(TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamStartEngine\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamStartup(unsigned int uUsingMask, TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamStartup:\n");
	if((uUsingMask & STEAM_USING_FILESYSTEM) != 0){Log->Write("\tUsing FileSystem\n");}
	if((uUsingMask & STEAM_USING_LOGGING) != 0){Log->Write("\tUsing Logging\n");}
	if((uUsingMask & STEAM_USING_USERID) != 0){Log->Write("\tUsing UserID\n");}
	if((uUsingMask & STEAM_USING_ACCOUNT) != 0){Log->Write("\tUsing Account\n");}
#endif

	SteamClearError(pError);
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamCleanup(TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamCleanup\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamGetVersion(char *szVersion, unsigned int uVersionBufSize)
{
#ifdef DEBUG
	Log->Write("SteamGetVersion: buffsize:%u\n", uVersionBufSize);
#endif
	return 0;
}

OPENEMU_API int OPENEMU_CALL SteamShutdownEngine(TSteamError *pError)
{
#ifdef DEBUG
	Log->Write("SteamShutdownEngine\n");
#endif
	return 1;
}