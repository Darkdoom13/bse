#ifndef INCLUDED_OPENEMU_H
#define INCLUDED_OPENEMU_H

#pragma once

#ifndef INCLUDED_OPENEMU_COMMON_OPENEMUCOMMON_H
	#include ".\OpenEmuCommon.h"
#endif

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the OPENEMU_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// OPENEMU_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifdef __cplusplus
extern "C"
{
#endif

/******************************************************************************
**
** Exported function prototypes
**
******************************************************************************/

/*
** Interface
*/

OPENEMU_API unsigned int		OPENEMU_CALL	CreateInterface(const char* cszSteamDLLAppsystemInterfaceVersion, TSteamError *pError);
OPENEMU_API unsigned int		OPENEMU_CALL	_f(const char* cszSteamInterface);

/*
** Initialization
*/

OPENEMU_API int					OPENEMU_CALL	SteamStartEngine( TSteamError *pError );
OPENEMU_API int					OPENEMU_CALL	SteamStartup( unsigned int uUsingMask, TSteamError *pError );
OPENEMU_API int					OPENEMU_CALL	SteamCleanup( TSteamError *pError );
OPENEMU_API int					OPENEMU_CALL	SteamGetVersion( char *szVersion, unsigned int uVersionBufSize );
OPENEMU_API int					OPENEMU_CALL	SteamShutdownEngine( TSteamError *pError );

/*
** Asynchrounous call handling
*/

OPENEMU_API int					OPENEMU_CALL	SteamProcessCall( SteamCallHandle_t handle, TSteamProgress *pProgress, TSteamError *pError );
OPENEMU_API int					OPENEMU_CALL	SteamAbortCall( SteamCallHandle_t handle, TSteamError *pError );
OPENEMU_API int					OPENEMU_CALL	SteamBlockingCall( SteamCallHandle_t handle, unsigned int uiProcessTickMS, TSteamError *pError );
OPENEMU_API int					OPENEMU_CALL	SteamSetMaxStallCount( unsigned int uNumStalls, TSteamError *pError );
							
/*
** Filesystem
*/

OPENEMU_API int					OPENEMU_CALL	SteamMountAppFilesystem(TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamUnmountAppFilesystem(TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamMountFilesystem(unsigned int uAppId, const char *szMountPath, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamUnmountFilesystem(const char *szMountPath, TSteamError *pError);
OPENEMU_API SteamHandle_t		OPENEMU_CALL	SteamOpenFileEx(const char *cszFileName, const char *cszMode, unsigned int *puSize, TSteamError *pError);
OPENEMU_API SteamHandle_t		OPENEMU_CALL	SteamOpenFile(const char *cszFileName, const char *cszMode, TSteamError *pError);
OPENEMU_API SteamHandle_t		OPENEMU_CALL	SteamOpenTmpFile(TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamFlushFile(SteamHandle_t hFile, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamCloseFile(SteamHandle_t hFile, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamSetvBuf(SteamHandle_t hFile, void* pBuf, ESteamBufferMethod eMethod, unsigned int uBytes, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetc(SteamHandle_t hFile, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamPutc(int cChar, SteamHandle_t hFile, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamPrintFile(SteamHandle_t hFile, TSteamError *pError, const char *cszFormat, ...);
OPENEMU_API unsigned int		OPENEMU_CALL	SteamReadFile(void *pBuf, unsigned int uSize, unsigned int uCount, SteamHandle_t hFile, TSteamError *pError);
OPENEMU_API unsigned int		OPENEMU_CALL	SteamWriteFile(const void *pBuf, unsigned int uSize, unsigned int uCount, SteamHandle_t hFile, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamSeekFile(SteamHandle_t hFile, long lOffset, ESteamSeekMethod esMethod, TSteamError *pError);
OPENEMU_API long				OPENEMU_CALL	SteamSizeFile(SteamHandle_t hFile, TSteamError *pError);
OPENEMU_API long				OPENEMU_CALL	SteamTellFile(SteamHandle_t hFile, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamStat(const char *cszFileName, TSteamElemInfo *pInfo, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamFindClose(SteamHandle_t hFind, TSteamError *pError);
OPENEMU_API SteamHandle_t		OPENEMU_CALL	SteamFindFirst(const char *cszPattern, ESteamFindFilter eFilter, TSteamElemInfo *pFindInfo, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamFindNext(SteamHandle_t hFind, TSteamElemInfo *pFindInfo, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetLocalFileCopy(const char *cszFileName, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamIsFileImmediatelyAvailable(const char *cszName, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamHintResourceNeed(const char *cszHintList, int bForgetEverything, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamForgetAllHints(const char *cszMountPath, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamPauseCachePreloading(const char *cszMountPath, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamResumeCachePreloading(const char *cszMountPath, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamWaitForResources(const char *cszMasterList, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamFlushCache(unsigned int uAppId, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetCacheDecryptionKey();
OPENEMU_API int					OPENEMU_CALL	SteamGetCacheDefaultDirectory(char *szPath, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamSetCacheDefaultDirectory(const char *szPath, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetCacheFilePath();
OPENEMU_API int					OPENEMU_CALL	SteamIsFileNeededByCache();
OPENEMU_API int					OPENEMU_CALL	SteamRepairOrDecryptCaches();
OPENEMU_API int					OPENEMU_CALL	SteamCreateCachePreloaders(TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamIsCacheLoadingEnabled(unsigned int uAppId, int *pbIsLoading, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamLoadCacheFromDir(unsigned int uAppId, const char *szPath, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamLoadFileToCache();
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamStartLoadingCache(unsigned int uAppId, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamStopLoadingCache(unsigned int uAppId, TSteamError *pError);

/*
** Logging
*/

OPENEMU_API SteamHandle_t		OPENEMU_CALL	SteamCreateLogContext( const char *cszName );
OPENEMU_API int					OPENEMU_CALL	SteamLog( SteamHandle_t hContext, const char *cszMsg );
OPENEMU_API void				OPENEMU_CALL	SteamLogResourceLoadStarted( const char *cszMsg );
OPENEMU_API void				OPENEMU_CALL	SteamLogResourceLoadFinished( const char *cszMsg );

/*
** Account
*/

OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamCreateAccount(const char *cszUser, const char *cszPassphrase, const char *cszCreationKey, const char *cszPersonalQuestion, const char *cszAnswerToQuestion, int *pbCreated, unsigned int uUnknown, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamDeleteAccount(TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamChangeAccountName();
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamChangeEmailAddress(const char *cszNewEmailAddress, int *pbChanged, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamChangeForgottenPassword();
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamChangePassword(const char *cszCurrentPassphrase, const char *cszNewPassphrase, int *pbChanged, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamChangePersonalQA(const char *cszCurrentPassphrase, const char *cszNewPersonalQuestion, const char *cszNewAnswerToQuestion, int *pbChanged, TSteamError *pError);







OPENEMU_API int					OPENEMU_CALL	SteamCheckAppOwnership();
OPENEMU_API int					OPENEMU_CALL	SteamDefragCaches();
OPENEMU_API int					OPENEMU_CALL	SteamFindFirst64();
OPENEMU_API int					OPENEMU_CALL	SteamFindNext64();
OPENEMU_API int					OPENEMU_CALL	SteamGetCachePercentFragmentation();
OPENEMU_API int					OPENEMU_CALL	SteamIsFileNeededByApp();
OPENEMU_API int					OPENEMU_CALL	SteamLoadFileToApp();
OPENEMU_API int					OPENEMU_CALL	SteamMiniDumpInit();
OPENEMU_API int					OPENEMU_CALL	SteamOpenFile64();
OPENEMU_API int					OPENEMU_CALL	SteamRefreshAccountInfo2();
OPENEMU_API int					OPENEMU_CALL	SteamSeekFile64();
OPENEMU_API int					OPENEMU_CALL	SteamSizeFile64();
OPENEMU_API int					OPENEMU_CALL	SteamStat64();
OPENEMU_API int					OPENEMU_CALL	SteamTellFile64();
OPENEMU_API int					OPENEMU_CALL	SteamWaitForAppResources();
OPENEMU_API int					OPENEMU_CALL	SteamWasBlobRegistryDeleted();
OPENEMU_API int					OPENEMU_CALL	SteamWriteMiniDumpWithAppID();




OPENEMU_API int					OPENEMU_CALL	SteamEnumerateApp(unsigned int AppId, TSteamApp *pApp, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamEnumerateAppDependency(unsigned int AppId, unsigned int uDependency, TSteamAppDependencyInfo *pDependencyInfo, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamEnumerateAppIcon(unsigned int uAppId, unsigned int uIconIndex, unsigned char *pIconData, unsigned int uIconDataBufSize, unsigned int *puSizeOfIconData, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamEnumerateAppLaunchOption(unsigned int uAppId, unsigned int uLaunchOptionIndex, TSteamAppLaunchOption *pLaunchOption, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamEnumerateAppVersion(unsigned int uAppId, unsigned int uVersionIndex, TSteamAppVersion *pAppVersion, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamEnumerateSubscription(unsigned int uSubId, TSteamSubscription *pSubscription, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamEnumerateSubscriptionDiscount();
OPENEMU_API int					OPENEMU_CALL	SteamEnumerateSubscriptionDiscountQualifier();
OPENEMU_API int					OPENEMU_CALL	SteamGenerateSuggestedAccountNames();
OPENEMU_API int					OPENEMU_CALL	SteamGetAccountStatus();
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamGetAppCacheSize(unsigned int uAppId, unsigned int *pCacheSizeInMb, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetAppDependencies();
OPENEMU_API int					OPENEMU_CALL	SteamGetAppDir();
OPENEMU_API int					OPENEMU_CALL	SteamGetAppIds(unsigned int *puAppIds, unsigned int uMaxIds, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetAppPurchaseCountry(int appID, char* szCountryCode, unsigned int a3, unsigned int* pPurchaseTime, TSteamError* pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetAppStats(TSteamAppStats *pAppStats, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetAppUpdateStats(unsigned int uAppId, unsigned int uStatType, TSteamUpdateStats *pUpdateStats, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetAppUserDefinedInfo(unsigned int uAppId, const char *cszPropertyName, char *szPropertyValue, unsigned int uBufSize, unsigned int *puPropertyValueLength, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetAppUserDefinedRecord(unsigned int uAppId, unsigned int arg2, unsigned int arg3, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetCurrentEmailAddress(char *szEmailaddress, unsigned int uBufSize, unsigned int *puEmailaddressChars, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetNumAccountsWithEmailAddress();
OPENEMU_API int					OPENEMU_CALL	SteamGetSponsorUrl(unsigned int uAppId, char *szUrl, unsigned int uBufSize, unsigned int *pUrlChars, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetSubscriptionExtendedInfo();
OPENEMU_API int					OPENEMU_CALL	SteamGetSubscriptionIds(unsigned int *puSubIds, unsigned int uMaxIds, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetSubscriptionPurchaseCountry();
OPENEMU_API int					OPENEMU_CALL	SteamGetSubscriptionReceipt();
OPENEMU_API int					OPENEMU_CALL	SteamGetSubscriptionStats(TSteamSubscriptionStats *pSubscriptionStats, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetTotalUpdateStats(TSteamUpdateStats *pUpdateStats, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetUser(char *szUser, unsigned int uBufSize, unsigned int *puUserChars, int bIsSecureComputer, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamGetUserType();
OPENEMU_API int					OPENEMU_CALL	SteamIsAccountNameInUse();
OPENEMU_API int					OPENEMU_CALL	SteamIsAppSubscribed(unsigned int uAppId, int *pbIsAppSubscribed, int *pReserved, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamIsLoggedIn(int *pbIsLoggedIn, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamIsSecureComputer(int *pbIsSecure, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamIsSubscribed(unsigned int uSubscriptionId, int *pbIsSubscribed, int *pReserved, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamLaunchApp(unsigned int uAppId, unsigned int uLaunchOption, const char *cszArgs, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamLogin(const char *cszUser, const char *cszPassphrase, int bIsSecureComputer, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamLogout(TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamMoveApp(unsigned int uAppId, const char *szPath, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamRefreshAccountInfo();
OPENEMU_API int					OPENEMU_CALL	SteamRefreshAccountInfoEx();
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamRefreshLogin(const char *cszPassphrase, int bIsSecureComputer, TSteamError * pError);
OPENEMU_API int					OPENEMU_CALL	SteamRequestAccountsByCdKeyEmail();
OPENEMU_API int					OPENEMU_CALL	SteamRequestAccountsByEmailAddressEmail();
OPENEMU_API int					OPENEMU_CALL	SteamRequestEmailAddressVerificationEmail();
OPENEMU_API int					OPENEMU_CALL	SteamRequestForgottenPasswordEmail();
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamSetUser(const char *cszUser, int *pbUserSet, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamSubscribe(unsigned int uSubscriptionId, const TSteamSubscriptionBillingInfo *pSubscriptionBillingInfo, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamUnsubscribe(unsigned int uSubscriptionId, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamUpdateAccountBillingInfo(const TSteamPaymentCardInfo *pPaymentCardInfo, int *pbChanged, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamUpdateSubscriptionBillingInfo(unsigned int uSubscriptionId, const TSteamSubscriptionBillingInfo *pSubscriptionBillingInfo, int *pbChanged, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamVerifyEmailAddress();
OPENEMU_API int					OPENEMU_CALL	SteamVerifyPassword();
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamWaitForAppReadyToLaunch(unsigned int uAppId, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamAckSubscriptionReceipt();
OPENEMU_API int					OPENEMU_CALL	SteamRemoveAppDependency();
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamSetAppCacheSize(unsigned int uAppId, unsigned int nCacheSizeInMb, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamSetAppVersion(unsigned int uAppId, unsigned int uAppVersionId, TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamInsertAppDependency();
OPENEMU_API int					OPENEMU_CALL	SteamNumAppsRunning();
OPENEMU_API int					OPENEMU_CALL	SteamFindApp();

/*
** Minidump
*/

OPENEMU_API int					OPENEMU_CALL	SteamWriteMiniDumpFromAssert();
OPENEMU_API int					OPENEMU_CALL	SteamWriteMiniDumpSetComment(const char *cszComment);
OPENEMU_API int					OPENEMU_CALL	SteamWriteMiniDumpUsingExceptionInfo();
OPENEMU_API int					OPENEMU_CALL	SteamWriteMiniDumpUsingExceptionInfoWithBuildId();

/*
** User ID functions
*/

OPENEMU_API ESteamError			OPENEMU_CALL	SteamGetEncryptedUserIDTicket(const void *pEncryptionKeyReceivedFromAppServer, unsigned int uEncryptionKeyLength, void *pOutputBuffer, unsigned int uSizeOfOutputBuffer, unsigned int *pReceiveSizeOfEncryptedTicket, TSteamError *pError);
OPENEMU_API ESteamError			OPENEMU_CALL	SteamInitializeUserIDTicketValidator(const char * pszOptionalPublicEncryptionKeyFilename, const char *	pszOptionalPrivateDecryptionKeyFilename, unsigned int ClientClockSkewToleranceInSeconds, unsigned int ServerClockSkewToleranceInSeconds, unsigned int MaxNumLoginsWithinClientClockSkewTolerancePerClient, unsigned int	HintPeakSimultaneousValidations, unsigned int AbortValidationAfterStallingForNProcessSteps);
OPENEMU_API ESteamError			OPENEMU_CALL	SteamShutdownUserIDTicketValidator();
OPENEMU_API const unsigned char*OPENEMU_CALL	SteamGetEncryptionKeyToSendToNewClient(unsigned int * pReceiveSizeOfEncryptionKey);
OPENEMU_API ESteamError			OPENEMU_CALL	SteamStartValidatingUserIDTicket(void *pEncryptedUserIDTicketFromClient, unsigned int uSizeOfEncryptedUserIDTicketFromClient, unsigned int ObservedClientIPAddr, SteamUserIDTicketValidationHandle_t *pReceiveHandle);
OPENEMU_API ESteamError			OPENEMU_CALL	SteamStartValidatingNewValveCDKey(void *pEncryptedNewValveCDKeyFromClient, unsigned int uSizeOfEncryptedNewValveCDKeyFromClient, unsigned int ObservedClientIPAddr, struct sockaddr *pPrimaryValidateNewCDKeyServerSockAddr, struct sockaddr *pSecondaryValidateNewCDKeyServerSockAddr, SteamUserIDTicketValidationHandle_t *pReceiveHandle);
OPENEMU_API ESteamError			OPENEMU_CALL	SteamProcessOngoingUserIDTicketValidation(SteamUserIDTicketValidationHandle_t Handle, TSteamGlobalUserID *pReceiveValidSteamGlobalUserID, unsigned int *pReceiveClientLocalIPAddr, unsigned char *pOptionalReceiveProofOfAuthenticationToken, size_t SizeOfOptionalAreaToReceiveProofOfAuthenticationToken, size_t *pOptionalReceiveSizeOfProofOfAuthenticationToken);
OPENEMU_API void				OPENEMU_CALL	SteamAbortOngoingUserIDTicketValidation(SteamUserIDTicketValidationHandle_t Handle);
OPENEMU_API ESteamError			OPENEMU_CALL	SteamOptionalCleanUpAfterClientHasDisconnected(unsigned int ObservedClientIPAddr, unsigned int ClientLocalIPAddr);

/*
** Misc
*/

OPENEMU_API void				OPENEMU_CALL	SteamClearError(TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	InternalSteamNumClientsConnectedToEngine();
OPENEMU_API int					OPENEMU_CALL	InternalSteamShouldShutdownEngine2();
OPENEMU_API int					OPENEMU_CALL	SteamGetLocalClientVersion();
OPENEMU_API int					OPENEMU_CALL	SteamChangeOfflineStatus();
OPENEMU_API int					OPENEMU_CALL	SteamGetOfflineStatus(unsigned int* buIsOffline, TSteamError *pError);
OPENEMU_API SteamCallHandle_t	OPENEMU_CALL	SteamUninstall(TSteamError *pError);
OPENEMU_API int					OPENEMU_CALL	SteamWeakVerifyNewValveCDKey();
OPENEMU_API int					OPENEMU_CALL	SteamGetEncryptedNewValveCDKey();
OPENEMU_API int					OPENEMU_CALL	SteamDecryptDataForThisMachine();
OPENEMU_API int					OPENEMU_CALL	SteamEncryptDataForThisMachine();
OPENEMU_API int					OPENEMU_CALL	SteamFindServersGetErrorString();
OPENEMU_API int					OPENEMU_CALL	SteamFindServersIterateServer(int arg1, int arg2, char *szServerAddress, unsigned int uServerAddressChars);
OPENEMU_API int					OPENEMU_CALL	SteamFindServersNumServers(unsigned int arg1);
OPENEMU_API int					OPENEMU_CALL	SteamGetContentServerInfo();
OPENEMU_API int					OPENEMU_CALL	SteamRefreshMinimumFootprintFiles();
OPENEMU_API int					OPENEMU_CALL	SteamSetNotificationCallback(SteamNotificationCallback_t pCallbackFunction, TSteamError *pError);

#ifdef __cplusplus
}
#endif

#endif