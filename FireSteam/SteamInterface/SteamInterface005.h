#pragma once

#include ".\SteamInterface004.h"

class CSteamInterface005 : public CSteamInterface004
{
public:
	CSteamInterface005();
	~CSteamInterface005();

	virtual int GetLocalClientVersion();
	virtual int IsFileNeededByCache();
	virtual int LoadFileToCache();
	virtual int GetCacheDecryptionKey();
	virtual int GetSubscriptionExtendedInfo();
	virtual int GetSubscriptionPurchaseCountry();
	virtual int GetAppUserDefinedRecord(unsigned int uAppId, unsigned int arg2, unsigned int arg3, TSteamError *pError);
};
