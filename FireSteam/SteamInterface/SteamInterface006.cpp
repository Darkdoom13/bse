#include "..\OpenEmu.h"
#include ".\SteamInterface006.h"

CSteamInterface006::CSteamInterface006(){}
CSteamInterface006::~CSteamInterface006(){}

int CSteamInterface006::FindServersNumServers(unsigned int arg1)
{
	return SteamFindServersNumServers(arg1);
}
int CSteamInterface006::FindServersIterateServer(int arg1, int arg2, char *szServerAddress, unsigned int uServerAddressChars)
{
	return SteamFindServersIterateServer(arg1, arg2, szServerAddress, uServerAddressChars);
}
int CSteamInterface006::FindServersGetErrorString()
{
	return SteamFindServersGetErrorString();
}
