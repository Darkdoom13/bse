#include "..\OpenEmu.h"
#include ".\SteamInterface003.h"

CSteamInterface003::CSteamInterface003(){}
CSteamInterface003::~CSteamInterface003(){}

SteamCallHandle_t CSteamInterface003::ChangePassword(const char *cszCurrentPassphrase, const char *cszNewPassphrase, int *pbChanged, TSteamError *pError)
{
	return SteamChangePassword(cszCurrentPassphrase, cszNewPassphrase, pbChanged, pError);
}
int CSteamInterface003::GetCurrentEmailAddress(char *szEmailaddress, unsigned int uBufSize, unsigned int *puEmailaddressChars, TSteamError *pError)
{
	return SteamGetCurrentEmailAddress(szEmailaddress, uBufSize, puEmailaddressChars, pError);
}
SteamCallHandle_t CSteamInterface003::ChangePersonalQA(const char *cszCurrentPassphrase, const char *cszNewPersonalQuestion, const char *cszNewAnswerToQuestion, int *pbChanged, TSteamError *pError)
{
	return SteamChangePersonalQA(cszCurrentPassphrase, cszNewPersonalQuestion, cszNewAnswerToQuestion, pbChanged, pError);
}
SteamCallHandle_t CSteamInterface003::ChangeEmailAddress(const char *cszNewEmailAddress, int *pbChanged, TSteamError *pError)
{
	return SteamChangeEmailAddress(cszNewEmailAddress, pbChanged, pError);
}
int CSteamInterface003::VerifyEmailAddress()
{
	return SteamVerifyEmailAddress();
}
int CSteamInterface003::RequestEmailAddressVerificationEmail()
{
	return SteamRequestEmailAddressVerificationEmail();
}
int CSteamInterface003::ChangeAccountName()
{
	return SteamChangeAccountName();
}
int CSteamInterface003::MountAppFilesystem(TSteamError *pError)
{
	return SteamMountAppFilesystem(pError);
}
int CSteamInterface003::UnmountAppFilesystem(TSteamError* pError)
{
	return SteamUnmountAppFilesystem(pError);
}
int CSteamInterface003::MountFilesystem(unsigned int uAppId, const char *szMountPath, TSteamError *pError)
{
	return SteamMountFilesystem(uAppId, szMountPath, pError);
}
int CSteamInterface003::UnmountFilesystem(const char *szMountPath, TSteamError *pError)
{
	return SteamUnmountFilesystem(szMountPath, pError);
}
int CSteamInterface003::Stat(const char *cszName, TSteamElemInfo *pInfo, TSteamError *pError)
{
	return SteamStat(cszName, pInfo, pError);
}
int CSteamInterface003::SetvBuf(SteamHandle_t hFile, void* pBuf, ESteamBufferMethod eMethod, unsigned int uBytes, TSteamError *pError)
{
	return SteamSetvBuf(hFile, pBuf, eMethod, uBytes, pError);
}
int CSteamInterface003::FlushFile(SteamHandle_t hFile, TSteamError *pError)
{
	return SteamFlushFile(hFile, pError);
}
SteamHandle_t CSteamInterface003::OpenFile(const char *cszName, const char *cszMode, TSteamError *pError)
{
	return SteamOpenFile(cszName, cszMode, pError);
}
SteamHandle_t CSteamInterface003::OpenFileEx(const char *cszFileName, const char *cszMode, unsigned int *size, TSteamError *pError)
{
	return SteamOpenFileEx(cszFileName, cszMode, size, pError);
}
SteamHandle_t CSteamInterface003::OpenTmpFile(TSteamError* pError)
{
	return SteamOpenTmpFile(pError);
}
void CSteamInterface003::ClearError(TSteamError* pError)
{
	SteamClearError(pError);
}
int CSteamInterface003::GetVersion(char* szVersion, unsigned int uVersionBufSize)
{
	return SteamGetVersion(szVersion, uVersionBufSize);
}
int CSteamInterface003::GetOfflineStatus(unsigned int* buIsOffline, TSteamError *pError)
{
	return SteamGetOfflineStatus(buIsOffline, pError);
}
int CSteamInterface003::ChangeOfflineStatus()
{
	return SteamChangeOfflineStatus();
}
int CSteamInterface003::ProcessCall(SteamCallHandle_t handle, TSteamProgress *pProgress, TSteamError *pError)
{
	return SteamProcessCall(handle, pProgress, pError);
}
int CSteamInterface003::AbortCall(SteamCallHandle_t handle, TSteamError *pError)
{
	return SteamAbortCall(handle, pError);
}
int CSteamInterface003::BlockingCall(SteamCallHandle_t handle, unsigned int uiProcessTickMS, TSteamError *pError)
{
	return SteamBlockingCall(handle, uiProcessTickMS, pError);
}
int CSteamInterface003::SetMaxStallCount(unsigned int uNumStalls, TSteamError *pError)
{
	return SteamSetMaxStallCount(uNumStalls, pError);
}
int CSteamInterface003::CloseFile(SteamHandle_t hFile, TSteamError *pError)
{
	return SteamCloseFile(hFile, pError);
}
unsigned int CSteamInterface003::ReadFile(void *pBuf, unsigned int uSize, unsigned int uCount, SteamHandle_t hFile, TSteamError *pError)
{
	return SteamReadFile(pBuf, uSize, uCount, hFile, pError);
}
unsigned int CSteamInterface003::WriteFile(const void *pBuf, unsigned int uSize, unsigned int uCount, SteamHandle_t hFile, TSteamError *pError)
{
	return SteamWriteFile(pBuf, uSize, uCount, hFile, pError);
}
int CSteamInterface003::Getc(SteamHandle_t hFile, TSteamError *pError)
{
	return SteamGetc(hFile, pError);
}
int CSteamInterface003::Putc(int cChar, SteamHandle_t hFile, TSteamError *pError)
{
	return SteamPutc(cChar, hFile, pError);
}
int CSteamInterface003::SeekFile(SteamHandle_t hFile, long lOffset, ESteamSeekMethod sm, TSteamError *pError)
{
	return SteamSeekFile(hFile, lOffset, sm, pError);
}
long CSteamInterface003::TellFile(SteamHandle_t hFile, TSteamError *pError)
{
	return SteamTellFile(hFile, pError);
}
int CSteamInterface003::SizeFile(SteamHandle_t hFile, TSteamError *pError)
{
	return SteamSizeFile(hFile, pError);
}
SteamHandle_t CSteamInterface003::FindFirst(const char *cszPattern, ESteamFindFilter eFilter, TSteamElemInfo *pFindInfo, TSteamError *pError)
{
	return SteamFindFirst(cszPattern, eFilter, pFindInfo, pError);
}
int CSteamInterface003::FindNext(SteamHandle_t hFind, TSteamElemInfo *pFindInfo, TSteamError *pError)
{
	return SteamFindNext(hFind, pFindInfo, pError);
}
int CSteamInterface003::FindClose(SteamHandle_t hFind, TSteamError *pError)
{
	return SteamFindClose(hFind, pError);
}
int CSteamInterface003::GetLocalFileCopy(const char *cszName, TSteamError *pError)
{
	return SteamGetLocalFileCopy(cszName, pError);
}
int CSteamInterface003::IsFileImmediatelyAvailable(const char *cszName, TSteamError *pError)
{
	return SteamIsFileImmediatelyAvailable(cszName, pError);
}
int CSteamInterface003::HintResourceNeed(const char *cszHintList, int bForgetEverything, TSteamError *pError)
{
	return SteamHintResourceNeed(cszHintList, bForgetEverything, pError);
}
int CSteamInterface003::ForgetAllHints(const char *cszMountPath, TSteamError *pError)
{
	return SteamForgetAllHints(cszMountPath, pError);
}
int CSteamInterface003::PauseCachePreloading(const char *cszMountPath, TSteamError *pError)
{
	return SteamPauseCachePreloading(cszMountPath, pError);
}
int CSteamInterface003::ResumeCachePreloading(const char *cszMountPath, TSteamError *pError)
{
	return SteamResumeCachePreloading(cszMountPath, pError);
}
SteamCallHandle_t CSteamInterface003::WaitForResources(const char *cszMasterList, TSteamError *pError)
{
	return SteamWaitForResources(cszMasterList, pError);
}
int CSteamInterface003::StartEngine(TSteamError *pError)
{
	return SteamStartEngine(pError);
}
int CSteamInterface003::ShutdownEngine(TSteamError *pError)
{
	return SteamShutdownEngine(pError);
}
int CSteamInterface003::Startup(unsigned int uUsingMask, TSteamError *pError)
{
	return SteamStartup(uUsingMask, pError);
}
int CSteamInterface003::Cleanup(TSteamError *pError)
{
	return SteamCleanup(pError);
}
int CSteamInterface003::NumAppsRunning()
{
	return SteamNumAppsRunning();
}
SteamCallHandle_t CSteamInterface003::CreateAccount(const char *cszUser, const char *cszPassphrase, const char *cszCreationKey, const char *cszPersonalQuestion, const char *cszAnswerToQuestion, int *pbCreated, unsigned int uUnknown, TSteamError *pError)
{
	return SteamCreateAccount(cszUser, cszPassphrase, cszCreationKey, cszPersonalQuestion, cszAnswerToQuestion, pbCreated, uUnknown, pError);
}
int CSteamInterface003::GenerateSuggestedAccountNames()
{
	return SteamGenerateSuggestedAccountNames();
}
int CSteamInterface003::IsLoggedIn(int *pbIsLoggedIn, TSteamError *pError)
{
	return SteamIsLoggedIn(pbIsLoggedIn, pError);
}
SteamCallHandle_t CSteamInterface003::Logout(TSteamError *pError)
{
	return SteamLogout(pError);
}
int CSteamInterface003::IsSecureComputer(int *pbIsSecure, TSteamError *pError)
{
	return SteamIsSecureComputer(pbIsSecure, pError);
}
SteamHandle_t CSteamInterface003::CreateLogContext(const char *cszName)
{
	return SteamCreateLogContext(cszName);
}
int CSteamInterface003::Log(SteamHandle_t hContext, const char *cszMsg)
{
	return SteamLog(hContext, cszMsg);
}
void CSteamInterface003::LogResourceLoadStarted(const char *cszMsg)
{
	SteamLogResourceLoadStarted(cszMsg);
}
void CSteamInterface003::LogResourceLoadFinished(const char *cszMsg)
{
	SteamLogResourceLoadFinished(cszMsg);
}
SteamCallHandle_t CSteamInterface003::RefreshLogin(const char *cszPassphrase, int bIsSecureComputer, TSteamError *pError)
{
	return SteamRefreshLogin(cszPassphrase, bIsSecureComputer, pError);
}
int CSteamInterface003::VerifyPassword()
{
	return SteamVerifyPassword();
}
int CSteamInterface003::GetUserType()
{
	return SteamGetUserType();
}
int CSteamInterface003::GetAppStats(TSteamAppStats *pAppStats, TSteamError *pError)
{
	return SteamGetAppStats(pAppStats, pError);
}
int CSteamInterface003::IsAccountNameInUse()
{
	return SteamIsAccountNameInUse();
}
int CSteamInterface003::GetAppIds(unsigned int *puIds, unsigned int uMaxIds, TSteamError *pError)
{
	return SteamGetAppIds(puIds, uMaxIds, pError);
}
int CSteamInterface003::GetSubscriptionStats(TSteamSubscriptionStats *pSubscriptionStats, TSteamError *pError)
{
	return SteamGetSubscriptionStats(pSubscriptionStats, pError);
}
int CSteamInterface003::RefreshAccountInfo()
{
	return SteamRefreshAccountInfo();
}
SteamCallHandle_t CSteamInterface003::Subscribe(unsigned int uSubscriptionId, const TSteamSubscriptionBillingInfo *pSubscriptionBillingInfo, TSteamError *pError)
{
	return SteamSubscribe(uSubscriptionId, pSubscriptionBillingInfo, pError);
}
SteamCallHandle_t CSteamInterface003::Unsubscribe(unsigned int uSubscriptionId, TSteamError *pError)
{
	return SteamUnsubscribe(uSubscriptionId, pError);
}
int CSteamInterface003::GetSubscriptionReceipt()
{
	return SteamGetSubscriptionReceipt();
}
int CSteamInterface003::GetAccountStatus()
{
	return SteamGetAccountStatus();
}
SteamCallHandle_t CSteamInterface003::SetUser(const char *cszUser, int *pbUserSet, TSteamError *pError)
{
	return SteamSetUser(cszUser, pbUserSet, pError);
}
int CSteamInterface003::GetUser(char *szUser, unsigned int uBufSize, unsigned int *puUserChars, int bIsSecureComputer, TSteamError *pError)
{
	return SteamGetUser(szUser, uBufSize, puUserChars, bIsSecureComputer, pError);
}
SteamCallHandle_t CSteamInterface003::Login(const char *cszUser, const char *cszPassphrase, int bIsSecureComputer, TSteamError *pError)
{
	return SteamLogin(cszUser, cszPassphrase, bIsSecureComputer, pError);
}
int CSteamInterface003::AckSubscriptionReceipt()
{
	return SteamAckSubscriptionReceipt();
}
int CSteamInterface003::IsAppSubscribed(unsigned int uAppId, int *pbIsAppSubscribed, int *pReserved, TSteamError *pError)
{
	return SteamIsAppSubscribed(uAppId, pbIsAppSubscribed, pReserved, pError);
}
int CSteamInterface003::GetSubscriptionIds(unsigned int *puIds, unsigned int uMaxIds, TSteamError *pError)
{
	return SteamGetSubscriptionIds(puIds, uMaxIds, pError);
}
int CSteamInterface003::EnumerateSubscription(unsigned int uId, TSteamSubscription *pSubscription, TSteamError *pError)
{
	return SteamEnumerateSubscription(uId, pSubscription, pError);
}
int CSteamInterface003::EnumerateSubscriptionDiscount()
{
	return SteamEnumerateSubscriptionDiscount();
}
int CSteamInterface003::EnumerateSubscriptionDiscountQualifier()
{
	return SteamEnumerateSubscriptionDiscountQualifier();
}
int CSteamInterface003::EnumerateApp(unsigned int uId, TSteamApp *pApp, TSteamError *pError)
{
	return SteamEnumerateApp(uId, pApp, pError);
}
int CSteamInterface003::EnumerateAppLaunchOption(unsigned int uAppId, unsigned int uLaunchOptionIndex, TSteamAppLaunchOption *pLaunchOption, TSteamError *pError)
{
	return SteamEnumerateAppLaunchOption(uAppId, uLaunchOptionIndex, pLaunchOption, pError);
}
SteamCallHandle_t CSteamInterface003::DeleteAccount(TSteamError *pError)
{
	return SteamDeleteAccount(pError);
}
int CSteamInterface003::EnumerateAppIcon(unsigned int uAppId, unsigned int uIconIndex, unsigned char *pIconData, unsigned int uIconDataBufSize,  unsigned int *puSizeOfIconData, TSteamError *pError)
{
	return SteamEnumerateAppIcon(uAppId, uIconIndex, pIconData, uIconDataBufSize, puSizeOfIconData, pError);
}
SteamCallHandle_t CSteamInterface003::LaunchApp(unsigned int uAppId, unsigned int uLaunchOption, const char *cszArgs, TSteamError *pError)
{
	return SteamLaunchApp(uAppId, uLaunchOption, cszArgs, pError);
}
int CSteamInterface003::GetCacheFilePath()
{
	return SteamGetCacheFilePath();
}
int CSteamInterface003::EnumerateAppVersion(unsigned int uAppId, unsigned int uVersionIndex, TSteamAppVersion *pAppVersion, TSteamError *pError)
{
	return SteamEnumerateAppVersion(uAppId, uVersionIndex, pAppVersion, pError);
}
int CSteamInterface003::EnumerateAppDependency(unsigned int AppId, unsigned int uDependency, TSteamAppDependencyInfo *pDependencyInfo, TSteamError *pError)
{
	return SteamEnumerateAppDependency(AppId, uDependency, pDependencyInfo, pError);
}
SteamCallHandle_t CSteamInterface003::StartLoadingCache(unsigned int uAppId, TSteamError *pError)
{
	return SteamStartLoadingCache(uAppId, pError);
}
int CSteamInterface003::InsertAppDependency()
{
	return SteamInsertAppDependency();
}
int CSteamInterface003::RemoveAppDependency()
{
	return SteamRemoveAppDependency();
}
int CSteamInterface003::FindApp()
{
	return SteamFindApp();
}
int CSteamInterface003::GetAppDependencies()
{
	return SteamGetAppDependencies();
}
int CSteamInterface003::IsSubscribed(unsigned int uSubscriptionId, int *pbIsSubscribed, int *pReserved, TSteamError *pError)
{
	return SteamIsSubscribed(uSubscriptionId, pbIsSubscribed, pReserved, pError);
}
int CSteamInterface003::GetAppUserDefinedInfo(unsigned int uAppId, const char *cszPropertyName, char *szPropertyValue, unsigned int uBufSize, unsigned int *puPropertyValueLength, TSteamError *pError)
{
	return SteamGetAppUserDefinedInfo(uAppId, cszPropertyName, szPropertyValue, uBufSize, puPropertyValueLength, pError);
}
int CSteamInterface003::WaitForAppReadyToLaunch(unsigned int uAppId, TSteamError *pError)
{
	return SteamWaitForAppReadyToLaunch(uAppId, pError);
}
int CSteamInterface003::IsCacheLoadingEnabled(unsigned int uAppId, int *pbIsLoading, TSteamError *pError)
{
	return SteamIsCacheLoadingEnabled(uAppId, pbIsLoading, pError);
}
SteamCallHandle_t CSteamInterface003::StopLoadingCache(unsigned int uAppId, TSteamError *pError)
{
	return SteamStopLoadingCache(uAppId, pError);
}
int CSteamInterface003::GetEncryptedUserIDTicket(const void *pEncryptionKeyReceivedFromAppServer, unsigned int uEncryptionKeyLength, void *pOutputBuffer, unsigned int uSizeOfOutputBuffer, unsigned int *pReceiveSizeOfEncryptedTicket, TSteamError *pError)
{
	return SteamGetEncryptedUserIDTicket(pEncryptionKeyReceivedFromAppServer, uEncryptionKeyLength, pOutputBuffer, uSizeOfOutputBuffer, pReceiveSizeOfEncryptedTicket, pError);
}
int CSteamInterface003::FlushCache(unsigned int uAppId, TSteamError *pError)
{
	return SteamFlushCache(uAppId, pError);
}
int CSteamInterface003::RepairOrDecryptCaches()
{
	return SteamRepairOrDecryptCaches();
}
int CSteamInterface003::LoadCacheFromDir(unsigned int uAppId, const char *cszPath, TSteamError *pError)
{
	return SteamLoadCacheFromDir(uAppId, cszPath, pError);
}
int CSteamInterface003::GetCacheDefaultDirectory(char *szPath, TSteamError *pError)
{
	return SteamGetCacheDefaultDirectory(szPath, pError);
}
int CSteamInterface003::SetCacheDefaultDirectory(const char *cszPath, TSteamError *pError)
{
	return SteamSetCacheDefaultDirectory(cszPath, pError);
}
int CSteamInterface003::GetAppDir()
{
	return SteamGetAppDir();
}
SteamCallHandle_t CSteamInterface003::MoveApp(unsigned int uAppId, const char *szPath, TSteamError *pError)
{
	return SteamMoveApp(uAppId, szPath, pError);
}
SteamCallHandle_t CSteamInterface003::GetAppCacheSize(unsigned int uAppId, unsigned int *puCacheSizeInMb, TSteamError *pError)
{
	return SteamGetAppCacheSize(uAppId, puCacheSizeInMb, pError);
}
SteamCallHandle_t CSteamInterface003::SetAppCacheSize(unsigned int uAppId, unsigned int uCacheSizeInMb, TSteamError *pError)
{
	return SteamSetAppCacheSize(uAppId, uCacheSizeInMb, pError);
}
int CSteamInterface003::SetAppVersion(unsigned int uAppId, unsigned int uAppVersionId, TSteamError *pError)
{
	return SteamSetAppVersion(uAppId, uAppVersionId, pError);
}
SteamCallHandle_t CSteamInterface003::Uninstall(TSteamError *pError)
{
	return SteamUninstall(pError);
}
int CSteamInterface003::SetNotificationCallback(SteamNotificationCallback_t pCallbackFunction, TSteamError *pError)
{
	return SteamSetNotificationCallback(pCallbackFunction, pError);
}
int CSteamInterface003::ChangeForgottenPassword()
{
	return SteamChangeForgottenPassword();
}
int CSteamInterface003::RequestForgottenPasswordEmail()
{
	return SteamRequestForgottenPasswordEmail();
}
int CSteamInterface003::RequestAccountsByEmailAddressEmail()
{
	return SteamRequestAccountsByEmailAddressEmail();
}
int CSteamInterface003::RequestAccountsByCdKeyEmail()
{
	return SteamRequestAccountsByCdKeyEmail();
}
int CSteamInterface003::GetNumAccountsWithEmailAddress()
{
	return SteamGetNumAccountsWithEmailAddress();
}
int CSteamInterface003::UpdateAccountBillingInfo(const TSteamPaymentCardInfo *pPaymentCardInfo, int *pbChanged, TSteamError *pError)
{
	return SteamUpdateAccountBillingInfo(pPaymentCardInfo, pbChanged, pError);
}
int CSteamInterface003::UpdateSubscriptionBillingInfo(unsigned int uSubscriptionId, const TSteamSubscriptionBillingInfo *pSubscriptionBillingInfo, int *pbChanged, TSteamError *pError)
{
	return SteamUpdateSubscriptionBillingInfo(uSubscriptionId, pSubscriptionBillingInfo, pbChanged, pError);
}
int CSteamInterface003::GetSponsorUrl(unsigned int uAppId, char *szUrl, unsigned int uBufSize, unsigned int *pUrlChars, TSteamError *pError)
{
	return SteamGetSponsorUrl(uAppId, szUrl, uBufSize, pUrlChars, pError);
}
int CSteamInterface003::GetContentServerInfo()
{
	return SteamGetContentServerInfo();
}
int CSteamInterface003::GetAppUpdateStats(unsigned int uAppId, unsigned int uStatType, TSteamUpdateStats *pUpdateStats, TSteamError *pError)
{
	return SteamGetAppUpdateStats(uAppId, uStatType, pUpdateStats, pError);
}
int CSteamInterface003::GetTotalUpdateStats(TSteamUpdateStats *pUpdateStats, TSteamError *pError)
{
	return SteamGetTotalUpdateStats(pUpdateStats, pError);
}
SteamCallHandle_t CSteamInterface003::CreateCachePreloaders(TSteamError *pError)
{
	return SteamCreateCachePreloaders(pError);
}
