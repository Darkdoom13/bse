#include "..\OpenEmu.h"
#include ".\SteamInterface005.h"

CSteamInterface005::CSteamInterface005(){}
CSteamInterface005::~CSteamInterface005(){}

int CSteamInterface005::GetLocalClientVersion()
{
	return SteamGetLocalClientVersion();
}
int CSteamInterface005::IsFileNeededByCache()
{
	return SteamIsFileNeededByCache();
}
int CSteamInterface005::LoadFileToCache()
{
	return SteamLoadFileToCache();
}
int CSteamInterface005::GetCacheDecryptionKey()
{
	return SteamGetCacheDecryptionKey();
}
int CSteamInterface005::GetSubscriptionExtendedInfo()
{
	return SteamGetSubscriptionExtendedInfo();
}
int CSteamInterface005::GetSubscriptionPurchaseCountry()
{
	return SteamGetSubscriptionPurchaseCountry();
}
int CSteamInterface005::GetAppUserDefinedRecord(unsigned int uAppId, unsigned int arg2, unsigned int arg3, TSteamError *pError)
{
	return SteamGetAppUserDefinedRecord(uAppId, arg2, arg3, pError);
}
