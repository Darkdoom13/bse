#pragma once

#include ".\SteamInterface005.h"

class CSteamInterface006 : public CSteamInterface005
{
public:
	CSteamInterface006();
	~CSteamInterface006();

	virtual int FindServersNumServers(unsigned int arg1);
	virtual int FindServersIterateServer(int arg1, int arg2, char *szServerAddress, unsigned int uServerAddressChars);
	virtual int FindServersGetErrorString();
};
