#pragma once

#include ".\SteamInterface003.h"

class CSteamInterface004 : public CSteamInterface003
{
public:
	CSteamInterface004();
	~CSteamInterface004();

	virtual int WriteMiniDumpSetComment(const char *cszComment);
	virtual int Dummy1();
	virtual int Dummy2();
	virtual int Dummy3();
	virtual int Dummy4();
	virtual int GetAppPurchaseCountry(int appID, char* szCountryCode, unsigned int a3, unsigned int* pPurchaseTime, TSteamError* pError);
};
