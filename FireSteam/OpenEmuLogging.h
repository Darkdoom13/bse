#pragma once

/*
** Logging
*/

OPENEMU_API SteamHandle_t OPENEMU_CALL SteamCreateLogContext(const char *cszName)
{
#ifdef DEBUG
	Log->Write("SteamCreateLogContext\n");
#endif
	return 1;
}

OPENEMU_API int OPENEMU_CALL SteamLog(SteamHandle_t hContext, const char *cszMsg)
{
#ifdef DEBUG
	Log->Write("SteamLog\n");
#endif
	return 1;
}

OPENEMU_API void OPENEMU_CALL SteamLogResourceLoadStarted(const char *cszMsg)
{
#ifdef DEBUG
	Log->Write("SteamLogResourceLoadStarted: %s\n", cszMsg);
#endif
}

OPENEMU_API void OPENEMU_CALL SteamLogResourceLoadFinished(const char *cszMsg)
{
#ifdef DEBUG
	Log->Write("SteamLogResourceLoadFinished\n");
#endif
}