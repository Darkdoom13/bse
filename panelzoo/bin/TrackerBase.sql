DROP TABLE IF EXISTS `userdistribution`;
CREATE TABLE `userdistribution` (
  `serverName` char(60) collate utf8_bin NOT NULL default '',
  `catalogName` char(60) collate utf8_bin NOT NULL default '',
  `backupServerName` char(60) collate utf8_bin NOT NULL default '',
  `backupCatalogName` char(64) collate utf8_bin NOT NULL default '',
  `useridminimum` int(10) unsigned NOT NULL,
  `useridmaximum` int(10) unsigned NOT NULL default '0',
  `nextValidUserId` int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (`serverName`,`catalogName`,`backupServerName`,`backupCatalogName`,`nextValidUserId`,`useridminimum`,`useridmaximum`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Tables';

INSERT INTO `userdistribution` (`serverName`,`catalogName`,`backupServerName`,`backupCatalogName`,`useridminimum`,`useridmaximum`,`nextValidUserId`) VALUES 
 ('localhost','newtracker','bservername','bcatalogname','1','999','2');