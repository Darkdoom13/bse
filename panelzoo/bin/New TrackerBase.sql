-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.67-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema newtracker
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ newtracker;
USE newtracker;

--
-- Table structure for table `newtracker`.`message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `messageid` int(10) unsigned NOT NULL,
  `fromUserID` int(10) unsigned NOT NULL,
  `toUserID` int(10) unsigned NOT NULL,
  `dateSent` char(60) collate utf8_bin NOT NULL default '',
  `fromUserName` char(60) collate utf8_bin NOT NULL default '',
  `body` char(60) collate utf8_bin NOT NULL default '',
  `flags` int(10) unsigned NOT NULL,
  `minimumBuild` int(10) unsigned NOT NULL,
  `maximumBuild` int(10) unsigned NOT NULL,
  `deleteDate` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`dateSent`,`fromUserName`,`body`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Tables';

--
-- Dumping data for table `newtracker`.`message`
--

/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;


--
-- Table structure for table `newtracker`.`user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(10) unsigned NOT NULL,
  `firstName` char(60) collate utf8_bin NOT NULL default '',
  `lastName` char(60) collate utf8_bin NOT NULL default '',
  `nickName` char(60) collate utf8_bin NOT NULL default '',
  `password` char(64) collate utf8_bin NOT NULL default '',
  `firstLogon` char(60) collate utf8_bin NOT NULL default '',
  `lastLogon` char(64) collate utf8_bin NOT NULL default '',
  `emailAddress` char(64) collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`firstName`,`lastName`,`nickName`,`password`,`emailAddress`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Tables';

--
-- Table structure for table `newtracker`.`userauth`
--

DROP TABLE IF EXISTS `userauth`;
CREATE TABLE `userauth` (
  `userID` int(10) unsigned NOT NULL,
  `targetID` int(10) unsigned NOT NULL,
  `authLevel` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`userID`,`targetID`,`authLevel`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Tables';

--
-- Dumping data for table `newtracker`.`userauth`
--

/*!40000 ALTER TABLE `userauth` DISABLE KEYS */;
/*!40000 ALTER TABLE `userauth` ENABLE KEYS */;


--
-- Table structure for table `newtracker`.`userblocked`
--

DROP TABLE IF EXISTS `userblocked`;
CREATE TABLE `userblocked` (
  `userID` int(10) unsigned NOT NULL,
  `blockedID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`userID`,`blockedID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Tables';

--
-- Dumping data for table `newtracker`.`userblocked`
--

/*!40000 ALTER TABLE `userblocked` DISABLE KEYS */;
/*!40000 ALTER TABLE `userblocked` ENABLE KEYS */;


--
-- Table structure for table `newtracker`.`userstatus`
--

DROP TABLE IF EXISTS `userstatus`;
CREATE TABLE `userstatus` (
  `userID` int(10) unsigned NOT NULL default '0',
  `serverID` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `userIP` int(10) unsigned NOT NULL,
  `userPort` int(10) unsigned NOT NULL,
  `sessionID` int(10) unsigned NOT NULL,
  `lastModified` int(10) unsigned default NULL,
  `buildNumber` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`userID`,`serverID`,`status`,`userIP`,`userPort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Tables';

--
-- Table structure for table `newtracker`.`watcher`
--

DROP TABLE IF EXISTS `watcher`;
CREATE TABLE `watcher` (
  `watcherID` int(10) unsigned NOT NULL,
  `watcherSessionID` int(10) unsigned NOT NULL,
  `watcherServerID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`watcherID`,`watcherSessionID`,`watcherServerID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Tables';

--
-- Dumping data for table `newtracker`.`watcher`
--

/*!40000 ALTER TABLE `watcher` DISABLE KEYS */;
/*!40000 ALTER TABLE `watcher` ENABLE KEYS */;


--
-- Table structure for table `newtracker`.`watchmap`
--

DROP TABLE IF EXISTS `watchmap`;
CREATE TABLE `watchmap` (
  `watcherID` int(10) unsigned NOT NULL,
  `targetID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`watcherID`,`targetID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Tables';

--
-- Dumping data for table `newtracker`.`watchmap`
--

/*!40000 ALTER TABLE `watchmap` DISABLE KEYS */;
/*!40000 ALTER TABLE `watchmap` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
